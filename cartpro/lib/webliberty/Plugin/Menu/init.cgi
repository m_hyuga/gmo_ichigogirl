#Menuプラグイン Ver 1.00：設定ファイル
#Copyright(C) 2002-2007 Knight, All rights reserved.

package webliberty::Plugin::Menu::init;

use strict;

sub get_init {
	my $init;

	#――――― 基本設定 ―――――――――――――――――――

	#ファイルアップロード機能の使用(0 … 使用しない / 1 … 使用する)
	$init->{image_mode} = 0;

	#――――― システムの設定 ――――――――――――――――

	#メニュー情報ファイル
	$init->{data_menu} = './lib/webliberty/Plugin/Menu/menu.log';

	#メニュー画像のアップロード先
	$init->{data_dir} = './menu/';

	#メニュー画像のアップロード先URL
	$init->{data_path} = 'http://your.site.addr/cartpro/menu/';

	#メニュー表示スキン
	$init->{skin_menu} = './lib/webliberty/Plugin/Menu/menu.html';

	#作業内容一覧画面スキン
	$init->{skin_work} = './lib/webliberty/Plugin/Menu/admin_work.html';

	#ナビゲーションスキン
	$init->{skin_navi} = './lib/webliberty/Plugin/Menu/admin_navi.html';

	#コンテンツ管理画面スキン
	$init->{skin_edit} = './lib/webliberty/Plugin/Menu/admin_edit.html';

	#コンテンツ作成画面スキン
	$init->{skin_form} = './lib/webliberty/Plugin/Menu/admin_form.html';

	return $init;
}

1;
