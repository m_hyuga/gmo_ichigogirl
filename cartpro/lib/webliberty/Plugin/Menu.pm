#Menuプラグイン Ver 1.00 (2007/08/11)
#Copyright(C) 2002-2007 Knight, All rights reserved.

package webliberty::Plugin::Menu;

use strict;
use base qw(webliberty::Basis);
use webliberty::String;
use webliberty::File;
use webliberty::Lock;
use webliberty::Skin;
use webliberty::App::Catalog;

### コンストラクタ
sub new {
	my $class = shift;

	my $self = {
		init    => shift,
		config  => shift,
		query   => shift,
		plugin  => undef,
		result  => undef,
		message => undef
	};
	bless $self, $class;

	require $self->{init}->{plugin_dir} . 'Menu/init.cgi';

	$self->{plugin} = &webliberty::Plugin::Menu::init::get_init;

	return $self;
}

### メイン処理
sub run {
	my $self = shift;

	my $result;

	if ($self->{query}->{plugin}) {
		my $plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{dammy});
		%{$self->{result}} = $plugin_ins->run;

		require webliberty::App::Admin;
		my $app_ins = new webliberty::App::Admin($self->{init}, $self->{config}, $self->{query});
		if (!$app_ins->check_password) {
			$self->error('管理者ページにログインしてください。');
		}

		if ($self->{query}->{work} eq 'new') {
			if ($self->{query}->{exec_regist}) {
				$app_ins->check_access;
				$self->check;
				$self->regist;
				$self->output_edit;
			} else {
				$self->output_form;
			}
		} elsif ($self->{query}->{work} eq 'edit') {
			if ($self->{query}->{exec_regist}) {
				$app_ins->check_access;
				$self->check;
				$self->regist;
			} elsif ($self->{query}->{exec_del}) {
				$app_ins->check_access;
				$self->del;
			} elsif ($self->{query}->{exec_move}) {
				$app_ins->check_access;
				$self->move;
			}
			if ($self->{query}->{exec_form}) {
				$self->output_form;
			} else {
				$self->output_edit;
			}
		} else {
			$self->output_edit;
		}

		exit;
	} else {
		$result = $self->output_menu;
	}

	return $result;
}

### 入力内容チェック
sub check {
	my $self = shift;

	my $url_ins  = new webliberty::String($self->{query}->{url});
	my $name_ins = new webliberty::String($self->{query}->{name});

	if (!$url_ins->get_string) {
		$self->error('URLが入力されていません。');
	}
	if (!$name_ins->get_string) {
		$self->error('メニュー名が入力されていません。');
	}

	return;
}

### メニュー登録
sub regist {
	my $self = shift;

	my $edit_ins = new webliberty::String($self->{query}->{edit});
	my $url_ins  = new webliberty::String($self->{query}->{url});
	my $name_ins = new webliberty::String($self->{query}->{name});

	$edit_ins->create_line;
	$url_ins->create_line;
	$name_ins->create_line;

	my $new_image;
	if ($self->{plugin}->{image_mode}) {
		if ($self->{query}->{'delimage'}) {
			unlink($self->{plugin}->{data_dir} . $self->{query}->{'delimage'});
		} elsif ($self->{query}->{'image'}) {
			my $file_ins = new webliberty::File($self->{query}->{'image'}->{file_name});
			$new_image = $file_ins->get_name . '.' . $file_ins->get_ext;

			open(FH, ">$self->{plugin}->{data_dir}$new_image") or $self->error("Write Error : $self->{plugin}->{data_dir}$new_image");
			binmode(FH);
			print FH $self->{query}->{'image'}->{file_data};
			close(FH);
		}
	}

	my $lock_ins = new webliberty::Lock($self->{init}->{data_lock});
	if (!$lock_ins->file_lock) {
		$self->error('ファイルがロックされています。時間をおいてもう一度登録してください。');
	}

	if ($self->{query}->{edit}) {
		my $new_data;

		open(FH, $self->{plugin}->{data_menu}) or &error("Read Error : $self->{plugin}->{data_menu}");
		while (<FH>) {
			chomp;
			my($url, $name, $image) = split(/\t/);

			if ($edit_ins->get_string eq $url) {
				if (!$new_image and !$self->{query}->{'delimage'}) {
					$new_image = $image;
				}

				$new_data .= $url_ins->get_string . "\t" . $name_ins->get_string . "\t$new_image\n";
			} else {
				$new_data .= "$_\n";
			}
		}
		close(FH);

		open(FH, ">$self->{plugin}->{data_menu}") or $self->error("Write Error : $self->{plugin}->{data_menu}");
		print FH $new_data;
		close(FH);
	} else {
		open(FH, ">>$self->{plugin}->{data_menu}") or $self->error("Write Error : $self->{plugin}->{data_menu}");
		print FH $url_ins->get_string . "\t" . $name_ins->get_string . "\t$new_image\n";
		close(FH);
	}

	$lock_ins->file_unlock;

	return;
}

### メニュー削除
sub del {
	my $self = shift;

	if (!$self->{query}->{del}) {
		$self->error('削除したいメニューを選択してください。');
	}

	my $lock_ins = new webliberty::Lock($self->{init}->{data_lock});
	if (!$lock_ins->file_lock) {
		$self->error('ファイルがロックされています。時間をおいてもう一度登録してください。');
	}

	my $new_data;

	open(FH, $self->{plugin}->{data_menu}) or $self->error("Read Error : $self->{plugin}->{data_menu}");
	while (<FH>) {
		chomp;
		my($url, $name, $image) = split(/\t/);

		if ($self->{query}->{del} =~ /(^|\n)$url(\n|$)/) {
			unlink($self->{plugin}->{data_dir} . $image);
		} else {
			$new_data .= "$_\n";
		}
	}
	close(FH);

	$self->{message} = 'メニューを削除しました。';

	open(FH, ">$self->{plugin}->{data_menu}") or $self->error("Write Error : $self->{plugin}->{data_menu}");
	print FH $new_data;
	close(FH);

	$lock_ins->file_unlock;

	return;
}

### メニュー移動
sub move {
	my $self = shift;

	if (!$self->{query}->{from} or !$self->{query}->{to}) {
		$self->error('移動元と移動先を選択してください。');
	}

	my $lock_ins = new webliberty::Lock($self->{init}->{data_lock});
	if (!$lock_ins->file_lock) {
		$self->error('ファイルがロックされています。時間をおいてもう一度登録してください。');
	}

	my(%menus, $new_data);

	open(FH, $self->{plugin}->{data_menu}) or $self->error("Read Error : $self->{plugin}->{data_menu}");
	while (<FH>) {
		chomp;
		my($url, $name, $image) = split(/\t/);

		$menus{$url} = $_;
	}
	seek(FH, 0, 0);
	while (<FH>) {
		chomp;
		my($url, $name, $image) = split(/\t/);

		if ($self->{query}->{to} eq $url) {
			$new_data .= $menus{$self->{query}->{from}} . "\n";
		}
		if ($self->{query}->{from} ne $url) {
			$new_data .= $_ . "\n";
		}
	}
	close(FH);

	open(FH, ">$self->{plugin}->{data_menu}") or $self->error("Write Error : $self->{plugin}->{data_menu}");
	print FH $new_data;
	close(FH);

	$lock_ins->file_unlock;

	$self->{message} = 'メニューを移動しました。';

	return;
}

### メニュー登録フォーム
sub output_form {
	my $self = shift;

	my $skin_ins = new webliberty::Skin;
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_header}");
	$skin_ins->parse_skin("$self->{plugin}->{skin_work}");
	$skin_ins->parse_skin("$self->{plugin}->{skin_form}");
	$skin_ins->parse_skin("$self->{plugin}->{skin_navi}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_footer}");

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});

	$skin_ins->replace_skin(
		$catalog_ins->info,
		%{$self->{result}}
	);

	my(%form, $form_label);

	if ($self->{query}->{work} eq 'edit') {
		if (!$self->{query}->{edit}) {
			$self->error('編集したいメニューを選択してください。');
		}

		my($edit_id, $edit_field) = split(/:/, $self->{query}->{edit});

		my($i, $flag);

		open(FH, $self->{plugin}->{data_menu}) or $self->error("Read Error : $self->{plugin}->{data_menu}");
		while (<FH>) {
			chomp;
			my($url, $name, $image) = split(/\t/);

			if ($self->{query}->{edit} eq $url) {
				my $url_ins   = new webliberty::String($url);
				my $name_ins  = new webliberty::String($name);
				my $image_ins = new webliberty::String($image);

				$url_ins->create_line;
				$name_ins->create_line;
				$image_ins->create_line;

				$form{'edit'}  = $url_ins->create_plain;
				$form{'url'}   = $url_ins->create_plain;
				$form{'name'}  = $name_ins->create_plain;
				$form{'image'} = $image_ins->create_plain;

				if ($form{'image'}) {
					$form{'image'} = "<input type=\"checkbox\" name=\"delimage\" id=\"delimage_checkbox\" value=\"$form{'image'}\"> <label for=\"delimage_checkbox\">$form{'image'}を削除</label>";
				}

				$flag = 1;

				last;
			}
		}

		if (!$flag) {
			$self->error('指定されたメニューは存在しません。');
		}

		$form_label = 'メニュー編集';
	} else {
		$form{'edit'}  = '';
		$form{'url'}   = 'http://';
		$form{'name'}  = '';
		$form{'image'} = '';

		$form_label = 'メニュー追加';
	}

	my($image_start, $image_end);
	if (!$self->{plugin}->{image_mode}) {
		$image_start = '<!--';
		$image_end   = '-->';
	}

	print $self->header;
	print $skin_ins->get_data('header');
	print $skin_ins->get_data('work_head');
	print $self->_work_navi($skin_ins);
	print $skin_ins->get_data('work_foot');
	print $skin_ins->get_replace_data(
		'form',
		FORM_LABEL       => $form_label,
		FORM_WORK        => $self->{query}->{work},
		FORM_EDIT        => $form{'edit'},
		FORM_URL         => $form{'url'},
		FORM_NAME        => $form{'name'},
		FORM_IMAGE       => $form{'image'},
		FORM_IMAGE_START => $image_start,
		FORM_IMAGE_END   => $image_end
	);

	print $skin_ins->get_data('navi');
	print $skin_ins->get_data('footer');

	return;
}

### メニュー管理
sub output_edit {
	my $self = shift;

	if (!$self->{message}) {
		$self->{message} = 'メニューを選択し、<em>編集ボタン</em>か<em>削除ボタン</em>を押してください。';
	}

	my $skin_ins = new webliberty::Skin;
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_header}");
	$skin_ins->parse_skin("$self->{plugin}->{skin_work}");
	$skin_ins->parse_skin("$self->{plugin}->{skin_edit}");
	$skin_ins->parse_skin("$self->{plugin}->{skin_navi}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_footer}");

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});

	$skin_ins->replace_skin(
		$catalog_ins->info,
		%{$self->{result}},
		INFO_MESSAGE => $self->{message}
	);

	print $self->header;
	print $skin_ins->get_data('header');

	print $skin_ins->get_data('work_head');
	print $self->_work_navi($skin_ins);
	print $skin_ins->get_data('work_foot');

	my($image_start, $image_end);
	if (!$self->{plugin}->{image_mode}) {
		$image_start = '<!--';
		$image_end   = '-->';
	}

	print $skin_ins->get_replace_data(
		'menu_head',
		MENU_IMAGE_START => $image_start,
		MENU_IMAGE_END   => $image_end
	);

	open(FH, $self->{plugin}->{data_menu}) or $self->error("Read Error : $self->{plugin}->{data_menu}");
	while (<FH>) {
		chomp;
		my($url, $name, $image) = split(/\t/);

		print $skin_ins->get_replace_data(
			'menu',
			MENU_URL         => $url,
			MENU_NAME        => $name,
			MENU_IMAGE       => $image,
			MENU_IMAGE_START => $image_start,
			MENU_IMAGE_END   => $image_end
		);
	}
	close(FH);

	print $skin_ins->get_replace_data(
		'menu_foot',
		MENU_IMAGE_START => $image_start,
		MENU_IMAGE_END   => $image_end
	);

	print $skin_ins->get_data('navi');
	print $skin_ins->get_data('footer');

	return;
}

### メニュー表示
sub output_menu {
	my $self = shift;

	my $skin_ins = new webliberty::Skin;
	$skin_ins->parse_skin("$self->{plugin}->{skin_menu}");

	my $data;

	$data .= $skin_ins->get_data('menu_head');

	open(FH, $self->{plugin}->{data_menu}) or $self->error("Read Error : $self->{plugin}->{data_menu}");
	while (<FH>) {
		chomp;
		my($url, $name, $image) = split(/\t/);

		my $info;
		if ($image) {
			my $file_ins = new webliberty::File($self->{plugin}->{data_dir} . $image);
			my($width, $height) = $file_ins->get_size;

			$info = "<img src=\"$self->{plugin}->{data_path}$image\" alt=\"$name\" width=\"$width\" height=\"$height\" />";;
		} else {
			$info = $name;
		}

		$data .= $skin_ins->get_replace_data(
			'menu',
			MENU_INFO  => $info,
			MENU_URL   => $url,
			MENU_NAME  => $name,
			MENU_IMAGE => $self->{plugin}->{data_path} . $image
		);
	}
	close(FH);

	$data .= $skin_ins->get_data('menu_foot');

	return $data;
}

### 作業内容一覧
sub _work_navi {
	my $self     = shift;
	my $skin_ins = shift;

	my $work_data;
	$work_data .= "new\tメニュー追加\n";
	$work_data .= "edit\tメニュー編集\n";

	foreach (split(/\n/, $work_data)) {
		my($work_id, $work_name) = split(/\t/);

		if ($self->{query}->{work} eq $work_id) {
			print $skin_ins->get_replace_data(
				'work_selected',
				WORK_ID   => $work_id,
				WORK_NAME => $work_name
			);
		} else {
			print $skin_ins->get_replace_data(
				'work',
				WORK_ID   => $work_id,
				WORK_NAME => $work_name
			);
		}
	}

	return;
}

1;
