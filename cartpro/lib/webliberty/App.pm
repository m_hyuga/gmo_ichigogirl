#webliberty::App.pm (2006/10/08)
#Copyright(C) 2002-2006 Knight, All rights reserved.

package webliberty::App;

use strict;
use base qw(webliberty::Basis);
use webliberty::Parser;
use webliberty::Configure;
use webliberty::App::Init;

### コンストラクタ
sub new {
	my $class = shift;

	my $self = {
	};
	bless $self, $class;

	return $self;
}

### メイン処理
sub run {
	my $self = shift;

	my $init_ins   = new webliberty::App::Init;
	my $parser_ins = new webliberty::Parser(max => $init_ins->get_init('parse_size'), jcode => $init_ins->get_init('jcode_mode'));

	if (-e $init_ins->get_init('data_config')) {
		my $config_ins = new webliberty::Configure($init_ins->get_init('data_config'));

		if ($parser_ins->get_query('plugin')) {
			require webliberty::Plugin;
			my $app_ins = new webliberty::Plugin($init_ins->get_init, $config_ins->get_config, $parser_ins->get_query);
			$app_ins->run;
		}

		if ($parser_ins->get_query('mode') eq 'setup') {
			require webliberty::App::Setup;
			my $app_ins = new webliberty::App::Setup($init_ins->get_init, $config_ins->get_config, $parser_ins->get_query);
			$app_ins->run;
		} elsif ($parser_ins->get_query('mode') eq 'info') {
			require webliberty::App::Info;
			my $app_ins = new webliberty::App::Info($init_ins->get_init, $config_ins->get_config, $parser_ins->get_query);
			$app_ins->run;
		} elsif ($parser_ins->get_query('mode') eq 'admin') {
			require webliberty::App::Admin;
			my $app_ins = new webliberty::App::Admin($init_ins->get_init, $config_ins->get_config, $parser_ins->get_query);
			$app_ins->run;
		} elsif ($parser_ins->get_query('mode') eq 'password') {
			require webliberty::App::Password;
			my $app_ins = new webliberty::App::Password($init_ins->get_init, $config_ins->get_config, $parser_ins->get_query);
			$app_ins->run;
		} elsif ($parser_ins->get_query('mode') eq 'login') {
			require webliberty::App::Login;
			my $app_ins = new webliberty::App::Login($init_ins->get_init, $config_ins->get_config, $parser_ins->get_query);
			$app_ins->run;
		} elsif ($parser_ins->get_query('mode') eq 'regist') {
			require webliberty::App::Regist;
			my $app_ins = new webliberty::App::Regist($init_ins->get_init, $config_ins->get_config, $parser_ins->get_query);
			$app_ins->run;
		} elsif ($parser_ins->get_query('mode') eq 'order') {
			require webliberty::App::Order;
			my $app_ins = new webliberty::App::Order($init_ins->get_init, $config_ins->get_config, $parser_ins->get_query);
			$app_ins->run;
		} elsif ($parser_ins->get_query('mode') eq 'cart') {
			require webliberty::App::Cart;
			my $app_ins = new webliberty::App::Cart($init_ins->get_init, $config_ins->get_config, $parser_ins->get_query);
			$app_ins->run;
		} elsif ($parser_ins->get_query('mode') eq 'rank') {
			require webliberty::App::Rank;
			my $app_ins = new webliberty::App::Rank($init_ins->get_init, $config_ins->get_config, $parser_ins->get_query);
			$app_ins->run;
		} elsif ($parser_ins->get_query('mode') eq 'rate') {
			require webliberty::App::Rate;
			my $app_ins = new webliberty::App::Rate($init_ins->get_init, $config_ins->get_config, $parser_ins->get_query);
			$app_ins->run;
		} elsif ($parser_ins->get_query('mode') eq 'rss') {
			require webliberty::App::Rss;
			my $app_ins = new webliberty::App::Rss($init_ins->get_init, $config_ins->get_config, $parser_ins->get_query);
			$app_ins->run;
		} elsif ($parser_ins->get_query('mode') eq 'profile') {
			require webliberty::App::Profile;
			my $app_ins = new webliberty::App::Profile($init_ins->get_init, $config_ins->get_config, $parser_ins->get_query);
			$app_ins->run;
		} elsif ($parser_ins->get_query('mode') eq 'search') {
			require webliberty::App::Search;
			my $app_ins = new webliberty::App::Search($init_ins->get_init, $config_ins->get_config, $parser_ins->get_query);
			$app_ins->run;
		} elsif ($parser_ins->get_query('mode') eq 'edit') {
			require webliberty::App::Edit;
			my $app_ins = new webliberty::App::Edit($init_ins->get_init, $config_ins->get_config, $parser_ins->get_query);
			$app_ins->run;
		} elsif ($parser_ins->get_query('mode') eq 'image') {
			require webliberty::App::Image;
			my $app_ins = new webliberty::App::Image($init_ins->get_init, $config_ins->get_config, $parser_ins->get_query);
			$app_ins->run;
		} elsif ($parser_ins->get_query('mode') eq 'trackback') {
			require webliberty::App::Trackback;
			my $app_ins = new webliberty::App::Trackback($init_ins->get_init, $config_ins->get_config, $parser_ins->get_query);
			$app_ins->run;
		} elsif ($parser_ins->get_query('mode') eq 'comment') {
			require webliberty::App::Comment;
			my $app_ins = new webliberty::App::Comment($init_ins->get_init, $config_ins->get_config, $parser_ins->get_query);
			$app_ins->run;
		} else {
			require webliberty::App::List;
			my $app_ins = new webliberty::App::List($init_ins->get_init, $config_ins->get_config, $parser_ins->get_query);
			$app_ins->run;
		}
	} else {
		require webliberty::App::Setup;
		my $app_ins = new webliberty::App::Setup($init_ins->get_init, '', $parser_ins->get_query);
		$app_ins->run;
	}

	return;
}

1;
