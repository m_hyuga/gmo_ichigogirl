#webliberty::App::Admin.pm (2008/07/05)
#Copyright(C) 2002-2008 Knight, All rights reserved.

package webliberty::App::Admin;

use strict;
use base qw(webliberty::Basis Exporter);
use vars qw(@EXPORT_OK);
use webliberty::String;
use webliberty::Decoration;
use webliberty::File;
use webliberty::Host;
use webliberty::Cookie;
use webliberty::Lock;
use webliberty::Skin;
use webliberty::Trackback;
use webliberty::Ping;
use webliberty::Plugin;
use webliberty::App::Init;
use webliberty::App::Catalog;

@EXPORT_OK = qw(check_password get_user get_authority set_user record_log check_access check regist edit del del_trackback);

### コンストラクタ
sub new {
	my $class = shift;

	my $self = {
		init    => shift,
		config  => shift,
		query   => shift,
		plugin  => undef,
		login   => undef,
		html    => undef,
		message => undef,
		update  => undef
	};
	bless $self, $class;

	my $cookie_ins = new webliberty::Cookie($self->{config}->{cookie_admin}, $self->{init}->{des_key});

	my %query;
	if ($self->{query}) {
		%query = %{$self->{query}};
	}

	my($admin_user, $admin_pwd);
	if (exists $query{'admin_user'}) {
		$admin_user = $self->{query}->{admin_user};
	} else {
		$admin_user = $cookie_ins->get_cookie('admin_user');
	}
	if (exists $query{'admin_pwd'}) {
		$admin_pwd = $self->{query}->{admin_pwd};
	} else {
		$admin_pwd = $cookie_ins->get_cookie('admin_pwd');
	}

	if ($admin_pwd) {
		my(%pwd, %authority, $default_user);

		open(USER, $self->{init}->{data_user}) or $self->error("Read Error : $self->{init}->{data_user}");
		while (<USER>) {
			chomp;
			my($user, $pwd, $authority) = split(/\t/);

			if (!$default_user) {
				$default_user = $user;
			}

			$pwd{$user}       = $pwd;
			$authority{$user} = $authority;
		}
		close(USER);

		if (!$admin_user) {
			$admin_user = $default_user;
		}

		my $pwd_ins = new webliberty::String($admin_pwd);
		if ($pwd_ins->get_string and $pwd_ins->check_password($pwd{$admin_user})) {
			$self->{login}->{user}      = $admin_user;
			$self->{login}->{pwd}       = $pwd{$admin_user};
			$self->{login}->{authority} = $authority{$admin_user};

			if ($self->{query}->{admin_pwd}) {
				if ($self->{query}->{hold}) {
					$cookie_ins->set_holddays(3650);
				}
				$cookie_ins->set_cookie(
					admin_user => $admin_user,
					admin_pwd  => $admin_pwd
				);
			}

			$self->{login}->{stat} = 1;
		} else {
			$self->{login}->{stat} = 0;
		}
	} else {
		$self->{login}->{stat} = 0;
	}

	return $self;
}

### メイン処理
sub run {
	my $self = shift;

	if ($self->{init}->{rewrite_mode}) {
		my $catalog_ins = new webliberty::App::Catalog($self->{init}, '', $self->{query});
		$self->{init} = $catalog_ins->rewrite(%{$self->{init}->{rewrite}});
	}

	if ($self->check_password) {
		if ($self->{query}->{work} eq 'new') {
			if ($self->{query}->{exec_regist}) {
				$self->check_access;
				$self->check;
				$self->regist;
				$self->output_edit;
			} elsif ($self->{query}->{exec_preview}) {
				$self->check_access;
				$self->check;
				$self->output_preview;
			} else {
				$self->output_form;
			}
		} elsif ($self->{query}->{work} eq 'edit') {
			if ($self->{query}->{exec_regist}) {
				$self->check_access;
				$self->check;
				$self->edit;
			} elsif ($self->{query}->{exec_del}) {
				$self->check_access;
				$self->del;
			}
			if ($self->{query}->{exec_preview}) {
				$self->check;
				$self->output_preview;
			} elsif ($self->{query}->{exec_form}) {
				$self->output_form;
			} elsif ($self->{query}->{exec_confirm}) {
				$self->output_confirm;
			} else {
				$self->output_edit;
			}
		} elsif ($self->{query}->{work} eq 'comment') {
			require webliberty::App::Edit;
			my $app_ins = new webliberty::App::Edit($self->{init}, $self->{config}, $self->{query});
			if ($self->{query}->{exec_regist}) {
				$app_ins->edit;
				$self->{message} = $app_ins->{message};
				$self->record_log($self->{message});
			} elsif ($self->{query}->{exec_del}) {
				$app_ins->del;
				$self->{message} = $app_ins->{message};
				$self->record_log($self->{message});
			} elsif ($self->{query}->{exec_stat}) {
				$self->check_access;
				$self->stat_comment;
			}
			if ($self->{query}->{exec_preview}) {
				$app_ins->output_preview;
			} elsif ($self->{query}->{exec_form}) {
				$app_ins->output_form;
			} elsif ($self->{query}->{exec_confirm}) {
				$self->output_confirm;
			} elsif ($self->{query}->{exec_view}) {
				$self->output_view;
			} else {
				$self->output_comment;
			}
		} elsif ($self->{query}->{work} eq 'trackback') {
			if ($self->{query}->{exec_del}) {
				$self->check_access;
				$self->del_trackback;
			} elsif ($self->{query}->{exec_stat}) {
				$self->check_access;
				$self->stat_trackback;
			}
			if ($self->{query}->{exec_confirm}) {
				$self->output_confirm;
			} elsif ($self->{query}->{exec_view}) {
				$self->output_view;
			} else {
				$self->output_trackback;
			}
		} elsif ($self->{query}->{work} eq 'customer') {
			require webliberty::App::Customer;
			my $app_ins = new webliberty::App::Customer($self->{init}, $self->{config}, $self->{query});
			if ($self->{query}->{exec_regist}) {
				$app_ins->edit;
				$self->{message} = $app_ins->{message};
				$self->record_log($self->{message});
			} elsif ($self->{query}->{exec_del}) {
				$app_ins->del;
				$self->{message} = $app_ins->{message};
				$self->record_log($self->{message});
			} elsif ($self->{query}->{exec_stat}) {
				$self->check_access;
				$self->stat_customer;
			}
			if ($self->{query}->{exec_form}) {
				$self->output_modify;
			} elsif ($self->{query}->{exec_confirm}) {
				$self->output_confirm;
			} else {
				$self->output_customer;
			}
		} elsif ($self->{query}->{work} eq 'field') {
			if ($self->{query}->{exec_add}) {
				$self->check_access;
				$self->add_field;
			} elsif ($self->{query}->{exec_edit}) {
				$self->check_access;
				$self->edit_field;
			}
			$self->output_field;
		} elsif ($self->{query}->{work} eq 'icon') {
			if ($self->{query}->{exec_add}) {
				$self->check_access;
				$self->add_icon;
			} elsif ($self->{query}->{exec_del}) {
				$self->check_access;
				$self->del_icon;
			}
			$self->output_icon;
		} elsif ($self->{query}->{work} eq 'option') {
			if ($self->{query}->{exec_add}) {
				$self->check_access;
				$self->add_option;
			} elsif ($self->{query}->{exec_edit}) {
				$self->check_access;
				$self->edit_option;
			}
			$self->output_option;
		} elsif ($self->{query}->{work} eq 'top') {
			if ($self->{query}->{exec_top}) {
				$self->check_access;
				$self->top;
			}
			$self->output_top;
		} elsif ($self->{query}->{work} eq 'menu') {
			if ($self->{query}->{exec_add}) {
				$self->check_access;
				$self->add_menu;
			} elsif ($self->{query}->{exec_edit}) {
				$self->check_access;
				$self->edit_menu;
			}
			$self->output_menu;
		} elsif ($self->{query}->{work} eq 'link') {
			if ($self->{query}->{exec_add}) {
				$self->check_access;
				$self->add_link;
			} elsif ($self->{query}->{exec_edit}) {
				$self->check_access;
				$self->edit_link;
			}
			$self->output_link;
		} elsif ($self->{query}->{work} eq 'profile') {
			if ($self->{query}->{exec_profile}) {
				$self->check_access;
				$self->profile;
			}
			$self->output_profile;
		} elsif ($self->{query}->{work} eq 'pwd') {
			if ($self->{query}->{exec_pwd}) {
				$self->check_access;
				$self->pwd;
			}
			$self->output_pwd;
		} elsif ($self->{query}->{work} eq 'env') {
			if ($self->{query}->{exec_env} or $self->{query}->{exec_default}) {
				$self->check_access;
				$self->env;
			}
			if ($self->{query}->{exec_confirm}) {
				$self->output_confirm;
			} else {
				$self->output_env;
			}
		} elsif ($self->{query}->{work} eq 'build') {
			if ($self->{query}->{exec_build}) {
				$self->check_access;
				$self->build;
			}
			$self->output_build;
		} elsif ($self->{query}->{work} eq 'user') {
			if ($self->{query}->{exec_add}) {
				$self->check_access;
				$self->add_user;
			} elsif ($self->{query}->{exec_edit}) {
				$self->check_access;
				$self->edit_user;
			}
			$self->output_user;
		} elsif ($self->{query}->{work} eq 'record') {
			$self->output_record;
		} elsif ($self->{query}->{work} eq 'logout') {
			$self->logout;
			$self->output_login;
		} else {
			$self->output_status;
		}
	} else {
		$self->output_login;
	}

	return;
}

### パスワード認証結果チェック
sub check_password {
	my $self = shift;

	return $self->{login}->{stat};
}

### ログインユーザー名取得
sub get_user {
	my $self = shift;

	return $self->{login}->{user};
}

### ログインユーザー権限取得
sub get_authority {
	my $self = shift;

	return $self->{login}->{authority};
}

### ログインユーザー名設定
sub set_user {
	my $self = shift;
	my $user = shift;

	$self->{login}->{user} = $user;

	return;
}

### 操作履歴保存
sub record_log {
	my $self = shift;
	my $log  = shift;

	my $new_data;
	my $i;

	open(FH, $self->{init}->{data_record}) or $self->error("Read Error : $self->{init}->{data_record}");
	while (<FH>) {
		$i++;
		if ($i >= $self->{config}->{record_size}) {
			last;
		}

		$new_data .= $_;
	}
	close(FH);

	my $host_ins = new webliberty::Host;

	$new_data = time . "\t" . $self->get_user . "\t$log\t" . $host_ins->get_host . "\n$new_data";

	open(FH, ">$self->{init}->{data_record}") or $self->error("Write Error : $self->{init}->{data_record}");
	print FH $new_data;
	close(FH);

	return;
}

### アクセスチェック
sub check_access {
	my $self  = shift;
	my $agent = shift;

	my $flag;

	if ($agent ne 'mobile' and $ENV{'REQUEST_METHOD'} ne 'POST') {
		$flag = 1;
	}
	if ($ENV{'HTTP_REFERER'} and $self->{config}->{base_url} and $ENV{'HTTP_REFERER'} !~ $self->{config}->{base_url}) {
		$flag = 1;
	}
	if (!$self->{config}->{proxy_mode} and ($ENV{'HTTP_VIA'} or $ENV{'HTTP_FORWARDED'} or $ENV{'HTTP_X_FORWARDED_FOR'})) {
		$flag = 1;
	}

	my $host_ins = new webliberty::Host;
	foreach (split(/<>/, $self->{config}->{black_list})) {
		$_ = quotemeta($_);

		if ($host_ins->get_host =~ /$_/i) {
			$flag = 1;
			last;
		}
	}

	if ($flag) {
		$self->error('不正なアクセスです。');
	}

	return;
}

### 入力内容チェック
sub check {
	my $self = shift;

	my $edit_ins  = new webliberty::String($self->{query}->{edit});
	my $id_ins    = new webliberty::String($self->{query}->{id});
	my $subj_ins  = new webliberty::String($self->{query}->{subj});
	my $text_ins  = new webliberty::String($self->{query}->{text});
	my $price_ins = new webliberty::String($self->{query}->{price});
	my $stock_ins = new webliberty::String($self->{query}->{stock});

	my $year_ins   = new webliberty::String($self->{query}->{year});
	my $month_ins  = new webliberty::String($self->{query}->{month});
	my $day_ins    = new webliberty::String($self->{query}->{day});
	my $hour_ins   = new webliberty::String($self->{query}->{hour});
	my $minute_ins = new webliberty::String($self->{query}->{minute});

	my $init_ins = new webliberty::App::Init;
	my %label = %{$init_ins->get_label};

	if (!$id_ins->get_string) {
		$self->error("$label{'pc_id'}が入力されていません。");
	}
	if (!$subj_ins->get_string) {
		$self->error("$label{'pc_subj'}が入力されていません。");
	}
	if (!$text_ins->get_string) {
		$self->error("$label{'pc_text'}が入力されていません。");
	}
	if (!$price_ins->get_string) {
		$self->error("$label{'pc_price'}が入力されていません。");
	}

	if ($id_ins->get_string =~ /[^\w\d\-\_]/) {
		$self->error("$label{'pc_id'}は半角英数字で入力してください。");
	} elsif ($id_ins->get_string !~ /[a-zA-Z]/) {
		$self->error("$label{'pc_id'}は英字を含む値を指定してください。");
	}
	if ($price_ins->get_string =~ /[^\d]/) {
		$self->error("$label{'pc_price'}は半角数字で入力してください。");
	}
	if ($stock_ins->get_string and $stock_ins->get_string =~ /[^\d]/) {
		$self->error("在庫数は半角数字で入力してください。");
	}
	if (!($year_ins->get_string =~ /^\d\d\d\d$/)) {
		$self->error("$label{'pc_date'}の年が正しく指定されていません。");
	}
	if (!($month_ins->get_string =~ /^\d\d$/)) {
		$self->error("$label{'pc_date'}の月が正しく指定されていません。");
	}
	if (!($day_ins->get_string =~ /^\d\d$/)) {
		$self->error("$label{'pc_date'}の日が正しく指定されていません。");
	}
	if (!($hour_ins->get_string =~ /^\d\d$/)) {
		$self->error("$label{'pc_date'}の時間が正しく指定されていません。");
	}
	if (!($minute_ins->get_string =~ /^\d\d$/)) {
		$self->error("$label{'pc_date'}の分が正しく指定されていません。");
	}

	if ($id_ins->get_string) {
		open(FH, $self->{init}->{data_catalog_index}) or $self->error("Read Error : $self->{init}->{data_catalog_index}");
		while (<FH>) {
			chomp;
			my($date, $no, $id, $stat, $field, $name) = split(/\t/);

			if ($self->{query}->{work} eq 'new' and $id_ins->get_string eq $id) {
				$self->error("指定された$label{'pc_id'}はすでに使用されています。");
			}
			if ($self->{query}->{work} eq 'edit' and $edit_ins->get_string != $no and $id_ins->get_string eq $id) {
				$self->error("指定された$label{'pc_id'}はすでに使用されています。");
			}
		}
		close(FH);
	}

	if ($self->{query}->{image}) {
		my $file_ins = new webliberty::File($self->{query}->{image}->{file_name});
		if ($file_ins->get_ext ne 'gif' and $file_ins->get_ext ne 'png' and $file_ins->get_ext ne 'jpeg' and $file_ins->get_ext ne 'jpg' and $file_ins->get_ext ne 'jpe') {
			$self->error("$label{'pc_image'}は画像ファイル（GIF、PNG、JPEG）を指定してください。");
		}
	}

	return;
}

### 新規登録
sub regist {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	my $id_ins     = new webliberty::String($self->{query}->{id});
	my $stat_ins   = new webliberty::String($self->{query}->{stat});
	my $break_ins  = new webliberty::String($self->{query}->{break});
	my $view_ins   = new webliberty::String($self->{query}->{view});
	my $comt_ins   = new webliberty::String($self->{query}->{comt});
	my $tb_ins     = new webliberty::String($self->{query}->{tb});
	my $name_ins   = new webliberty::String($self->{query}->{name});
	my $subj_ins   = new webliberty::String($self->{query}->{subj});
	my $text_ins   = new webliberty::String($self->{query}->{text});
	my $price_ins  = new webliberty::String($self->{query}->{price});
	my $stock_ins  = new webliberty::String($self->{query}->{stock});
	my $relate_ins = new webliberty::String($self->{query}->{relate});
	my $icon_ins   = new webliberty::String($self->{query}->{icon});
	my $tburl_ins  = new webliberty::String($self->{query}->{tb_url});

	$id_ins->create_line;
	$stat_ins->create_number;
	$break_ins->create_number;
	$view_ins->create_number;
	$comt_ins->create_number;
	$tb_ins->create_number;
	$name_ins->create_line;
	$subj_ins->create_line;
	$text_ins->create_text;
	$price_ins->create_number;
	$stock_ins->create_number;
	$relate_ins->create_text;
	$icon_ins->create_line;
	$tburl_ins->create_text;

	my $log_file = "$self->{init}->{data_catalog_dir}$self->{query}->{year}$self->{query}->{month}\.$self->{init}->{data_ext}";
	my $now      = "$self->{query}->{year}$self->{query}->{month}$self->{query}->{day}$self->{query}->{hour}$self->{query}->{minute}";
	my @logs;

	my $lock_ins = new webliberty::Lock($self->{init}->{data_lock});
	if (!$lock_ins->file_lock) {
		$self->error('ファイルがロックされています。時間をおいてもう一度投稿してください。');
	}

	#商品記録ファイルオープン
	if (-e $log_file) {
		open(FH, $log_file) or $self->error("Read Error : $log_file");
		@logs = <FH>;
		close(FH);
	} else {
		open(FH, ">$log_file") or $self->error("Write Error : $log_file");
		close(FH);

		if ($self->{init}->{chmod_mode}) {
			if ($self->{init}->{suexec_mode}) {
				chmod(0600, "$log_file") or $self->error("Chmod Error : $log_file");
			} else {
				chmod(0666, "$log_file") or $self->error("Chmod Error : $log_file");
			}
		}
	}

	#記録済みデータ読み込み
	open(FH, $self->{init}->{data_catalog_index}) or $self->error("Read Error : $self->{init}->{data_catalog_index}");
	my @index = <FH>;
	close(FH);

	my @numbers = map { (split(/\t/))[1] } @index;
	@index = @index[sort { $numbers[$b] <=> $numbers[$a] } (0 .. $#numbers)];

	my $new_no   = (split(/\t/, $index[0]))[1] + 1;
	my $host_ins = new webliberty::Host;

	my($new_field, $i);

	open(FH, $self->{init}->{data_field}) or $self->error("Read Error : $self->{init}->{data_field}");
	while (<FH>) {
		chomp;

		if ($self->{query}->{field} == ++$i) {
			$new_field = $_;
		}
	}
	close(FH);

	if (!$name_ins->get_string) {
		$name_ins->set_string($self->get_user);
	}

	#在庫数登録
	if ($self->{config}->{use_stock}) {
		open(FH, ">$self->{init}->{data_stock_dir}$new_no\.$self->{init}->{data_ext}") or $self->error("Write Error : $self->{init}->{data_stock_dir}$new_no\.$self->{init}->{data_ext}");
		print FH $stock_ins->get_string;
		close(FH);
	}

	#関連商品登録
	if ($self->{config}->{use_relate}) {
		my $relate;
		foreach (split(/<br \/>/, $relate_ins->get_string)) {
			if (!$_) {
				next;
			}
			my($id, $subj) = split(/,/, $_, 2);

			$relate .= "$id\t$subj\n";
		}

		open(FH, ">$self->{init}->{data_relate_dir}$new_no\.$self->{init}->{data_ext}") or $self->error("Write Error : $self->{init}->{data_relate_dir}$new_no\.$self->{init}->{data_ext}");
		print FH $relate;
		close(FH);
	}

	#オプション項目登録
	if (-s $self->{init}->{data_option}) {
		my $option;
		foreach (keys %{$self->{query}}) {
			if ($_ =~ /^option_.+/ and ${$self->{query}}{$_} ne '') {
				${$self->{query}}{$_} =~ s/\r?\n/\r/g;
				${$self->{query}}{$_} =~ s/\r/<>/g;

				$option .= "$_\t${$self->{query}}{$_}\n";
			}
		}
		if ($option) {
			open(FH, ">$self->{init}->{data_option_dir}$new_no\.$self->{init}->{data_ext}") or $self->error("Write Error : $self->{init}->{data_option_dir}$new_no\.$self->{init}->{data_ext}");
			print FH $option;
			close(FH);
		}
	}

	my $file_name;
	if ($id_ins->get_string) {
		$file_name = $id_ins->get_string;
	} else {
		$file_name = $new_no;
	}

	#ミニ画像保存
	my $image;
	if ($self->{query}->{image}) {
		my $file_ins = new webliberty::File($self->{query}->{image}->{file_name});
		$image = "$file_name\." . $file_ins->get_ext;

		open(FH, ">$self->{init}->{data_image_dir}$image") or $self->error("Write Error : $self->{init}->{data_image_dir}$image");
		binmode(FH);
		print FH $self->{query}->{image}->{file_data};
		close(FH);
	} elsif ($self->{query}->{image_ext}) {
		$image = "$file_name\.$self->{query}->{image_ext}";

		rename("$self->{init}->{data_image_dir}$self->{init}->{data_tmp_file}" . $self->get_user, "$self->{init}->{data_image_dir}$image");
	}

	#アップロードファイル保存
	my(%file, $files, $flag);

	foreach (1 .. $self->{config}->{max_file}) {
		if ($self->{query}->{'file' . $_} or $self->{query}->{'ext' . $_}) {
			$flag = 1;
		}
	}
	if ($flag) {
		foreach (1 .. $self->{config}->{max_file}) {
			if ($self->{query}->{'file' . $_}) {
				my $file_ins = new webliberty::File($self->{query}->{'file' . $_}->{file_name});
				if ($id_ins->get_string and $_ == 1) {
					$file{$_} = "$file_name\." . $file_ins->get_ext;
				} else {
					$file{$_} = "$file_name\-$_\." . $file_ins->get_ext;
				}

				open(FH, ">$self->{init}->{data_upfile_dir}$file{$_}") or $self->error("Write Error : $self->{init}->{data_upfile_dir}$file{$_}");
				binmode(FH);
				print FH $self->{query}->{'file' . $_}->{file_data};
				close(FH);
			} elsif ($self->{query}->{'ext' . $_}) {
				if ($id_ins->get_string and $_ == 1) {
					$file{$_} = "$file_name\.$self->{query}->{'ext' . $_}";
				} else {
					$file{$_} = "$file_name\-$_\.$self->{query}->{'ext' . $_}";
				}
				rename("$self->{init}->{data_upfile_dir}$self->{init}->{data_tmp_file}" . $self->get_user . $_, "$self->{init}->{data_upfile_dir}$file{$_}");
			}
		}

		foreach (1 .. $self->{config}->{max_file}) {
			if ($_ > 1) {
				$files .= '<>';
			}
			$files .= "$file{$_}";
		}
	}

	#記録用データ作成
	my $catalog = "$new_no\t" . $id_ins->get_string . "\t" . $stat_ins->get_string . "\t" . $break_ins->get_string . "\t" . $view_ins->get_string . "\t" . $comt_ins->get_string . "\t" . $tb_ins->get_string . "\t$new_field\t$now\t" . $name_ins->get_string . "\t" . $subj_ins->get_string . "\t" . $text_ins->get_string . "\t" . $price_ins->get_string . "\t" . $icon_ins->get_string . "\t$image\t$files\t" . $host_ins->get_host;

	my($new_data, $flag);

	open(FH, $log_file) or $self->error("Read Error : $log_file");
	while (<FH>) {
		chomp;
		my($no, $id, $stat, $break, $view, $comt, $tb, $field, $date, $name, $subj, $text, $price, $icon, $image, $file, $host) = split(/\t/);

		if (!$flag and $now > $date) {
			$new_data .= "$catalog\n";
			$flag      = 1;
		}
		$new_data .= "$_\n";
	}
	if (!$flag) {
		$new_data .= "$catalog\n";
	}
	close(FH);

	#商品登録
	open(FH, ">$log_file") or $self->error("Write Error : $log_file");
	print FH $new_data;
	close(FH);

	#インデックス用データ作成
	my $index = "$now\t$new_no\t" . $id_ins->get_string . "\t" . $stat_ins->get_string . "\t$new_field\t" . $name_ins->get_string;

	my($new_index, $flag);

	open(FH, $self->{init}->{data_catalog_index}) or $self->error("Read Error : $self->{init}->{data_catalog_index}");
	while (<FH>) {
		chomp;
		my($date, $no, $id, $stat, $field, $name) = split(/\t/);

		if (!$flag and $now > $date) {
			$new_index .= "$index\n";
			$flag       = 1;
		}
		$new_index .= "$_\n";
	}
	if (!$flag) {
		$new_index .= "$index\n";
	}
	close(FH);

	#インデックス登録
	open(FH, ">$self->{init}->{data_catalog_index}") or $self->error("Write Error : $self->{init}->{data_catalog_index}");
	print FH $new_index;
	close(FH);

	$lock_ins->file_unlock;

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	#データ更新
	$self->{update}->{query}->{no} = $new_no;

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{update}->{query});
	$catalog_ins->update;

	my $alert_message;

	#トラックバック送信
	if ($stat_ins->get_string and $tburl_ins->get_string) {
		my $article_url;
		if ($id_ins->get_string) {
			$article_url = $id_ins->get_string;
		} else {
			$article_url = $new_no;
		}

		if ($self->{config}->{html_archive_mode}) {
			if ($self->{init}->{archive_dir} =~ /([^\/\\]*\/)$/) {
				$article_url = "$self->{config}->{site_url}$1$article_url\.$self->{init}->{archive_ext}";
			}
		} else {
			if ($self->{init}->{script_file} =~ /([^\/\\]*)$/) {
				if ($id_ins->get_string) {
					$article_url = "$self->{config}->{site_url}$1?id=$article_url";
				} else {
					$article_url = "$self->{config}->{site_url}$1?no=$article_url";
				}
			}
		}

		my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});
		my %article = $catalog_ins->catalog_article($new_no, $id_ins->get_string, $stat_ins->get_string, $break_ins->get_string, $view_ins->get_string, $comt_ins->get_string, $tb_ins->get_string, $new_field, $now, $name_ins->get_string, $subj_ins->get_string, $text_ins->get_string, $price_ins->get_string, $icon_ins->get_string, $image, $files, $host_ins->get_host);
		$text_ins->set_string($article{'ARTICLE_TEXT'});

		my $trackback_ins = new webliberty::Trackback;
		foreach (split(/<br \/>/, $tburl_ins->get_string)) {
			if (!$_) {
				next;
			}
			my($flag, $message) = $trackback_ins->send_trackback(
				trackback_url => $_,
				title         => $subj_ins->get_string,
				url           => $article_url,
				excerpt       => $text_ins->get_string,
				blog_name     => $self->{config}->{site_title},
				user_agent    => $self->{init}->{script} . '/' . $self->{init}->{version}
			);
			if (!$flag) {
				$alert_message .= "<strong>$_ へのトラックバック送信に失敗しました。$message</strong>";
			}
		}
	}

	#更新PING送信
	if ($stat_ins->get_string and $self->{query}->{ping}) {
		my $ping_ins = new webliberty::Ping;
		foreach (split(/<>/, $self->{config}->{ping_list})) {
			if (!$_) {
				next;
			}
			my($flag, $message) = $ping_ins->send_ping(
				ping_url  => $_,
				url       => $self->{config}->{site_url},
				blog_name => $self->{config}->{site_title}
			);
			if (!$flag) {
				$alert_message .= "<strong>$_ への更新PING送信に失敗しました。$message</strong>";
			}
		}
	}

	$self->{message} = '商品を新規に登録しました。';

	$self->record_log($self->{message});

	return;
}

### 商品編集
sub edit {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	my $edit_ins   = new webliberty::String($self->{query}->{edit});
	my $id_ins     = new webliberty::String($self->{query}->{id});
	my $stat_ins   = new webliberty::String($self->{query}->{stat});
	my $break_ins  = new webliberty::String($self->{query}->{break});
	my $view_ins   = new webliberty::String($self->{query}->{view});
	my $comt_ins   = new webliberty::String($self->{query}->{comt});
	my $tb_ins     = new webliberty::String($self->{query}->{tb});
	my $name_ins   = new webliberty::String($self->{query}->{name});
	my $subj_ins   = new webliberty::String($self->{query}->{subj});
	my $text_ins   = new webliberty::String($self->{query}->{text});
	my $price_ins  = new webliberty::String($self->{query}->{price});
	my $stock_ins  = new webliberty::String($self->{query}->{stock});
	my $relate_ins = new webliberty::String($self->{query}->{relate});
	my $icon_ins   = new webliberty::String($self->{query}->{icon});
	my $tburl_ins  = new webliberty::String($self->{query}->{tb_url});

	$edit_ins->create_number;
	$id_ins->create_line;
	$stat_ins->create_number;
	$break_ins->create_number;
	$view_ins->create_number;
	$comt_ins->create_number;
	$tb_ins->create_number;
	$name_ins->create_line;
	$subj_ins->create_line;
	$text_ins->create_text;
	$price_ins->create_number;
	$stock_ins->create_number;
	$relate_ins->create_text;
	$icon_ins->create_line;
	$tburl_ins->create_text;

	if (!$edit_ins->get_string) {
		$self->error('編集したい商品を選択してください。');
	}

	my $lock_ins = new webliberty::Lock($self->{init}->{data_lock});
	if (!$lock_ins->file_lock) {
		$self->error('ファイルがロックされています。時間をおいてもう一度投稿してください。');
	}

	#記録用データ作成
	my($new_field, $i);

	open(FH, $self->{init}->{data_field}) or $self->error("Read Error : $self->{init}->{data_field}");
	while (<FH>) {
		chomp;

		if ($self->{query}->{field} == ++$i) {
			$new_field = $_;
		}
	}
	close(FH);

	my $new_date = "$self->{query}->{year}$self->{query}->{month}$self->{query}->{day}$self->{query}->{hour}$self->{query}->{minute}";

	#インデックス用データ作成
	my(@new_index, $edit_date);

	open(FH, $self->{init}->{data_catalog_index}) or $self->error("Read Error : $self->{init}->{data_catalog_index}");
	while (<FH>) {
		chomp;
		my($date, $no, $id, $stat, $field, $name) = split(/\t/);

		if ($edit_ins->get_string == $no) {
			$edit_date = $date;

			if (!$name_ins->get_string) {
				$name_ins->set_string($name);
			}

			push(@new_index, "$new_date\t$no\t" . $id_ins->get_string . "\t" . $stat_ins->get_string . "\t$new_field\t" . $name_ins->get_string . "\n");
		} else {
			push(@new_index, "$_\n");
		}
	}
	close(FH);

	#編集データ検索
	my $edit_file;
	if ($edit_date =~ /^(\d\d\d\d)(\d\d)(\d\d)(\d\d)(\d\d)$/) {
		$edit_file = "$self->{init}->{data_catalog_dir}$1$2\.$self->{init}->{data_ext}";
	}

	my($new_data, $org_id, $org_name, $org_image, $org_files, $flag);

	open(FH, $edit_file) or $self->error("Read Error : $edit_file");
	while (<FH>) {
		chomp;
		my($no, $id, $stat, $break, $view, $comt, $tb, $field, $date, $name, $subj, $text, $price, $icon, $image, $file, $host) = split(/\t/);

		if ($edit_ins->get_string == $no) {
			$org_id    = $id;
			$org_name  = $name;
			$org_image = $image;
			$org_files = $file;
			$flag      = 1;
		} else {
			$new_data .= "$_\n";
		}
	}
	close(FH);

	if (!$flag) {
		$self->error('指定された商品は存在しません。');
	}

	if (!$name_ins->get_string) {
		$name_ins->set_string($org_name);
	}

	#在庫数登録
	if ($self->{config}->{use_stock}) {
		open(FH, ">$self->{init}->{data_stock_dir}" . $edit_ins->get_string . "\.$self->{init}->{data_ext}") or $self->error("Write Error : $self->{init}->{data_stock_dir}" . $edit_ins->get_string . "\.$self->{init}->{data_ext}");
		print FH $stock_ins->get_string;
		close(FH);
	}

	#関連商品登録
	if ($self->{config}->{use_relate}) {
		my $relate;
		foreach (split(/<br \/>/, $relate_ins->get_string)) {
			my($id, $subj) = split(/,/, $_, 2);

			if ($id) {
				$relate .= "$id\t$subj\n";
			}
		}

		open(FH, ">$self->{init}->{data_relate_dir}" . $edit_ins->get_string . "\.$self->{init}->{data_ext}") or $self->error("Write Error : $self->{init}->{data_relate_dir}" . $edit_ins->get_string . "\.$self->{init}->{data_ext}");
		print FH $relate;
		close(FH);
	}

	#オプション項目登録
	if (-s $self->{init}->{data_option}) {
		my $option;
		foreach (keys %{$self->{query}}) {
			if ($_ =~ /^option_.+/ and ${$self->{query}}{$_} ne '') {
				${$self->{query}}{$_} =~ s/\r?\n/\r/g;
				${$self->{query}}{$_} =~ s/\r/<>/g;

				$option .= "$_\t${$self->{query}}{$_}\n";
			}
		}
		if ($option) {
			open(FH, ">$self->{init}->{data_option_dir}" . $edit_ins->get_string . "\.$self->{init}->{data_ext}") or $self->error("Write Error : $self->{init}->{data_option_dir}" . $edit_ins->get_string . "\.$self->{init}->{data_ext}");
			print FH $option;
			close(FH);
		} else {
			unlink($self->{init}->{data_option_dir} . $edit_ins->get_string . "\.$self->{init}->{data_ext}");
		}
	}

	my $file_name;
	if ($org_id) {
		$file_name = $org_id;
	} else {
		$file_name = $edit_ins->get_string;
	}

	#ミニ画像保存
	my $image;
	if ($self->{query}->{delimage}) {
		unlink("$self->{init}->{data_image_dir}$org_image");
	} elsif ($self->{query}->{image}) {
		my $file_ins = new webliberty::File($self->{query}->{image}->{file_name});
		$image = "$file_name\." . $file_ins->get_ext;

		open(FH, ">$self->{init}->{data_image_dir}$image") or $self->error("Write Error : $self->{init}->{data_image_dir}$image");
		binmode(FH);
		print FH $self->{query}->{image}->{file_data};
		close(FH);
	} elsif ($self->{query}->{image_ext}) {
		$image = $edit_ins->get_string . "\.$self->{query}->{image_ext}";

		rename("$self->{init}->{data_image_dir}$self->{init}->{data_tmp_file}" . $self->get_user, "$self->{init}->{data_image_dir}$image");
	} elsif (($id_ins->get_string or $org_id) and $id_ins->get_string ne $org_id and $org_image) {
		my $file_ins = new webliberty::File($org_image);
		if ($id_ins->get_string) {
			$image = $id_ins->get_string . '.' . $file_ins->get_ext;
		} else {
			$image = $edit_ins->get_string . '.' . $file_ins->get_ext;
		}
		rename("$self->{init}->{data_image_dir}$org_image", "$self->{init}->{data_image_dir}$image");
	} else {
		$image = $org_image;
	}

	#アップロードファイル保存
	my(%file, $files);
	$flag = '';

	foreach (1 .. $self->{config}->{max_file}) {
		if ($self->{query}->{'file' . $_} or $self->{query}->{'ext' . $_} or $self->{query}->{'delfile' . $_}) {
			$flag = 1;
		}
	}
	if ($flag) {
		my @org_files = split(/<>/, $org_files);

		foreach (1 .. $self->{config}->{max_file}) {
			my $file;

			if ($self->{query}->{'delfile' . $_}) {
				if (-e $self->{init}->{data_thumbnail_dir} . $org_files[$_ - 1]) {
					unlink($self->{init}->{data_thumbnail_dir} . $org_files[$_ - 1]);
				}
				unlink($self->{init}->{data_upfile_dir} . $org_files[$_ - 1]);
			} elsif ($self->{query}->{'file' . $_}) {
				if (-e $self->{init}->{data_thumbnail_dir} . $org_files[$_ - 1]) {
					unlink($self->{init}->{data_thumbnail_dir} . $org_files[$_ - 1]);
				}

				my $file_ins = new webliberty::File($self->{query}->{'file' . $_}->{file_name});
				if ($org_id and $_ == 1) {
					$file{$_} = "$file_name\." . $file_ins->get_ext;
				} else {
					$file{$_} = "$file_name\-$_\." . $file_ins->get_ext;
				}

				open(FH, ">$self->{init}->{data_upfile_dir}$file{$_}") or $self->error("Write Error : $self->{init}->{data_upfile_dir}$file{$_}");
				binmode(FH);
				print FH $self->{query}->{'file' . $_}->{file_data};
				close(FH);
			} elsif ($self->{query}->{'ext' . $_}) {
				if ($org_id and $_ == 1) {
					$file{$_} = "$self->{query}->{edit}\.$self->{query}->{'ext' . $_}";
				} else {
					$file{$_} = "$self->{query}->{edit}\-$_\.$self->{query}->{'ext' . $_}";
				}
				rename("$self->{init}->{data_upfile_dir}$self->{init}->{data_tmp_file}" . $self->get_user . $_, "$self->{init}->{data_upfile_dir}$file{$_}");
			} elsif ($org_files[$_ - 1]) {
				$file{$_} = $org_files[$_ - 1];
			}
		}

		foreach (1 .. $self->{config}->{max_file}) {
			if ($_ > 1) {
				$files .= '<>';
			}
			$files .= "$file{$_}";
		}
	} else {
		$files = $org_files;
	}
	if (($id_ins->get_string or $org_id) and $id_ins->get_string ne $org_id) {
		my @files = split(/<>/, $files);

		$files = '';
		foreach (1 .. $self->{config}->{max_file}) {
			my $file_ins = new webliberty::File($files[$_ - 1]);

			my $new_file;
			if ($files[$_ - 1]) {
				if ($id_ins->get_string) {
					if ($_ == 1) {
						$new_file = $id_ins->get_string . '.' . $file_ins->get_ext;
					} else {
						$new_file = $id_ins->get_string . "\-$_\." . $file_ins->get_ext;
					}
				} else {
					$new_file = $edit_ins->get_string . "\-$_\." . $file_ins->get_ext;
				}

				rename($self->{init}->{data_upfile_dir} . $files[$_ - 1], "$self->{init}->{data_upfile_dir}$new_file");
			}

			if ($_ > 1) {
				$files .= '<>';
			}
			$files .= $new_file;
		}
	}

	#HTMLファイル削除
	if ($self->{config}->{html_archive_mode}) {
		unlink("$self->{init}->{archive_dir}$file_name\.$self->{init}->{archive_ext}");
	}

	#編集データ登録
	open(FH, ">$edit_file") or $self->error("Write Error : $edit_file");
	print FH $new_data;
	close(FH);

	#記録用データ作成
	my $host_ins = new webliberty::Host;

	my $catalog  = $edit_ins->get_string . "\t" . $id_ins->get_string . "\t" . $stat_ins->get_string . "\t" . $break_ins->get_string . "\t" . $view_ins->get_string . "\t" . $comt_ins->get_string . "\t" . $tb_ins->get_string . "\t$new_field\t$new_date\t" . $name_ins->get_string . "\t" . $subj_ins->get_string . "\t" . $text_ins->get_string . "\t" . $price_ins->get_string . "\t" . $icon_ins->get_string . "\t$image\t$files\t" . $host_ins->get_host;
	$edit_file = "$self->{init}->{data_catalog_dir}$self->{query}->{year}$self->{query}->{month}\.$self->{init}->{data_ext}";

	if (!-e $edit_file) {
		open(FH, ">$edit_file") or $self->error("Write Error : $edit_file");
		close(FH);

		if ($self->{init}->{chmod_mode}) {
			if ($self->{init}->{suexec_mode}) {
				chmod(0600, "$edit_file") or $self->error("Chmod Error : $edit_file");
			} else {
				chmod(0666, "$edit_file") or $self->error("Chmod Error : $edit_file");
			}
		}
	}

	my $record_data;
	$flag = '';

	open(FH, $edit_file) or $self->error("Read Error : $edit_file");
	while (<FH>) {
		chomp;
		my($no, $id, $stat, $break, $view, $comt, $tb, $field, $date, $name, $subj, $text, $price, $icon, $image, $file, $host) = split(/\t/);

		if (!$flag and (($new_date == $date and $edit_ins->get_string < $no) or $new_date > $date)) {
			$record_data .= "$catalog\n";
			$flag         = 1;
		}
		$record_data .= "$_\n";
	}
	if (!$flag) {
		$record_data .= "$catalog\n";
	}

	#商品登録
	open(FH, ">$edit_file") or $self->error("Write Error : $edit_file");
	print FH $record_data;
	close(FH);

	#インデックス登録
	open(FH, ">$self->{init}->{data_catalog_index}") or $self->error("Write Error : $self->{init}->{data_catalog_index}");
	print FH sort { $b <=> $a } @new_index;
	close(FH);

	$lock_ins->file_unlock;

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	#データ更新
	$self->{update}->{query}->{no} = $edit_ins->get_string;

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{update}->{query});
	$catalog_ins->update;

	my $alert_message;

	#トラックバック送信
	if ($stat_ins->get_string and $tburl_ins->get_string) {
		my $article_url;
		if ($id_ins->get_string) {
			$article_url = $id_ins->get_string;
		} else {
			$article_url = $edit_ins->get_string;
		}

		if ($self->{config}->{html_archive_mode}) {
			if ($self->{init}->{archive_dir} =~ /([^\/\\]*\/)$/) {
				$article_url = "$self->{config}->{site_url}$1$article_url\.$self->{init}->{archive_ext}";
			}
		} else {
			if ($self->{init}->{script_file} =~ /([^\/\\]*)$/) {
				if ($id_ins->get_string) {
					$article_url = "$self->{config}->{site_url}$1?id=$article_url";
				} else {
					$article_url = "$self->{config}->{site_url}$1?no=$article_url";
				}
			}
		}

		my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});
		my %article = $catalog_ins->catalog_article($edit_ins->get_string, $id_ins->get_string, $stat_ins->get_string, $break_ins->get_string, $view_ins->get_string, $comt_ins->get_string, $tb_ins->get_string, $new_field, $new_date, $name_ins->get_string, $subj_ins->get_string, $text_ins->get_string, $price_ins->get_string, $icon_ins->get_string, $image, $files, $host_ins->get_host);
		$text_ins->set_string($article{'ARTICLE_TEXT'});

		my $trackback_ins = new webliberty::Trackback;
		foreach (split(/<br \/>/, $tburl_ins->get_string)) {
			if (!$_) {
				next;
			}
			my($flag, $message) = $trackback_ins->send_trackback(
				trackback_url => $_,
				title         => $subj_ins->get_string,
				url           => $article_url,
				excerpt       => $text_ins->get_string,
				blog_name     => $self->{config}->{site_title},
				user_agent    => $self->{init}->{script} . '/' . $self->{init}->{version}
			);
			if (!$flag) {
				$alert_message .= "<strong>$_ へのトラックバック送信に失敗しました。$message</strong>";
			}
		}
	}

	#更新PING送信
	if ($stat_ins->get_string and $self->{query}->{ping}) {
		my $ping_ins = new webliberty::Ping;
		foreach (split(/<>/, $self->{config}->{ping_list})) {
			if (!$_) {
				next;
			}
			my($flag, $message) = $ping_ins->send_ping(
				ping_url  => $_,
				url       => $self->{config}->{site_url},
				blog_name => $self->{config}->{site_title}
			);
			if (!$flag) {
				$alert_message .= "<strong>$_ への更新PING送信に失敗しました。$message</strong>";
			}
		}
	}

	$self->{message} = "No." . $edit_ins->get_string . "の商品を編集しました。$alert_message";

	$self->record_log($self->{message});

	return;
}

### 商品削除
sub del {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	if (!$self->{query}->{del}) {
		$self->error('削除したい商品を選択してください。');
	}

	my $lock_ins = new webliberty::Lock($self->{init}->{data_lock});
	if (!$lock_ins->file_lock) {
		$self->error('ファイルがロックされています。時間をおいてもう一度投稿してください。');
	}

	#削除データ検索
	my($new_index, %del_file, $del_id, $del_name);

	open(FH, $self->{init}->{data_catalog_index}) or $self->error("Read Error : $self->{init}->{data_catalog_index}");
	while (<FH>) {
		chomp;
		my($date, $no, $id, $stat, $field, $name) = split(/\t/);

		if ($self->{query}->{del} =~ /(^|\n)$no(\n|$)/) {
			if ($date =~ /^(\d\d\d\d)(\d\d)(\d\d)(\d\d)(\d\d)$/) {
				$del_file{"$self->{init}->{data_catalog_dir}$1$2\.$self->{init}->{data_ext}"} = 1;
			}
			$del_id   = $id;
			$del_name = $name;
		} else {
			$new_index .= "$_\n";
		}
	}
	close(FH);

	if ($self->get_authority ne 'root' and $self->get_user ne $del_name) {
		$self->error('他のユーザーの商品は削除できません。');
	}

	#商品削除
	my $flag;

	foreach my $entry (keys %del_file) {
		my $new_data;

		open(FH, $entry) or $self->error("Read Error : $entry");
		while (<FH>) {
			chomp;
			my($no, $id, $stat, $break, $view, $comt, $tb, $field, $date, $name, $subj, $text, $price, $icon, $image, $file, $host) = split(/\t/);

			if ($self->{query}->{del} =~ /(^|\n)$no(\n|$)/) {
				if ($image) {
					unlink("$self->{init}->{data_image_dir}$image");
				}
				if ($file) {
					foreach my $del_file (split(/<>/, $file)) {
						if ($del_file) {
							if (-e "$self->{init}->{data_thumbnail_dir}$del_file") {
								unlink("$self->{init}->{data_thumbnail_dir}$del_file");
							}
							unlink("$self->{init}->{data_upfile_dir}$del_file");
						}
					}
				}

				$flag = 1;
			} else {
				$new_data .= "$_\n";
			}
		}
		close(FH);

		open(FH, ">$entry") or $self->error("Write Error : $entry");
		print FH $new_data;
		close(FH);
	}

	if (!$flag) {
		$self->error('指定された商品は存在しません。');
	}

	#コメント削除
	my $comt_index;
	open(FH, $self->{init}->{data_comt_index}) or $self->error("Read Error : $self->{init}->{data_comt_index}");
	while (<FH>) {
		chomp;
		my($no, $pno, $stat, $date, $name, $subj, $host) = split(/\t/);

		if ($self->{query}->{del} =~ /(^|\n)$pno(\n|$)/) {
			if (-e "$self->{init}->{data_comt_dir}$pno\.$self->{init}->{data_ext}") {
				unlink("$self->{init}->{data_comt_dir}$pno\.$self->{init}->{data_ext}");
			}
		} else {
			$comt_index .= "$_\n";
		}
	}
	close(FH);

	#コメントインデックス登録
	open(FH, ">$self->{init}->{data_comt_index}") or $self->error("Write Error : $self->{init}->{data_comt_index}");
	print FH $comt_index;
	close(FH);

	#トラックバック削除
	my $tb_index;
	open(FH, $self->{init}->{data_tb_index}) or $self->error("Read Error : $self->{init}->{data_tb_index}");
	while (<FH>) {
		chomp;
		my($no, $pno, $stat, $date, $blog, $title, $url) = split(/\t/);

		if ($self->{query}->{del} =~ /(^|\n)$pno(\n|$)/) {
			if (-e "$self->{init}->{data_tb_dir}$pno\.$self->{init}->{data_ext}") {
				unlink("$self->{init}->{data_tb_dir}$pno\.$self->{init}->{data_ext}");
			}
		} else {
			$tb_index .= "$_\n";
		}
	}
	close(FH);

	#トラックバックインデックス登録
	open(FH, ">$self->{init}->{data_tb_index}") or $self->error("Write Error : $self->{init}->{data_tb_index}");
	print FH $tb_index;
	close(FH);

	#インデックス登録
	open(FH, ">$self->{init}->{data_catalog_index}") or $self->error("Write Error : $self->{init}->{data_catalog_index}");
	print FH $new_index;
	close(FH);

	$lock_ins->file_unlock;

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	#データ更新
	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{update}->{query});
	$catalog_ins->update;

	my $del_list;
	foreach (split(/\n/, $self->{query}->{del})) {
		if ($del_list) {
			$del_list .= '、';
		}
		$del_list .= "No.$_";
	}

	if ($self->{config}->{html_archive_mode}) {
		if ($del_id) {
			unlink("$self->{init}->{archive_dir}$del_id\.$self->{init}->{archive_ext}");
		} else {
			unlink("$self->{init}->{archive_dir}$self->{query}->{del}\.$self->{init}->{archive_ext}");
		}
	}

	$self->{message} = "$del_listの商品を削除しました。";

	$self->record_log($self->{message});

	return;
}

### コメントステータス変更
sub stat_comment {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	my $lock_ins = new webliberty::Lock($self->{init}->{data_lock});
	if (!$lock_ins->file_lock) {
		$self->error('ファイルがロックされています。時間をおいてもう一度投稿してください。');
	}

	my($new_data, $flag);

	open(FH, "$self->{init}->{data_comt_dir}$self->{query}->{pno}\.$self->{init}->{data_ext}") or $self->error("Read Error : $self->{init}->{data_comt_dir}$self->{query}->{pno}\.$self->{init}->{data_ext}");
	while (<FH>) {
		chomp;
		my($no, $pno, $stat, $date, $name, $mail, $url, $subj, $text, $rate, $pwd, $host) = split(/\t/);

		if ($self->{query}->{stat} == $no) {
			if ($stat) {
				$new_data .= "$no\t$pno\t0\t$date\t$name\t$mail\t$url\t$subj\t$text\t$rate\t$pwd\t$host\n";
			} else {
				$new_data .= "$no\t$pno\t1\t$date\t$name\t$mail\t$url\t$subj\t$text\t$rate\t$pwd\t$host\n";
			}

			$flag = 1;
		} else {
			$new_data .= "$_\n";
		}
	}
	close(FH);

	if (!$flag) {
		$self->error('指定されたコメントは存在しません。');
	}

	my($index_data, $flag);

	open(FH, $self->{init}->{data_comt_index}) or $self->error("Read Error : $self->{init}->{data_comt_index}");
	while (<FH>) {
		chomp;
		my($no, $pno, $stat, $date, $name, $subj, $host) = split(/\t/);

		if ($self->{query}->{stat} == $no) {
			if ($stat) {
				$index_data .= "$no\t$pno\t0\t$date\t$name\t$subj\t$host\n";
			} else {
				$index_data .= "$no\t$pno\t1\t$date\t$name\t$subj\t$host\n";
			}

			$flag = 1;
		} else {
			$index_data .= "$_\n";
		}
	}
	close(FH);

	if (!$flag) {
		$self->error('指定されたインデックスは存在しません。');
	}

	open(FH, ">$self->{init}->{data_comt_dir}$self->{query}->{pno}\.$self->{init}->{data_ext}") or $self->error("Write Error : $self->{init}->{data_comt_dir}$self->{query}->{pno}\.$self->{init}->{data_ext}");
	print FH $new_data;
	close(FH);

	open(FH, ">$self->{init}->{data_comt_index}") or $self->error("Write Error : $self->{init}->{data_comt_index}");
	print FH $index_data;
	close(FH);

	$lock_ins->file_unlock;

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	#データ更新
	$self->{update}->{query}->{no} = $self->{query}->{pno};

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{update}->{query});
	$catalog_ins->update;

	$self->{message} = "No.$self->{query}->{stat}のコメントのステータスを変更しました。";

	$self->record_log($self->{message});

	return;
}

### トラックバック削除
sub del_trackback {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	if (!$self->{query}->{del}) {
		$self->error('削除したいトラックバックを選択してください。');
	}

	my $lock_ins = new webliberty::Lock($self->{init}->{data_lock});
	if (!$lock_ins->file_lock) {
		$self->error('ファイルがロックされています。時間をおいてもう一度投稿してください。');
	}

	my($new_index, $catalog_no, %del_file);

	#削除データ検索
	open(FH, $self->{init}->{data_tb_index}) or $self->error("Read Error : $self->{init}->{data_tb_index}");
	while (<FH>) {
		chomp;
		my($no, $pno, $stat, $date, $blog, $title, $url) = split(/\t/);

		if ($self->{query}->{del} =~ /(^|\n)$no(\n|$)/) {
			$del_file{"$self->{init}->{data_tb_dir}$pno\.$self->{init}->{data_ext}"} = 1;
		} else {
			$new_index .= "$_\n";
		}
	}
	close(FH);

	#トラックバック削除
	my $flag;

	foreach my $entry (keys %del_file) {
		my $new_data;

		open(FH, $entry) or $self->error("Read Error : $entry");
		while (<FH>) {
			chomp;
			my($no, $pno, $stat, $date, $blog, $title, $url, $excerpt) = split(/\t/);

			if ($self->{query}->{del} =~ /(^|\n)$no(\n|$)/) {
				$catalog_no = $pno;
				$flag = 1;
			} else {
				$new_data .= "$_\n";
			}
		}
		close(FH);

		open(FH, ">$entry") or $self->error("Write Error : $entry");
		print FH $new_data;
		close(FH);
	}

	if (!$flag) {
		$self->error('指定されたトラックバックは存在しません。');
	}

	#インデックス登録
	open(FH, ">$self->{init}->{data_tb_index}") or $self->error("Write Error : $self->{init}->{data_tb_index}");
	print FH $new_index;
	close(FH);

	$lock_ins->file_unlock;

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	my $del_list;
	foreach (split(/\n/, $self->{query}->{del})) {
		$del_list .= "No.$_ ";
	}

	#データ更新
	$self->{update}->{query}->{no} = $catalog_no;

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{update}->{query});
	$catalog_ins->update;

	$self->{message} = "$del_listのトラックバックを削除しました。";

	$self->record_log($self->{message});

	return;
}

### トラックバックステータス変更
sub stat_trackback {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	my $lock_ins = new webliberty::Lock($self->{init}->{data_lock});
	if (!$lock_ins->file_lock) {
		$self->error('ファイルがロックされています。時間をおいてもう一度投稿してください。');
	}

	my($new_data, $flag);

	open(FH, "$self->{init}->{data_tb_dir}$self->{query}->{pno}\.$self->{init}->{data_ext}") or $self->error("Read Error : $self->{init}->{data_tb_dir}$self->{query}->{pno}\.$self->{init}->{data_ext}");
	while (<FH>) {
		chomp;
		my($no, $pno, $stat, $date, $blog, $title, $url, $excerpt) = split(/\t/);

		if ($self->{query}->{stat} == $no) {
			if ($stat) {
				$new_data .= "$no\t$pno\t0\t$date\t$blog\t$title\t$url\t$excerpt\n";
			} else {
				$new_data .= "$no\t$pno\t1\t$date\t$blog\t$title\t$url\t$excerpt\n";
			}

			$flag = 1;
		} else {
			$new_data .= "$_\n";
		}
	}
	close(FH);

	if (!$flag) {
		$self->error('指定されたトラックバックは存在しません。');
	}

	my($index_data, $flag);

	open(FH, $self->{init}->{data_tb_index}) or $self->error("Read Error : $self->{init}->{data_tb_index}");
	while (<FH>) {
		chomp;
		my($no, $pno, $stat, $date, $blog, $title, $url) = split(/\t/);

		if ($self->{query}->{stat} == $no) {
			if ($stat) {
				$index_data .= "$no\t$pno\t0\t$date\t$blog\t$title\t$url\n";
			} else {
				$index_data .= "$no\t$pno\t1\t$date\t$blog\t$title\t$url\n";
			}

			$flag = 1;
		} else {
			$index_data .= "$_\n";
		}
	}
	close(FH);

	if (!$flag) {
		$self->error('指定されたインデックスは存在しません。');
	}

	open(FH, ">$self->{init}->{data_tb_dir}$self->{query}->{pno}\.$self->{init}->{data_ext}") or $self->error("Write Error : $self->{init}->{data_tb_dir}$self->{query}->{pno}\.$self->{init}->{data_ext}");
	print FH $new_data;
	close(FH);

	open(FH, ">$self->{init}->{data_tb_index}") or $self->error("Write Error : $self->{init}->{data_tb_index}");
	print FH $index_data;
	close(FH);

	$lock_ins->file_unlock;

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	#データ更新
	$self->{update}->{query}->{no} = $self->{query}->{pno};

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{update}->{query});
	$catalog_ins->update;

	$self->{message} = "No.$self->{query}->{stat}のトラックバックのステータスを変更しました。";

	$self->record_log($self->{message});

	return;
}

### 顧客ステータス変更
sub stat_customer {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	my $lock_ins = new webliberty::Lock($self->{init}->{data_lock});
	if (!$lock_ins->file_lock) {
		$self->error('ファイルがロックされています。時間をおいてもう一度投稿してください。');
	}

	my($new_data, $flag);

	open(FH, "$self->{init}->{data_customer}") or $self->error("Read Error : $self->{init}->{data_customer}");
	while (<FH>) {
		chomp;
		my($date, $stat, $point, $payment, $delivery, $user_pwd, $order_name, $order_kana, $order_mail, $order_post, $order_pref, $order_addr, $order_phone, $order_fax, $send_name, $send_kana, $send_post, $send_pref, $send_addr, $send_phone, $send_fax, $host) = split(/\t/);

		if ($self->{query}->{stat} eq $order_mail) {
			if ($stat) {
				$new_data .= "$date\t0\t$point\t$payment\t$delivery\t$user_pwd\t$order_name\t$order_kana\t$order_mail\t$order_post\t$order_pref\t$order_addr\t$order_phone\t$order_fax\t$send_name\t$send_kana\t$send_post\t$send_pref\t$send_addr\t$send_phone\t$send_fax\t$host\n";
			} else {
				$new_data .= "$date\t1\t$point\t$payment\t$delivery\t$user_pwd\t$order_name\t$order_kana\t$order_mail\t$order_post\t$order_pref\t$order_addr\t$order_phone\t$order_fax\t$send_name\t$send_kana\t$send_post\t$send_pref\t$send_addr\t$send_phone\t$send_fax\t$host\n";
			}

			$flag = 1;
		} else {
			$new_data .= "$_\n";
		}
	}
	close(FH);

	if (!$flag) {
		$self->error('指定された顧客は存在しません。');
	}

	open(FH, ">$self->{init}->{data_customer}") or $self->error("Write Error : $self->{init}->{data_customer}");
	print FH $new_data;
	close(FH);

	$lock_ins->file_unlock;

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	$self->{message} = "顧客のステータスを変更しました。";

	$self->record_log($self->{message});

	return;
}

### 分類追加
sub add_field {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	if ($self->get_authority ne 'root' and !$self->{config}->{auth_field}) {
		$self->error('この操作を行う権限が与えられていません。');
	}

	my $field_ins  = new webliberty::String($self->{query}->{field});
	my $parent_ins = new webliberty::String($self->{query}->{parent});

	$field_ins->create_line;
	$parent_ins->create_line;

	if (!$field_ins->get_string) {
		$self->error('分類名を入力してください。');
	}

	my $lock_ins = new webliberty::Lock($self->{init}->{data_lock});
	if (!$lock_ins->file_lock) {
		$self->error('ファイルがロックされています。時間をおいてもう一度投稿してください。');
	}

	if ($parent_ins->get_string) {
		my($new_data, $flag);

		open(FH, $self->{init}->{data_field}) or $self->error("Read Error : $self->{init}->{data_field}");
		while (<FH>) {
			chomp;
			my($field, $child) = split(/<>/);

			if ($parent_ins->get_string eq $field) {
				$flag = 1;
			} elsif ($flag and $parent_ins->get_string ne $field) {
				$new_data .= $parent_ins->get_string . '<>' . $field_ins->get_string . "\n";
				$flag = 0;
			}

			$new_data .= "$_\n";
		}
		if ($flag) {
			$new_data .= $parent_ins->get_string . '<>' . $field_ins->get_string . "\n";
		}
		close(FH);

		open(FH, ">$self->{init}->{data_field}") or $self->error("Write Error : $self->{init}->{data_field}");
		print FH $new_data;
		close(FH);
	} else {
		open(FH, ">>$self->{init}->{data_field}") or $self->error("Write Error : $self->{init}->{data_field}");
		print FH "$self->{query}->{parent}$self->{query}->{field}\n";
		close(FH);
	}

	$lock_ins->file_unlock;

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	#データ更新
	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{update}->{query});
	$catalog_ins->update;

	$self->{message} = '分類を追加しました。';

	$self->record_log($self->{message});

	return;
}

### 分類編集
sub edit_field {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	if ($self->get_authority ne 'root' and !$self->{config}->{auth_field}) {
		$self->error('この操作を行う権限が与えられていません。');
	}

	my $lock_ins = new webliberty::Lock($self->{init}->{data_lock});
	if (!$lock_ins->file_lock) {
		$self->error('ファイルがロックされています。時間をおいてもう一度投稿してください。');
	}

	my(@fields, %field, %parent, $new_data, $parent, $flag);
	my $i = 0;

	open(FH, $self->{init}->{data_field}) or $self->error("Read Error : $self->{init}->{data_field}");
	while (<FH>) {
		chomp;
		my($field, $child) = split(/<>/);

		if (!$child) {
			$parent{$field} = $field;
		}
		$field{$_} = $_;

		push(@fields, $_);
	}
	seek(FH, 0, 0);
	$i = 0;
	while (<FH>) {
		chomp;
		my($field, $child) = split(/<>/);

		if ($self->{query}->{from} ne '' and $self->{query}->{to} ne '') {
			if ($self->{query}->{to} == $i) {
				$new_data .= $fields[$self->{query}->{from}] . "\n";
			}
			if ($self->{query}->{from} != $i) {
				$new_data .= "$_\n";
			}
		} elsif (!$self->{query}->{'del' . $i}) {
			my $field_ins  = new webliberty::String($self->{query}->{'field' . $i});
			my $parent_ins = new webliberty::String($self->{query}->{'parent' . $i});

			$field_ins->create_line;
			$parent_ins->create_line;

			if ($parent and $child) {
				if ($_ ne $parent{$parent_ins->get_string} . '<>' . $field_ins->get_string) {
					$flag = 1;
				}

				$field{$_} = $parent{$parent_ins->get_string} . '<>' . $field_ins->get_string;

				$new_data .= "$field{$_}\n";
			} else {
				if ($_ ne $field_ins->get_string) {
					$flag = 1;
				}

				$parent     = $field_ins->get_string;
				$parent{$_} = $parent;
				$field{$_}  = $parent;

				$new_data .= "$parent{$_}\n";
			}
		}

		$i++;
	}
	close(FH);

	open(FH, ">$self->{init}->{data_field}") or $self->error("Write Error : $self->{init}->{data_field}");
	print FH $new_data;
	close(FH);

	if ($flag) {
		#ログ更新
		opendir(DIR, $self->{init}->{data_catalog_dir}) or $self->error("Read Error : $self->{init}->{data_catalog_dir}");
		my @dir = sort { $b <=> $a } readdir(DIR);
		closedir(DIR);

		foreach my $entry (@dir) {
			if ($entry !~ /^\d\d\d\d\d\d\.$self->{init}->{data_ext}$/) {
				next;
			}

			my $new_data;

			open(FH, "$self->{init}->{data_catalog_dir}$entry") or $self->error("Read Error : $self->{init}->{data_catalog_dir}$entry");
			while (<FH>) {
				chomp;
				my($no, $id, $stat, $break, $view, $comt, $tb, $field, $date, $name, $subj, $text, $price, $icon, $image, $file, $host) = split(/\t/);

				if ($field =~ /^.+<>.+$/) {
					$field = $field{$field};
				} else {
					$field = $parent{$field};
				}

				$new_data .= "$no\t$id\t$stat\t$break\t$view\t$comt\t$tb\t$field\t$date\t$name\t$subj\t$text\t$price\t$icon\t$image\t$file\t$host\n";
			}
			close(FH);

			open(FH, ">$self->{init}->{data_catalog_dir}$entry") or $self->error("Write Error : $self->{init}->{data_catalog_dir}$entry");
			print FH $new_data;
			close(FH);
		}

		#インデックス更新
		my $new_index;

		open(FH, $self->{init}->{data_catalog_index}) or $self->error("Read Error : $self->{init}->{data_catalog_index}");
		while (<FH>) {
			chomp;
			my($date, $no, $id, $stat, $field, $name) = split(/\t/);

			if ($field =~ /^.+<>.+$/) {
				$field = $field{$field};
			} else {
				$field = $parent{$field};
			}

			$new_index .= "$date\t$no\t$id\t$stat\t$field\t$name\n";
		}
		close(FH);

		open(FH, ">$self->{init}->{data_catalog_index}") or $self->error("Write Error : $self->{init}->{data_catalog_index}");
		print FH $new_index;
		close(FH);
	}

	$lock_ins->file_unlock;

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	#データ更新
	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{update}->{query});
	$catalog_ins->update;

	$self->{message} = '分類を編集しました。';

	$self->record_log($self->{message});

	return;
}

### アイコン追加
sub add_icon {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	if ($self->get_authority ne 'root' and !$self->{config}->{auth_icon}) {
		$self->error('この操作を行う権限が与えられていません。');
	}

	my $init_ins = new webliberty::App::Init;
	my %label = %{$init_ins->get_label};

	if (!$self->{query}->{file}) {
		$self->error('ファイルを選択してください。');
	}

	my $file_ins = new webliberty::File($self->{query}->{file}->{file_name});
	my $name_ins = new webliberty::String($self->{query}->{name});

	$name_ins->create_line;

	if ($file_ins->get_name =~ /[^\w\-\_]/) {
		$self->error('ファイル名は半角英数字で指定してください。');
	}
	if ($file_ins->get_ext ne 'gif' and $file_ins->get_ext ne 'jpeg' and $file_ins->get_ext ne 'jpg' and $file_ins->get_ext ne 'jpe' and $file_ins->get_ext ne 'png') {
		$self->error('アップロードできるファイル形式は、<em>GIF</em>、<em>JPEG</em>、<em>PNG</em>です。');
	}
	if (!$name_ins->get_string) {
		$self->error("$label{'pc_icon'}名を入力してください。");
	}

	if ($name_ins->check_length > 20 * 2) {
		$self->error("$label{'pc_icon'}名の長さは全角20文字までにしてください。");
	}

	my $lock_ins = new webliberty::Lock($self->{init}->{data_lock});
	if (!$lock_ins->file_lock) {
		$self->error('ファイルがロックされています。時間をおいてもう一度投稿してください。');
	}

	my $file = $file_ins->get_name . '.' . $file_ins->get_ext;

	open(FILE, ">$self->{init}->{data_icon_dir}$file") or $self->error("アップロードファイルが保存できません。");
	binmode(FILE);
	print FILE $self->{query}->{file}->{file_data};
	close(FILE);

	open(FH, $self->{init}->{data_icon}) or $self->error("Read Error : $self->{init}->{data_icon}");
	my @icon = <FH>;
	close(FH);

	push(@icon, "$file\t" . $name_ins->get_string . "\t\t\t\n");

	open(FH, ">$self->{init}->{data_icon}") or $self->error("Write Error : $self->{init}->{data_icon}");
	print FH $self->_sort_icon(@icon);
	close(FH);

	$lock_ins->file_unlock;

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	$self->{message} = "$label{'pc_icon'}を新規に登録しました。";

	$self->record_log($self->{message});

	return;
}

### アイコン削除
sub del_icon {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	if ($self->get_authority ne 'root' and !$self->{config}->{auth_icon}) {
		$self->error('この操作を行う権限が与えられていません。');
	}

	my $init_ins = new webliberty::App::Init;
	my %label = %{$init_ins->get_label};

	if (!$self->{query}->{edit}) {
		$self->error("削除したい$label{'pc_icon'}を選択してください。");
	}

	my $lock_ins = new webliberty::Lock($self->{init}->{data_lock});
	if (!$lock_ins->file_lock) {
		$self->error('ファイルがロックされています。時間をおいてもう一度投稿してください。');
	}

	my $new_data;

	open(FH, $self->{init}->{data_icon}) or $self->error("Read Error : $self->{init}->{data_icon}");
	while (<FH>) {
		chomp;
		my($file, $name, $field, $user, $pwd) = split(/\t/);

		if ($self->{query}->{edit} eq $file) {
			unlink("$self->{init}->{data_icon_dir}$file") or $self->error('アイコン画像が削除できません。');
		} else {
			$new_data .= "$_\n";
		}
	}
	close(FH);

	open(FH, ">$self->{init}->{data_icon}") or $self->error("Write Error : $self->{init}->{data_icon}");
	print FH $new_data;
	close(FH);

	$lock_ins->file_unlock;

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	$self->{message} = "$label{'pc_icon'}$self->{query}->{edit}を削除しました。";

	$self->record_log($self->{message});

	return;
}

### オプション追加
sub add_option {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	if ($self->get_authority ne 'root' and !$self->{config}->{auth_option}) {
		$self->error('この操作を行う権限が与えられていません。');
	}

	my $id_ins      = new webliberty::String($self->{query}->{id});
	my $name_ins    = new webliberty::String($self->{query}->{name});
	my $type_ins    = new webliberty::String($self->{query}->{type});
	my $default_ins = new webliberty::String($self->{query}->{default});

	$id_ins->create_line;
	$name_ins->create_line;
	$type_ins->create_line;
	$default_ins->create_text;

	if (!$id_ins->get_string) {
		$self->error('オプションIDを入力してください。');
	}
	if (!$name_ins->get_string) {
		$self->error('オプション名を入力してください。');
	}
	if (!$type_ins->get_string) {
		$self->error('入力形式を入力してください。');
	}

	if ($id_ins->get_string =~ /[^\w\d\-\_]/) {
		$self->error("オプションIDは半角英数字で入力してください。");
	} elsif ($id_ins->get_string !~ /[a-zA-Z]/) {
		$self->error("オプションIDは英字を含む値を指定してください。");
	}

	my $lock_ins = new webliberty::Lock($self->{init}->{data_lock});
	if (!$lock_ins->file_lock) {
		$self->error('ファイルがロックされています。時間をおいてもう一度投稿してください。');
	}

	my($new_data, $flag);

	open(FH, "$self->{init}->{data_option}") or $self->error("Read Error : $self->{init}->{data_option}");
	my @options = <FH>;
	close(FH);

	push(@options, $id_ins->get_string . "\t" . $name_ins->get_string . "\t" . $type_ins->get_string . "\t" . $default_ins->get_string . "\n");

	open(FH, ">$self->{init}->{data_option}") or $self->error("Write Error : $self->{init}->{data_option}");
	print FH @options;
	close(FH);

	$lock_ins->file_unlock;

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	$self->{message} = 'オプションを追加しました。';

	$self->record_log($self->{message});

	return;
}

### オプション編集
sub edit_option {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	if ($self->get_authority ne 'root' and !$self->{config}->{auth_option}) {
		$self->error('この操作を行う権限が与えられていません。');
	}

	my $lock_ins = new webliberty::Lock($self->{init}->{data_lock});
	if (!$lock_ins->file_lock) {
		$self->error('ファイルがロックされています。時間をおいてもう一度投稿してください。');
	}

	my(@options, $new_data, $field);
	my $i = 0;

	open(FH, $self->{init}->{data_option}) or $self->error("Read Error : $self->{init}->{data_option}");
	while (<FH>) {
		push(@options, $_);
	}
	seek(FH, 0, 0);
	while (<FH>) {
		chomp;
		my($id, $name, $type, $default) = split(/\t/);

		if ($self->{query}->{from} ne '' and $self->{query}->{to} ne '') {
			if ($self->{query}->{to} == $i) {
				$new_data .= $options[$self->{query}->{from}];
			}
			if ($self->{query}->{from} != $i) {
				$new_data .= "$_\n";
			}
		} elsif (!$self->{query}->{'del' . $i}) {
			my $id_ins      = new webliberty::String($self->{query}->{'id' . $i});
			my $name_ins    = new webliberty::String($self->{query}->{'name' . $i});
			my $type_ins    = new webliberty::String($self->{query}->{'type' . $i});
			my $default_ins = new webliberty::String($self->{query}->{'default' . $i});

			$id_ins->create_line;
			$name_ins->create_line;
			$type_ins->create_line;
			$default_ins->create_text;

			$new_data .= $id_ins->get_string . "\t" . $name_ins->get_string . "\t" . $type_ins->get_string . "\t" . $default_ins->get_string . "\n";
		}

		$i++;
	}
	close(FH);

	open(FH, ">$self->{init}->{data_option}") or $self->error("Write Error : $self->{init}->{data_option}");
	print FH $new_data;
	close(FH);

	$lock_ins->file_unlock;

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	$self->{message} = 'オプションを編集しました。';

	$self->record_log($self->{message});

	return;
}

### インデックスページ設定
sub top {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	if ($self->get_authority ne 'root' and !$self->{config}->{auth_top}) {
		$self->error('この操作を行う権限が与えられていません。');
	}

	my $text_ins = new webliberty::String($self->{query}->{text});

	open(FH, ">$self->{init}->{data_top}") or $self->error("Write Error : $self->{init}->{data_top}");
	print FH $text_ins->create_text;
	close(FH);

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	#データ更新
	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{update}->{query});
	$catalog_ins->update;

	$self->{message} = 'インデックスページのテキストを設定しました。';

	$self->record_log($self->{message});

	return;
}

### コンテンツ追加
sub add_menu {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	if ($self->get_authority ne 'root' and !$self->{config}->{auth_menu}) {
		$self->error('この操作を行う権限が与えられていません。');
	}

	my $field_ins = new webliberty::String($self->{query}->{field});
	my $name_ins  = new webliberty::String($self->{query}->{name});
	my $url_ins   = new webliberty::String($self->{query}->{url});

	$field_ins->create_line;
	$name_ins->create_line;
	$url_ins->create_line;

	if (!$name_ins->get_string) {
		$self->error('コンテンツ名を入力してください。');
	}

	my $lock_ins = new webliberty::Lock($self->{init}->{data_lock});
	if (!$lock_ins->file_lock) {
		$self->error('ファイルがロックされています。時間をおいてもう一度投稿してください。');
	}

	my($new_data, $flag);

	open(FH, "$self->{init}->{data_menu}") or $self->error("Read Error : $self->{init}->{data_menu}");
	my @menus = <FH>;
	close(FH);

	push(@menus, $field_ins->get_string . "\t" . $name_ins->get_string . "\t" . $url_ins->get_string . "\n");

	open(FH, ">$self->{init}->{data_menu}") or $self->error("Write Error : $self->{init}->{data_menu}");
	print FH $self->_sort_item(@menus);
	close(FH);

	$lock_ins->file_unlock;

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	#データ更新
	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{update}->{query});
	$catalog_ins->update;

	$self->{message} = 'コンテンツを追加しました。';

	$self->record_log($self->{message});

	return;
}

### コンテンツ編集
sub edit_menu {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	if ($self->get_authority ne 'root' and !$self->{config}->{auth_menu}) {
		$self->error('この操作を行う権限が与えられていません。');
	}

	my $lock_ins = new webliberty::Lock($self->{init}->{data_lock});
	if (!$lock_ins->file_lock) {
		$self->error('ファイルがロックされています。時間をおいてもう一度投稿してください。');
	}

	my(@menus, $new_data, $field);
	my $i = 0;

	open(FH, $self->{init}->{data_menu}) or $self->error("Read Error : $self->{init}->{data_menu}");
	while (<FH>) {
		push(@menus, $_);
	}
	seek(FH, 0, 0);
	while (<FH>) {
		chomp;
		my($field, $name, $url) = split(/\t/);

		if ($self->{query}->{from} ne '' and $self->{query}->{to} ne '') {
			if ($self->{query}->{to} == $i) {
				$new_data .= $menus[$self->{query}->{from}];
			}
			if ($self->{query}->{from} != $i) {
				$new_data .= "$_\n";
			}
		} elsif (!$self->{query}->{'del' . $i}) {
			my $field_ins = new webliberty::String($self->{query}->{'field' . $i});
			my $name_ins  = new webliberty::String($self->{query}->{'name' . $i});
			my $url_ins   = new webliberty::String($self->{query}->{'url' . $i});

			$name_ins->create_line;
			$field_ins->create_line;
			$url_ins->create_line;

			$new_data .= $field_ins->get_string . "\t" . $name_ins->get_string . "\t" . $url_ins->get_string . "\n";
		}

		$i++;
	}
	close(FH);

	open(FH, ">$self->{init}->{data_menu}") or $self->error("Write Error : $self->{init}->{data_menu}");
	print FH $new_data;
	close(FH);

	$lock_ins->file_unlock;

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	#データ更新
	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{update}->{query});
	$catalog_ins->update;

	$self->{message} = 'コンテンツを編集しました。';

	$self->record_log($self->{message});

	return;
}

### リンク追加
sub add_link {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	if ($self->get_authority ne 'root' and !$self->{config}->{auth_link}) {
		$self->error('この操作を行う権限が与えられていません。');
	}

	my $field_ins = new webliberty::String($self->{query}->{field});
	my $name_ins  = new webliberty::String($self->{query}->{name});
	my $url_ins   = new webliberty::String($self->{query}->{url});

	$field_ins->create_line;
	$name_ins->create_line;
	$url_ins->create_line;

	if (!$name_ins->get_string) {
		$self->error('サイト名を入力してください。');
	}

	my $lock_ins = new webliberty::Lock($self->{init}->{data_lock});
	if (!$lock_ins->file_lock) {
		$self->error('ファイルがロックされています。時間をおいてもう一度投稿してください。');
	}

	open(FH, $self->{init}->{data_link}) or $self->error("Read Error : $self->{init}->{data_link}");
	my @links = <FH>;
	close(FH);

	push(@links, $field_ins->get_string . "\t" . $name_ins->get_string . "\t" . $url_ins->get_string . "\n");

	open(FH, ">$self->{init}->{data_link}") or $self->error("Write Error : $self->{init}->{data_link}");
	print FH $self->_sort_item(@links);
	close(FH);

	$lock_ins->file_unlock;

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	#データ更新
	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{update}->{query});
	$catalog_ins->update;

	$self->{message} = 'サイトを追加しました。';

	$self->record_log($self->{message});

	return;
}

### リンク編集
sub edit_link {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	if ($self->get_authority ne 'root' and !$self->{config}->{auth_link}) {
		$self->error('この操作を行う権限が与えられていません。');
	}

	my $lock_ins = new webliberty::Lock($self->{init}->{data_lock});
	if (!$lock_ins->file_lock) {
		$self->error('ファイルがロックされています。時間をおいてもう一度投稿してください。');
	}

	my(@links, $new_data, $field);
	my $i = 0;

	open(FH, $self->{init}->{data_link}) or $self->error("Read Error : $self->{init}->{data_link}");
	while (<FH>) {
		push(@links, $_);
	}
	seek(FH, 0, 0);
	while (<FH>) {
		chomp;
		my($field, $name, $url) = split(/\t/);

		if ($self->{query}->{from} ne '' and $self->{query}->{to} ne '') {
			if ($self->{query}->{to} == $i) {
				$new_data .= $links[$self->{query}->{from}];
			}
			if ($self->{query}->{from} != $i) {
				$new_data .= "$_\n";
			}
		} elsif (!$self->{query}->{'del' . $i}) {
			my $field_ins = new webliberty::String($self->{query}->{'field' . $i});
			my $name_ins  = new webliberty::String($self->{query}->{'name' . $i});
			my $url_ins   = new webliberty::String($self->{query}->{'url' . $i});

			$name_ins->create_line;
			$field_ins->create_line;
			$url_ins->create_line;

			$new_data .= $field_ins->get_string . "\t" . $name_ins->get_string . "\t" . $url_ins->get_string . "\n";
		}

		$i++;
	}
	close(FH);

	open(FH, ">$self->{init}->{data_link}") or $self->error("Write Error : $self->{init}->{data_link}");
	print FH $new_data;
	close(FH);

	$lock_ins->file_unlock;

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	#データ更新
	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{update}->{query});
	$catalog_ins->update;

	$self->{message} = 'リンク集を編集しました。';

	$self->record_log($self->{message});

	return;
}

### プロフィール設定
sub profile {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	my $edit_ins = new webliberty::String($self->{query}->{edit});
	my $name_ins = new webliberty::String($self->{query}->{name});
	my $text_ins = new webliberty::String($self->{query}->{text});

	my($new_data, $flag);

	my $lock_ins = new webliberty::Lock($self->{init}->{data_lock});
	if (!$lock_ins->file_lock) {
		$self->error('ファイルがロックされています。時間をおいてもう一度投稿してください。');
	}

	open(FH, $self->{init}->{data_profile}) or $self->error("Read Error : $self->{init}->{data_profile}");
	while (<FH>) {
		chomp;
		my($user, $name, $text) = split(/\t/);

		if ($self->get_authority eq 'root' and $edit_ins->get_string) {
			if ($edit_ins->get_string eq $user) {
				$new_data .= "$user\t" . $name_ins->create_line . "\t" . $text_ins->create_text . "\n";

				$flag = 1;
			} else {
				$new_data .= "$user\t$name\t$text\n";
			}
		} else {
			if ($self->get_user eq $user) {
				$new_data .= "$user\t" . $name_ins->create_line . "\t" . $text_ins->create_text . "\n";

				$flag = 1;
			} else {
				$new_data .= "$user\t$name\t$text\n";
			}
		}
	}
	close(FH);

	if (!$flag) {
		if ($self->get_authority eq 'root' and $edit_ins->get_string) {
			$new_data .= $edit_ins->create_line . "\t" . $name_ins->create_line . "\t" . $text_ins->create_text . "\n";
		} else {
			$new_data .= $self->get_user . "\t" . $name_ins->create_line . "\t" . $text_ins->create_text . "\n";
		}
	}

	open(FH, ">$self->{init}->{data_profile}") or $self->error("Write Error : $self->{init}->{data_profile}");
	print FH $new_data;
	close(FH);

	$lock_ins->file_unlock;

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	if ($self->get_authority eq 'root' and $edit_ins->get_string) {
		$self->{message} = $edit_ins->get_string . 'のプロフィールを設定しました。';
	} else {
		$self->{message} = 'プロフィールを設定しました。';
	}

	$self->record_log($self->{message});

	return;
}

### ログイン用パスワード設定
sub pwd {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	my $old_pwd_ins = new webliberty::String($self->{query}->{old_pwd});
	my $new_pwd_ins = new webliberty::String($self->{query}->{new_pwd});
	my $cfm_pwd_ins = new webliberty::String($self->{query}->{cfm_pwd});

	if (!$old_pwd_ins->get_string) {
		$self->error('以前のパスワードを入力してください。');
	}
	if (!$new_pwd_ins->get_string) {
		$self->error('新しく設定したいパスワードを入力してください。');
	}
	if (!$cfm_pwd_ins->get_string) {
		$self->error('確認用パスワードを入力してください。');
	}
	if ($new_pwd_ins->get_string ne $cfm_pwd_ins->get_string) {
		$self->error('新しいパスワードと確認用パスワードは、同じものを入力してください。');
	}
	if ($new_pwd_ins->check_length < 4) {
		$self->error('ログイン用パスワードは4文字以上を指定してください。');
	}

	my $flag;

	open(FH, $self->{init}->{data_user}) or $self->error("Read Error : $self->{init}->{data_user}");
	while (<FH>) {
		chomp;
		my($user, $pwd, $authority) = split(/\t/);

		if ($self->get_user eq $user and $old_pwd_ins->check_password($pwd)) {
			$flag = 1;
		}
	}
	close(FH);

	if (!$flag) {
		$self->error('以前のパスワードが違います。');
	}

	my $lock_ins = new webliberty::Lock($self->{init}->{data_lock});
	if (!$lock_ins->file_lock) {
		$self->error('ファイルがロックされています。時間をおいてもう一度投稿してください。');
	}

	my $new_data;

	open(FH, $self->{init}->{data_user}) or $self->error("Read Error : $self->{init}->{data_user}");
	while (<FH>) {
		chomp;
		my($user, $pwd, $authority) = split(/\t/);

		if ($self->get_user eq $user) {
			$new_data .= "$user\t" . $new_pwd_ins->create_password . "\t$authority\n";
		} else {
			$new_data .= "$user\t$pwd\t$authority\n";
		}
	}
	close(FH);

	open(FH, ">$self->{init}->{data_user}") or $self->error("Write Error : $self->{init}->{data_user}");
	print FH $new_data;
	close(FH);

	$lock_ins->file_unlock;

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	my $cookie_ins = new webliberty::Cookie($self->{config}->{cookie_admin}, $self->{init}->{des_key});
	$cookie_ins->set_cookie(
		admin_user => $self->get_user,
		admin_pwd  => $cfm_pwd_ins->get_string
	);

	$self->{message} = 'パスワードを設定しました。';

	$self->record_log($self->{message});

	return;
}

### 環境設定
sub env {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	if ($self->get_authority ne 'root') {
		$self->error('この操作を行う権限が与えられていません。');
	}
	if ($self->{query}->{env_site_url} and $self->{query}->{env_site_url} !~ /\/$/) {
		$self->error('サイトのURLはディレクトリまでを指定してください。');
	}

	my $new_data;
	if ($self->{query}->{exec_default}) {
		my $init_ins = new webliberty::App::Init;
		my %default  = %{$init_ins->get_config};

		foreach (keys %default) {
			$new_data .= "$_=$default{$_}\n";
		}
	} else {
		foreach (keys %{$self->{query}}) {
			if ($_ =~ /^env_/) {
				my $key   = $_;
				my $value = $self->{query}->{$_};

				$key   =~ s/^env_//;
				$value =~ s/\r?\n/\r/g;
				$value =~ s/\r/<>/g;

				$new_data .= "$key=$value\n";
			}
		}
	}

	open(FH, ">$self->{init}->{data_config}") or $self->error("Write Error : $self->{init}->{data_config}");
	print FH $new_data;
	close(FH);

	my $config_ins = new webliberty::Configure($self->{init}->{data_config});
	$self->{config} = $config_ins->get_config;

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	#データ更新
	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{update}->{query});
	$catalog_ins->update;

	if ($self->{query}->{exec_default}) {
		$self->{message} = '環境設定を初期値に戻しました。';
	} else {
		$self->{message} = '環境設定を実行しました。';
	}

	$self->record_log($self->{message});

	if ($self->{config}->{html_index_mode} or $self->{config}->{html_archive_mode}) {
		$self->{message} .= "<strong>外観に関する設定を変更した場合、<a href=\"$self->{init}->{script_file}?mode=admin&amp;work=build\">サイトの再構築</a>を行ってください。</strong>";
	}

	return;
}

### サイト再構築
sub build {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	require webliberty::App::List;
	my $stdout = *STDOUT;

	#各分類を構築
	if ($self->{config}->{html_field_mode} and ($self->{query}->{build} eq 'index' or $self->{query}->{build} eq 'all')) {
		foreach (split(/<>/, $self->{config}->{html_field_list})) {
			my($file, $field) = split(/,/, $_, 2);

			$field =~ s/&/&amp;/g;
			$field =~ s/::/&lt;&gt;/g;

			my $dammy;
			if ($self->{init}->{rewrite_mode}) {
				my $init_ins = new webliberty::App::Init;
				$dammy->{init} = $init_ins->get_init;
			} else {
				$dammy->{init} = $self->{init};
			}
			$dammy->{query}->{field} = $field;

			open(HTML, ">$file") or $self->error("Write Error : $file");
			*STDOUT = *HTML;
			my $app_ins = new webliberty::App::List($self->{init}, $self->{config}, $dammy->{query});
			$app_ins->run;
			close(HTML);

			if ($self->{init}->{chmod_mode}) {
				if ($self->{init}->{suexec_mode}) {
					chmod(0604, "$file") or $self->error("Chmod Error : $file");
				} else {
					chmod(0666, "$file") or $self->error("Chmod Error : $file");
				}
			}
		}
	}

	#アーカイブを構築
	if ($self->{config}->{html_archive_mode} and ($self->{query}->{build} =~ /^\d+$/ or $self->{query}->{build} eq 'all')) {
		my($from, $to);
		if ($self->{query}->{build} =~ /^(\d+)$/) {
			$from = $1;
			$to   = $from + 50 - 1;
		}

		open(FH, $self->{init}->{data_catalog_index}) or $self->error("Read Error : $self->{init}->{data_catalog_index}");
		while (<FH>) {
			chomp;
			my($date, $no, $id, $stat, $field, $name) = split(/\t/);

			if ($self->{query}->{build} ne 'all' and ($no < $from or $no > $to)) {
				next;
			}

			my $file_name;
			if ($id) {
				$file_name = $id;
			} else {
				$file_name = $no;
			}

			my $dammy;
			if ($self->{init}->{rewrite_mode}) {
				my $init_ins = new webliberty::App::Init;
				$dammy->{init} = $init_ins->get_init;
			} else {
				$dammy->{init} = $self->{init};
			}
			$dammy->{query}->{no} = $no;

			open(HTML, ">$self->{init}->{archive_dir}$file_name\.$self->{init}->{archive_ext}") or $self->error("Write Error : $self->{init}->{archive_dir}$file_name\.$self->{init}->{archive_ext}");
			*STDOUT = *HTML;
			my $app_ins = new webliberty::App::List($dammy->{init}, $self->{config}, $dammy->{query});
			$app_ins->run;
			close(HTML);

			if ($self->{init}->{chmod_mode}) {
				if ($self->{init}->{suexec_mode}) {
					chmod(0604, "$self->{init}->{archive_dir}$file_name\.$self->{init}->{archive_ext}") or $self->error("Chmod Error : $self->{init}->{archive_dir}$file_name\.$self->{init}->{archive_ext}");
				} else {
					chmod(0666, "$self->{init}->{archive_dir}$file_name\.$self->{init}->{archive_ext}") or $self->error("Chmod Error : $self->{init}->{archive_dir}$file_name\.$self->{init}->{archive_ext}");
				}
			}
		}
		close(FH);
	}

	#インデックスを構築
	if ($self->{config}->{html_index_mode} and ($self->{query}->{build} eq 'index' or $self->{query}->{build} eq 'all')) {
		my $dammy;
		if ($self->{init}->{rewrite_mode}) {
			my $init_ins = new webliberty::App::Init;
			$dammy->{init} = $init_ins->get_init;
		} else {
			$dammy->{init} = $self->{init};
		}

		open(HTML, ">$self->{init}->{html_file}") or $self->error("Write Error : $self->{init}->{html_file}");
		*STDOUT = *HTML;
		my $app_ins = new webliberty::App::List($dammy->{init}, $self->{config});
		$app_ins->run;
		close(HTML);
	}

	#ナビゲーションを構築
	if ($self->{config}->{show_navigation}) {
		my $dammy;
		if ($self->{init}->{rewrite_mode}) {
			my $init_ins = new webliberty::App::Init;
			$dammy->{init} = $init_ins->get_init;
		} else {
			$dammy->{init} = $self->{init};
		}

		my $app_ins     = new webliberty::App::List($dammy->{init}, $self->{config});
		my $catalog_ins = new webliberty::App::Catalog($dammy->{init}, $self->{config});

		my $skin_ins = new webliberty::Skin;
		$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_view}");
		$skin_ins->replace_skin(
			$catalog_ins->info,
			%{$self->{plugin}}
		);

		my($navi_start, $navi_end);
		if ($self->{config}->{pos_navigation}) {
			$navi_start = $app_ins->get_navi . $skin_ins->get_data('logs_head');
			$navi_end   = $skin_ins->get_data('logs_foot');
		} else {
			$navi_start = $skin_ins->get_data('logs_head');
			$navi_end   = $skin_ins->get_data('logs_foot') . $app_ins->get_navi;
		}

		my $script_ins = new webliberty::Script;

		my($flag, $message) = $script_ins->create_jscript(
			file     => $self->{init}->{js_navi_start_file},
			contents => $navi_start,
			break    => 1
		);
		if (!$flag) {
			$self->error($message);
		}

		my($flag, $message) = $script_ins->create_jscript(
			file     => $self->{init}->{js_navi_end_file},
			contents => $navi_end,
			break    => 1
		);
		if (!$flag) {
			$self->error($message);
		}
	}

	*STDOUT = $stdout;

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	$self->{message} = 'サイトを再構築しました。';

	$self->record_log($self->{message});

	return;
}

### ユーザー追加
sub add_user {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	if ($self->get_authority ne 'root') {
		$self->error('この操作を行う権限が与えられていません。');
	}

	my $user_ins      = new webliberty::String($self->{query}->{user});
	my $pwd_ins       = new webliberty::String($self->{query}->{pwd});
	my $cfm_ins       = new webliberty::String($self->{query}->{cfm});
	my $authority_ins = new webliberty::String($self->{query}->{authority});

	$user_ins->create_line;
	$pwd_ins->create_line;
	$cfm_ins->create_line;
	$authority_ins->create_line;

	if (!$user_ins->get_string) {
		$self->error('ユーザー名を入力してください。');
	}
	if ($user_ins->get_string =~ /[^\w\d\-\_]/) {
		$self->error("ユーザー名は半角英数字で入力してください。");
	}
	if (!$pwd_ins->get_string) {
		$self->error('パスワードを入力してください。');
	}
	if (!$cfm_ins->get_string) {
		$self->error('確認用パスワードを入力してください。');
	}
	if ($pwd_ins->get_string ne $cfm_ins->get_string) {
		$self->error('パスワードと確認用パスワードは、同じものを入力してください。');
	}
	if ($pwd_ins->check_length < 4) {
		$self->error('パスワードは4文字以上を指定してください。');
	}

	$pwd_ins->create_password;

	my $lock_ins = new webliberty::Lock($self->{init}->{data_lock});
	if (!$lock_ins->file_lock) {
		$self->error('ファイルがロックされています。時間をおいてもう一度投稿してください。');
	}

	open(FH, $self->{init}->{data_user}) or $self->error("Read Error : $self->{init}->{data_user}");
	my @users = <FH>;
	close(FH);

	foreach (@users) {
		my($user, $pwd, $authority) = split(/\t/);

		if ($user_ins->get_string eq $user) {
			$self->error("ユーザー名 $user は、すでに使用されています。");
		}
	}

	push(@users, $user_ins->get_string . "\t" . $pwd_ins->get_string . "\t" . $authority_ins->get_string . "\n");

	open(FH, ">$self->{init}->{data_user}") or $self->error("Write Error : $self->{init}->{data_user}");
	print FH sort { $a cmp $b } @users;
	close(FH);

	$lock_ins->file_unlock;

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	#データ更新
	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{update}->{query});
	$catalog_ins->update;

	$self->{message} = 'ユーザー' . $user_ins->get_string . 'を追加しました。';

	$self->record_log($self->{message});

	if ($self->{config}->{html_index_mode} or $self->{config}->{profile_mode}) {
		$self->{message} .= "このユーザーの<a href=\"$self->{init}->{script_file}?mode=admin&amp;work=profile\">プロフィールの設定</a>を行う事ができます。</em>";
	}

	return;
}

### ユーザー編集
sub edit_user {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	if ($self->get_authority ne 'root') {
		$self->error('この操作を行う権限が与えられていません。');
	}

	my $lock_ins = new webliberty::Lock($self->{init}->{data_lock});
	if (!$lock_ins->file_lock) {
		$self->error('ファイルがロックされています。時間をおいてもう一度投稿してください。');
	}

	my(@users, $new_data, $del_data);
	my $i = 0;

	open(FH, $self->{init}->{data_user}) or $self->error("Read Error : $self->{init}->{data_user}");
	while (<FH>) {
		chomp;
		my($user, $pwd, $authority) = split(/\t/);

		if ($self->{query}->{'del' . $i}) {
			if ($user eq $self->get_user) {
				$self->error('ログイン中のユーザーは削除できません。');
			}

			$del_data .= "$user\n";
		} else {
			my $authority_ins = new webliberty::String($self->{query}->{'authority' . $i});
			$authority_ins->create_line;

			if ($user eq $self->get_user and $authority_ins->get_string ne $authority) {
				$self->error('ログインユーザーの権限は変更できません。');
			}

			$new_data .= "$user\t$pwd\t" . $authority_ins->get_string . "\n";
		}

		$i++;
	}
	close(FH);

	open(FH, ">$self->{init}->{data_user}") or $self->error("Write Error : $self->{init}->{data_user}");
	print FH $new_data;
	close(FH);

	$new_data = '';

	open(FH, $self->{init}->{data_profile}) or $self->error("Read Error : $self->{init}->{data_profile}");
	while (<FH>) {
		chomp;
		my($user, $name, $text) = split(/\t/);

		if ($del_data !~ /$user\n/) {
			$new_data .= "$_\n"
		}
	}
	close(FH);

	open(FH, ">$self->{init}->{data_profile}") or $self->error("Write Error : $self->{init}->{data_profile}");
	print FH $new_data;
	close(FH);

	$lock_ins->file_unlock;

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	#データ更新
	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{update}->{query});
	$catalog_ins->update;

	$self->{message} = 'ユーザー情報を編集しました。';

	$self->record_log($self->{message});

	return;
}

### ログアウト
sub logout {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	my $cookie_ins = new webliberty::Cookie($self->{config}->{cookie_admin}, $self->{init}->{des_key});
	$cookie_ins->set_cookie(
		admin_user => '',
		admin_pwd  => ''
	);

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

### 登録フォーム表示
sub output_form {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}


	if (!$self->{message}) {
		if ($self->{query}->{edit}) {
			$self->{message} = "No.$self->{query}->{edit}の商品情報を入力し、<em>登録ボタン</em>を押してください。";
		} else {
			$self->{message} = '商品情報を入力し、<em>登録ボタン</em>を押してください。';
		}
	}

	my $skin_ins = new webliberty::Skin;
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_header}", available => 'header');
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_work}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_form}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_navi}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_footer}", available => 'footer');

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});

	$skin_ins->replace_skin(
		$catalog_ins->info,
		%{$self->{plugin}},
		INFO_MESSAGE => $self->{message}
	);

	my(%form, $form_label, $files);

	if ($self->{query}->{work} eq 'edit') {
		if (!$self->{query}->{edit}) {
			$self->error('編集したい商品を選択してください。');
		}

		my($edit_date, $edit_name);
		open(FH, $self->{init}->{data_catalog_index}) or $self->error("Read Error : $self->{init}->{data_catalog_index}");
		while (<FH>) {
			chomp;
			my($date, $no, $id, $stat, $field, $name) = split(/\t/);

			if ($self->{query}->{edit} == $no) {
				$edit_date = $date;
				$edit_name = $name;

				last;
			}
		}
		close(FH);

		if ($self->get_authority ne 'root' and $self->get_user ne $edit_name) {
			$self->error('他のユーザーの商品は編集できません。');
		}

		my $edit_file;
		if ($edit_date =~ /^(\d\d\d\d)(\d\d)(\d\d)(\d\d)(\d\d)$/) {
			$edit_file = "$self->{init}->{data_catalog_dir}$1$2\.$self->{init}->{data_ext}";
		}

		open(FH, "$edit_file") or $self->error("Read Error : $edit_file");
		while (<FH>) {
			chomp;
			my($no, $id, $stat, $break, $view, $comt, $tb, $field, $date, $name, $subj, $text, $price, $icon, $image, $file, $host) = split(/\t/);

			if ($self->{query}->{edit} == $no) {
				$files = $file;

				%form = $catalog_ins->catalog_form($no, $id, $stat, $break, $view, $comt, $tb, $field, $date, $name, $subj, $text, $price, $icon, $image, $file, $host);
			}
		}
		close(FH);

		$form_label = '商品編集';
	} else {
		my($sec, $min, $hour, $day, $mon, $year) = localtime(time);
		my $date = sprintf("%04d%02d%02d%02d%02d", $year + 1900, $mon + 1, $day, $hour, $min);

		%form = $catalog_ins->catalog_form('', '', $self->{config}->{default_stat}, $self->{config}->{default_break}, $self->{config}->{default_view}, $self->{config}->{default_comt}, $self->{config}->{default_tb}, '', $date, '', '', '', '', '', '', '');

		$form_label = '新規登録';
	}

	my $init_ins = new webliberty::App::Init;
	my %label = %{$init_ins->get_label};

	print $self->header;
	print $skin_ins->get_data('header');
	print $skin_ins->get_data('work_head');
	print $self->work_navi($skin_ins);
	print $skin_ins->get_data('work_foot');
	print $skin_ins->get_replace_data(
		'form_head',
		%form,
		FORM_LABEL => $form_label,
		FORM_WORK  => $self->{query}->{work},
		FORM_TBURL => $self->{query}->{tb_url},
		FORM_PING  => ' checked="checked"'
	);

	if ($self->{config}->{use_file}) {
		my @form_file = split(/<>/, $files);

		print $skin_ins->get_data('file_head');

		foreach (1 .. $self->{config}->{max_file}) {
			my $check;
			if ($self->{query}->{'delfile' . $_}) {
				$check = ' checked="checked"';
			}
			if ($form_file[$_ - 1]) {
				$form_file[$_ - 1] = "<input type=\"checkbox\" name=\"delfile$_\" id=\"delfile${_}_checkbox\" value=\"on\"$check /> <label for=\"delfile${_}_checkbox\">" . $form_file[$_ - 1] . "を削除</label>";
			}

			print $skin_ins->get_replace_data(
				'file',
				FILE_TITLE => $label{'pc_file'} . $_,
				FILE_VALUE => "<input type=\"file\" name=\"file$_\" size=\"30\" />" . $form_file[$_ - 1]
			);
		}

		print $skin_ins->get_data('file_foot');
	}

	if (-s $self->{init}->{data_option}) {
		my %option;
		if ($self->{query}->{work} eq 'edit' and -s "$self->{init}->{data_option_dir}$self->{query}->{edit}\.$self->{init}->{data_ext}") {
			open(FH, "$self->{init}->{data_option_dir}$self->{query}->{edit}\.$self->{init}->{data_ext}") or $self->error("Write Error : $self->{init}->{data_option_dir}$self->{query}->{edit}\.$self->{init}->{data_ext}");
			while (<FH>) {
				chomp;
				my($key, $value) = split(/\t/);

				$option{$key} = $value;
			}
			close(FH);
		}

		print $skin_ins->get_data('option_head');

		open(FH, $self->{init}->{data_option}) or $self->error("Read Error : $self->{init}->{data_option}");
		while (<FH>) {
			chomp;
			my($id, $name, $type, $default) = split(/\t/);

			my $value;
			if ($type eq 'text') {
				if (exists $option{"option_$id"}) {
					$default = $option{"option_$id"};
				}

				$value = "<input type=\"text\" name=\"option_$id\" size=\"50\" value=\"$default\" />";
			} elsif ($type eq 'textarea') {
				if (exists $option{"option_$id"}) {
					$default = $option{"option_$id"};
					$default =~ s/<>/\n/g;
				} elsif ($self->{query}->{work} ne 'edit') {
					$default =~ s/<br \/>/\n/g;
				} else {
					$default = '';
				}

				$value = "<textarea name=\"option_$id\" cols=\"60\" rows=\"3\">$default</textarea>";
			} elsif ($type eq 'select') {
				foreach my $data (split(/<br \/>/, $default)) {
					if (exists $option{"option_$id"} and $data eq $option{"option_$id"}) {
						$value .= "<option value=\"$data\" selected=\"selected\">$data</option>";
					} else {
						$value .= "<option value=\"$data\">$data</option>";
					}
				}
				$value = "<select name=\"option_$id\" xml:lang=\"ja\" lang=\"ja\">$value</select>";
			} elsif ($type eq 'checkbox') {
				my $i = 0;
				foreach my $data (split(/<br \/>/, $default)) {
					if (exists $option{"option_$id"} and $option{"option_$id"} =~ /(^|<>)$data(<>|$)/) {
						$value .= "<input type=\"checkbox\" name=\"option_$id\" id=\"${id}_checkbox_$i\" value=\"$data\" checked=\"checked\" /> <label for=\"${id}_checkbox_$i\">$data</label>\n";
					} else {
						$value .= "<input type=\"checkbox\" name=\"option_$id\" id=\"${id}_checkbox_$i\" value=\"$data\" /> <label for=\"${id}_checkbox_$i\">$data</label>\n";
					}
					$i++;
				}
			} elsif ($type eq 'option') {
				if (exists $option{"option_$id"}) {
					$default = $option{"option_$id"};
					$default =~ s/<>/\n/g;
				} elsif ($self->{query}->{work} ne 'edit') {
					$default =~ s/<br \/>/\n/g;
				} else {
					$default = '';
				}

				$value = "<textarea name=\"option_$id\" cols=\"30\" rows=\"5\">$default</textarea>";
			}

			print $skin_ins->get_replace_data(
				'option',
				OPTION_TITLE => $name,
				OPTION_VALUE => $value
			);
		}
		close(FH);

		print $skin_ins->get_data('option_foot');
	}

	print $skin_ins->get_replace_data(
		'form_foot',
		%form,
		FORM_LABEL => $form_label,
		FORM_WORK  => $self->{query}->{work},
		FORM_TBURL => $self->{query}->{tb_url},
		FORM_PING  => ' checked="checked"'
	);
	print $skin_ins->get_data('navi');
	print $skin_ins->get_data('footer');

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

### プレビュー表示
sub output_preview {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	my $date = "$self->{query}->{year}$self->{query}->{month}$self->{query}->{day}$self->{query}->{hour}$self->{query}->{minute}";

	my($new_field, $i);

	open(FH, $self->{init}->{data_field}) or $self->error("Read Error : $self->{init}->{data_field}");
	while (<FH>) {
		chomp;

		if ($self->{query}->{field} == ++$i) {
			$new_field = $_;
		}
	}
	close(FH);

	my(%file, %ext, $files, $image, $flag);

	foreach (1 .. $self->{config}->{max_file}) {
		if ($self->{query}->{work} eq 'edit' or $self->{query}->{image} or $self->{query}->{'file' . $_} or $self->{query}->{'ext' . $_}) {
			$flag = 1;
		}
	}

	if ($flag) {
		my($org_image, $org_files);

		if ($self->{query}->{work} eq 'edit') {
			my $edit_file;
			if ($date =~ /^(\d\d\d\d)(\d\d)(\d\d)(\d\d)(\d\d)$/) {
				$edit_file = "$self->{init}->{data_catalog_dir}$1$2\.$self->{init}->{data_ext}";
			}

			open(FH, $edit_file) or $self->error("Read Error : $edit_file");
			while (<FH>) {
				chomp;
				my($no, $id, $stat, $break, $view, $comt, $tb, $field, $date, $name, $subj, $text, $price, $icon, $image, $file, $host) = split(/\t/);

				if ($self->{query}->{edit} == $no) {
					$org_image = $image;
					$org_files = $file;

					last;
				}
			}
			close(FH);
		}

		if ($self->{query}->{image}) {
			$image = $self->{init}->{data_tmp_file};

			open(FH, ">$self->{init}->{data_image_dir}$self->{init}->{data_tmp_file}" . $self->get_user) or $self->error("Write Error : $self->{init}->{data_image_dir}$self->{init}->{data_tmp_file}" . $self->get_user);
			binmode(FH);
			print FH $self->{query}->{image}->{file_data};
			close(FH);
		} elsif ($self->{query}->{image_ext}) {
			$image = $self->{init}->{data_tmp_file};
		} elsif ($org_image) {
			$image = $org_image;
		}

		foreach (1 .. $self->{config}->{max_file}) {
			my @org_files = split(/<>/, $org_files);

			if ($self->{query}->{'file' . $_}) {
				my $file_ins = new webliberty::File($self->{query}->{'file' . $_}->{file_name});
				$file{$_} = $self->{init}->{data_tmp_file};
				$ext{$_}  = $file_ins->get_ext;

				open(FH, ">$self->{init}->{data_upfile_dir}$self->{init}->{data_tmp_file}" . $self->get_user . $_) or $self->error("Write Error : $self->{init}->{data_upfile_dir}$self->{init}->{data_tmp_file}" . $self->get_user . $_);
				binmode(FH);
				print FH $self->{query}->{'file' . $_}->{file_data};
				close(FH);
			} elsif ($self->{query}->{'ext' . $_}) {
				$file{$_} = $self->{init}->{data_tmp_file};
				$ext{$_}  = $self->{query}->{'ext' . $_};
			} elsif ($org_files[$_ - 1]) {
				$file{$_} = $org_files[$_ - 1];
			}
		}

		foreach (1 .. $self->{config}->{max_file}) {
			if ($_ > 1) {
				$files .= '<>';
			}
			$files .= "$file{$_}";
		}
	}

	my $form_ping;
	if ($self->{query}->{ping}) {
		$form_ping  = ' checked="checked"';
	} else {
		$form_ping  = '';
	}

	if (!$self->{message}) {
		$self->{message} = 'この内容で登録します。よろしければ<em>登録ボタン</em>を押してください。';
	}

	my $skin_ins = new webliberty::Skin;
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_header}", available => 'header');
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_work}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_form}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_view}", available => 'catalog_head,catalog,catalog_delimiter,catalog_foot');
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_navi}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_footer}", available => 'footer');

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});

	$skin_ins->replace_skin(
		$catalog_ins->info,
		%{$self->{plugin}},
		INFO_MESSAGE => $self->{message}
	);

	my $name_ins = new webliberty::String($self->{query}->{name});
	my $subj_ins = new webliberty::String($self->{query}->{subj});
	my $text_ins = new webliberty::String($self->{query}->{text});

	$name_ins->create_line;
	$subj_ins->create_line;
	$text_ins->create_text;

	my %form = $catalog_ins->catalog_form($self->{query}->{edit}, $self->{query}->{id}, $self->{query}->{stat}, $self->{query}->{break}, $self->{query}->{view}, $self->{query}->{comt}, $self->{query}->{tb}, $self->{query}->{field}, $date, $name_ins->get_string, $subj_ins->get_string, $text_ins->get_string, $self->{query}->{price}, $self->{query}->{icon}, $image, $files, ''),

	my $init_ins = new webliberty::App::Init;
	my %label = %{$init_ins->get_label};

	print $self->header;
	print $skin_ins->get_data('header');
	print $skin_ins->get_data('work_head');
	print $self->work_navi($skin_ins);
	print $skin_ins->get_data('work_foot');
	print $skin_ins->get_replace_data(
		'form_head',
		%form,
		FORM_LABEL => 'プレビュー',
		FORM_WORK  => $self->{query}->{work},
		FORM_TBURL => $self->{query}->{tb_url},
		FORM_PING  => $form_ping
	);

	if ($self->{config}->{use_file}) {
		my @form_file = split(/<>/, $files);

		print $skin_ins->get_data('file_head');

		foreach (1 .. $self->{config}->{max_file}) {
			if ($form_file[$_ - 1]) {
				my $check;
				if ($self->{query}->{'delfile' . $_}) {
					$check = ' checked="checked"';
				}
				if ($form_file[$_ - 1]) {
					$form_file[$_ - 1] = "<input type=\"checkbox\" name=\"delfile$_\" id=\"delfile${_}_checkbox\" value=\"on\"$check /> <label for=\"delfile${_}_checkbox\">" . $form_file[$_ - 1] . "を削除</label>";
				}
			}

			print $skin_ins->get_replace_data(
				'file',
				FILE_TITLE => $label{'pc_file'} . $_,
				FILE_VALUE => "<input type=\"file\" name=\"file$_\" size=\"30\" />" . $form_file[$_ - 1] . "<input type=\"hidden\" name=\"ext$_\" value=\"$ext{$_}\" />"
			);
		}

		print $skin_ins->get_data('file_foot');
	}

	if (-s $self->{init}->{data_option}) {
		print $skin_ins->get_data('option_head');

		open(FH, $self->{init}->{data_option}) or $self->error("Read Error : $self->{init}->{data_option}");
		while (<FH>) {
			chomp;
			my($id, $name, $type, $default) = split(/\t/);

			my $value;
			if ($type eq 'text') {
				if ($self->{query}->{'option_' . $id}) {
					$default = $self->{query}->{'option_' . $id};
				} else {
					$default = '';
				}

				$value = "<input type=\"text\" name=\"option_$id\" size=\"50\" value=\"$default\" />";
			} elsif ($type eq 'textarea') {
				if ($self->{query}->{'option_' . $id}) {
					$default = $self->{query}->{'option_' . $id};
					$default =~ s/<br \/>/\n/g;
				} else {
					$default = '';
				}

				$value = "<textarea name=\"option_$id\" cols=\"60\" rows=\"3\">$default</textarea>";
			} elsif ($type eq 'select') {
				foreach my $data (split(/<br \/>/, $default)) {
					if ($self->{query}->{'option_' . $id} and $data eq $self->{query}->{'option_' . $id}) {
						$value .= "<option value=\"$data\" selected=\"selected\">$data</option>";
					} else {
						$value .= "<option value=\"$data\">$data</option>";
					}
				}
				$value = "<select name=\"option_$id\" xml:lang=\"ja\" lang=\"ja\">$value</select>";
			} elsif ($type eq 'checkbox') {
				my $i = 0;
				foreach my $data (split(/<br \/>/, $default)) {
					if ($self->{query}->{'option_' . $id} and $self->{query}->{'option_' . $id} =~ /(^|\n)$data(\n|$)/) {
						$value .= "<input type=\"checkbox\" name=\"option_$id\" id=\"${id}_checkbox_$i\" value=\"$data\" checked=\"checked\" /> <label for=\"${id}_checkbox_$i\">$data</label>\n";
					} else {
						$value .= "<input type=\"checkbox\" name=\"option_$id\" id=\"${id}_checkbox_$i\" value=\"$data\" /> <label for=\"${id}_checkbox_$i\">$data</label>\n";
					}
					$i++;
				}
			} elsif ($type eq 'option') {
				if ($self->{query}->{'option_' . $id}) {
					$default = $self->{query}->{'option_' . $id};
					$default =~ s/<br \/>/\n/g;
				} else {
					$default = '';
				}

				$value = "<textarea name=\"option_$id\" cols=\"30\" rows=\"5\">$default</textarea>";
			}

			print $skin_ins->get_replace_data(
				'option',
				OPTION_TITLE => $name,
				OPTION_VALUE => $value
			);
		}
		close(FH);

		print $skin_ins->get_data('option_foot');
	}

	print $skin_ins->get_replace_data(
		'form_foot',
		%form,
		FORM_LABEL => 'プレビュー',
		FORM_WORK  => $self->{query}->{work},
		FORM_TBURL => $self->{query}->{tb_url},
		FORM_PING  => $form_ping
	);
	print $skin_ins->get_data('catalog_head');
	print $skin_ins->get_replace_data(
		'catalog',
		$catalog_ins->catalog_article(0, '', $self->{query}->{stat}, $self->{query}->{break}, $self->{query}->{view}, $self->{query}->{comt}, $self->{query}->{tb}, $new_field, $date, '', $subj_ins->get_string, $text_ins->get_string, $self->{query}->{price}, $self->{query}->{icon}, '', '', '')
	);
	print $skin_ins->get_data('catalog_delimiter');
	print $skin_ins->get_data('catalog_foot');
	print $skin_ins->get_data('navi');
	print $skin_ins->get_data('footer');

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

### 登録商品一覧表示
sub output_edit {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	if (!$self->{message}) {
		$self->{message} = '商品を選択し、<em>編集ボタン</em>か<em>削除ボタン</em>を押してください。';
	}

	my $skin_ins = new webliberty::Skin;
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_header}", available => 'header');
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_work}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_edit}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_navi}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_footer}", available => 'footer');

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});

	$skin_ins->replace_skin(
		$catalog_ins->info,
		%{$self->{plugin}},
		INFO_MESSAGE => $self->{message}
	);

	$self->{html}->{header}    = $skin_ins->get_data('header');
	$self->{html}->{work_head} = $skin_ins->get_data('work_head');
	$self->{html}->{work}      = $self->work_navi($skin_ins);
	$self->{html}->{work_foot} = $skin_ins->get_data('work_foot');

	my($cond_and, $cond_or);
	if ($self->{query}->{cond} eq 'or') {
		$cond_and = '';
		$cond_or  = ' selected="selected"';
	} else {
		$cond_and = ' selected="selected"';
		$cond_or  = '';
	}

	$self->{html}->{contents} = $skin_ins->get_replace_data(
		'contents',
		FORM_WORD     => $self->{query}->{word},
		FORM_COND_AND => $cond_and,
		FORM_COND_OR  => $cond_or
	);

	my @words;
	if ($self->{query}->{word}) {
		my $words = $self->{query}->{word};
		$words =~ s/　/ /g;
		@words = split(/\s+/, $words);
	}

	my($index_size, $index_date, $index_no);

	open(FH, $self->{init}->{data_catalog_index}) or $self->error("Read Error : $self->{init}->{data_catalog_index}");
	while (<FH>) {
		chomp;
		my($date, $no, $id, $stat, $field, $name) = split(/\t/);

		if ($index_size == $self->{query}->{page} * $self->{config}->{admin_size}) {
			$index_date = $date;
			$index_no   = $no;
		}

		$index_size++;
	}
	close(FH);

	my $start_file;
	if ($index_date =~ /^(\d\d\d\d)(\d\d)(\d\d)(\d\d)(\d\d)$/) {
		$start_file = "$self->{init}->{data_catalog_dir}$1$2\.$self->{init}->{data_ext}";
	}

	opendir(DIR, $self->{init}->{data_catalog_dir}) or $self->error("Read Error : $self->{init}->{data_catalog_dir}");
	my @dir = sort { $b <=> $a } readdir(DIR);
	closedir(DIR);

	my($dir_flag, $file_flag, $i);

	$self->{html}->{catalog_head} = $skin_ins->get_data('catalog_head');
	foreach my $entry (@dir) {
		if ($entry !~ /^\d\d\d\d\d\d\.$self->{init}->{data_ext}$/) {
			next;
		}
		if ($start_file eq "$self->{init}->{data_catalog_dir}$entry" or $self->{query}->{word}) {
			$dir_flag = 1;
		}
		if ($dir_flag) {
			open(FH, "$self->{init}->{data_catalog_dir}$entry") or $self->error("Read Error : $self->{init}->{data_catalog_dir}$entry");
			while (<FH>) {
				chomp;
				my($no, $id, $stat, $break, $view, $comt, $tb, $field, $date, $name, $subj, $text, $price, $icon, $image, $file, $host) = split(/\t/);

				if ($no == $index_no or $self->{query}->{word}) {
					$file_flag = 1;
				}
				if ($file_flag) {
					if ($self->{query}->{word}) {
						my $options;
						if (open(OPTION, "$self->{init}->{data_option_dir}$no\.$self->{init}->{data_ext}")) {
							while (my $option = <OPTION>) {
								chomp;
								my($key, $value) = split(/\t/, $option);

								$options .= "\t$value";
							}
							close(OPTION);
						}

						my $flag;
						foreach my $word (@words) {
							$word = lc($word);
							my $string = lc("$_$options");

							if (index($string, $word) >= 0) {
								$flag = 1;
								if ($self->{query}->{cond} eq 'or') {
									last;
								}
							} else {
								if ($self->{query}->{cond} eq 'and') {
									$flag = 0;

									last;
								}
							}
						}
						if (!$flag) {
							next;
						}

						$i++;
						if ($i <= $self->{query}->{page} * $self->{config}->{admin_size}) {
							next;
						} elsif ($i > $self->{query}->{page} * $self->{config}->{admin_size} + $self->{config}->{admin_size}) {
							next;
						}
					} else {
						$i++;
						if ($i > $self->{config}->{admin_size}) {
							$file_flag = 0;
							last;
						}
					}

					$self->{html}->{catalog} .= $skin_ins->get_replace_data(
						'catalog',
						$catalog_ins->catalog_article($no, $id, $stat, $break, $view, $comt, $tb, $field, $date, $name, $subj, $text, $price, $icon, $image, $file, $host)
					);
				}
			}
			close(FH);
		}
	}
	$self->{html}->{catalog_foot} = $skin_ins->get_data('catalog_foot');

	if ($self->{query}->{word}) {
		$index_size = $i;
	}

	my $page_list;
	foreach (0 .. int(($index_size - 1) / $self->{config}->{admin_size})) {
		if ($_ == $self->{query}->{page}) {
			$page_list .= "<option value=\"$_\" selected=\"selected\">ページ" . ($_ + 1) . "</option>";
		} else {
			$page_list .= "<option value=\"$_\">ページ" . ($_ + 1) . "</option>";
		}
	}
	$self->{html}->{page} = $skin_ins->get_replace_data(
		'page',
		FORM_WORD => $self->{query}->{word},
		FORM_COND => $self->{query}->{cond},
		PAGE_LIST => $page_list
	);

	$self->{html}->{navi}   = $skin_ins->get_data('navi');
	$self->{html}->{footer} = $skin_ins->get_data('footer');

	print $self->header;
	foreach ($skin_ins->get_list) {
		print $self->{html}->{$_};
	}

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

### コメント一覧表示
sub output_comment {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	if ($self->get_authority ne 'root' and !$self->{config}->{auth_comment}) {
		$self->error('この操作を行う権限が与えられていません。');
	}

	if (!$self->{message}) {
		$self->{message} = 'コメントを選択し、<em>編集ボタン</em>か<em>削除ボタン</em>を押してください。';
	}

	my $skin_ins = new webliberty::Skin;
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_header}", available => 'header');
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_work}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_comment}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_navi}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_footer}", available => 'footer');

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});

	$skin_ins->replace_skin(
		$catalog_ins->info,
		%{$self->{plugin}},
		INFO_MESSAGE => $self->{message}
	);

	$self->{html}->{header}    = $skin_ins->get_data('header');
	$self->{html}->{work_head} = $skin_ins->get_data('work_head');
	$self->{html}->{work}      = $self->work_navi($skin_ins);
	$self->{html}->{work_foot} = $skin_ins->get_data('work_foot');
	$self->{html}->{contents}  = $skin_ins->get_data('contents');

	$self->{html}->{catalog_head} = $skin_ins->get_data('catalog_head');

	my($index_size, $i);

	my $comt_start = $self->{config}->{admin_size} * $self->{query}->{page};
	my $comt_end   = $comt_start + $self->{config}->{admin_size};

	open(FH, $self->{init}->{data_comt_index}) or $self->error("Read Error : $self->{init}->{data_comt_index}");
	while (<FH>) {
		$index_size++;
	}
	seek(FH, 0, 0);
	while (<FH>) {
		chomp;
		my($no, $pno, $stat, $date, $name, $subj, $host) = split(/\t/);

		$i++;
		if ($i <= $comt_start) {
			next;
		} elsif ($i > $comt_end) {
			last;
		}

		$self->{html}->{comment} .= $skin_ins->get_replace_data(
			'comment',
			$catalog_ins->comment_article($no, $pno, $stat, $date, $name, '', '', $subj, '', '', '', $host)
		);
	}
	close(FH);

	$self->{html}->{catalog_foot} = $skin_ins->get_data('catalog_foot');

	my $page_list;
	foreach (0 .. int(($index_size - 1) / $self->{config}->{admin_size})) {
		if ($_ == $self->{query}->{page}) {
			$page_list .= "<option value=\"$_\" selected=\"selected\">ページ" . ($_ + 1) . "</option>";
		} else {
			$page_list .= "<option value=\"$_\">ページ" . ($_ + 1) . "</option>";
		}
	}
	$self->{html}->{page} = $skin_ins->get_replace_data(
		'page',
		PAGE_LIST => $page_list
	);

	$self->{html}->{navi}   = $skin_ins->get_data('navi');
	$self->{html}->{footer} = $skin_ins->get_data('footer');

	print $self->header;
	foreach ($skin_ins->get_list) {
		print $self->{html}->{$_};
	}

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

### トラックバック一覧表示
sub output_trackback {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	if ($self->get_authority ne 'root' and !$self->{config}->{auth_trackback}) {
		$self->error('この操作を行う権限が与えられていません。');
	}

	if (!$self->{message}) {
		$self->{message} = 'トラックバックを選択し、<em>削除ボタン</em>を押してください。';
	}

	my $skin_ins = new webliberty::Skin;
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_header}", available => 'header');
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_work}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_trackback}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_navi}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_footer}", available => 'footer');

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});

	$skin_ins->replace_skin(
		$catalog_ins->info,
		%{$self->{plugin}},
		INFO_MESSAGE => $self->{message}
	);

	$self->{html}->{header}    = $skin_ins->get_data('header');
	$self->{html}->{work_head} = $skin_ins->get_data('work_head');
	$self->{html}->{work}      = $self->work_navi($skin_ins);
	$self->{html}->{work_foot} = $skin_ins->get_data('work_foot');
	$self->{html}->{contents}  = $skin_ins->get_data('contents');

	$self->{html}->{trackback_head} = $skin_ins->get_data('trackback_head');

	my($index_size, $i);

	my $tb_start = $self->{config}->{admin_size} * $self->{query}->{page};
	my $tb_end   = $tb_start + $self->{config}->{admin_size};

	open(FH, $self->{init}->{data_tb_index}) or $self->error("Read Error : $self->{init}->{data_tb_index}");
	while (<FH>) {
		$index_size++;
	}
	seek(FH, 0, 0);
	while (<FH>) {
		chomp;
		my($no, $pno, $stat, $date, $blog, $title, $url) = split(/\t/);

		$i++;
		if ($i <= $tb_start) {
			next;
		} elsif ($i > $tb_end) {
			last;
		}

		$self->{html}->{trackback} .= $skin_ins->get_replace_data(
			'trackback',
			$catalog_ins->trackback_article($no, $pno, $stat, $date, $blog, $title, $url, '')
		);
	}
	close(FH);

	$self->{html}->{trackback_foot} = $skin_ins->get_data('trackback_foot');

	my $page_list;
	foreach (0 .. int(($index_size - 1) / $self->{config}->{admin_size})) {
		if ($_ == $self->{query}->{page}) {
			$page_list .= "<option value=\"$_\" selected=\"selected\">ページ" . ($_ + 1) . "</option>";
		} else {
			$page_list .= "<option value=\"$_\">ページ" . ($_ + 1) . "</option>";
		}
	}
	$self->{html}->{page} = $skin_ins->get_replace_data(
		'page',
		PAGE_LIST => $page_list
	);

	$self->{html}->{navi}   = $skin_ins->get_data('navi');
	$self->{html}->{footer} = $skin_ins->get_data('footer');

	print $self->header;
	foreach ($skin_ins->get_list) {
		print $self->{html}->{$_};
	}

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

### 顧客情報編集フォーム表示
sub output_modify {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	if ($self->get_authority ne 'root' and !$self->{config}->{auth_customer}) {
		$self->error('この操作を行う権限が与えられていません。');
	}

	if (!$self->{query}->{edit}) {
		$self->error('編集したい顧客を選択してください。');
	}

	my $skin_ins = new webliberty::Skin;
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_header}", available => 'header');
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_work}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_modify}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_navi}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_footer}", available => 'footer');

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});

	$skin_ins->replace_skin(
		$catalog_ins->info,
		%{$self->{plugin}},
		INFO_MESSAGE => $self->{message}
	);

	$self->{html}->{header}    = $skin_ins->get_data('header');
	$self->{html}->{work_head} = $skin_ins->get_data('work_head');
	$self->{html}->{work}      = $self->work_navi($skin_ins);
	$self->{html}->{work_foot} = $skin_ins->get_data('work_foot');

	my $flag;

	open(FH, "$self->{init}->{data_customer}") or $self->error("Read Error : $self->{init}->{data_customer}");
	while (<FH>) {
		chomp;
		my($data_date, $data_stat, $data_point, $data_payment, $data_delivery, $data_user_pwd, $data_order_name, $data_order_kana, $data_order_mail, $data_order_post, $data_order_pref, $data_order_addr, $data_order_phone, $data_order_fax, $data_send_name, $data_send_kana, $data_send_post, $data_send_pref, $data_send_addr, $data_send_phone, $data_send_fax) = split(/\t/);

		if ($self->{query}->{edit} eq $data_order_mail) {
			my($payment, $delivery, $delivery_start, $delivery_end, $order_pref, $send_pref, $point_start, $point_end);

			foreach (split(/<>/, $self->{config}->{order_payment})) {
				my($name, $fee) = split(/,/);

				if ($data_payment eq $name) {
					$payment .= "<option value=\"$name\" selected=\"selected\">$name</option>";
				} else {
					$payment .= "<option value=\"$name\">$name</option>";
				}
			}

			if ($self->{config}->{order_delivery}) {
				foreach (split(/<>/, $self->{config}->{order_delivery})) {
					if ($data_delivery eq $_) {
						$delivery .= "<option value=\"$_\" selected=\"selected\">$_</option>";
					} else {
						$delivery .= "<option value=\"$_\">$_</option>";
					}
				}
			} else {
				$delivery_start = '<!--';
				$delivery_end   = '-->';
			}

			foreach (split(/<>/, $self->{config}->{order_pref})) {
				my($name, $fee) = split(/,/);

				if ($data_order_pref eq $name) {
					$order_pref .= "<option value=\"$name\" selected=\"selected\">$name</option>";
				} else {
					$order_pref .= "<option value=\"$name\">$name</option>";
				}
				if ($data_send_pref eq $name) {
					$send_pref .= "<option value=\"$name\" selected=\"selected\">$name</option>";
				} else {
					$send_pref .= "<option value=\"$name\">$name</option>";
				}
			}

			if (!$self->{config}->{point_mode}) {
				$point_start = '<!--';
				$point_end   = '-->';
			}

			$self->{html}->{form} = $skin_ins->get_replace_data(
				'form',
				FORM_LOGIN_MAIL => $self->{query}->{edit},

				FORM_PAYMENT        => $payment,
				FORM_DELIVERY       => $delivery,
				FORM_DELIVERY_START => $delivery_start,
				FORM_DELIVERY_END   => $delivery_end,

				FORM_ORDER_NAME  => $data_order_name,
				FORM_ORDER_KANA  => $data_order_kana,
				FORM_ORDER_MAIL  => $data_order_mail,
				FORM_ORDER_POST  => $data_order_post,
				FORM_ORDER_PREF  => $order_pref,
				FORM_ORDER_ADDR  => $data_order_addr,
				FORM_ORDER_PHONE => $data_order_phone,
				FORM_ORDER_FAX   => $data_order_fax,

				FORM_SEND_NAME  => $data_send_name,
				FORM_SEND_KANA  => $data_send_kana,
				FORM_SEND_POST  => $data_send_post,
				FORM_SEND_PREF  => $send_pref,
				FORM_SEND_ADDR  => $data_send_addr,
				FORM_SEND_PHONE => $data_send_phone,
				FORM_SEND_FAX   => $data_send_fax,

				FORM_POINT       => $data_point,
				FORM_POINT_START => $point_start,
				FORM_POINT_END   => $point_end
			);

			$flag = 1;

			last;
		}
	}
	close(FH);

	if (!$flag) {
		$self->error('指定されたお客様情報は存在しません。');
	}

	$self->{html}->{navi}   = $skin_ins->get_data('navi');
	$self->{html}->{footer} = $skin_ins->get_data('footer');

	print $self->header;
	foreach ($skin_ins->get_list) {
		print $self->{html}->{$_};
	}

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

### 顧客一覧表示
sub output_customer {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	if ($self->get_authority ne 'root' and !$self->{config}->{auth_customer}) {
		$self->error('この操作を行う権限が与えられていません。');
	}

	if (!$self->{message}) {
		$self->{message} = '顧客を選択し、<em>編集ボタン</em>か<em>削除ボタン</em>を押してください。';
	}

	my $skin_ins = new webliberty::Skin;
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_header}", available => 'header');
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_work}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_customer}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_navi}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_footer}", available => 'footer');

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});

	$skin_ins->replace_skin(
		$catalog_ins->info,
		%{$self->{plugin}},
		INFO_MESSAGE => $self->{message}
	);

	$self->{html}->{header}    = $skin_ins->get_data('header');
	$self->{html}->{work_head} = $skin_ins->get_data('work_head');
	$self->{html}->{work}      = $self->work_navi($skin_ins);
	$self->{html}->{work_foot} = $skin_ins->get_data('work_foot');

	my($cond_and, $cond_or);
	if ($self->{query}->{cond} eq 'or') {
		$cond_and = '';
		$cond_or  = ' selected="selected"';
	} else {
		$cond_and = ' selected="selected"';
		$cond_or  = '';
	}

	$self->{html}->{contents} = $skin_ins->get_replace_data(
		'contents',
		FORM_WORD     => $self->{query}->{word},
		FORM_COND_AND => $cond_and,
		FORM_COND_OR  => $cond_or
	);

	my @words;
	if ($self->{query}->{word}) {
		my $words = $self->{query}->{word};
		$words =~ s/　/ /g;
		@words = split(/\s+/, $words);
	}

	$self->{html}->{customer_head} = $skin_ins->get_data('customer_head');

	my($data_size, $i);

	my $customer_start = $self->{config}->{admin_size} * $self->{query}->{page};
	my $customer_end   = $customer_start + $self->{config}->{admin_size};

	open(FH, $self->{init}->{data_customer}) or $self->error("Read Error : $self->{init}->{data_customer}");
	while (<FH>) {
		$data_size++;
	}
	seek(FH, 0, 0);
	while (<FH>) {
		chomp;
		my($date, $stat, $point, $payment, $delivery, $user_pwd, $order_name, $order_kana, $order_mail, $order_post, $order_pref, $order_addr, $order_phone, $order_fax, $send_name, $send_kana, $send_post, $send_pref, $send_addr, $send_phone, $send_fax, $host) = split(/\t/);

		if ($self->{query}->{word}) {
			my $flag;
			foreach my $word (@words) {
				$word = lc($word);
				my $string = lc($_);

				if (index($string, $word) >= 0) {
					$flag = 1;
					if ($self->{query}->{cond} eq 'or') {
						last;
					}
				} else {
					if ($self->{query}->{cond} eq 'and') {
						$flag = 0;

						last;
					}
				}
			}
			if (!$flag) {
				next;
			}

			$i++;
			if ($i <= $customer_start) {
				next;
			} elsif ($i > $customer_end) {
				next;
			}
		} else {
			$i++;
			if ($i <= $customer_start) {
				next;
			} elsif ($i > $customer_end) {
				last;
			}
		}

		my($sec, $min, $hour, $day, $mon, $year, $week) = localtime($date);

		my $customer_year   = sprintf("%04d", $year + 1900);
		my $customer_month  = sprintf("%02d", $mon + 1);
		my $customer_day    = sprintf("%02d", $day);
		my $customer_hour   = sprintf("%02d", $hour);
		my $customer_minute = sprintf("%02d", $min);
		my $customer_week   = ${$self->{init}->{weeks}}[$week];

		if ($stat == 1) {
			$stat = '承認';
		} else {
			$stat = '未承認';
		}

		my $mail_ins = new webliberty::Encoder($order_mail);

		$self->{html}->{customer} .= $skin_ins->get_replace_data(
			'customer',
			CUSTOMER_CODE     => $mail_ins->url_encode,
			CUSTOMER_YEAR     => $customer_year,
			CUSTOMER_MONTH    => $customer_month,
			CUSTOMER_DAY      => $customer_day,
			CUSTOMER_HOUR     => $customer_hour,
			CUSTOMER_MINUTE   => $customer_minute,
			CUSTOMER_WEEK     => $customer_week,
			CUSTOMER_STAT     => $stat,
			CUSTOMER_POINT    => $point,
			CUSTOMER_PAYMENT  => $payment,
			CUSTOMER_DELIVERY => $delivery,
			CUSTOMER_NAME     => $order_name,
			CUSTOMER_KANA     => $order_kana,
			CUSTOMER_MAIL     => $order_mail,
			CUSTOMER_POST     => $order_post,
			CUSTOMER_PREF     => $order_pref,
			CUSTOMER_ADDR     => $order_addr,
			CUSTOMER_PHONE    => $order_phone,
			CUSTOMER_FAX      => $order_fax,
			CUSTOMER_HOST     => $host
		);
	}
	close(FH);

	if ($self->{query}->{word}) {
		$data_size = $i;
	}

	$self->{html}->{customer_foot} = $skin_ins->get_data('customer_foot');

	my $page_list;
	foreach (0 .. int(($data_size - 1) / $self->{config}->{admin_size})) {
		if ($_ == $self->{query}->{page}) {
			$page_list .= "<option value=\"$_\" selected=\"selected\">ページ" . ($_ + 1) . "</option>";
		} else {
			$page_list .= "<option value=\"$_\">ページ" . ($_ + 1) . "</option>";
		}
	}
	$self->{html}->{page} = $skin_ins->get_replace_data(
		'page',
		FORM_WORD => $self->{query}->{word},
		FORM_COND => $self->{query}->{cond},
		PAGE_LIST => $page_list
	);

	$self->{html}->{navi}   = $skin_ins->get_data('navi');
	$self->{html}->{footer} = $skin_ins->get_data('footer');

	print $self->header;
	foreach ($skin_ins->get_list) {
		print $self->{html}->{$_};
	}

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

### 処理内容確認表示
sub output_confirm {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	my($info_heading, $info_message);
	if ($self->{query}->{work} eq 'edit') {
		$info_heading = '商品削除';
		$info_message = '商品';
	} elsif ($self->{query}->{work} eq 'comment') {
		if ($self->{query}->{del}) {
			$info_heading = 'コメント削除';
		} else {
			$info_heading = 'コメント承認';
		}
		$info_message = 'コメント';
	} elsif ($self->{query}->{work} eq 'trackback') {
		if ($self->{query}->{del}) {
			$info_heading = 'トラックバック削除';
		} else {
			$info_heading = 'トラックバック承認';
		}
		$info_message = 'トラックバック';
	} elsif ($self->{query}->{work} eq 'customer') {
		if ($self->{query}->{del}) {
			$info_heading = '顧客削除';
		} else {
			$info_heading = '顧客承認';
		}
		$info_message = '顧客';
	} elsif ($self->{query}->{work} eq 'env') {
		$info_heading = '環境設定';
	}

	if ($self->{query}->{work} ne 'env' and !$self->{query}->{del} and !$self->{query}->{stat}) {
		$self->error("作業対象を選択してください。");
	}

	if (!$self->{message}) {
		if ($self->{query}->{work} eq 'env') {
			$self->{message} = "環境設定内容を初期値に戻します。よろしければ、<em>実行ボタン</em>を押してください。";
		} elsif ($self->{query}->{del}) {
			if ($self->{query}->{work} eq 'edit') {
				my $flag;

				open(FH, $self->{init}->{data_catalog_index}) or $self->error("Read Error : $self->{init}->{data_catalog_index}");
				while (<FH>) {
					chomp;
					my($date, $no, $id, $stat, $field, $name) = split(/\t/);

					if ($self->{query}->{del} =~ /(^|\n)$no(\n|$)/) {
						if ($self->get_authority ne 'root' and $self->get_user ne $name) {
							$flag = 1;

							last;
						}
					}
				}
				close(FH);

				if ($flag) {
					$self->error('他のユーザーの商品は削除できません。');
				}
			}

			my $del_list;
			foreach (split(/\n/, $self->{query}->{del})) {
				if ($del_list) {
					$del_list .= '、';
				}
				if ($self->{query}->{work} eq 'customer') {
					$del_list .= "$_";
				} else {
					$del_list .= "No.$_";
				}
			}

			$self->{message} = "$del_listの$info_messageを削除します。よろしければ、<em>実行ボタン</em>を押してください。";
		} else {
			my $flag;

			if ($self->{query}->{work} eq 'comment') {
				open(FH, "$self->{init}->{data_comt_dir}$self->{query}->{pno}\.$self->{init}->{data_ext}") or $self->error("Read Error : $self->{init}->{data_comt_dir}$self->{query}->{pno}\.$self->{init}->{data_ext}");
				while (<FH>) {
					chomp;
					my($no, $pno, $stat, $date, $name, $mail, $url, $subj, $text, $rate, $pwd, $host) = split(/\t/);

					if ($self->{query}->{stat} == $no) {
						if ($stat) {
							$flag = 1;
						}
					}
				}
				close(FH);
			} elsif ($self->{query}->{work} eq 'trackback') {
				open(FH, "$self->{init}->{data_tb_dir}$self->{query}->{pno}\.$self->{init}->{data_ext}") or $self->error("Read Error : $self->{init}->{data_tb_dir}$self->{query}->{pno}\.$self->{init}->{data_ext}");
				while (<FH>) {
					chomp;
					my($no, $pno, $stat, $date, $blog, $title, $url, $excerpt) = split(/\t/);

					if ($self->{query}->{stat} == $no) {
						if ($stat) {
							$flag = 1;
						}

						last;
					}
				}
				close(FH);
			} else {
				open(FH, "$self->{init}->{data_customer}") or $self->error("Read Error : $self->{init}->{data_customer}");
				while (<FH>) {
					chomp;
					my($date, $stat, $point, $payment, $delivery, $user_pwd, $order_name, $order_kana, $order_mail, $order_post, $order_pref, $order_addr, $order_phone, $order_fax, $send_name, $send_kana, $send_post, $send_pref, $send_addr, $send_phone, $send_fax, $host) = split(/\t/);

					if ($self->{query}->{stat} eq $order_mail) {
						if ($stat) {
							$flag = 1;
						}

						last;
					}
				}
				close(FH);
			}

			if ($flag) {
				$self->{message} = "No.$self->{query}->{stat}の$info_messageを未承認にします。よろしければ、<em>実行ボタン</em>を押してください。";
			} else {
				$self->{message} = "No.$self->{query}->{stat}の$info_messageを承認します。よろしければ、<em>実行ボタン</em>を押してください。";
			}
		}
	}

	my $skin_ins = new webliberty::Skin;
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_header}", available => 'header');
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_work}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_confirm}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_navi}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_footer}", available => 'footer');

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});

	my $confirm_data;
	if ($self->{query}->{work} eq 'env') {
		$confirm_data = "<input type=\"hidden\" name=\"exec_default\" value=\"on\" />";
	} elsif ($self->{query}->{del}) {
		$confirm_data = "<input type=\"hidden\" name=\"exec_del\" value=\"on\" />";

		foreach (split(/\n/, $self->{query}->{del})) {
			$confirm_data .= "<input type=\"hidden\" name=\"del\" value=\"$_\" />";
		}
	} else {
		$confirm_data  = "<input type=\"hidden\" name=\"exec_stat\" value=\"on\" />";
		$confirm_data .= "<input type=\"hidden\" name=\"stat\" value=\"$self->{query}->{stat}\" />";
		$confirm_data .= "<input type=\"hidden\" name=\"pno\" value=\"$self->{query}->{pno}\" />";
	}

	$skin_ins->replace_skin(
		$catalog_ins->info,
		%{$self->{plugin}},
		INFO_MESSAGE => $self->{message}
	);

	$self->{html}->{header}    = $skin_ins->get_data('header');
	$self->{html}->{work_head} = $skin_ins->get_data('work_head');
	$self->{html}->{work}      = $self->work_navi($skin_ins);
	$self->{html}->{work_foot} = $skin_ins->get_data('work_foot');

	$self->{html}->{contents} = $skin_ins->get_replace_data(
		'contents',
		CONFIRM_HEADING => $info_heading,
		CONFIRM_WORK    => $self->{query}->{work},
		CONFIRM_DATA    => $confirm_data
	);

	$self->{html}->{navi}   = $skin_ins->get_data('navi');
	$self->{html}->{footer} = $skin_ins->get_data('footer');

	print $self->header;
	foreach ($skin_ins->get_list) {
		print $self->{html}->{$_};
	}

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

### データ確認
sub output_view {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	if (!$self->{query}->{no}) {
		if ($self->{query}->{work} eq 'comment') {
			$self->error('表示したいコメントを選択してください。');
		} else {
			$self->error('表示したいトラックバックを選択してください。');
		}
	}

	my $skin_ins = new webliberty::Skin;
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_header}", available => 'header');
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_work}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_view}", available => 'comment_head,comment,comment_foot,trackback_head,trackback,trackback_foot');
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_navi}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_footer}", available => 'footer');

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});

	$skin_ins->replace_skin(
		$catalog_ins->info,
		%{$self->{plugin}},
		INFO_MESSAGE => $self->{message}
	);

	$self->{html}->{header}    = $skin_ins->get_data('header');
	$self->{html}->{work_head} = $skin_ins->get_data('work_head');
	$self->{html}->{work}      = $self->work_navi($skin_ins);
	$self->{html}->{work_foot} = $skin_ins->get_data('work_foot');

	if ($self->{query}->{work} eq 'comment') {
		$self->{html}->{comment} = $skin_ins->get_replace_data(
			'comment_head',
			ARTICLE_COMMENT_START => '<!--',
			ARTICLE_COMMENT_END   => '-->',
		);

		my $flag;

		open(FH, "$self->{init}->{data_comt_dir}$self->{query}->{pno}\.$self->{init}->{data_ext}") or $self->error("Read Error : $self->{init}->{data_comt_dir}$self->{query}->{pno}\.$self->{init}->{data_ext}");
		while (<FH>) {
			chomp;
			my($no, $pno, $stat, $date, $name, $mail, $url, $subj, $text, $rate, $pwd, $host) = split(/\t/);

			if ($self->{query}->{no} == $no) {
				$self->{html}->{comment} .= $skin_ins->get_replace_data(
					'comment',
					$catalog_ins->comment_article($no, $pno, $stat, $date, $name, $mail, $url, $subj, $text, $rate, $pwd, $host)
				);

				$flag = 1;
			}
		}
		close(FH);

		$self->{html}->{comment} .= $skin_ins->get_replace_data(
			'comment_foot',
			ARTICLE_COMMENT_START => '<!--',
			ARTICLE_COMMENT_END   => '-->',
		);

		if (!$flag) {
			$self->error('指定されたコメントは存在しません。');
		}
	} else {
		$self->{html}->{trackback} .= $skin_ins->get_replace_data(
			'trackback_head',
			ARTICLE_TRACKBACK_START => '<!--',
			ARTICLE_TRACKBACK_END   => '-->',
		);

		my $flag;

		open(FH, "$self->{init}->{data_tb_dir}$self->{query}->{pno}\.$self->{init}->{data_ext}") or $self->error("Read Error : $self->{init}->{data_tb_dir}$self->{query}->{pno}\.$self->{init}->{data_ext}");
		while (<FH>) {
			chomp;
			my($no, $pno, $stat, $date, $blog, $title, $url, $excerpt) = split(/\t/);

			if ($self->{query}->{no} == $no) {
				$self->{html}->{trackback} .= $skin_ins->get_replace_data(
					'trackback',
					$catalog_ins->trackback_article($no, $pno, $stat, $date, $blog, $title, $url, $excerpt)
				);

				$flag = 1;
			}
		}
		close(FH);

		$self->{html}->{trackback} .= $skin_ins->get_replace_data(
			'trackback_foot',
			ARTICLE_TRACKBACK_START => '<!--',
			ARTICLE_TRACKBACK_END   => '-->',
		);

		if (!$flag) {
			$self->error('指定されたトラックバックは存在しません。');
		}
	}

	$self->{html}->{navi}   = $skin_ins->get_data('navi');
	$self->{html}->{footer} = $skin_ins->get_data('footer');

	print $self->header;
	foreach ($skin_ins->get_list) {
		print $self->{html}->{$_};
	}

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

### 分類設定画面
sub output_field {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	if ($self->get_authority ne 'root' and !$self->{config}->{auth_field}) {
		$self->error('この操作を行う権限が与えられていません。');
	}

	if (!$self->{message}) {
		$self->{message} = '登録時の商品分類を設定する事ができます。';
	}

	my $skin_ins = new webliberty::Skin;
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_header}", available => 'header');
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_work}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_field}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_navi}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_footer}", available => 'footer');

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});

	$skin_ins->replace_skin(
		$catalog_ins->info,
		%{$self->{plugin}},
		INFO_MESSAGE => $self->{message}
	);

	print $self->header;
	print $skin_ins->get_data('header');
	print $skin_ins->get_data('work_head');
	print $self->work_navi($skin_ins);
	print $skin_ins->get_data('work_foot');

	my @parents;

	my $form_parent;
	open(FH, $self->{init}->{data_field}) or $self->error("Read Error : $self->{init}->{data_field}");
	while (<FH>) {
		chomp;
		my($field, $child) = split(/<>/);

		if (!$child) {
			$form_parent .= "<option value=\"$field\">$field</option>";

			push(@parents, $field);
		}
	}
	close(FH);

	print $skin_ins->get_replace_data(
		'contents',
		FORM_PARENT => $form_parent
	);
	print $skin_ins->get_data('field_head');

	my $i = 0;

	open(FH, $self->{init}->{data_field}) or $self->error("Read Error : $self->{init}->{data_field}");
	while (<FH>) {
		chomp;
		my($field, $child) = split(/<>/);

		if ($child) {
			my $field_list;
			foreach (@parents) {
				if ($field eq $_) {
					$field_list .= "<option value=\"$_\" selected=\"selected\">$_</option>";
				} else {
					$field_list .= "<option value=\"$_\">$_</option>";
				}
			}
			$field_list = "<select name=\"parent$i\" xml:lang=\"ja\" lang=\"ja\"><option value=\"\">なし</option>$field_list</select>";

			print $skin_ins->get_replace_data(
				'child',
				FIELD_NAME   => $child,
				FIELD_PARENT => $field_list,
				FIELD_NO     => $i
			);
		} else {
			print $skin_ins->get_replace_data(
				'parent',
				FIELD_NAME => $field,
				FIELD_NO   => $i
			);
		}

		$i++;
	}
	close(FH);

	print $skin_ins->get_data('field_foot');
	print $skin_ins->get_data('navi');
	print $skin_ins->get_data('footer');

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

### アイコン設定画面表示
sub output_icon {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	if ($self->get_authority ne 'root' and !$self->{config}->{auth_icon}) {
		$self->error('この操作を行う権限が与えられていません。');
	}

	if (!$self->{message}) {
		$self->{message} = 'アイコンを登録する事ができます。';
	}

	my $skin_ins = new webliberty::Skin;
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_header}", available => 'header');
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_work}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_icon}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_navi}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_footer}", available => 'footer');

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});

	$skin_ins->replace_skin(
		$catalog_ins->info,
		%{$self->{plugin}},
		INFO_MESSAGE => $self->{message}
	);

	print $self->header;
	print $skin_ins->get_data('header');
	print $skin_ins->get_data('work_head');
	print $self->work_navi($skin_ins);
	print $skin_ins->get_data('work_foot');

	print $skin_ins->get_replace_data(
		'contents',
		FORM_EDIT => '',
		FORM_FILE => '',
		FORM_NAME => '',
		FORM_USER => ''
	);
	print $skin_ins->get_data('icon_head');

	my $i = 0;

	open(FH, $self->{init}->{data_icon}) or $self->error("Read Error : $self->{init}->{data_icon}");
	while (<FH>) {
		chomp;
		my($file, $name, $field, $user, $pwd) = split(/\t/);

		my $file_path;
		if ($self->{init}->{data_icon_path}) {
			$file_path = $self->{init}->{data_icon_path};
		} else {
			$file_path = $self->{init}->{data_icon_dir};
		}
		my $image = "<img src=\"$file_path$file\" alt=\"$file\" />";

		print $skin_ins->get_replace_data(
			'icon',
			ICON_FILE  => $file,
			ICON_IMAGE => $image,
			ICON_NAME  => $name,
			ICON_USER  => $user
		);
	}
	close(FH);

	print $skin_ins->get_data('icon_foot');
	print $skin_ins->get_data('navi');
	print $skin_ins->get_data('footer');

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

### オプション設定画面表示
sub output_option {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	if ($self->get_authority ne 'root' and !$self->{config}->{auth_option}) {
		$self->error('この操作を行う権限が与えられていません。');
	}

	if (!$self->{message}) {
		$self->{message} = 'オプション項目を設定する事ができます。';
	}

	my $skin_ins = new webliberty::Skin;
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_header}", available => 'header');
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_work}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_option}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_navi}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_footer}", available => 'footer');

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});

	$skin_ins->replace_skin(
		$catalog_ins->info,
		%{$self->{plugin}},
		INFO_MESSAGE => $self->{message}
	);

	print $self->header;
	print $skin_ins->get_data('header');
	print $skin_ins->get_data('work_head');
	print $self->work_navi($skin_ins);
	print $skin_ins->get_data('work_foot');

	my @type = ('text', 'textarea', 'select', 'checkbox', 'option');
	my %type = ('text' => '一行入力', 'textarea' => '複数行入力', 'select' => '単一選択', 'checkbox' => '複数選択', 'option' => '商品オプション');

	my $form_type;
	foreach (@type) {
		$form_type .= "<option value=\"$_\">$type{$_}</option>";
	}

	print $skin_ins->get_replace_data(
		'contents',
		FORM_TYPE => $form_type
	);
	print $skin_ins->get_data('option_head');

	my $i = 0;

	open(FH, $self->{init}->{data_option}) or $self->error("Read Error : $self->{init}->{data_option}");
	while (<FH>) {
		chomp;
		my($id, $name, $type, $default) = split(/\t/);

		my $name_ins    = new webliberty::String($name);
		my $default_ins = new webliberty::String($default);

		$name_ins->create_plain;
		$default_ins->create_plain;

		my $type_list;
		foreach (@type) {
			if ($type eq $_) {
				$type_list .= "<option value=\"$_\" selected=\"selected\">$type{$_}</option>";
			} else {
				$type_list .= "<option value=\"$_\">$type{$_}</option>";
			}
		}
		$type_list = "<select name=\"type$i\" xml:lang=\"ja\" lang=\"ja\">$type_list</select>";

		print $skin_ins->get_replace_data(
			'option',
			OPTION_ID      => $id,
			OPTION_NAME    => $name_ins->get_string,
			OPTION_TYPE    => $type_list,
			OPTION_DEFAULT => $default_ins->get_string,
			OPTION_NO      => $i
		);

		$i++;
	}
	close(FH);

	print $skin_ins->get_data('option_foot');
	print $skin_ins->get_data('navi');
	print $skin_ins->get_data('footer');

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

### インデックスページ設定画面
sub output_top {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	if ($self->get_authority ne 'root' and !$self->{config}->{auth_top}) {
		$self->error('この操作を行う権限が与えられていません。');
	}

	if (!$self->{message}) {
		$self->{message} = 'インデックスページに表示するテキストを設定します。';
	}

	my $skin_ins = new webliberty::Skin;
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_header}", available => 'header');
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_work}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_top}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_navi}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_footer}", available => 'footer');

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});

	$skin_ins->replace_skin(
		$catalog_ins->info,
		%{$self->{plugin}},
		INFO_MESSAGE => $self->{message}
	);

	open(FH, $self->{init}->{data_top}) or $self->error("Read Error : $self->{init}->{data_top}");
	my $text = <FH>;
	close(FH);

	my $text_ins = new webliberty::String($text);

	print $self->header;
	print $skin_ins->get_data('header');
	print $skin_ins->get_data('work_head');
	print $self->work_navi($skin_ins);
	print $skin_ins->get_data('work_foot');
	print $skin_ins->get_replace_data(
		'contents',
		FORM_TEXT => $text_ins->create_plain
	);
	print $skin_ins->get_data('navi');
	print $skin_ins->get_data('footer');

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

### コンテンツ設定画面表示
sub output_menu {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	if ($self->get_authority ne 'root' and !$self->{config}->{auth_menu}) {
		$self->error('この操作を行う権限が与えられていません。');
	}

	if (!$self->{message}) {
		$self->{message} = 'コンテンツを設定する事ができます。';
	}

	my $skin_ins = new webliberty::Skin;
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_header}", available => 'header');
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_work}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_menu}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_navi}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_footer}", available => 'footer');

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});

	$skin_ins->replace_skin(
		$catalog_ins->info,
		%{$self->{plugin}},
		INFO_MESSAGE => $self->{message}
	);

	print $self->header;
	print $skin_ins->get_data('header');
	print $skin_ins->get_data('work_head');
	print $self->work_navi($skin_ins);
	print $skin_ins->get_data('work_foot');

	my $form_field;
	foreach (split(/<>/, $self->{config}->{menu_list})) {
		$form_field .= "<option value=\"$_\">$_</option>";
	}

	print $skin_ins->get_replace_data(
		'contents',
		FORM_FIELD => $form_field
	);
	print $skin_ins->get_data('menu_head');

	my $i = 0;

	open(FH, $self->{init}->{data_menu}) or $self->error("Read Error : $self->{init}->{data_menu}");
	while (<FH>) {
		chomp;
		my($field, $name, $url) = split(/\t/);

		my $field_list;
		foreach (split(/<>/, $self->{config}->{menu_list})) {
			if ($field eq $_) {
				$field_list .= "<option value=\"$_\" selected=\"selected\">$_</option>";
			} else {
				$field_list .= "<option value=\"$_\">$_</option>";
			}
		}
		$field_list = "<select name=\"field$i\" xml:lang=\"ja\" lang=\"ja\"><option value=\"\">なし</option>$field_list</select>";

		print $skin_ins->get_replace_data(
			'menu',
			MENU_FIELD => $field_list,
			MENU_NAME  => $name,
			MENU_URL   => $url,
			MENU_NO    => $i
		);

		$i++;
	}
	close(FH);

	print $skin_ins->get_data('menu_foot');
	print $skin_ins->get_data('navi');
	print $skin_ins->get_data('footer');

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

### リンク集設定画面表示
sub output_link {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	if ($self->get_authority ne 'root' and !$self->{config}->{auth_link}) {
		$self->error('この操作を行う権限が与えられていません。');
	}

	if (!$self->{message}) {
		$self->{message} = 'リンク集を設定する事ができます。';
	}

	my $skin_ins = new webliberty::Skin;
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_header}", available => 'header');
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_work}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_link}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_navi}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_footer}", available => 'footer');

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});

	$skin_ins->replace_skin(
		$catalog_ins->info,
		%{$self->{plugin}},
		INFO_MESSAGE => $self->{message}
	);

	print $self->header;
	print $skin_ins->get_data('header');
	print $skin_ins->get_data('work_head');
	print $self->work_navi($skin_ins);
	print $skin_ins->get_data('work_foot');

	my $form_field;
	foreach (split(/<>/, $self->{config}->{link_list})) {
		$form_field .= "<option value=\"$_\">$_</option>";
	}

	print $skin_ins->get_replace_data(
		'contents',
		FORM_FIELD => $form_field
	);
	print $skin_ins->get_data('link_head');

	my $i = 0;

	open(FH, $self->{init}->{data_link}) or $self->error("Read Error : $self->{init}->{data_link}");
	while (<FH>) {
		chomp;
		my($field, $name, $url) = split(/\t/);

		my $field_list;
		foreach (split(/<>/, $self->{config}->{link_list})) {
			if ($field eq $_) {
				$field_list .= "<option value=\"$_\" selected=\"selected\">$_</option>";
			} else {
				$field_list .= "<option value=\"$_\">$_</option>";
			}
		}
		$field_list = "<select name=\"field$i\" xml:lang=\"ja\" lang=\"ja\"><option value=\"\">なし</option>$field_list</select>";

		print $skin_ins->get_replace_data(
			'link',
			LINK_FIELD => $field_list,
			LINK_NAME  => $name,
			LINK_URL   => $url,
			LINK_NO    => $i
		);

		$i++;
	}
	close(FH);

	print $skin_ins->get_data('link_foot');
	print $skin_ins->get_data('navi');
	print $skin_ins->get_data('footer');

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

### プロフィール設定画面
sub output_profile {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	my $edit_ins = new webliberty::String($self->{query}->{edit});
	$edit_ins->create_line;

	if (!$self->{message}) {
		if ($self->get_authority eq 'root' and $edit_ins->get_string) {
			$self->{message} = '<em>' . $edit_ins->get_string . '</em>のプロフィールを設定します。';
		} else {
			$self->{message} = 'プロフィールを設定します。';
		}
	}

	my $skin_ins = new webliberty::Skin;
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_header}", available => 'header');
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_work}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_profile}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_navi}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_footer}", available => 'footer');

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});

	$skin_ins->replace_skin(
		$catalog_ins->info,
		%{$self->{plugin}},
		INFO_MESSAGE => $self->{message}
	);

	my(%name, %text);

	open(FH, $self->{init}->{data_profile}) or $self->error("Read Error : $self->{init}->{data_profile}");
	while (<FH>) {
		chomp;
		my($user, $name, $text) = split(/\t/);

		$name{$user} = $name;
		$text{$user} = $text;
	}
	close(FH);

	my($name_ins, $text_ins);

	if ($self->get_authority eq 'root' and $edit_ins->get_string) {
		$name_ins = new webliberty::String($name{$edit_ins->get_string});
		$text_ins = new webliberty::String($text{$edit_ins->get_string});
	} else {
		$name_ins = new webliberty::String($name{$self->get_user});
		$text_ins = new webliberty::String($text{$self->get_user});
	}

	print $self->header;
	print $skin_ins->get_data('header');
	print $skin_ins->get_data('work_head');
	print $self->work_navi($skin_ins);
	print $skin_ins->get_data('work_foot');
	print $skin_ins->get_replace_data(
		'contents',
		FORM_USER => $edit_ins->get_string,
		FORM_NAME => $name_ins->create_plain,
		FORM_TEXT => $text_ins->create_plain
	);

	if ($self->{config}->{user_mode} and $self->get_authority eq 'root') {
		print $skin_ins->get_data('profile_head');

		open(FH, $self->{init}->{data_user}) or $self->error("Read Error : $self->{init}->{data_user}");
		while (<FH>) {
			chomp;
			my($user, $pwd, $authority) = split(/\t/);

			my $authority_list;
			if ($authority eq 'root') {
				$authority = 'システム管理者';
			} else {
				$authority = 'ゲストユーザー';
			}

			print $skin_ins->get_replace_data(
				'profile',
				PROFILE_USER      => $user,
				PROFILE_PWD       => $pwd,
				PROFILE_AUTHORITY => $authority,
				PROFILE_NAME      => $name{$user},
				PROFILE_TEXT      => $text{$user}
			);
		}
		close(FH);

		print $skin_ins->get_data('profile_foot');
	}

	print $skin_ins->get_data('navi');
	print $skin_ins->get_data('footer');

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

### ログインパスワード設定画面
sub output_pwd {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	if (!$self->{message}) {
		$self->{message} = 'ログイン用パスワードを設定します。以前のパスワードと新しく設定したいパスワードを入力してください。';
	}

	my $skin_ins = new webliberty::Skin;
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_header}", available => 'header');
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_work}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_pwd}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_navi}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_footer}", available => 'footer');

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});

	$skin_ins->replace_skin(
		$catalog_ins->info,
		%{$self->{plugin}},
		INFO_MESSAGE => $self->{message}
	);

	print $self->header;
	print $skin_ins->get_data('header');
	print $skin_ins->get_data('work_head');
	print $self->work_navi($skin_ins);
	print $skin_ins->get_data('work_foot');
	print $skin_ins->get_data('contents');
	print $skin_ins->get_data('navi');
	print $skin_ins->get_data('footer');

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

### 環境設定画面表示
sub output_env {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	if ($self->get_authority ne 'root') {
		$self->error('この操作を行う権限が与えられていません。');
	}

	if (!$self->{message}) {
		$self->{message} = '設定を変更し、<em>設定ボタン</em>を押してください。';
	}

	my $skin_ins = new webliberty::Skin;
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_header}", available => 'header');
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_work}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_env}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_navi}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_footer}", available => 'footer');

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});

	$skin_ins->replace_skin(
		$catalog_ins->info,
		%{$self->{plugin}},
		INFO_MESSAGE => $self->{message}
	);

	my $info_path;
	if ($self->{init}->{script_file} =~ /([^\/\\]*)$/) {
		$info_path = $1;
	}

	my $order_date_mode;
	if ($self->{config}->{order_date_mode} eq '1') {
		$order_date_mode = '<option value="1" selected="selected">ON</option><option value="0">OFF</option>';
	} else {
		$order_date_mode = '<option value="1">ON</option><option value="0" selected="selected">OFF</option>';
	}

	my $customer_mode;
	if ($self->{config}->{customer_mode} eq '1') {
		$customer_mode = '<option value="1" selected="selected">ON</option><option value="0">OFF</option>';
	} else {
		$customer_mode = '<option value="1">ON</option><option value="0" selected="selected">OFF</option>';
	}

	my $point_mode;
	if ($self->{config}->{point_mode} eq '1') {
		$point_mode = '<option value="1" selected="selected">ON</option><option value="0">OFF</option>';
	} else {
		$point_mode = '<option value="1">ON</option><option value="0" selected="selected">OFF</option>';
	}

	my $top_field;
	if ($self->{config}->{top_field} eq '2') {
		$top_field = '<option value="2" selected="selected">分類別表示（子分類も別々に表示）</option><option value="1">分類別表示</option><option value="0">時系列表示</option>';
	} elsif ($self->{config}->{top_field} eq '1') {
		$top_field = '<option value="2">分類別表示（子分類も別々に表示）</option><option value="1" selected="selected">分類別表示</option><option value="0">時系列表示</option>';
	} else {
		$top_field = '<option value="2">分類別表示（子分類も別々に表示）</option><option value="1">分類別表示</option><option value="0" selected="selected">時系列表示</option>';
	}

	my $top_text;
	if ($self->{config}->{top_text} eq '1') {
		$top_text = '<option value="1" selected="selected">表示する</option><option value="0">表示しない</option>';
	} else {
		$top_text = '<option value="1">表示する</option><option value="0" selected="selected">表示しない</option>';
	}

	my $top_break;
	if ($self->{config}->{top_break} eq '1') {
		$top_break = '<option value="1" selected="selected">変換する</option><option value="0">変換しない</option>';
	} else {
		$top_break = '<option value="1">変換する</option><option value="0" selected="selected">変換しない</option>';
	}

	my $data_sort;
	if ($self->{config}->{data_sort} eq '1') {
		$data_sort = '<option value="1" selected="selected">登録日時の昇順</option><option value="0">登録日時の降順</option>';
	} else {
		$data_sort = '<option value="1">登録日時の昇順</option><option value="0" selected="selected">登録日時の降順</option>';
	}

	my $rss_mode;
	if ($self->{config}->{rss_mode} eq '2') {
		$rss_mode = '<option value="2" selected="selected">全文を配信（ファイルも配信）</option><option value="1">全文を配信</option><option value="0">概要を配信</option>';
	} elsif ($self->{config}->{rss_mode} eq '1') {
		$rss_mode = '<option value="2">全文を配信（ファイルも配信）</option><option value="1" selected="selected">全文を配信</option><option value="0">概要を配信</option>';
	} else {
		$rss_mode = '<option value="2">全文を配信（ファイルも配信）</option><option value="1">全文を配信</option><option value="0" selected="selected">概要を配信</option>';
	}

	my $use_field;
	if ($self->{config}->{use_field} eq '1') {
		$use_field = '<option value="1" selected="selected">表示する</option><option value="0">表示しない</option>';
	} else {
		$use_field = '<option value="1">表示する</option><option value="0" selected="selected">表示しない</option>';
	}

	my $use_icon;
	if ($self->{config}->{use_icon} eq '1') {
		$use_icon = '<option value="1" selected="selected">表示する</option><option value="0">表示しない</option>';
	} else {
		$use_icon = '<option value="1">表示する</option><option value="0" selected="selected">表示しない</option>';
	}

	my $use_image;
	if ($self->{config}->{use_image} eq '1') {
		$use_image = '<option value="1" selected="selected">表示する</option><option value="0">表示しない</option>';
	} else {
		$use_image = '<option value="1">表示する</option><option value="0" selected="selected">表示しない</option>';
	}

	my $use_file;
	if ($self->{config}->{use_file} eq '1') {
		$use_file = '<option value="1" selected="selected">表示する</option><option value="0">表示しない</option>';
	} else {
		$use_file = '<option value="1">表示する</option><option value="0" selected="selected">表示しない</option>';
	}

	my $use_stock;
	if ($self->{config}->{use_stock} eq '1') {
		$use_stock = '<option value="1" selected="selected">表示する</option><option value="0">表示しない</option>';
	} else {
		$use_stock = '<option value="1">表示する</option><option value="0" selected="selected">表示しない</option>';
	}

	my $use_relate;
	if ($self->{config}->{use_relate} eq '1') {
		$use_relate = '<option value="1" selected="selected">表示する</option><option value="0">表示しない</option>';
	} else {
		$use_relate = '<option value="1">表示する</option><option value="0" selected="selected">表示しない</option>';
	}

	my $use_tburl;
	if ($self->{config}->{use_tburl} eq '1') {
		$use_tburl = '<option value="1" selected="selected">表示する</option><option value="0">表示しない</option>';
	} else {
		$use_tburl = '<option value="1">表示する</option><option value="0" selected="selected">表示しない</option>';
	}

	my $default_stat;
	if ($self->{config}->{default_stat} eq '1') {
		$default_stat = '<option value="1" selected="selected">公開する</option><option value="0">公開しない</option>';
	} else {
		$default_stat = '<option value="1">公開する</option><option value="0" selected="selected">公開しない</option>';
	}

	my $default_break;
	if ($self->{config}->{default_break} eq '1') {
		$default_break = '<option value="1" selected="selected">変換する</option><option value="0">変換しない</option>';
	} else {
		$default_break = '<option value="1">変換する</option><option value="0" selected="selected">変換しない</option>';
	}

	my $default_view;
	if ($self->{config}->{default_view} eq '1') {
		$default_view = '<option value="1" selected="selected">ページ別に表示する</option><option value="0">すべて表示する</option>';
	} else {
		$default_view = '<option value="1">ページ別に表示する</option><option value="0" selected="selected">すべて表示する</option>';
	}

	my $default_comt;
	if ($self->{config}->{default_comt} eq '1') {
		$default_comt = '<option value="1" selected="selected">受け付ける</option><option value="0">受け付けない</option>';
	} else {
		$default_comt = '<option value="1">受け付ける</option><option value="0" selected="selected">受け付けない</option>';
	}

	my $default_tb;
	if ($self->{config}->{default_tb} eq '1') {
		$default_tb = '<option value="1" selected="selected">受け付ける</option><option value="0">受け付けない</option>';
	} else {
		$default_tb = '<option value="1">受け付ける</option><option value="0" selected="selected">受け付けない</option>';
	}

	my $comt_stat;
	if ($self->{config}->{comt_stat} eq '1') {
		$comt_stat = '<option value="1" selected="selected">承認済み</option><option value="0">未承認</option>';
	} else {
		$comt_stat = '<option value="1">承認済み</option><option value="0" selected="selected">未承認</option>';
	}

	my $tb_stat;
	if ($self->{config}->{tb_stat} eq '1') {
		$tb_stat = '<option value="1" selected="selected">承認済み</option><option value="0">未承認</option>';
	} else {
		$tb_stat = '<option value="1">承認済み</option><option value="0" selected="selected">未承認</option>';
	}

	my $title_mode;
	if ($self->{config}->{title_mode} eq '3') {
		$title_mode = '<option value="0">ショッピングページタイトルのみ表示</option><option value="1">ショッピングページタイトル＋商品名</option><option value="2">商品名＋ショッピングページタイトル</option><option value="3" selected="selected">商品名のみ表示</option>';
	} elsif ($self->{config}->{title_mode} eq '2') {
		$title_mode = '<option value="0">ショッピングページタイトルのみ表示</option><option value="1">ショッピングページタイトル＋商品名</option><option value="2" selected="selected">商品名＋ショッピングページタイトル</option><option value="3">商品名のみ表示</option>';
	} elsif ($self->{config}->{title_mode} eq '1') {
		$title_mode = '<option value="0">ショッピングページタイトルのみ表示</option><option value="1" selected="selected">ショッピングページタイトル＋商品名</option><option value="2">商品名＋ショッピングページタイトル</option><option value="3">商品名のみ表示</option>';
	} else {
		$title_mode = '<option value="0" selected="selected">ショッピングページタイトルのみ表示</option><option value="1">ショッピングページタイトル＋商品名</option><option value="2">商品名＋ショッピングページタイトル</option><option value="3">商品名のみ表示</option>';
	}

	my $paragraph_mode;
	if ($self->{config}->{paragraph_mode} eq '1') {
		$paragraph_mode = '<option value="1" selected="selected">変換する</option><option value="0">変換しない</option>';
	} else {
		$paragraph_mode = '<option value="1">変換する</option><option value="0" selected="selected">変換しない</option>';
	}

	my $autolink_mode;
	if ($self->{config}->{autolink_mode} eq '1') {
		$autolink_mode = '<option value="1" selected="selected">リンクする</option><option value="0">リンクしない</option>';
	} else {
		$autolink_mode = '<option value="1">リンクする</option><option value="0" selected="selected">リンクしない</option>';
	}

	my $decoration_mode;
	if ($self->{config}->{decoration_mode} eq '1') {
		$decoration_mode = '<option value="1" selected="selected">装飾する</option><option value="0">装飾しない</option>';
	} else {
		$decoration_mode = '<option value="1">装飾する</option><option value="0" selected="selected">装飾しない</option>';
	}

	my $thumbnail_mode;
	if ($self->{config}->{thumbnail_mode} eq '2') {
		$thumbnail_mode = '<option value="2" selected="selected">repng2jpegで作成</option><option value="1">ImageMagickで作成</option><option value="0">作成しない</option>';
	} elsif ($self->{config}->{thumbnail_mode} eq '1') {
		$thumbnail_mode = '<option value="2">repng2jpegで作成</option><option value="1" selected="selected">ImageMagickで作成</option><option value="0">作成しない</option>';
	} else {
		$thumbnail_mode = '<option value="2">repng2jpegで作成</option><option value="1">ImageMagickで作成</option><option value="0" selected="selected">作成しない</option>';
	}

	my $rate_mode;
	if ($self->{config}->{rate_mode} eq '1') {
		$rate_mode = '<option value="1" selected="selected">ON</option><option value="0">OFF</option>';
	} else {
		$rate_mode = '<option value="1">ON</option><option value="0" selected="selected">OFF</option>';
	}

	my $rank_mode;
	if ($self->{config}->{rank_mode} eq '1') {
		$rank_mode = '<option value="1" selected="selected">ON</option><option value="0">OFF</option>';
	} else {
		$rank_mode = '<option value="1">ON</option><option value="0" selected="selected">OFF</option>';
	}

	my $show_calendar;
	if ($self->{config}->{show_calendar} eq '1') {
		$show_calendar = '<option value="1" selected="selected">表示する</option><option value="0">表示しない</option>';
	} else {
		$show_calendar = '<option value="1">表示する</option><option value="0" selected="selected">表示しない</option>';
	}

	my $show_field;
	if ($self->{config}->{show_field} eq '1') {
		$show_field = '<option value="1" selected="selected">表示する</option><option value="0">表示しない</option>';
	} else {
		$show_field = '<option value="1">表示する</option><option value="0" selected="selected">表示しない</option>';
	}

	my $show_search;
	if ($self->{config}->{show_search} eq '1') {
		$show_search = '<option value="1" selected="selected">表示する</option><option value="0">表示しない</option>';
	} else {
		$show_search = '<option value="1">表示する</option><option value="0" selected="selected">表示しない</option>';
	}

	my $show_past;
	if ($self->{config}->{show_past} eq '1') {
		$show_past = '<option value="1" selected="selected">表示する</option><option value="0">表示しない</option>';
	} else {
		$show_past = '<option value="1">表示する</option><option value="0" selected="selected">表示しない</option>';
	}

	my $show_menu;
	if ($self->{config}->{show_menu} eq '1') {
		$show_menu = '<option value="1" selected="selected">表示する</option><option value="0">表示しない</option>';
	} else {
		$show_menu = '<option value="1">表示する</option><option value="0" selected="selected">表示しない</option>';
	}

	my $show_link;
	if ($self->{config}->{show_link} eq '1') {
		$show_link = '<option value="1" selected="selected">表示する</option><option value="0">表示しない</option>';
	} else {
		$show_link = '<option value="1">表示する</option><option value="0" selected="selected">表示しない</option>';
	}

	my $date_navigation;
	if ($self->{config}->{date_navigation} eq '1') {
		$date_navigation = '<option value="1" selected="selected">表示する</option><option value="0">表示しない</option>';
	} else {
		$date_navigation = '<option value="1">表示する</option><option value="0" selected="selected">表示しない</option>';
	}

	my $field_navigation;
	if ($self->{config}->{field_navigation} eq '1') {
		$field_navigation = '<option value="1" selected="selected">表示する</option><option value="0">表示しない</option>';
	} else {
		$field_navigation = '<option value="1">表示する</option><option value="0" selected="selected">表示しない</option>';
	}

	my $show_navigation;
	if ($self->{config}->{show_navigation} eq '2') {
		$show_navigation = '<option value="2" selected="selected">常に表示する</option><option value="1">固定URLページで表示する</option><option value="0">表示しない</option>';
	} elsif ($self->{config}->{show_navigation} eq '1') {
		$show_navigation = '<option value="2">常に表示する</option><option value="1" selected="selected">固定URLページで表示する</option><option value="0">表示しない</option>';
	} else {
		$show_navigation = '<option value="2">常に表示する</option><option value="1">固定URLページで表示する</option><option value="0" selected="selected">表示しない</option>';
	}

	my $pos_navigation;
	if ($self->{config}->{pos_navigation} eq '1') {
		$pos_navigation = '<option value="1" selected="selected">商品の前に出力</option><option value="0">商品の後に出力</option>';
	} else {
		$pos_navigation = '<option value="1">商品の前に出力</option><option value="0" selected="selected">商品の後に出力</option>';
	}

	my $profile_mode;
	if ($self->{config}->{profile_mode} eq '1') {
		$profile_mode = '<option value="1" selected="selected">ON</option><option value="0">OFF</option>';
	} else {
		$profile_mode = '<option value="1">ON</option><option value="0" selected="selected">OFF</option>';
	}

	my $profile_break;
	if ($self->{config}->{profile_break} eq '1') {
		$profile_break = '<option value="1" selected="selected">変換する</option><option value="0">変換しない</option>';
	} else {
		$profile_break = '<option value="1">変換する</option><option value="0" selected="selected">変換しない</option>';
	}

	my $user_mode;
	if ($self->{config}->{user_mode} eq '1') {
		$user_mode = '<option value="1" selected="selected">ON</option><option value="0">OFF</option>';
	} else {
		$user_mode = '<option value="1">ON</option><option value="0" selected="selected">OFF</option>';
	}

	my $auth_comment;
	if ($self->{config}->{auth_comment} eq '1') {
		$auth_comment = '<input type="checkbox" name="env_auth_comment" id="auth_comment_checkbox" value="1" checked="checked" /> <label for="auth_comment_checkbox">コメントの管理</label>';
	} else {
		$auth_comment = '<input type="checkbox" name="env_auth_comment" id="auth_comment_checkbox" value="1" /> <label for="auth_comment_checkbox">コメントの管理</label>';
	}

	my $auth_trackback;
	if ($self->{config}->{auth_trackback} eq '1') {
		$auth_trackback = '<input type="checkbox" name="env_auth_trackback" id="auth_trackback_checkbox" value="1" checked="checked" /> <label for="auth_trackback_checkbox">トラックバックの管理</label>';
	} else {
		$auth_trackback = '<input type="checkbox" name="env_auth_trackback" id="auth_trackback_checkbox" value="1" /> <label for="auth_trackback_checkbox">トラックバックの管理</label>';
	}

	my $auth_customer;
	if ($self->{config}->{auth_customer} eq '1') {
		$auth_customer = '<input type="checkbox" name="env_auth_customer" id="auth_customer_checkbox" value="1" checked="checked" /> <label for="auth_customer_checkbox">顧客の管理</label>';
	} else {
		$auth_customer = '<input type="checkbox" name="env_auth_customer" id="auth_customer_checkbox" value="1" /> <label for="auth_customer_checkbox">顧客の管理</label>';
	}

	my $auth_field;
	if ($self->{config}->{auth_field} eq '1') {
		$auth_field = '<input type="checkbox" name="env_auth_field" id="auth_field_checkbox" value="1" checked="checked" /> <label for="auth_field_checkbox">分類の管理</label>';
	} else {
		$auth_field = '<input type="checkbox" name="env_auth_field" id="auth_field_checkbox" value="1" /> <label for="auth_field_checkbox">分類の管理</label>';
	}

	my $auth_icon;
	if ($self->{config}->{auth_icon} eq '1') {
		$auth_icon = '<input type="checkbox" name="env_auth_icon" id="auth_icon_checkbox" value="1" checked="checked" /> <label for="auth_icon_checkbox">アイコンの管理</label>';
	} else {
		$auth_icon = '<input type="checkbox" name="env_auth_icon" id="auth_icon_checkbox" value="1" /> <label for="auth_icon_checkbox">アイコンの管理</label>';
	}

	my $auth_option;
	if ($self->{config}->{auth_option} eq '1') {
		$auth_option = '<input type="checkbox" name="env_auth_option" id="auth_option_checkbox" value="1" checked="checked" /> <label for="auth_option_checkbox">オプションの管理</label>';
	} else {
		$auth_option = '<input type="checkbox" name="env_auth_option" id="auth_option_checkbox" value="1" /> <label for="auth_option_checkbox">オプションの管理</label>';
	}

	my $auth_top;
	if ($self->{config}->{auth_top} eq '1') {
		$auth_top = '<input type="checkbox" name="env_auth_top" id="auth_top_checkbox" value="1" checked="checked" /> <label for="auth_top_checkbox">インデックスページの管理</label>';
	} else {
		$auth_top = '<input type="checkbox" name="env_auth_top" id="auth_top_checkbox" value="1" /> <label for="auth_top_checkbox">インデックスページの管理</label>';
	}

	my $auth_menu;
	if ($self->{config}->{auth_menu} eq '1') {
		$auth_menu = '<input type="checkbox" name="env_auth_menu" id="auth_menu_checkbox" value="1" checked="checked" /> <label for="auth_menu_checkbox">コンテンツの管理</label>';
	} else {
		$auth_menu = '<input type="checkbox" name="env_auth_menu" id="auth_menu_checkbox" value="1" /> <label for="auth_menu_checkbox">コンテンツの管理</label>';
	}

	my $auth_link;
	if ($self->{config}->{auth_link} eq '1') {
		$auth_link = '<input type="checkbox" name="env_auth_link" id="auth_link_checkbox" value="1" checked="checked" /> <label for="auth_link_checkbox">リンク集の管理</label>';
	} else {
		$auth_link = '<input type="checkbox" name="env_auth_link" id="auth_link_checkbox" value="1" /> <label for="auth_link_checkbox">リンク集の管理</label>';
	}

	my $sendmail_cmt_mode;
	if ($self->{config}->{sendmail_cmt_mode} eq '1') {
		$sendmail_cmt_mode = '<option value="1" selected="selected">通知する</option><option value="0">通知しない</option>';
	} else {
		$sendmail_cmt_mode = '<option value="1">通知する</option><option value="0" selected="selected">通知しない</option>';
	}

	my $sendmail_tb_mode;
	if ($self->{config}->{sendmail_tb_mode} eq '1') {
		$sendmail_tb_mode = '<option value="1" selected="selected">通知する</option><option value="0">通知しない</option>';
	} else {
		$sendmail_tb_mode = '<option value="1">通知する</option><option value="0" selected="selected">通知しない</option>';
	}

	my $sendmail_stock_mode;
	if ($self->{config}->{sendmail_stock_mode} eq '1') {
		$sendmail_stock_mode = '<option value="1" selected="selected">通知する</option><option value="0">通知しない</option>';
	} else {
		$sendmail_stock_mode = '<option value="1">通知する</option><option value="0" selected="selected">通知しない</option>';
	}

	my $sendmail_detail;
	if ($self->{config}->{sendmail_detail} eq '1') {
		$sendmail_detail = '<option value="1" selected="selected">通知する</option><option value="0">通知しない</option>';
	} else {
		$sendmail_detail = '<option value="1">通知する</option><option value="0" selected="selected">通知しない</option>';
	}

	my $ping_mode;
	if ($self->{config}->{ping_mode} eq '1') {
		$ping_mode = '<option value="1" selected="selected">通知する</option><option value="0">通知しない</option>';
	} else {
		$ping_mode = '<option value="1">通知する</option><option value="0" selected="selected">通知しない</option>';
	}

	my $html_index_mode;
	if ($self->{config}->{html_index_mode} eq '1') {
		$html_index_mode = '<option value="1" selected="selected">ON</option><option value="0">OFF</option>';
	} else {
		$html_index_mode = '<option value="1">ON</option><option value="0" selected="selected">OFF</option>';
	}

	my $html_archive_mode;
	if ($self->{config}->{html_archive_mode} eq '1') {
		$html_archive_mode = '<option value="1" selected="selected">ON</option><option value="0">OFF</option>';
	} else {
		$html_archive_mode = '<option value="1">ON</option><option value="0" selected="selected">OFF</option>';
	}

	my $html_field_mode;
	if ($self->{config}->{html_field_mode} eq '1') {
		$html_field_mode = '<option value="1" selected="selected">ON</option><option value="0">OFF</option>';
	} else {
		$html_field_mode = '<option value="1">ON</option><option value="0" selected="selected">OFF</option>';
	}

	my $js_title_mode;
	if ($self->{config}->{js_title_mode} eq '1') {
		$js_title_mode = '<option value="1" selected="selected">ON</option><option value="0">OFF</option>';
	} else {
		$js_title_mode = '<option value="1">ON</option><option value="0" selected="selected">OFF</option>';
	}

	my $js_title_field_mode;
	if ($self->{config}->{js_title_field_mode} eq '1') {
		$js_title_field_mode = '<option value="1" selected="selected">ON</option><option value="0">OFF</option>';
	} else {
		$js_title_field_mode = '<option value="1">ON</option><option value="0" selected="selected">OFF</option>';
	}

	my $js_text_mode;
	if ($self->{config}->{js_text_mode} eq '1') {
		$js_text_mode = '<option value="1" selected="selected">ON</option><option value="0">OFF</option>';
	} else {
		$js_text_mode = '<option value="1">ON</option><option value="0" selected="selected">OFF</option>';
	}

	my $js_text_field_mode;
	if ($self->{config}->{js_text_field_mode} eq '1') {
		$js_text_field_mode = '<option value="1" selected="selected">ON</option><option value="0">OFF</option>';
	} else {
		$js_text_field_mode = '<option value="1">ON</option><option value="0" selected="selected">OFF</option>';
	}

	my $proxy_mode;
	if ($self->{config}->{proxy_mode} eq '1') {
		$proxy_mode = '<option value="1" selected="selected">許可</option><option value="0">禁止</option>';
	} else {
		$proxy_mode = '<option value="1">許可</option><option value="0" selected="selected">禁止</option>';
	}

	my $need_japanese;
	if ($self->{config}->{need_japanese} eq '1') {
		$need_japanese = '<option value="1" selected="selected">必須</option><option value="0">任意</option>';
	} else {
		$need_japanese = '<option value="1">必須</option><option value="0" selected="selected">任意</option>';
	}

	my $need_japanese_tb;
	if ($self->{config}->{need_japanese_tb} eq '1') {
		$need_japanese_tb = '<option value="1" selected="selected">必須</option><option value="0">任意</option>';
	} else {
		$need_japanese_tb = '<option value="1">必須</option><option value="0" selected="selected">任意</option>';
	}

	my $need_link_tb;
	if ($self->{config}->{need_link_tb} eq '1') {
		$need_link_tb = '<option value="1" selected="selected">必須</option><option value="0">任意</option>';
	} else {
		$need_link_tb = '<option value="1">必須</option><option value="0" selected="selected">任意</option>';
	}

	my $rss_field_list = $self->{config}->{rss_field_list};
	$rss_field_list =~ s/<>/\n/g;

	my $top_field_list = $self->{config}->{top_field_list};
	$top_field_list =~ s/<>/\n/g;

	my $sendmail_list = $self->{config}->{sendmail_list};
	$sendmail_list =~ s/<>/\n/g;

	my $order_payment = $self->{config}->{order_payment};
	$order_payment =~ s/<>/\n/g;

	my $order_delivery = $self->{config}->{order_delivery};
	$order_delivery =~ s/<>/\n/g;

	my $order_time_list = $self->{config}->{order_time_list};
	$order_time_list =~ s/<>/\n/g;

	my $order_pref = $self->{config}->{order_pref};
	$order_pref =~ s/<>/\n/g;

	my $sendmail_order_body = $self->{config}->{sendmail_order_body};
	$sendmail_order_body =~ s/<>/\n/g;

	my $sendmail_regist_body = $self->{config}->{sendmail_regist_body};
	$sendmail_regist_body =~ s/<>/\n/g;

	my $sendmail_edit_body = $self->{config}->{sendmail_edit_body};
	$sendmail_edit_body =~ s/<>/\n/g;

	my $sendmail_delete_body = $self->{config}->{sendmail_delete_body};
	$sendmail_delete_body =~ s/<>/\n/g;

	my $sendmail_pwd_body = $self->{config}->{sendmail_pwd_body};
	$sendmail_pwd_body =~ s/<>/\n/g;

	my $sendmail_signature = $self->{config}->{sendmail_signature};
	$sendmail_signature =~ s/<>/\n/g;

	my $rate_info = $self->{config}->{rate_info};
	$rate_info =~ s/<>/\n/g;

	my $menu_list = $self->{config}->{menu_list};
	$menu_list =~ s/<>/\n/g;

	my $link_list = $self->{config}->{link_list};
	$link_list =~ s/<>/\n/g;

	my $sendmail_admin = $self->{config}->{sendmail_admin};
	$sendmail_admin =~ s/<>/\n/g;

	my $ping_list = $self->{config}->{ping_list};
	$ping_list =~ s/<>/\n/g;

	my $html_field_list = $self->{config}->{html_field_list};
	$html_field_list =~ s/<>/\n/g;

	my $js_title_field_list = $self->{config}->{js_title_field_list};
	$js_title_field_list =~ s/<>/\n/g;

	my $js_text_field_list = $self->{config}->{js_text_field_list};
	$js_text_field_list =~ s/<>/\n/g;

	my $black_list = $self->{config}->{black_list};
	$black_list =~ s/<>/\n/g;

	my $ng_word = $self->{config}->{ng_word};
	$ng_word =~ s/<>/\n/g;

	my $need_word = $self->{config}->{need_word};
	$need_word =~ s/<>/\n/g;

	my $black_list_tb = $self->{config}->{black_list_tb};
	$black_list_tb =~ s/<>/\n/g;

	my $ng_word_tb = $self->{config}->{ng_word_tb};
	$ng_word_tb =~ s/<>/\n/g;

	my $need_word_tb = $self->{config}->{need_word_tb};
	$need_word_tb =~ s/<>/\n/g;

	print $self->header;
	print $skin_ins->get_data('header');
	print $skin_ins->get_data('work_head');
	print $self->work_navi($skin_ins);
	print $skin_ins->get_data('work_foot');
	print $skin_ins->get_data('contents_head');

	my @envlist = (
		'env_basic',      '基本設定',
		'env_order',      '商品注文の設定',
		'env_mail',       '送信メールの設定',
		'env_customer',   '顧客管理の設定',
		'env_top',        'インデックスページの設定',
		'env_log',        'ログの表示設定',
		'env_rss',        'RSSの設定',
		'env_form',       '登録画面の表示設定',
		'env_default',    '登録商品の初期設定',
		'env_show',       '登録商品の表示設定',
		'env_rate',       '商品評価の設定',
		'env_rank',       '売り上げランキングの設定',
		'env_navigation', 'ナビゲーションの表示設定',
		'env_profile',    'プロフィールの設定',
		'env_user',       'ユーザー管理の設定',
		'env_sendmail',   'メール通知の設定',
		'env_ping',       '更新PINGの設定',
		'env_cookie',     'Cookieの設定',
		'env_html',       'HTMLファイル書き出しの設定',
		'env_js',         'JSファイル書き出しの設定',
		'env_access',     '投稿制限の設定'
	);

	print $skin_ins->get_data('envlist_head');
	my $i = 0;
	foreach (0 .. ($#envlist / 2)) {
		print $skin_ins->get_replace_data(
			'envlist',
			ENV_ID    => $envlist[$i++],
			ENV_TITLE => $envlist[$i++]
		);
	}
	print $skin_ins->get_data('envlist_foot');

	print $skin_ins->get_replace_data(
		'env_title',
		ENV_TITLE => '基本設定',
		ENV_ID    => 'env_basic'
	);
	print $skin_ins->get_data('env_head');
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'ショッピングページタイトル（<em>必ず設定してください</em>）',
		ENV_VALUE => "<input type=\"text\" name=\"env_site_title\" size=\"30\" value=\"$self->{config}->{site_title}\" />"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'ショッピングページからの戻り先',
		ENV_VALUE => "<input type=\"text\" name=\"env_back_url\" size=\"50\" value=\"$self->{config}->{back_url}\" style=\"ime-mode:disabled;\" />"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'サイトの概要',
		ENV_VALUE => "<input type=\"text\" name=\"env_site_description\" size=\"50\" value=\"$self->{config}->{site_description}\" />"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => "サイトのURL（<code>$info_path</code> を格納しているディレクトリのURL / <em>必ず設定してください</em>）",
		ENV_VALUE => "<input type=\"text\" name=\"env_site_url\" size=\"50\" value=\"$self->{config}->{site_url}\" />"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => "サイトのSSL用URL（<code>$info_path</code> を格納しているディレクトリのURL / SSL使用時に設定）",
		ENV_VALUE => "<input type=\"text\" name=\"env_ssl_url\" size=\"50\" value=\"$self->{config}->{ssl_url}\" />"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'Sendmailのパス（<em>必ず設定してください</em>）',
		ENV_VALUE => "<input type=\"text\" name=\"env_sendmail_path\" size=\"30\" value=\"$self->{config}->{sendmail_path}\" style=\"ime-mode:disabled;\" />"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'メールの通知先（改行区切りで複数指定可能 / <em>必ず設定してください</em>）',
		ENV_VALUE => "<textarea name=\"env_sendmail_list\" cols=\"50\" rows=\"5\" style=\"ime-mode:disabled;\">$sendmail_list</textarea>"
	);
	print $skin_ins->get_data('env_foot');

	print $skin_ins->get_replace_data(
		'env_title',
		ENV_TITLE => '商品注文の設定',
		ENV_ID    => 'env_order'
	);
	print $skin_ins->get_data('env_head');
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '選択可能な支払方法（支払方法と手数料をコンマ区切りで指定）',
		ENV_VALUE => "<textarea name=\"env_order_payment\" cols=\"30\" rows=\"5\">$order_payment</textarea>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '選択可能な配達方法（改行区切りで複数指定可能）',
		ENV_VALUE => "<textarea name=\"env_order_delivery\" cols=\"30\" rows=\"5\">$order_delivery</textarea>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '配達日時の指定',
		ENV_VALUE => "<select name=\"env_order_date_mode\" xml:lang=\"ja\" lang=\"ja\">$order_date_mode</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '選択可能な配達時間（改行区切りで複数指定可能）',
		ENV_VALUE => "<textarea name=\"env_order_time_list\" cols=\"30\" rows=\"5\">$order_time_list</textarea>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '選択可能な都道府県（都道府県と送料をコンマ区切りで指定 / 配達方法が複数ある場合、送料を『/』で区切ってすべて指定）',
		ENV_VALUE => "<textarea name=\"env_order_pref\" cols=\"30\" rows=\"10\">$order_pref</textarea>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '送料が無料になる商品金額（0にすると送料必須）',
		ENV_VALUE => "<input type=\"text\" name=\"env_order_free\" size=\"10\" value=\"$self->{config}->{order_free}\" style=\"ime-mode:disabled;\" />円"
	);
	print $skin_ins->get_data('env_foot');

	print $skin_ins->get_replace_data(
		'env_title',
		ENV_TITLE => '送信メールの設定',
		ENV_ID    => 'env_mail'
	);
	print $skin_ins->get_data('env_head');
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '商品ご注文時に送信するメールの件名',
		ENV_VALUE => "<input type=\"text\" name=\"env_sendmail_order_subj\" size=\"30\" value=\"$self->{config}->{sendmail_order_subj}\" />"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '商品ご注文時に送信するメールの内容',
		ENV_VALUE => "<textarea name=\"env_sendmail_order_body\" cols=\"50\" rows=\"5\">$sendmail_order_body</textarea>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'お客様情報登録時に送信するメールの件名（顧客管理機能利用時）',
		ENV_VALUE => "<input type=\"text\" name=\"env_sendmail_regist_subj\" size=\"30\" value=\"$self->{config}->{sendmail_regist_subj}\" />"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'お客様情報登録時に送信するメールの内容（顧客管理機能利用時）',
		ENV_VALUE => "<textarea name=\"env_sendmail_regist_body\" cols=\"50\" rows=\"5\">$sendmail_regist_body</textarea>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'お客様情報変更時に送信するメールの件名（顧客管理機能利用時）',
		ENV_VALUE => "<input type=\"text\" name=\"env_sendmail_edit_subj\" size=\"30\" value=\"$self->{config}->{sendmail_edit_subj}\" />"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'お客様情報変更時に送信するメールの内容（顧客管理機能利用時）',
		ENV_VALUE => "<textarea name=\"env_sendmail_edit_body\" cols=\"50\" rows=\"5\">$sendmail_edit_body</textarea>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'お客様情報削除時に送信するメールの件名（顧客管理機能利用時）',
		ENV_VALUE => "<input type=\"text\" name=\"env_sendmail_delete_subj\" size=\"30\" value=\"$self->{config}->{sendmail_delete_subj}\" />"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'お客様情報削除時に送信するメールの内容（顧客管理機能利用時）',
		ENV_VALUE => "<textarea name=\"env_sendmail_delete_body\" cols=\"50\" rows=\"5\">$sendmail_delete_body</textarea>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'パスワード再発行時に送信するメールの件名（顧客管理機能利用時）',
		ENV_VALUE => "<input type=\"text\" name=\"env_sendmail_pwd_subj\" size=\"30\" value=\"$self->{config}->{sendmail_pwd_subj}\" />"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'パスワード再発行時に送信するメールの内容（顧客管理機能利用時）',
		ENV_VALUE => "<textarea name=\"env_sendmail_pwd_body\" cols=\"50\" rows=\"5\">$sendmail_pwd_body</textarea>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '各メールの送信者名',
		ENV_VALUE => "<input type=\"text\" name=\"env_sendmail_name\" size=\"30\" value=\"$self->{config}->{sendmail_name}\" />"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '各メールの送信元アドレス',
		ENV_VALUE => "<input type=\"text\" name=\"env_sendmail_addr\" size=\"30\" value=\"$self->{config}->{sendmail_addr}\" />"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '各メールに挿入する署名',
		ENV_VALUE => "<textarea name=\"env_sendmail_signature\" cols=\"50\" rows=\"5\">$sendmail_signature</textarea>"
	);
	print $skin_ins->get_data('env_foot');

	print $skin_ins->get_replace_data(
		'env_title',
		ENV_TITLE => '顧客管理の設定',
		ENV_ID    => 'env_customer'
	);
	print $skin_ins->get_data('env_head');
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '顧客管理機能',
		ENV_VALUE => "<select name=\"env_customer_mode\" xml:lang=\"ja\" lang=\"ja\">$customer_mode</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'ポイントシステム（顧客管理機能利用時）',
		ENV_VALUE => "<select name=\"env_point_mode\" xml:lang=\"ja\" lang=\"ja\">$point_mode</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'ポイントを付加する価格',
		ENV_VALUE => "<input type=\"text\" name=\"env_point_price\" size=\"10\" value=\"$self->{config}->{point_price}\" style=\"ime-mode:disabled;\" />円"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '付加するポイント数',
		ENV_VALUE => "<input type=\"text\" name=\"env_point_value\" size=\"10\" value=\"$self->{config}->{point_value}\" style=\"ime-mode:disabled;\" />ポイント"
	);
	print $skin_ins->get_data('env_foot');

	print $skin_ins->get_replace_data(
		'env_title',
		ENV_TITLE => 'インデックスページの設定',
		ENV_ID    => 'env_top'
	);
	print $skin_ins->get_data('env_head');
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'インデックスページに表示する商品の件数',
		ENV_VALUE => "<input type=\"text\" name=\"env_top_size\" size=\"5\" value=\"$self->{config}->{top_size}\" style=\"ime-mode:disabled;\" />件"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '表示分割件数',
		ENV_VALUE => "<input type=\"text\" name=\"env_top_delimiter_size\" size=\"5\" value=\"$self->{config}->{top_delimiter_size}\" style=\"ime-mode:disabled;\" />件ずつ"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'インデックスページに表示する記事の分類（改行区切りで複数指定可能 / 空欄にするとすべて表示）',
		ENV_VALUE => "<textarea name=\"env_top_field_list\" cols=\"30\" rows=\"5\">$top_field_list</textarea>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'インデックスページの表示方法',
		ENV_VALUE => "<select name=\"env_top_field\" xml:lang=\"ja\" lang=\"ja\">$top_field</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'インデックスページテキストの表示',
		ENV_VALUE => "<select name=\"env_top_text\" xml:lang=\"ja\" lang=\"ja\">$top_text</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'インデックスページテキストの改行の変換',
		ENV_VALUE => "<select name=\"env_top_break\" xml:lang=\"ja\" lang=\"ja\">$top_break</select>"
	);
	print $skin_ins->get_data('env_foot');

	print $skin_ins->get_replace_data(
		'env_title',
		ENV_TITLE => 'ログの表示設定',
		ENV_ID    => 'env_log'
	);
	print $skin_ins->get_data('env_head');
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '商品一覧での表示順',
		ENV_VALUE => "<select name=\"env_data_sort\" xml:lang=\"ja\" lang=\"ja\">$data_sort</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '商品一覧での商品名の表示サイズ',
		ENV_VALUE => "<input type=\"text\" name=\"env_subj_length\" size=\"5\" value=\"$self->{config}->{subj_length}\" style=\"ime-mode:disabled;\" />byte"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '1ページの商品表示件数',
		ENV_VALUE => "<input type=\"text\" name=\"env_page_size\" size=\"5\" value=\"$self->{config}->{page_size}\" style=\"ime-mode:disabled;\" />件"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'ページナビゲーションの表示ページ数（0にすると非表示）',
		ENV_VALUE => "<input type=\"text\" name=\"env_navi_size\" size=\"5\" value=\"$self->{config}->{navi_size}\" style=\"ime-mode:disabled;\" />ページまで"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '新着商品の表示件数（0にすると非表示）',
		ENV_VALUE => "<input type=\"text\" name=\"env_list_size\" size=\"5\" value=\"$self->{config}->{list_size}\" style=\"ime-mode:disabled;\" />件"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '新着コメントの表示件数（0にすると非表示）',
		ENV_VALUE => "<input type=\"text\" name=\"env_cmtlist_size\" size=\"5\" value=\"$self->{config}->{cmtlist_size}\" style=\"ime-mode:disabled;\" />件"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '新着トラックバックの表示件数（0にすると非表示）',
		ENV_VALUE => "<input type=\"text\" name=\"env_tblist_size\" size=\"5\" value=\"$self->{config}->{tblist_size}\" style=\"ime-mode:disabled;\" />件"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '1ページの商品表示件数（管理モード時）',
		ENV_VALUE => "<input type=\"text\" name=\"env_admin_size\" size=\"5\" value=\"$self->{config}->{admin_size}\" style=\"ime-mode:disabled;\" />件"
	);
	print $skin_ins->get_data('env_foot');

	print $skin_ins->get_replace_data(
		'env_title',
		ENV_TITLE => 'RSSの設定',
		ENV_ID    => 'env_rss'
	);
	print $skin_ins->get_data('env_head');
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'RSSの配信内容',
		ENV_VALUE => "<select name=\"env_rss_mode\" xml:lang=\"ja\" lang=\"ja\">$rss_mode</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '概要の配信サイズ',
		ENV_VALUE => "<input type=\"text\" name=\"env_rss_length\" size=\"5\" value=\"$self->{config}->{rss_length}\" style=\"ime-mode:disabled;\" />byte"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'RSSの配信件数',
		ENV_VALUE => "<input type=\"text\" name=\"env_rss_size\" size=\"5\" value=\"$self->{config}->{rss_size}\" style=\"ime-mode:disabled;\" />件"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '配信する記事の分類（改行区切りで複数指定可能 / 空欄にするとすべて配信）',
		ENV_VALUE => "<textarea name=\"env_rss_field_list\" cols=\"30\" rows=\"5\">$rss_field_list</textarea>"
	);
	print $skin_ins->get_data('env_foot');

	print $skin_ins->get_replace_data(
		'env_title',
		ENV_TITLE => '登録画面の表示設定',
		ENV_ID    => 'env_form'
	);
	print $skin_ins->get_data('env_head');
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '分類選択項目の表示',
		ENV_VALUE => "<select name=\"env_use_field\" xml:lang=\"ja\" lang=\"ja\">$use_field</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'アイコン選択項目の表示',
		ENV_VALUE => "<select name=\"env_use_icon\" xml:lang=\"ja\" lang=\"ja\">$use_icon</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'ミニ画像選択項目の表示',
		ENV_VALUE => "<select name=\"env_use_image\" xml:lang=\"ja\" lang=\"ja\">$use_image</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'ファイル選択項目の表示',
		ENV_VALUE => "<select name=\"env_use_file\" xml:lang=\"ja\" lang=\"ja\">$use_file</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'アップロードできるファイルの最大数',
		ENV_VALUE => "<input type=\"text\" name=\"env_max_file\" size=\"5\" value=\"$self->{config}->{max_file}\" style=\"ime-mode:disabled;\" />個"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '在庫数入力項目の表示',
		ENV_VALUE => "<select name=\"env_use_stock\" xml:lang=\"ja\" lang=\"ja\">$use_stock</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '関連商品入力項目の表示',
		ENV_VALUE => "<select name=\"env_use_relate\" xml:lang=\"ja\" lang=\"ja\">$use_relate</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'トラックバックURL入力項目の表示',
		ENV_VALUE => "<select name=\"env_use_tburl\" xml:lang=\"ja\" lang=\"ja\">$use_tburl</select>"
	);
	print $skin_ins->get_data('env_foot');

	print $skin_ins->get_replace_data(
		'env_title',
		ENV_TITLE => '登録商品の初期設定',
		ENV_ID    => 'env_default'
	);
	print $skin_ins->get_data('env_head');
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '商品の公開',
		ENV_VALUE => "<select name=\"env_default_stat\" xml:lang=\"ja\" lang=\"ja\">$default_stat</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '改行の変換',
		ENV_VALUE => "<select name=\"env_default_break\" xml:lang=\"ja\" lang=\"ja\">$default_break</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'ファイルの表示方法',
		ENV_VALUE => "<select name=\"env_default_view\" xml:lang=\"ja\" lang=\"ja\">$default_view</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'コメントの受付',
		ENV_VALUE => "<select name=\"env_default_comt\" xml:lang=\"ja\" lang=\"ja\">$default_comt</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'トラックバックの受付',
		ENV_VALUE => "<select name=\"env_default_tb\" xml:lang=\"ja\" lang=\"ja\">$default_tb</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '受信コメントの初期状態',
		ENV_VALUE => "<select name=\"env_comt_stat\" xml:lang=\"ja\" lang=\"ja\">$comt_stat</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '受信トラックバックの初期状態',
		ENV_VALUE => "<select name=\"env_tb_stat\" xml:lang=\"ja\" lang=\"ja\">$tb_stat</select>"
	);
	print $skin_ins->get_data('env_foot');

	print $skin_ins->get_replace_data(
		'env_title',
		ENV_TITLE => '登録商品の表示設定',
		ENV_ID    => 'env_show'
	);
	print $skin_ins->get_data('env_head');
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '各商品ページでのタイトル表記',
		ENV_VALUE => "<select name=\"env_title_mode\" xml:lang=\"ja\" lang=\"ja\">$title_mode</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '空行を段落に変換',
		ENV_VALUE => "<select name=\"env_paragraph_mode\" xml:lang=\"ja\" lang=\"ja\">$paragraph_mode</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '文中のURLとメールアドレスに自動的にリンク',
		ENV_VALUE => "<select name=\"env_autolink_mode\" xml:lang=\"ja\" lang=\"ja\">$autolink_mode</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '自動リンク時に付加する属性',
		ENV_VALUE => "<input type=\"text\" name=\"env_autolink_attribute\" size=\"30\" value=\"$self->{config}->{autolink_attribute}\" />"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '省略紹介文表示用リンクのテキスト',
		ENV_VALUE => "<input type=\"text\" name=\"env_continue_text\" size=\"30\" value=\"$self->{config}->{continue_text}\" />"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '新着マーク表示日数',
		ENV_VALUE => "<input type=\"text\" name=\"env_new_days\" size=\"5\" value=\"$self->{config}->{new_days}\" style=\"ime-mode:disabled;\" />日間"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '本文の装飾',
		ENV_VALUE => "<select name=\"env_decoration_mode\" xml:lang=\"ja\" lang=\"ja\">$decoration_mode</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'アップロードファイルの最大表示横幅',
		ENV_VALUE => "<input type=\"text\" name=\"env_img_maxwidth\" size=\"5\" value=\"$self->{config}->{img_maxwidth}\" style=\"ime-mode:disabled;\" />px"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'サムネイル専用画像を自動作成（<em>ImageMagickかrepng2jpegが必須</em>）',
		ENV_VALUE => "<select name=\"env_thumbnail_mode\" xml:lang=\"ja\" lang=\"ja\">$thumbnail_mode</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'アップロードファイルへのリンク時に付加する属性',
		ENV_VALUE => "<input type=\"text\" name=\"env_file_attribute\" size=\"30\" value=\"$self->{config}->{file_attribute}\" />"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '引用文字色',
		ENV_VALUE => "<input type=\"text\" name=\"env_quotation_color\" size=\"10\" value=\"$self->{config}->{quotation_color}\" style=\"ime-mode:disabled;\" />"
	);
	print $skin_ins->get_data('env_foot');

	print $skin_ins->get_replace_data(
		'env_title',
		ENV_TITLE => '商品評価の設定',
		ENV_ID    => 'env_rate'
	);
	print $skin_ins->get_data('env_head');
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '商品評価機能',
		ENV_VALUE => "<select name=\"env_rate_mode\" xml:lang=\"ja\" lang=\"ja\">$rate_mode</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '商品評価情報（得点と説明をコンマ区切りで指定）',
		ENV_VALUE => "<textarea name=\"env_rate_info\" cols=\"30\" rows=\"10\">$rate_info</textarea>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '評価一覧ページでの表示件数',
		ENV_VALUE => "<input type=\"text\" name=\"env_rate_size\" size=\"5\" value=\"$self->{config}->{rate_size}\" style=\"ime-mode:disabled;\" />件"
	);
	print $skin_ins->get_data('env_foot');

	print $skin_ins->get_replace_data(
		'env_title',
		ENV_TITLE => '売り上げランキングの設定',
		ENV_ID    => 'env_rank'
	);
	print $skin_ins->get_data('env_head');
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '売り上げランキング機能',
		ENV_VALUE => "<select name=\"env_rank_mode\" xml:lang=\"ja\" lang=\"ja\">$rank_mode</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '売り上げランキングの表示件数',
		ENV_VALUE => "<input type=\"text\" name=\"env_rank_size\" size=\"5\" value=\"$self->{config}->{rank_size}\" style=\"ime-mode:disabled;\" />件"
	);
	print $skin_ins->get_data('env_foot');

	print $skin_ins->get_replace_data(
		'env_title',
		ENV_TITLE => 'ナビゲーションの表示設定',
		ENV_ID    => 'env_navigation'
	);
	print $skin_ins->get_data('env_head');
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'カレンダーの表示',
		ENV_VALUE => "<select name=\"env_show_calendar\" xml:lang=\"ja\" lang=\"ja\">$show_calendar</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '分類一覧の表示',
		ENV_VALUE => "<select name=\"env_show_field\" xml:lang=\"ja\" lang=\"ja\">$show_field</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '検索フォームの表示',
		ENV_VALUE => "<select name=\"env_show_search\" xml:lang=\"ja\" lang=\"ja\">$show_search</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '過去ログ一覧の表示',
		ENV_VALUE => "<select name=\"env_show_past\" xml:lang=\"ja\" lang=\"ja\">$show_past</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'コンテンツの表示',
		ENV_VALUE => "<select name=\"env_show_menu\" xml:lang=\"ja\" lang=\"ja\">$show_menu</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'コンテンツの分類（改行区切りで複数指定可能）',
		ENV_VALUE => "<textarea name=\"env_menu_list\" cols=\"30\" rows=\"5\">$menu_list</textarea>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'リンク集の表示',
		ENV_VALUE => "<select name=\"env_show_link\" xml:lang=\"ja\" lang=\"ja\">$show_link</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'リンク集の分類（改行区切りで複数指定可能）',
		ENV_VALUE => "<textarea name=\"env_link_list\" cols=\"30\" rows=\"5\">$link_list</textarea>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '日時別ページでのナビゲーション表示',
		ENV_VALUE => "<select name=\"env_date_navigation\" xml:lang=\"ja\" lang=\"ja\">$date_navigation</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '分類別ページでのナビゲーション表示',
		ENV_VALUE => "<select name=\"env_field_navigation\" xml:lang=\"ja\" lang=\"ja\">$field_navigation</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '各商品ページでのナビゲーション表示（JavaScriptが必要）',
		ENV_VALUE => "<select name=\"env_show_navigation\" xml:lang=\"ja\" lang=\"ja\">$show_navigation</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'ナビゲーションの表示位置',
		ENV_VALUE => "<select name=\"env_pos_navigation\" xml:lang=\"ja\" lang=\"ja\">$pos_navigation</select>"
	);
	print $skin_ins->get_data('env_foot');

	print $skin_ins->get_replace_data(
		'env_title',
		ENV_TITLE => 'プロフィールの設定',
		ENV_ID    => 'env_profile'
	);
	print $skin_ins->get_data('env_head');
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'プロフィールの管理',
		ENV_VALUE => "<select name=\"env_profile_mode\" xml:lang=\"ja\" lang=\"ja\">$profile_mode</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'プロフィールテキストの改行の変換',
		ENV_VALUE => "<select name=\"env_profile_break\" xml:lang=\"ja\" lang=\"ja\">$profile_break</select>"
	);
	print $skin_ins->get_data('env_foot');

	print $skin_ins->get_replace_data(
		'env_title',
		ENV_TITLE => 'ユーザー管理の設定',
		ENV_ID    => 'env_user'
	);
	print $skin_ins->get_data('env_head');
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '複数ユーザーの管理',
		ENV_VALUE => "<select name=\"env_user_mode\" xml:lang=\"ja\" lang=\"ja\">$user_mode</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'ゲストユーザーに許可する操作',
		ENV_VALUE => "$auth_comment<br />$auth_trackback<br />$auth_customer<br />$auth_field<br />$auth_icon<br />$auth_option<br />$auth_top<br />$auth_menu<br />$auth_link"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '管理機能操作履歴の保存件数',
		ENV_VALUE => "<input type=\"text\" name=\"env_record_size\" size=\"5\" value=\"$self->{config}->{record_size}\" style=\"ime-mode:disabled;\" />件"
	);
	print $skin_ins->get_data('env_foot');

	print $skin_ins->get_replace_data(
		'env_title',
		ENV_TITLE => 'メール通知の設定',
		ENV_ID    => 'env_sendmail'
	);
	print $skin_ins->get_data('env_head');
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'コメントを受信するとメールで通知',
		ENV_VALUE => "<select name=\"env_sendmail_cmt_mode\" xml:lang=\"ja\" lang=\"ja\">$sendmail_cmt_mode</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'トラックバックを受信するとメールで通知',
		ENV_VALUE => "<select name=\"env_sendmail_tb_mode\" xml:lang=\"ja\" lang=\"ja\">$sendmail_tb_mode</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '在庫数が少なくなるとメールで通知',
		ENV_VALUE => "<select name=\"env_sendmail_stock_mode\" xml:lang=\"ja\" lang=\"ja\">$sendmail_stock_mode</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '通知しないコメント投稿者名（改行区切りで複数指定可能）',
		ENV_VALUE => "<textarea name=\"env_sendmail_admin\" cols=\"30\" rows=\"5\">$sendmail_admin</textarea>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '通知する投稿内容の最大量（超えた分は省略表示）',
		ENV_VALUE => "<input type=\"text\" name=\"env_sendmail_length\" size=\"5\" value=\"$self->{config}->{sendmail_length}\" style=\"ime-mode:disabled;\" />byte"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '受信商品ページのURLと投稿者情報の通知',
		ENV_VALUE => "<select name=\"env_sendmail_detail\" xml:lang=\"ja\" lang=\"ja\">$sendmail_detail</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '通知する在庫数（注文時にこの数を下回った商品を通知）',
		ENV_VALUE => "<input type=\"text\" name=\"env_sendmail_stock_size\" size=\"5\" value=\"$self->{config}->{sendmail_stock_size}\" style=\"ime-mode:disabled;\" />"
	);
	print $skin_ins->get_data('env_foot');

	print $skin_ins->get_replace_data(
		'env_title',
		ENV_TITLE => '更新PINGの設定',
		ENV_ID    => 'env_ping'
	);
	print $skin_ins->get_data('env_head');
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '更新PINGの送信',
		ENV_VALUE => "<select name=\"env_ping_mode\" xml:lang=\"ja\" lang=\"ja\">$ping_mode</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '更新PINGの通知先（改行区切りで複数指定可能）',
		ENV_VALUE => "<textarea name=\"env_ping_list\" cols=\"50\" rows=\"5\" style=\"ime-mode:disabled;\">$ping_list</textarea>"
	);
	print $skin_ins->get_data('env_foot');

	print $skin_ins->get_replace_data(
		'env_title',
		ENV_TITLE => 'Cookieの設定',
		ENV_ID    => 'env_cookie'
	);
	print $skin_ins->get_data('env_head');
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '投稿者情報Cookieの識別名',
		ENV_VALUE => "<input type=\"text\" name=\"env_cookie_id\" size=\"30\" value=\"$self->{config}->{cookie_id}\" style=\"ime-mode:disabled;\" />"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'カート情報Cookieの識別名',
		ENV_VALUE => "<input type=\"text\" name=\"env_cookie_cart\" size=\"30\" value=\"$self->{config}->{cookie_cart}\" style=\"ime-mode:disabled;\" />"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'お客様情報Cookieの識別名',
		ENV_VALUE => "<input type=\"text\" name=\"env_cookie_customer\" size=\"30\" value=\"$self->{config}->{cookie_customer}\" style=\"ime-mode:disabled;\" />"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'Cookieの保存日数',
		ENV_VALUE => "<input type=\"text\" name=\"env_cookie_holddays\" size=\"5\" value=\"$self->{config}->{cookie_holddays}\" style=\"ime-mode:disabled;\" />日間"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'ログイン情報Cookieの識別名',
		ENV_VALUE => "<input type=\"text\" name=\"env_cookie_admin\" size=\"30\" value=\"$self->{config}->{cookie_admin}\" style=\"ime-mode:disabled;\" />"
	);
	print $skin_ins->get_data('env_foot');

	print $skin_ins->get_replace_data(
		'env_title',
		ENV_TITLE => 'HTMLファイル書き出しの設定',
		ENV_ID    => 'env_html'
	);
	print $skin_ins->get_data('env_head');
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'トップページをHTMLファイルに書き出し',
		ENV_VALUE => "<select name=\"env_html_index_mode\" xml:lang=\"ja\" lang=\"ja\">$html_index_mode</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '各商品ページをHTMLファイルに書き出し',
		ENV_VALUE => "<select name=\"env_html_archive_mode\" xml:lang=\"ja\" lang=\"ja\">$html_archive_mode</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '各分類をHTMLファイルに書き出し',
		ENV_VALUE => "<select name=\"env_html_field_mode\" xml:lang=\"ja\" lang=\"ja\">$html_field_mode</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '書き出す分類の設定（書き出し先と分類をコンマ区切りで指定）',
		ENV_VALUE => "<textarea name=\"env_html_field_list\" cols=\"50\" rows=\"5\">$html_field_list</textarea>"
	);
	print $skin_ins->get_data('env_foot');

	print $skin_ins->get_replace_data(
		'env_title',
		ENV_TITLE => 'JSファイル書き出しの設定',
		ENV_ID    => 'env_js'
	);
	print $skin_ins->get_data('env_head');
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '商品名一覧をJSファイルに書き出し',
		ENV_VALUE => "<select name=\"env_js_title_mode\" xml:lang=\"ja\" lang=\"ja\">$js_title_mode</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '商品名一覧を分類別に書き出し',
		ENV_VALUE => "<select name=\"env_js_title_field_mode\" xml:lang=\"ja\" lang=\"ja\">$js_title_field_mode</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '書き出す分類の設定（書き出し先と分類をコンマ区切りで指定）',
		ENV_VALUE => "<textarea name=\"env_js_title_field_list\" cols=\"50\" rows=\"5\">$js_title_field_list</textarea>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '商品名一覧をJSファイルに書き出す件数',
		ENV_VALUE => "<input type=\"text\" name=\"env_js_title_size\" size=\"5\" value=\"$self->{config}->{js_title_size}\" style=\"ime-mode:disabled;\" />件"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '最近の商品をJSファイルに書き出し',
		ENV_VALUE => "<select name=\"env_js_text_mode\" xml:lang=\"ja\" lang=\"ja\">$js_text_mode</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '最近の商品を分類別に書き出し',
		ENV_VALUE => "<select name=\"env_js_text_field_mode\" xml:lang=\"ja\" lang=\"ja\">$js_text_field_mode</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '書き出す分類の設定（書き出し先と分類をコンマ区切りで指定）',
		ENV_VALUE => "<textarea name=\"env_js_text_field_list\" cols=\"50\" rows=\"5\">$js_text_field_list</textarea>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '最近の商品をJSファイルに書き出す件数',
		ENV_VALUE => "<input type=\"text\" name=\"env_js_text_size\" size=\"5\" value=\"$self->{config}->{js_text_size}\" style=\"ime-mode:disabled;\" />件"
	);
	print $skin_ins->get_data('env_foot');

	print $skin_ins->get_replace_data(
		'env_title',
		ENV_TITLE => '投稿制限の設定',
		ENV_ID    => 'env_access'
	);
	print $skin_ins->get_data('env_head');
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '自サイトのURL(このURLを含まないサイトからの投稿を拒否)',
		ENV_VALUE => "<input type=\"text\" name=\"env_base_url\" size=\"50\" value=\"$self->{config}->{base_url}\" style=\"ime-mode:disabled;\" />"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => '投稿制限対象ホスト（改行区切りで複数指定可能）',
		ENV_VALUE => "<textarea name=\"env_black_list\" cols=\"30\" rows=\"5\" style=\"ime-mode:disabled;\">$black_list</textarea>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'Proxy経由の投稿',
		ENV_VALUE => "<select name=\"env_proxy_mode\" xml:lang=\"ja\" lang=\"ja\">$proxy_mode</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'コメントの投稿禁止ワード（改行区切りで複数指定可能）',
		ENV_VALUE => "<textarea name=\"env_ng_word\" cols=\"30\" rows=\"5\">$ng_word</textarea>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'コメントの投稿必須ワード（改行区切りで複数指定可能）',
		ENV_VALUE => "<textarea name=\"env_need_word\" cols=\"30\" rows=\"5\">$need_word</textarea>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'コメントでの日本語の利用',
		ENV_VALUE => "<select name=\"env_need_japanese\" xml:lang=\"ja\" lang=\"ja\">$need_japanese</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'コメントに記述できるURLの最大数（0にすると無制限）',
		ENV_VALUE => "<input type=\"text\" name=\"env_max_link\" size=\"5\" value=\"$self->{config}->{max_link}\" style=\"ime-mode:disabled;\" />個"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'コメントの連続投稿を拒否する時間',
		ENV_VALUE => "<input type=\"text\" name=\"env_wait_time\" size=\"5\" value=\"$self->{config}->{wait_time}\" style=\"ime-mode:disabled;\" />秒"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'トラックバックの投稿制限対象サイト（改行区切りで複数指定可能）',
		ENV_VALUE => "<textarea name=\"env_black_list_tb\" cols=\"30\" rows=\"5\" style=\"ime-mode:disabled;\">$black_list_tb</textarea>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'トラックバックの投稿禁止ワード（改行区切りで複数指定可能）',
		ENV_VALUE => "<textarea name=\"env_ng_word_tb\" cols=\"30\" rows=\"5\">$ng_word_tb</textarea>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'トラックバックの投稿必須ワード（改行区切りで複数指定可能）',
		ENV_VALUE => "<textarea name=\"env_need_word_tb\" cols=\"30\" rows=\"5\">$need_word_tb</textarea>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'トラックバックでの日本語の利用',
		ENV_VALUE => "<select name=\"env_need_japanese_tb\" xml:lang=\"ja\" lang=\"ja\">$need_japanese_tb</select>"
	);
	print $skin_ins->get_replace_data(
		'env',
		ENV_TITLE => 'トラックバックでの引用リンク',
		ENV_VALUE => "<select name=\"env_need_link_tb\" xml:lang=\"ja\" lang=\"ja\">$need_link_tb</select>"
	);
	print $skin_ins->get_data('env_foot');

	print $skin_ins->get_data('contents_foot');
	print $skin_ins->get_data('navi');
	print $skin_ins->get_data('footer');

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

### ユーザー管理画面表示
sub output_user {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	if ($self->get_authority ne 'root') {
		$self->error('この操作を行う権限が与えられていません。');
	}

	if (!$self->{message}) {
		$self->{message} = '管理機能を利用するユーザーを設定する事ができます。';
	}

	my $skin_ins = new webliberty::Skin;
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_header}", available => 'header');
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_work}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_user}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_navi}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_footer}", available => 'footer');

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});

	$skin_ins->replace_skin(
		$catalog_ins->info,
		%{$self->{plugin}},
		INFO_MESSAGE => $self->{message}
	);

	print $self->header;
	print $skin_ins->get_data('header');
	print $skin_ins->get_data('work_head');
	print $self->work_navi($skin_ins);
	print $skin_ins->get_data('work_foot');
	print $skin_ins->get_replace_data(
		'contents',
		FORM_AUTHORITY => '<option value="guest">ゲストユーザー</option><option value="root">システム管理者</option>'
	);
	print $skin_ins->get_data('user_head');

	my $i = 0;

	open(FH, $self->{init}->{data_user}) or $self->error("Read Error : $self->{init}->{data_user}");
	while (<FH>) {
		chomp;
		my($user, $pwd, $authority) = split(/\t/);

		my $authority_list;
		if ($authority eq 'root') {
			$authority_list = '<option value="guest">ゲストユーザー</option><option value="root" selected="selected">システム管理者</option>';
		} else {
			$authority_list = '<option value="guest" selected="selected">ゲストユーザー</option><option value="root">システム管理者</option>';
		}
		$authority_list = "<select name=\"authority$i\" xml:lang=\"ja\" lang=\"ja\">$authority_list</select>";

		print $skin_ins->get_replace_data(
			'user',
			USER_NAME      => $user,
			USER_PWD       => $pwd,
			USER_AUTHORITY => $authority_list,
			USER_NO        => $i
		);

		$i++;
	}
	close(FH);

	print $skin_ins->get_data('user_foot');
	print $skin_ins->get_data('navi');
	print $skin_ins->get_data('footer');

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

### 再構築設定表示
sub output_build {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	if (!$self->{message}) {
		$self->{message} = '作業内容を選択し、<em>実行ボタン</em>を押してください。';
	}

	my $skin_ins = new webliberty::Skin;
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_header}", available => 'header');
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_work}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_build}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_navi}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_footer}", available => 'footer');

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});

	$skin_ins->replace_skin(
		$catalog_ins->info,
		%{$self->{plugin}},
		INFO_MESSAGE => $self->{message}
	);

	open(FH, $self->{init}->{data_catalog_index}) or $self->error("Read Error : $self->{init}->{data_catalog_index}");
	my @index = <FH>;
	close(FH);

	my @numbers = map { (split(/\t/))[1] } @index;
	@index = @index[sort { $numbers[$b] <=> $numbers[$a] } (0 .. $#numbers)];
	my $max_no = (split(/\t/, $index[0]))[1];

	my $form_list = "<option value=\"all\">すべてを構築</option>";
	if ($self->{config}->{html_index_mode}) {
		$form_list .= "<option value=\"index\">インデックスを構築</option>";
	}
	if ($self->{config}->{html_archive_mode}) {
		foreach (0 .. int(($max_no - 1) / 50)) {
			my $from = $_ * 50 + 1;
			my $to;
			if ($_ == int(($max_no - 1) / 50)) {
				$to = $from + ($max_no - 1) % 50;
			} else {
				$to = $from + 50 - 1;
			}
			$form_list .= "<option value=\"$from\">アーカイブ（No.$from～No.$to）を構築</option>";
		}
	}

	print $self->header;
	print $skin_ins->get_data('header');
	print $skin_ins->get_data('work_head');
	print $self->work_navi($skin_ins);
	print $skin_ins->get_data('work_foot');
	print $skin_ins->get_replace_data(
		'contents',
		FORM_LIST => $form_list
	);
	print $skin_ins->get_data('navi');
	print $skin_ins->get_data('footer');

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

### 操作履歴表示
sub output_record {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}


	if ($self->get_authority ne 'root') {
		$self->error('この操作を行う権限が与えられていません。');
	}

	my $skin_ins = new webliberty::Skin;
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_header}", available => 'header');
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_work}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_record}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_navi}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_footer}", available => 'footer');

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});

	$skin_ins->replace_skin(
		$catalog_ins->info,
		%{$self->{plugin}},
		INFO_MESSAGE => $self->{message}
	);

	$self->{html}->{header}    = $skin_ins->get_data('header');
	$self->{html}->{work_head} = $skin_ins->get_data('work_head');
	$self->{html}->{work}      = $self->work_navi($skin_ins);
	$self->{html}->{work_foot} = $skin_ins->get_data('work_foot');
	$self->{html}->{contents}  = $skin_ins->get_data('contents');

	$self->{html}->{record_head} = $skin_ins->get_data('record_head');

	my($record_size, $i);

	my $record_start = $self->{config}->{admin_size} * $self->{query}->{page};
	my $record_end   = $record_start + $self->{config}->{admin_size};

	open(FH, $self->{init}->{data_record}) or $self->error("Read Error : $self->{init}->{data_record}");
	while (<FH>) {
		$record_size++;
	}
	seek(FH, 0, 0);
	while (<FH>) {
		chomp;
		my($date, $user, $text, $host) = split(/\t/);

		$i++;
		if ($i <= $record_start) {
			next;
		} elsif ($i > $record_end) {
			last;
		}

		my($sec, $min, $hour, $day, $mon, $year, $week) = localtime($date);

		my $record_year   = sprintf("%02d", $year + 1900);
		my $record_month  = sprintf("%02d", $mon + 1);
		my $record_day    = sprintf("%02d", $day);
		my $record_hour   = sprintf("%02d", $hour);
		my $record_minute = sprintf("%02d", $min);
		my $record_week   = ${$self->{init}->{weeks}}[$week];

		$date = "$record_year年$record_month月$record_day日($record_week)$record_hour時$record_minute分";

		$self->{html}->{record} .= $skin_ins->get_replace_data(
			'record',
			RECORD_DATE   => $date,
			RECORD_YEAR   => $record_year,
			RECORD_MONTH  => $record_month,
			RECORD_DAY    => $record_day,
			RECORD_HOUR   => $record_hour,
			RECORD_MINUTE => $record_minute,
			RECORD_WEEK   => $record_week,
			RECORD_USER   => $user,
			RECORD_TEXT   => $text,
			RECORD_HOST   => $host
		);
	}
	close(FH);

	$self->{html}->{record_foot} = $skin_ins->get_data('record_foot');

	my $page_list;
	foreach (0 .. int(($record_size - 1) / $self->{config}->{admin_size})) {
		if ($_ == $self->{query}->{page}) {
			$page_list .= "<option value=\"$_\" selected=\"selected\">ページ" . ($_ + 1) . "</option>";
		} else {
			$page_list .= "<option value=\"$_\">ページ" . ($_ + 1) . "</option>";
		}
	}
	$self->{html}->{page} = $skin_ins->get_replace_data(
		'page',
		PAGE_LIST => $page_list
	);

	$self->{html}->{navi}   = $skin_ins->get_data('navi');
	$self->{html}->{footer} = $skin_ins->get_data('footer');

	print $self->header;
	foreach ($skin_ins->get_list) {
		print $self->{html}->{$_};
	}

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

### ステータス画面
sub output_status {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	my($catalog_size, $catalog_show_size, $catalog_hidden_size);

	open(FH, $self->{init}->{data_catalog_index}) or $self->error("Read Error : $self->{init}->{data_catalog_index}");
	while (<FH>) {
		chomp;
		my($date, $no, $id, $stat, $field, $name) = split(/\t/);

		if ($stat) {
			$catalog_show_size++;
		} else {
			$catalog_hidden_size++;
		}
		$catalog_size++;
	}
	close(FH);

	my($comt_size, $comt_show_size, $comt_hidden_size);

	open(FH, $self->{init}->{data_comt_index}) or $self->error("Read Error : $self->{init}->{data_comt_index}");
	while (<FH>) {
		chomp;
		my($no, $pno, $stat, $date, $name, $subj, $host) = split(/\t/);

		if ($stat) {
			$comt_show_size++;
		} else {
			$comt_hidden_size++;
		}
		$comt_size++;
	}
	close(FH);

	my($tb_size, $tb_show_size, $tb_hidden_size);

	open(FH, $self->{init}->{data_tb_index}) or $self->error("Read Error : $self->{init}->{data_tb_index}");
	while (<FH>) {
		chomp;
		my($no, $pno, $stat, $date, $blog, $title, $url) = split(/\t/);

		if ($stat) {
			$tb_show_size++;
		} else {
			$tb_hidden_size++;
		}
		$tb_size++;
	}
	close(FH);

	my($catalog_file_size, $comt_file_size, $tb_file_size, $upfile_file_size, $archive_file_size);

	opendir(DIR, $self->{init}->{data_catalog_dir}) or $self->error("Read Error : $self->{init}->{data_catalog_dir}");
	foreach my $file (readdir(DIR)) {
		if ($file ne '.' and $file ne '..') {
			$catalog_file_size += (-s "$self->{init}->{data_catalog_dir}$file");
		}
	}
	closedir(DIR);
	opendir(DIR, $self->{init}->{data_comt_dir}) or $self->error("Read Error : $self->{init}->{data_comt_dir}");
	foreach my $file (readdir(DIR)) {
		if ($file ne '.' and $file ne '..') {
			$comt_file_size += (-s "$self->{init}->{data_comt_dir}$file");
		}
	}
	closedir(DIR);

	opendir(DIR, $self->{init}->{data_tb_dir}) or $self->error("Read Error : $self->{init}->{data_tb_dir}");
	foreach my $file (readdir(DIR)) {
		if ($file ne '.' and $file ne '..') {
			$tb_file_size += (-s "$self->{init}->{data_tb_dir}$file");
		}
	}
	closedir(DIR);

	opendir(DIR, "$self->{init}->{data_upfile_dir}") or $self->error("Read Error : $self->{init}->{data_upfile_dir}");
	foreach my $file (readdir(DIR)) {
		if ($file ne '.' and $file ne '..') {
			$upfile_file_size += (-s "$self->{init}->{data_upfile_dir}$file");
		}
	}
	closedir(DIR);

	opendir(DIR, "$self->{init}->{archive_dir}") or $self->error("Read Error : $self->{init}->{archive_dir}");
	foreach my $file (readdir(DIR)) {
		if ($file ne '.' and $file ne '..') {
			$archive_file_size += (-s "$self->{init}->{archive_dir}$file");
		}
	}
	closedir(DIR);

	$catalog_file_size = sprintf("%.1f", $catalog_file_size / 1024);
	$comt_file_size    = sprintf("%.1f", $comt_file_size / 1024);
	$tb_file_size      = sprintf("%.1f", $tb_file_size / 1024);
	$upfile_file_size  = sprintf("%.1f", $upfile_file_size / 1024);
	$archive_file_size = sprintf("%.1f", $archive_file_size / 1024);

	my($archive_start, $archive_end);
	if (!$self->{config}->{html_archive_mode}) {
		$archive_start = '<!--';
		$archive_end   = '-->';
	}

	my($root_start, $root_end);
	if ($self->get_authority ne 'root') {
		$root_start = '<!--';
		$root_end   = '-->';
	}

	my($guest_start, $guest_end);
	if ($self->get_authority eq 'root') {
		$guest_start = '<!--';
		$guest_end   = '-->';
	}

	my $skin_ins = new webliberty::Skin;
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_header}", available => 'header');
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_work}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_status}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_navi}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_footer}", available => 'footer');

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});

	$skin_ins->replace_skin(
		$catalog_ins->info,
		%{$self->{plugin}},
		INFO_MESSAGE => $self->{message}
	);

	print $self->header;
	print $skin_ins->get_data('header');
	print $skin_ins->get_data('work_head');
	print $self->work_navi($skin_ins);
	print $skin_ins->get_data('work_foot');

	print $skin_ins->get_replace_data(
		'contents',
		STATUS_CATALOG_SIZE        => $catalog_size || 0,
		STATUS_CATALOG_SHOW_SIZE   => $catalog_show_size || 0,
		STATUS_CATALOG_HIDDEN_SIZE => $catalog_hidden_size || 0,
		STATUS_CATALOG_FILE_SIZE   => $catalog_file_size || 0,
		STATUS_COMT_SIZE           => $comt_size || 0,
		STATUS_COMT_SHOW_SIZE      => $comt_show_size || 0,
		STATUS_COMT_HIDDEN_SIZE    => $comt_hidden_size || 0,
		STATUS_COMT_FILE_SIZE      => $comt_file_size || 0,
		STATUS_TB_SIZE             => $tb_size || 0,
		STATUS_TB_SHOW_SIZE        => $tb_show_size || 0,
		STATUS_TB_HIDDEN_SIZE      => $tb_hidden_size || 0,
		STATUS_TB_FILE_SIZE        => $tb_file_size || 0,
		STATUS_UPFILE_FILE_SIZE    => $upfile_file_size || 0,
		STATUS_ARCHIVE_FILE_SIZE   => $archive_file_size || 0,
		STATUS_ARCHIVE_START       => $archive_start,
		STATUS_ARCHIVE_END         => $archive_end,
		STATUS_ROOT_START          => $root_start,
		STATUS_ROOT_END            => $root_end,
		STATUS_GUEST_START         => $guest_start,
		STATUS_GUEST_END           => $guest_end
	);

	print $skin_ins->get_data('navi');
	print $skin_ins->get_data('footer');

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

### 認証画面表示
sub output_login {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	if ($self->{query}->{admin_pwd}) {
		if ($self->{config}->{user_mode}) {
			$self->error('ユーザー名またはパスワードが違います。');
		} else {
			$self->error('パスワードが違います。');
		}
	}

	my($user_start, $user_end);
	if (!$self->{config}->{user_mode}) {
		$user_start = '<!--';
                $user_end   = '-->';
	}

	my $skin_ins = new webliberty::Skin;
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_header}", available => 'header');
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_footer}", available => 'footer');

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});

	$skin_ins->replace_skin(
		$catalog_ins->info,
		%{$self->{plugin}},
		INFO_USER_START => $user_start,
		INFO_USER_END   => $user_end
	);

	print $self->header;
	print $skin_ins->get_data('header');
	print $skin_ins->get_data('contents_head');
	print $self->work_navi($skin_ins);
	print $skin_ins->get_data('contents_foot');
	print $skin_ins->get_data('footer');

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

### 作業内容一覧
sub work_navi {
	my $self     = shift;
	my $skin_ins = shift;

	my $work_data;

	$work_data  = "\t登録\n";
	$work_data .= "new\t新規登録\n";
	$work_data .= "edit\t商品編集\n";
	if (!$self->get_authority or $self->get_authority eq 'root' or $self->{config}->{auth_comment} or $self->{config}->{auth_trackback} or $self->{config}->{auth_customer}) {
		$work_data .= "\tコミュニティ\n";
		if (!$self->get_authority or $self->get_authority eq 'root' or $self->{config}->{auth_comment}) {
			$work_data .= "comment\tコメント管理\n";
		}
		if (!$self->get_authority or $self->get_authority eq 'root' or $self->{config}->{auth_trackback}) {
			$work_data .= "trackback\tトラックバック管理\n";
		}

		if ($self->{config}->{customer_mode} and (!$self->get_authority or $self->get_authority eq 'root' or $self->{config}->{auth_customer})) {
			$work_data .= "customer\t顧客管理\n";
		}
	}

	$work_data .= "\t各種設定\n";
	if ($self->{config}->{use_field} and (!$self->get_authority or $self->get_authority eq 'root' or $self->{config}->{auth_field})) {
		$work_data .= "field\t分類設定\n";
	}
	if ($self->{config}->{use_icon} and (!$self->get_authority or $self->get_authority eq 'root' or $self->{config}->{auth_icon})) {
		$work_data .= "icon\tアイコン設定\n";
	}
	if (!$self->get_authority or $self->get_authority eq 'root' or $self->{config}->{auth_option}) {
		$work_data .= "option\tオプション設定\n";
	}
	if ($self->{config}->{top_text} and (!$self->get_authority or $self->get_authority eq 'root' or $self->{config}->{auth_top})) {
		$work_data .= "top\tインデックスページ管理\n";
	}
	if ($self->{config}->{show_menu} and (!$self->get_authority or $self->get_authority eq 'root' or $self->{config}->{auth_menu})) {
		$work_data .= "menu\tコンテンツ設定\n";
	}
	if ($self->{config}->{show_link} and (!$self->get_authority or $self->get_authority eq 'root' or $self->{config}->{auth_link})) {
		$work_data .= "link\tリンク集設定\n";
	}
	if ($self->{config}->{profile_mode}) {
		$work_data .= "profile\tプロフィール設定\n";
	}
	$work_data .= "pwd\tパスワード設定\n";
	if (!$self->get_authority or $self->get_authority eq 'root') {
		$work_data .= "env\t環境設定\n";
	}

	$work_data .= "\tユーティリティ\n";
	if ($self->{config}->{html_index_mode} or $self->{config}->{html_archive_mode}) {
		$work_data .= "build\tサイト再構築\n";
	}
	if ($self->{config}->{user_mode} and (!$self->get_authority or $self->get_authority eq 'root')) {
		$work_data .= "user\tユーザー管理\n";
	}
	if (!$self->get_authority or $self->get_authority eq 'root') {
		$work_data .= "record\t操作履歴表示\n";
	}
	$work_data .= "status\tステータス表示\n";

	my($work_list, $flag);

	foreach (split(/\n/, $work_data)) {
		my($work_id, $work_name) = split(/\t/);

		if ($work_id) {
			if ($self->{query}->{work} eq $work_id) {
				$work_list .= $skin_ins->get_replace_data(
					'work_selected',
					WORK_ID   => $work_id,
					WORK_NAME => $work_name
				);
			} else {
				$work_list .= $skin_ins->get_replace_data(
					'work',
					WORK_ID   => $work_id,
					WORK_NAME => $work_name
				);
			}
		} else {
			if ($flag) {
				$work_list .= $skin_ins->get_data('work_delimiter');
				$flag = 0;
			}

			$work_list .= $skin_ins->get_replace_data(
				'work_title',
				WORK_NAME => $work_name
			);

			$flag = 1;
		}
	}
	if ($flag) {
		$work_list .= $skin_ins->get_data('work_delimiter');
	}

	return $work_list;
}

### エラー出力
sub error {
	my $self    = shift;
	my $message = shift;

	if ($self->{query}->{exec_regist} or $self->{query}->{exec_preview}) {
		$self->{message} = $message;

		my $skin_ins = new webliberty::Skin;
		$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_header}", available => 'header');
		$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_work}");
		$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_form}");
		$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_admin_navi}");
		$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_footer}", available => 'footer');

		my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});

		$skin_ins->replace_skin(
			$catalog_ins->info,
			%{$self->{plugin}},
			INFO_MESSAGE => $self->{message}
		);

		my $subj_ins  = new webliberty::String($self->{query}->{subj});
		my $text_ins  = new webliberty::String($self->{query}->{text});
		my $price_ins = new webliberty::String($self->{query}->{price});

		$subj_ins->create_line;
		$text_ins->create_text;
		$price_ins->create_line;

		my $date = "$self->{query}->{year}$self->{query}->{month}$self->{query}->{day}$self->{query}->{hour}$self->{query}->{minute}";

		my $form_ping;
		if ($self->{query}->{ping}) {
			$form_ping  = ' checked="checked"';
		} else {
			$form_ping  = '';
		}

		my %form = $catalog_ins->catalog_form($self->{query}->{edit}, $self->{query}->{id}, $self->{query}->{stat}, $self->{query}->{break}, $self->{query}->{view}, $self->{query}->{comt}, $self->{query}->{tb}, '', $date, '', $subj_ins->get_string, $text_ins->get_string, $price_ins->get_string, '', '', ''),

		my $init_ins = new webliberty::App::Init;
		my %label = %{$init_ins->get_label};

		print $self->header;
		print $skin_ins->get_data('header');
		print $skin_ins->get_data('work_head');
		print $self->work_navi($skin_ins);
		print $skin_ins->get_data('work_foot');
		print $skin_ins->get_replace_data(
			'form_head',
			%form,
			FORM_LABEL => 'エラー',
			FORM_WORK  => $self->{query}->{work},
			FORM_TBURL => $self->{query}->{tb_url},
			FORM_PING  => $form_ping
		);

		if ($self->{config}->{use_file}) {
			print $skin_ins->get_data('file_head');

			foreach (1 .. $self->{config}->{max_file}) {
				print $skin_ins->get_replace_data(
					'file',
					FILE_TITLE => $label{'pc_file'} . $_,
					FILE_VALUE => "<input type=\"file\" name=\"file$_\" size=\"30\" />"
				);
			}

			print $skin_ins->get_data('file_foot');
		}

		if (-s $self->{init}->{data_option}) {
			print $skin_ins->get_data('option_head');

			open(FH, $self->{init}->{data_option}) or $self->error("Read Error : $self->{init}->{data_option}");
			while (<FH>) {
				chomp;
				my($id, $name, $type, $default) = split(/\t/);

				my $value;
				if ($type eq 'text') {
					if ($self->{query}->{'option_' . $id}) {
						$default = $self->{query}->{'option_' . $id};
					}

					$value = "<input type=\"text\" name=\"option_$id\" size=\"50\" value=\"$default\" />";
				} elsif ($type eq 'textarea') {
					if ($self->{query}->{'option_' . $id}) {
						$default = $self->{query}->{'option_' . $id};
					} else {
						$default = '';
					}

					$value = "<textarea name=\"option_$id\" cols=\"60\" rows=\"3\">$default</textarea>";
				} elsif ($type eq 'select') {
					foreach my $data (split(/<br \/>/, $default)) {
						if ($self->{query}->{'option_' . $id} and $data eq $self->{query}->{'option_' . $id}) {
							$value .= "<option value=\"$data\" selected=\"selected\">$data</option>";
						} else {
							$value .= "<option value=\"$data\">$data</option>";
						}
					}
					$value = "<select name=\"option_$id\" xml:lang=\"ja\" lang=\"ja\">$value</select>";
				} elsif ($type eq 'checkbox') {
					my $i = 0;
					foreach my $data (split(/<br \/>/, $default)) {
						if ($self->{query}->{'option_' . $id} and $self->{query}->{'option_' . $id} =~ /(^|\n)$data(\n|$)/) {
							$value .= "<input type=\"checkbox\" name=\"option_$id\" id=\"${id}_checkbox_$i\" value=\"$data\" checked=\"checked\" /> <label for=\"${id}_checkbox_$i\">$data</label>\n";
						} else {
							$value .= "<input type=\"checkbox\" name=\"option_$id\" id=\"${id}_checkbox_$i\" value=\"$data\" /> <label for=\"${id}_checkbox_$i\">$data</label>\n";
						}
						$i++;
					}
				} elsif ($type eq 'option') {
					if ($self->{query}->{'option_' . $id}) {
						$default = $self->{query}->{'option_' . $id};
					} else {
						$default = '';
					}

					$value = "<textarea name=\"option_$id\" cols=\"30\" rows=\"5\">$default</textarea>";
				}

				print $skin_ins->get_replace_data(
					'option',
					OPTION_TITLE => $name,
					OPTION_VALUE => $value
				);
			}
			close(FH);

			print $skin_ins->get_data('option_foot');
		}

		print $skin_ins->get_replace_data(
			'form_foot',
			%form,
			FORM_LABEL => 'エラー',
			FORM_WORK  => $self->{query}->{work},
			FORM_TBURL => $self->{query}->{tb_url},
			FORM_PING  => $form_ping
		);
		print $skin_ins->get_data('navi');
		print $skin_ins->get_data('footer');

		exit;
	}

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});
	$catalog_ins->error($message);

	exit;
}

### アイコン並び替え
sub _sort_icon {
	my $self = shift;

	my(@normal, @personal, @names);

	foreach (@_) {
		chomp;
		my($file, $name, $field, $user, $pwd) = split(/\t/);

		if ($user) {
			push(@personal, "$_\n");
		} else {
			push(@normal, "$_\n");
		}
	}

	@names  = map { (split(/\t/))[1] } @normal;
	@normal = @normal[sort { $names[$a] cmp $names[$b] } (0 .. $#names)];

	@names    = map { (split(/\t/))[1] } @personal;
	@personal = @personal[sort { $names[$a] cmp $names[$b] } (0 .. $#names)];

	return(@normal, @personal);
}

### アイテム並び替え
sub _sort_item {
	my $self = shift;
	my @item = @_;

	my @fields = map { (split(/\t/))[0] } @item;
	@item = @item[sort { $fields[$a] cmp $fields[$b] } (0 .. $#fields)];

	return(@item);
}

1;
