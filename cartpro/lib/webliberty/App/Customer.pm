#webliberty::App::Customer.pm (2008/07/05)
#Copyright(C) 2002-2008 Knight, All rights reserved.

package webliberty::App::Customer;

use strict;
use base qw(webliberty::Basis);
use webliberty::String;
use webliberty::Host;
use webliberty::App::Init;

### コンストラクタ
sub new {
	my $class = shift;

	my $self = {
		init    => shift,
		config  => shift,
		query   => shift,
		message => undef
	};
	bless $self, $class;

	return $self;
}

### 入力内容チェック
sub check {
	my $self = shift;

	my $flag;

	if ($ENV{'REQUEST_METHOD'} ne 'POST') {
		$flag = 1;
	}
	if ($ENV{'HTTP_REFERER'} and $self->{config}->{base_url} and $ENV{'HTTP_REFERER'} !~ $self->{config}->{base_url}) {
		$flag = 1;
	}
	if (!$self->{config}->{proxy_mode} and ($ENV{'HTTP_VIA'} or $ENV{'HTTP_FORWARDED'} or $ENV{'HTTP_X_FORWARDED_FOR'})) {
		$flag = 1;
	}

	my $host_ins = new webliberty::Host;
	foreach (split(/<>/, $self->{config}->{black_list})) {
		$_ = quotemeta($_);

		if ($host_ins->get_host =~ /$_/i) {
			$flag = 1;
			last;
		}
	}

	if ($flag) {
		$self->error('不正なアクセスです。');
	}

	my $login_mail_ins = new webliberty::String($self->{query}->{login_mail});
	my $login_pwd_ins  = new webliberty::String($self->{query}->{login_pwd});

	my $payment_ins  = new webliberty::String($self->{query}->{payment});
	my $delivery_ins = new webliberty::String($self->{query}->{delivery});

	my $order_name_ins  = new webliberty::String($self->{query}->{order_name});
	my $order_kana_ins  = new webliberty::String($self->{query}->{order_kana});
	my $order_mail_ins  = new webliberty::String($self->{query}->{order_mail});
	my $order_post_ins  = new webliberty::String($self->{query}->{order_post});
	my $order_pref_ins  = new webliberty::String($self->{query}->{order_pref});
	my $order_addr_ins  = new webliberty::String($self->{query}->{order_addr});
	my $order_phone_ins = new webliberty::String($self->{query}->{order_phone});
	my $order_fax_ins   = new webliberty::String($self->{query}->{order_fax});

	my $send_name_ins  = new webliberty::String($self->{query}->{send_name});
	my $send_kana_ins  = new webliberty::String($self->{query}->{send_kana});
	my $send_post_ins  = new webliberty::String($self->{query}->{send_post});
	my $send_pref_ins  = new webliberty::String($self->{query}->{send_pref});
	my $send_addr_ins  = new webliberty::String($self->{query}->{send_addr});
	my $send_phone_ins = new webliberty::String($self->{query}->{send_phone});
	my $send_fax_ins   = new webliberty::String($self->{query}->{send_fax});

	my $user_pwd_ins = new webliberty::String($self->{query}->{user_pwd});
	my $cfm_pwd_ins  = new webliberty::String($self->{query}->{cfm_pwd});
	my $comment_ins  = new webliberty::String($self->{query}->{comment});

	$payment_ins->create_line;
	$delivery_ins->create_line;

	$order_name_ins->create_line;
	$order_kana_ins->create_line;
	$order_mail_ins->create_line;
	$order_post_ins->create_line;
	$order_pref_ins->create_line;
	$order_addr_ins->create_line;
	$order_phone_ins->create_line;
	$order_fax_ins->create_line;

	$send_name_ins->create_line;
	$send_kana_ins->create_line;
	$send_post_ins->create_line;
	$send_pref_ins->create_line;
	$send_addr_ins->create_line;
	$send_phone_ins->create_line;
	$send_fax_ins->create_line;

	$user_pwd_ins->create_line;
	$cfm_pwd_ins->create_line;
	$comment_ins->create_text;

	my @error_message;

	if ($self->{query}->{mode} eq 'order') {
		if (!$payment_ins->get_string) {
			push(@error_message, 'お支払方法が選択されていません。');
		}
		if ($self->{config}->{order_delivery} and !$delivery_ins->get_string) {
			push(@error_message, '配達方法が選択されていません。');
		}
	}
	if (!$order_name_ins->get_string) {
		push(@error_message, 'お客様情報の名前が入力されていません。');
	}
	if (!$order_kana_ins->get_string) {
		push(@error_message, 'お客様情報のふりがなが入力されていません。');
	}
	if (!$order_mail_ins->get_string) {
		push(@error_message, 'お客様情報のＥメールが入力されていません。');
	}
	if (!$order_post_ins->get_string) {
		push(@error_message, 'お客様情報の郵便番号が入力されていません。');
	}
	if (!$order_pref_ins->get_string) {
		push(@error_message, 'お客様情報の都道府県が入力されていません。');
	}
	if (!$order_addr_ins->get_string) {
		push(@error_message, 'お客様情報の住所が入力されていません。');
	}
	if (!$order_phone_ins->get_string) {
		push(@error_message, 'お客様情報の電話番号が入力されていません。');
	}
	if ($send_name_ins->get_string) {
		if (!$send_kana_ins->get_string) {
			push(@error_message, '配送先情報のふりがなが入力されていません。');
		}
		if (!$send_post_ins->get_string) {
			push(@error_message, '配送先情報の郵便番号が入力されていません。');
		}
		if (!$send_pref_ins->get_string) {
			push(@error_message, '配送先情報の都道府県が入力されていません。');
		}
		if (!$send_addr_ins->get_string) {
			push(@error_message, '配送先情報の住所が入力されていません。');
		}
		if (!$send_phone_ins->get_string) {
			push(@error_message, '配送先情報の電話番号が入力されていません。');
		}
	}
	if ($self->{config}->{customer_mode}) {
		if ($login_mail_ins->get_string) {
			if (!$login_pwd_ins->get_string) {
				push(@error_message, 'パスワードが入力されていません。');
			}
		} else {
			if (!$user_pwd_ins->get_string) {
				push(@error_message, 'パスワードが入力されていません。');
			}
		}
		if ($cfm_pwd_ins->get_string) {
			if ($user_pwd_ins->get_string ne $cfm_pwd_ins->get_string) {
				push(@error_message, '希望パスワードと確認用パスワードが一致しません。');
			}
		}
	}

	if ($order_name_ins->get_string and $order_name_ins->check_length > 20 * 2) {
		push(@error_message, 'お客様情報の名前の長さは全角20文字までにしてください。');
	}
	if ($order_kana_ins->get_string and $order_kana_ins->check_length > 30 * 2) {
		push(@error_message, 'お客様情報のふりがなの長さは全角30文字までにしてください。');
	}
	if ($order_post_ins->get_string and $order_post_ins->check_length > 8) {
		push(@error_message, 'お客様情報の郵便番号の長さは半角8文字までにしてください。');
	}
	if ($order_pref_ins->get_string and $order_pref_ins->check_length > 20 * 2) {
		push(@error_message, 'お客様情報の都道府県の長さは全角20文字までにしてください。');
	}
	if ($order_addr_ins->get_string and $order_addr_ins->check_length > 50 * 2) {
		push(@error_message, 'お客様情報の住所の長さは全角50文字までにしてください。');
	}
	if ($order_phone_ins->get_string and $order_phone_ins->check_length > 20) {
		push(@error_message, 'お客様情報の電話番号の長さは半角20文字までにしてください。');
	}
	if ($order_fax_ins->get_string and $order_fax_ins->check_length > 20) {
		push(@error_message, 'お客様情報のFAX番号の長さは半角20文字までにしてください。');
	}
	if ($send_name_ins->get_string) {
		if ($send_name_ins->get_string and $send_name_ins->check_length > 20 * 2) {
			push(@error_message, '配送先情報の名前の長さは全角20文字までにしてください。');
		}
		if ($send_kana_ins->get_string and $send_kana_ins->check_length > 30 * 2) {
			push(@error_message, '配送先情報のふりがなの長さは全角30文字までにしてください。');
		}
		if ($send_post_ins->get_string and $send_post_ins->check_length > 8) {
			push(@error_message, '配送先情報の郵便番号の長さは半角8文字までにしてください。');
		}
		if ($send_pref_ins->get_string and $send_pref_ins->check_length > 20 * 2) {
			push(@error_message, '配送先情報の都道府県の長さは全角20文字までにしてください。');
		}
		if ($send_addr_ins->get_string and $send_addr_ins->check_length > 50 * 2) {
			push(@error_message, '配送先情報の住所の長さは全角50文字までにしてください。');
		}
		if ($send_phone_ins->get_string and $send_phone_ins->check_length > 20) {
			push(@error_message, '配送先情報の電話番号の長さは半角20文字までにしてください。');
		}
		if ($send_fax_ins->get_string and $send_fax_ins->check_length > 20) {
			push(@error_message, '配送先情報のFAX番号の長さは半角20文字までにしてください。');
		}
	}
	if ($comment_ins->get_string and $comment_ins->check_length > 1000 * 2) {
		push(@error_message, '連絡事項の長さは全角1000文字までにしてください。');
	}
	if ($self->{config}->{customer_mode}) {
		if ($user_pwd_ins->get_string and ($user_pwd_ins->check_length < 4 or $user_pwd_ins->check_length > 10)) {
			push(@error_message, 'パスワードの長さは半角4文字以上10文字以内にしてください。');
		}
	}

	if ($order_mail_ins->get_string and ($order_mail_ins->get_string =~ /[^\w\.\@\d\-\_]/ or $order_mail_ins->get_string !~ /(.+)\@(.+)\.(.+)/)) {
		push(@error_message, 'お客様情報のＥメールの入力内容が正しくありません。');
	}

	if ($self->{config}->{customer_mode} and $login_mail_ins->get_string ne $order_mail_ins->get_string) {
		open(FH, "$self->{init}->{data_customer}") or $self->error("Read Error : $self->{init}->{data_customer}");
		while (<FH>) {
			chomp;
			my($date, $stat, $point, $payment, $delivery, $user_pwd, $order_name, $order_kana, $order_mail, $order_post, $order_pref, $order_addr, $order_phone, $order_fax, $send_name, $send_kana, $send_post, $send_pref, $send_addr, $send_phone, $send_fax, $host) = split(/\t/);

			if ($login_mail_ins->get_string ne $order_mail and $order_mail_ins->get_string eq $order_mail) {
				push(@error_message, '入力されたメールアドレスは既に登録されています。');
			}
		}
		close(FH);
	}

	return @error_message;
}

### 顧客情報登録
sub regist {
	my $self = shift;
	my %args = @_;

	my $this_point = $args{'point'};

	my $login_mail_ins = new webliberty::String($self->{query}->{login_mail});
	my $login_pwd_ins  = new webliberty::String($self->{query}->{login_pwd});

	my $payment_ins  = new webliberty::String($self->{query}->{payment});
	my $delivery_ins = new webliberty::String($self->{query}->{delivery});
	my $point_ins    = new webliberty::String($self->{query}->{point});

	my $order_name_ins  = new webliberty::String($self->{query}->{order_name});
	my $order_kana_ins  = new webliberty::String($self->{query}->{order_kana});
	my $order_mail_ins  = new webliberty::String($self->{query}->{order_mail});
	my $order_post_ins  = new webliberty::String($self->{query}->{order_post});
	my $order_pref_ins  = new webliberty::String($self->{query}->{order_pref});
	my $order_addr_ins  = new webliberty::String($self->{query}->{order_addr});
	my $order_phone_ins = new webliberty::String($self->{query}->{order_phone});
	my $order_fax_ins   = new webliberty::String($self->{query}->{order_fax});

	my $send_name_ins  = new webliberty::String($self->{query}->{send_name});
	my $send_kana_ins  = new webliberty::String($self->{query}->{send_kana});
	my $send_post_ins  = new webliberty::String($self->{query}->{send_post});
	my $send_pref_ins  = new webliberty::String($self->{query}->{send_pref});
	my $send_addr_ins  = new webliberty::String($self->{query}->{send_addr});
	my $send_phone_ins = new webliberty::String($self->{query}->{send_phone});
	my $send_fax_ins   = new webliberty::String($self->{query}->{send_fax});

	my $user_pwd_ins = new webliberty::String($self->{query}->{user_pwd});

	$payment_ins->create_line;
	$delivery_ins->create_line;
	$point_ins->create_line;

	$order_name_ins->create_line;
	$order_kana_ins->create_line;
	$order_mail_ins->create_line;
	$order_post_ins->create_line;
	$order_pref_ins->create_line;
	$order_addr_ins->create_line;
	$order_phone_ins->create_line;
	$order_fax_ins->create_line;

	$send_name_ins->create_line;
	$send_kana_ins->create_line;
	$send_post_ins->create_line;
	$send_pref_ins->create_line;
	$send_addr_ins->create_line;
	$send_phone_ins->create_line;
	$send_fax_ins->create_line;

	$user_pwd_ins->create_line;
	$user_pwd_ins->create_password;

	my $host_ins = new webliberty::Host;

	my(@new, $flag);

	open(FH, "$self->{init}->{data_customer}") or $self->error("Read Error : $self->{init}->{data_customer}");
	while (<FH>) {
		chomp;
		my($date, $stat, $point, $payment, $delivery, $user_pwd, $order_name, $order_kana, $order_mail, $order_post, $order_pref, $order_addr, $order_phone, $order_fax, $send_name, $send_kana, $send_post, $send_pref, $send_addr, $send_phone, $send_fax, $host) = split(/\t/);

		if ($login_mail_ins->get_string eq $order_mail) {
			$point += $this_point;

			if ($point < 0) {
				$self->error("使用ポイント数が不正です。");
			} else {
				push(@new, time . "\t1\t$point\t" . $payment_ins->get_string . "\t" . $delivery_ins->get_string . "\t$user_pwd\t" . $order_name_ins->get_string . "\t" . $order_kana_ins->get_string . "\t" . $order_mail_ins->get_string . "\t" . $order_post_ins->get_string . "\t" . $order_pref_ins->get_string . "\t" . $order_addr_ins->get_string . "\t" . $order_phone_ins->get_string . "\t" . $order_fax_ins->get_string . "\t" . $send_name_ins->get_string . "\t" . $send_kana_ins->get_string . "\t" . $send_post_ins->get_string . "\t" . $send_pref_ins->get_string . "\t" . $send_addr_ins->get_string . "\t" . $send_phone_ins->get_string . "\t" . $send_fax_ins->get_string . "\t" . $host_ins->get_host . "\n");

				$flag = 1;
			}
		} else {
			push(@new, "$_\n");
		}
	}
	close(FH);

	if (!$flag) {
		push(@new, time . "\t1\t$this_point\t" . $payment_ins->get_string . "\t" . $delivery_ins->get_string . "\t" . $user_pwd_ins->get_string . "\t" . $order_name_ins->get_string . "\t" . $order_kana_ins->get_string . "\t" . $order_mail_ins->get_string . "\t" . $order_post_ins->get_string . "\t" . $order_pref_ins->get_string . "\t" . $order_addr_ins->get_string . "\t" . $order_phone_ins->get_string . "\t" . $order_fax_ins->get_string . "\t" . $send_name_ins->get_string . "\t" . $send_kana_ins->get_string . "\t" . $send_post_ins->get_string . "\t" . $send_pref_ins->get_string . "\t" . $send_addr_ins->get_string . "\t" . $send_phone_ins->get_string . "\t" . $send_fax_ins->get_string . "\t" . $host_ins->get_host . "\n");
	}

	@new = sort { $b <=> $a } @new;

	open(FH, ">$self->{init}->{data_customer}") or $self->error("Write Error : $self->{init}->{data_customer}");
	print FH @new;
	close(FH);

	$self->{message} = "お客様情報を新規に登録しました。";

	return;
}

### 顧客情報編集
sub edit {
	my $self = shift;

	my $payment_ins  = new webliberty::String($self->{query}->{payment});
	my $delivery_ins = new webliberty::String($self->{query}->{delivery});

	my $order_name_ins  = new webliberty::String($self->{query}->{order_name});
	my $order_kana_ins  = new webliberty::String($self->{query}->{order_kana});
	my $order_mail_ins  = new webliberty::String($self->{query}->{order_mail});
	my $order_post_ins  = new webliberty::String($self->{query}->{order_post});
	my $order_pref_ins  = new webliberty::String($self->{query}->{order_pref});
	my $order_addr_ins  = new webliberty::String($self->{query}->{order_addr});
	my $order_phone_ins = new webliberty::String($self->{query}->{order_phone});
	my $order_fax_ins   = new webliberty::String($self->{query}->{order_fax});

	my $send_name_ins  = new webliberty::String($self->{query}->{send_name});
	my $send_kana_ins  = new webliberty::String($self->{query}->{send_kana});
	my $send_post_ins  = new webliberty::String($self->{query}->{send_post});
	my $send_pref_ins  = new webliberty::String($self->{query}->{send_pref});
	my $send_addr_ins  = new webliberty::String($self->{query}->{send_addr});
	my $send_phone_ins = new webliberty::String($self->{query}->{send_phone});
	my $send_fax_ins   = new webliberty::String($self->{query}->{send_fax});

	my $point_ins    = new webliberty::String($self->{query}->{point});
	my $user_pwd_ins = new webliberty::String($self->{query}->{user_pwd});

	$payment_ins->create_line;
	$delivery_ins->create_line;

	$order_name_ins->create_line;
	$order_kana_ins->create_line;
	$order_mail_ins->create_line;
	$order_post_ins->create_line;
	$order_pref_ins->create_line;
	$order_addr_ins->create_line;
	$order_phone_ins->create_line;
	$order_fax_ins->create_line;

	$send_name_ins->create_line;
	$send_kana_ins->create_line;
	$send_post_ins->create_line;
	$send_pref_ins->create_line;
	$send_addr_ins->create_line;
	$send_phone_ins->create_line;
	$send_fax_ins->create_line;

	$point_ins->create_number;
	$user_pwd_ins->create_line;
	$user_pwd_ins->create_password;

	my @new;
	my $flag;

	open(FH, "$self->{init}->{data_customer}") or $self->error("Read Error : $self->{init}->{data_customer}");
	while (<FH>) {
		chomp;
		my($date, $stat, $point, $payment, $delivery, $user_pwd, $order_name, $order_kana, $order_mail, $order_post, $order_pref, $order_addr, $order_phone, $order_fax, $send_name, $send_kana, $send_post, $send_pref, $send_addr, $send_phone, $send_fax, $host) = split(/\t/);

		if ($self->{query}->{login_mail} eq $order_mail or $self->{query}->{edit} eq $order_mail) {
			if ($self->{query}->{mode} eq 'admin') {
				$point    = $point_ins->get_string;
				$payment  = $payment_ins->get_string;
				$delivery = $delivery_ins->get_string;
			} else {
				$date     = time;
				$user_pwd = $user_pwd_ins->get_string;

				my $host_ins = new webliberty::Host;
				$host = $host_ins->get_host;
			}

			push(@new, "$date\t$stat\t$point\t$payment\t$delivery\t$user_pwd\t" . $order_name_ins->get_string . "\t" . $order_kana_ins->get_string . "\t" . $order_mail_ins->get_string . "\t" . $order_post_ins->get_string . "\t" . $order_pref_ins->get_string . "\t" . $order_addr_ins->get_string . "\t" . $order_phone_ins->get_string . "\t" . $order_fax_ins->get_string . "\t" . $send_name_ins->get_string . "\t" . $send_kana_ins->get_string . "\t" . $send_post_ins->get_string . "\t" . $send_pref_ins->get_string . "\t" . $send_addr_ins->get_string . "\t" . $send_phone_ins->get_string . "\t" . $send_fax_ins->get_string . "\t$host\n");

			$flag = 1;
		} else {
			push(@new, "$_\n");
		}
	}
	close(FH);

	if (!$flag) {
		$self->error('指定されたお客様情報は存在しません。');
	}

	@new = sort { $b <=> $a } @new;

	open(FH, ">$self->{init}->{data_customer}") or $self->error("Write Error : $self->{init}->{data_customer}");
	print FH @new;
	close(FH);

	$self->{message} = "お客様情報を編集しました。";

	return;
}

### 顧客情報削除
sub del {
	my $self = shift;

	my @new;
	my $flag;

	open(FH, "$self->{init}->{data_customer}") or $self->error("Read Error : $self->{init}->{data_customer}");
	while (<FH>) {
		chomp;
		my($date, $stat, $point, $payment, $delivery, $user_pwd, $order_name, $order_kana, $order_mail, $order_post, $order_pref, $order_addr, $order_phone, $order_fax, $send_name, $send_kana, $send_post, $send_pref, $send_addr, $send_phone, $send_fax) = split(/\t/);

		if ($self->{query}->{login_mail} eq $order_mail or $self->{query}->{del} =~ /(^|\n)$order_mail(\n|$)/) {
			$flag = 1;
		} else {
			push(@new, "$_\n");
		}
	}
	close(FH);

	if (!$flag) {
		$self->error('指定されたお客様情報は存在しません。');
	}

	open(FH, ">$self->{init}->{data_customer}") or $self->error("Write Error : $self->{init}->{data_customer}");
	print FH @new;
	close(FH);

	$self->{message} = "お客様情報を削除しました。";

	return;
}

### パスワード再発行
sub password {
	my $self = shift;

	my $new_pwd = substr(crypt(time, pack('CC', int(rand(26) + 65), int(rand(10) + 48))), 0, 5);

	my $pwd_ins = new webliberty::String($new_pwd);
	$pwd_ins->create_password;

	my @new;
	my $flag;

	open(FH, "$self->{init}->{data_customer}") or $self->error("Read Error : $self->{init}->{data_customer}");
	while (<FH>) {
		chomp;
		my($date, $stat, $point, $payment, $delivery, $user_pwd, $order_name, $order_kana, $order_mail, $order_post, $order_pref, $order_addr, $order_phone, $order_fax, $send_name, $send_kana, $send_post, $send_pref, $send_addr, $send_phone, $send_fax, $host) = split(/\t/);

		if ($self->{query}->{login_mail} eq $order_mail) {
			my $host_ins = new webliberty::Host;
			$host = $host_ins->get_host;

			push(@new, time . "\t$stat\t$point\t$payment\t$delivery\t" . $pwd_ins->get_string . "\t$order_name\t$order_kana\t$order_mail\t$order_post\t$order_pref\t$order_addr\t$order_phone\t$order_fax\t$send_name\t$send_kana\t$send_post\t$send_pref\t$send_addr\t$send_phone\t$send_fax\t$host\n");

			$flag = 1;
		} else {
			push(@new, "$_\n");
		}
	}
	close(FH);

	if (!$flag) {
		$self->error('指定されたお客様情報は存在しません。');
	}

	@new = sort { $b <=> $a } @new;

	open(FH, ">$self->{init}->{data_customer}") or $self->error("Write Error : $self->{init}->{data_customer}");
	print FH @new;
	close(FH);

	$self->{message} = "パスワードを再発行しました。";

	return $new_pwd;
}

1;
