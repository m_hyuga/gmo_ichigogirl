#webliberty::App::Catalog.pm (2008/07/05)
#Copyright(C) 2002-2008 Knight, All rights reserved.

package webliberty::App::Catalog;

use strict;
use base qw(webliberty::Basis Exporter);
use vars qw(@EXPORT);
use webliberty::String;
use webliberty::Decoration;
use webliberty::Encoder;
use webliberty::File;
use webliberty::Date;
use webliberty::Cookie;
use webliberty::Skin;
use webliberty::Script;
use webliberty::Plugin;
use webliberty::App::Init;

@EXPORT = qw(error);

### コンストラクタ
sub new {
	my $class = shift;

	my $self = {
		init   => shift,
		config => shift,
		query  => shift,
		agent  => undef,
		field  => undef,
		user   => undef,
		admin  => undef
	};
	bless $self, $class;

	if ($self->{config}) {
		my $i;

		open(FH, $self->{init}->{data_field}) or $self->error("Read Error : $self->{init}->{data_field}");
		while (<FH>) {
			chomp;

			$self->{field}->{$_} = ++$i;
		}
		close(FH);

		if ($self->{config}->{user_mode}) {
			open(FH, $self->{init}->{data_profile}) or $self->error("Read Error : $self->{init}->{data_profile}");
			while (<FH>) {
				chomp;
				my($user, $name, $text) = split(/\t/);
		
				$self->{user}->{$user} = $name;
			}
			close(FH);
		}

		my $cookie_ins = new webliberty::Cookie($self->{config}->{cookie_admin}, $self->{init}->{des_key});
		if ($cookie_ins->get_cookie('admin_user')) {
			my %pwd;

			open(FH, $self->{init}->{data_user}) or $self->error("Read Error : $self->{init}->{data_user}");
			while (<FH>) {
				chomp;
				my($user, $pwd, $authority) = split(/\t/);

				$pwd{$user} = $pwd;
			}
			close(FH);

			my $pwd_ins = new webliberty::String($cookie_ins->get_cookie('admin_pwd'));
			if ($pwd_ins->get_string and $pwd_ins->check_password($pwd{$cookie_ins->get_cookie('admin_user')})) {
				$self->{admin} = 1;
			}
		}
	}

	return $self;
}

### 閲覧環境設定
sub set_agent {
	my $self  = shift;
	my $agent = shift;

	$self->{agent} = $agent;

	return;
}

### 基本情報作成
sub info {
	my $self = shift;

	my $page_ins = new webliberty::String($self->{query}->{page});
	$page_ins->create_number;

	my $info_path;
	if ($self->{init}->{script_file} =~ /([^\/\\]*)$/) {
		$info_path = "$self->{config}->{site_url}$1";
	}

	my $info_ssl;
	if ($self->{config}->{ssl_url} and $self->{init}->{script_file} =~ /([^\/\\]*)$/) {
		$info_ssl = "$self->{config}->{ssl_url}$1";
	} else {
		$info_ssl = $info_path;
	}

	my $info_tbpath;
	if ($self->{init}->{tb_file} =~ /([^\/\\]*)$/) {
		$info_tbpath = "$self->{config}->{site_url}$1";
	}

	my($info_user, $info_user_start, $info_user_end);
	if ($self->{config}->{user_mode} and $self->{query}->{mode} eq 'admin') {
		my $cookie_ins = new webliberty::Cookie($self->{config}->{cookie_admin}, $self->{init}->{des_key});
		$info_user = $cookie_ins->get_cookie('admin_user');

		if (!$info_user) {
			$info_user = $self->{query}->{admin_user};
		}
	} else {
		$info_user_start = '<!--';
		$info_user_end   = '-->';
	}

	return(
		INFO_SCRIPT      => $self->{init}->{script},
		INFO_VERSION     => $self->{init}->{version},
		INFO_COPYRIGHT   => $self->{init}->{copyright},
		INFO_FILE        => $self->{init}->{script_file},
		INFO_TITLE       => $self->{config}->{site_title},
		INFO_BACK        => $self->{config}->{back_url},
		INFO_DESCRIPTION => $self->{config}->{site_description},
		INFO_URL         => $self->{config}->{site_url},
		INFO_PATH        => $info_path,
		INFO_SSL         => $info_ssl,
		INFO_TBPATH      => $info_tbpath,
		INFO_PAGE        => $page_ins->get_string,
		INFO_USER        => $info_user,
		INFO_USER_START  => $info_user_start,
		INFO_USER_END    => $info_user_end,
		INFO_TIMESTAMP   => time
	);
}

### 商品フォーム作成
sub catalog_form {
	my $self = shift;
	my($no, $id, $stat, $break, $view, $comt, $tb, $field, $date, $name, $subj, $text, $price, $icon, $image, $file, $host) = @_;

	my $no_ins    = new webliberty::String($no);
	my $id_ins    = new webliberty::String($id);
	my $stat_ins  = new webliberty::String($stat);
	my $break_ins = new webliberty::String($break);
	my $view_ins  = new webliberty::String($view);
	my $comt_ins  = new webliberty::String($comt);
	my $tb_ins    = new webliberty::String($tb);
	my $field_ins = new webliberty::String($field);
	my $date_ins  = new webliberty::String($date);
	my $name_ins  = new webliberty::String($name);
	my $subj_ins  = new webliberty::String($subj);
	my $text_ins  = new webliberty::String($text);
	my $price_ins = new webliberty::String($price);
	my $icon_ins  = new webliberty::String($icon);
	my $image_ins = new webliberty::String($image);
	my $file_ins  = new webliberty::String($file);
	my $host_ins  = new webliberty::String($host);

	$no_ins->create_number;
	$id_ins->create_line;
	$stat_ins->create_number;
	$break_ins->create_number;
	$view_ins->create_number;
	$comt_ins->create_number;
	$tb_ins->create_number;
	$field_ins->create_line;
	$date_ins->create_line;
	$name_ins->create_line;
	$subj_ins->create_line;
	$text_ins->create_text;
	$price_ins->create_line;
	$icon_ins->create_line;
	$image_ins->create_line;
	$file_ins->create_line;
	$host_ins->create_line;

	$id_ins->create_plain;
	$field_ins->create_plain;
	$date_ins->create_plain;
	$name_ins->create_plain;
	$subj_ins->create_plain;
	$text_ins->create_plain;
	$price_ins->create_plain;
	$icon_ins->create_plain;
	$image_ins->create_plain;
	$file_ins->create_plain;
	$host_ins->create_plain;

	my $form_stat;
	if ($stat_ins->get_string == 1) {
		$form_stat = ' checked="checked"';
	} else {
		$form_stat = '';
	}

	my $form_break;
	if ($break_ins->get_string == 1) {
		$form_break = ' checked="checked"';
	} else {
		$form_break = '';
	}

	my $form_view;
	if ($view_ins->get_string == 1) {
		$form_view = ' checked="checked"';
	} else {
		$form_view = '';
	}

	my $form_comt;
	if ($comt_ins->get_string == 1) {
		$form_comt = ' checked="checked"';
	} else {
		$form_comt = '';
	}

	my $form_tb;
	if ($tb_ins->get_string == 1) {
		$form_tb = ' checked="checked"';
	} else {
		$form_tb = '';
	}

	my($form_field, $form_field_start, $form_field_end, $i);
	if ($self->{config}->{use_field}) {
		open(FH, $self->{init}->{data_field}) or $self->error("Read Error : $self->{init}->{data_field}");
		while (<FH>) {
			chomp;
			my($field, $child) = split(/<>/);

			$i++;

			if ($child) {
				$field = "└ $child";
			}

			if ($field_ins->get_string eq $_ or ($self->{query}->{exec_preview} and $field_ins->get_string == $i)) {
				$form_field .= "<option value=\"$i\" selected=\"selected\">$field</option>";
			} else {
				$form_field .= "<option value=\"$i\">$field</option>";
			}
		}
		close(FH);

		if (!$form_field) {
			$form_field = '<option value="">分類が登録されていません</option>';
		} else {
			$form_field = "<option value=\"\">選択してください</option>$form_field";
		}
		$form_field = "<select name=\"field\" xml:lang=\"ja\" lang=\"ja\">$form_field</select>";
	} else {
		$form_field_start = '<!--';
		$form_field_end   = '-->';
	}

	my($form_date, $form_year, $form_month, $form_day, $form_hour, $form_minute);

	if ($date_ins->get_string =~ /^(\d\d\d\d)(\d\d)(\d\d)(\d\d)(\d\d)$/) {
		$form_year   = $1;
		$form_month  = $2;
		$form_day    = $3;
		$form_hour   = $4;
		$form_minute = $5;
	}

	$form_date .= '<select name="year" xml:lang="ja" lang="ja">';
	foreach ($form_year - 5 .. $form_year + 5) {
		if ($form_year == $_) {
			$form_date .= "<option value=\"$_\" selected=\"selected\">$_年</option>";
		} else {
			$form_date .= "<option value=\"$_\">$_年</option>";
		}
	}
	$form_date .= '</select>';

	$form_date .= '<select name="month" xml:lang="ja" lang="ja">';
	foreach (1 .. 12) {
		my $month = sprintf("%02d", $_);
		if ($month == $form_month) {
			$form_date .= "<option value=\"$month\" selected=\"selected\">$_月</option>";
		} else {
			$form_date .= "<option value=\"$month\">$_月</option>";
		}
	}
	$form_date .= '</select>';

	$form_date .= '<select name="day" xml:lang="ja" lang="ja">';
	foreach (1 .. 31) {
		my $day = sprintf("%02d", $_);
		if ($day == $form_day) {
			$form_date .= "<option value=\"$day\" selected=\"selected\">$_日</option>";
		} else {
			$form_date .= "<option value=\"$day\">$_日</option>";
		}
	}
	$form_date .= '</select>';

	$form_date .= '<select name="hour" xml:lang="ja" lang="ja">';
	foreach (0 .. 23) {
		my $hour = sprintf("%02d", $_);
		if ($hour == $form_hour) {
			$form_date .= "<option value=\"$hour\" selected=\"selected\">$_時</option>";
		} else {
			$form_date .= "<option value=\"$hour\">$_時</option>";
		}
	}
	$form_date .= '</select>';

	$form_date .= '<select name="minute" xml:lang="ja" lang="ja">';
	foreach (0 .. 59) {
		my $minute = sprintf("%02d", $_);
		if ($minute == $form_minute) {
			$form_date .= "<option value=\"$minute\" selected=\"selected\">$_分</option>";
		} else {
			$form_date .= "<option value=\"$minute\">$_分</option>";
		}
	}
	$form_date .= '</select>';

	my($form_name, $form_name_start, $form_name_end);
	if ($self->{config}->{user_mode}) {
		my $cookie_ins = new webliberty::Cookie($self->{config}->{cookie_admin}, $self->{init}->{des_key});
		my $info_user = $cookie_ins->get_cookie('admin_user');

		my $flag;

		open(FH, $self->{init}->{data_user}) or $self->error("Read Error : $self->{init}->{data_user}");
		while (<FH>) {
			chomp;
			my($user, $pwd, $authority) = split(/\t/);

			my $name;
			if ($self->{user}->{$user}) {
				$name = "$user（$self->{user}->{$user}）";
			} else {
				$name = $user;
			}

			if ($user eq $name_ins->get_string) {
				$form_name .= "<option value=\"$user\" selected=\"selected\">$name</option>";
			} else {
				$form_name .= "<option value=\"$user\">$name</option>";
			}
			if ($user eq $info_user and $authority eq 'root') {
				$flag = 1;
			}
		}
		close(FH);

		if ($flag) {
			$form_name = "<select name=\"name\" xml:lang=\"ja\" lang=\"ja\">$form_name</select>";
		} else {
			$form_name = '';
			$form_name_start = '<!--';
			$form_name_end   = '-->';
		}
	} else {
		$form_name_start = '<!--';
		$form_name_end   = '-->';
	}

	my($form_stock, $form_stock_start, $form_stock_end);
	if ($self->{config}->{use_stock}) {
		if ($self->{query}->{stock}) {
			$form_stock = $self->{query}->{stock};
		} elsif (-s "$self->{init}->{data_stock_dir}" . $no_ins->get_string . "\.$self->{init}->{data_ext}") {
			open(FH, "$self->{init}->{data_stock_dir}" . $no_ins->get_string . "\.$self->{init}->{data_ext}") or $self->error("Read Error : $self->{init}->{data_stock_dir}" . $no_ins->get_string . "\.$self->{init}->{data_ext}");
			$form_stock = <FH>;
			close(FH);
		}
	} else {
		$form_stock_start = '<!--';
		$form_stock_end   = '-->';
	}

	my($form_relate, $form_relate_start, $form_relate_end);
	if ($self->{config}->{use_relate}) {
		if ($self->{query}->{relate}) {
			my $relate_ins = new webliberty::String($self->{query}->{relate});
			$relate_ins->create_text;
			$relate_ins->create_plain;

			$form_relate = $relate_ins->get_string;
		} elsif (-s "$self->{init}->{data_relate_dir}" . $no_ins->get_string . "\.$self->{init}->{data_ext}") {
			open(FH, "$self->{init}->{data_relate_dir}" . $no_ins->get_string . "\.$self->{init}->{data_ext}") or $self->error("Read Error : $self->{init}->{data_relate_dir}" . $no_ins->get_string . "\.$self->{init}->{data_ext}");
			while (<FH>) {
				chomp;
				my($id, $name) = split(/\t/);

				if ($name) {
					$form_relate .= "$id,$name\n";
				} else {
					$form_relate .= "$id\n";
				}
			}
			close(FH);
		}
	} else {
		$form_relate_start = '<!--';
		$form_relate_end   = '-->';
	}

	my($form_icon, $form_icon_start, $form_icon_end);
	if ($self->{config}->{use_icon}) {
		open(FH, $self->{init}->{data_icon}) or $self->error("Read Error : $self->{init}->{data_icon}");
		while (<FH>) {
			chomp;
			my($file, $name, $field, $user, $pwd) = split(/\t/);

			if ($icon_ins->get_string eq $file) {
				$form_icon .= "<option value=\"$file\" selected=\"selected\">$name</option>";
			} else {
				$form_icon .= "<option value=\"$file\">$name</option>";
			}
		}
		close(FH);

		if ($form_icon) {
			$form_icon = "<option value=\"\">選択してください</option>$form_icon";
		} else {
			$form_icon = '<option value="">アイコンが登録されていません</option>';
		}
		$form_icon = "<select name=\"icon\" xml:lang=\"ja\" lang=\"ja\">$form_icon</select>";
	} else {
		$form_icon_start = '<!--';
		$form_icon_end   = '-->';
	}

	my($form_image, $form_image_start, $form_image_end);
	if ($self->{config}->{use_image}) {
		if ($image_ins->get_string) {
			$form_image = $image_ins->get_string;

			my $check;
			if ($self->{query}->{delimage}) {
				$check = ' checked="checked"';
			}

			$form_image = "<input type=\"checkbox\" name=\"delimage\" id=\"delimage_checkbox\" value=\"on\"$check /> <label for=\"delimage_checkbox\">$form_imageを削除</label>";
		}

		if ($self->{query}->{image}) {
			my $file_ins = new webliberty::File($self->{query}->{image}->{file_name});

			$form_image .= '<input type="hidden" name="image_ext" value="' . $file_ins->get_ext . '" />';
		} elsif ($self->{query}->{image_ext}) {
			$form_image .= '<input type="hidden" name="image_ext" value="' . $self->{query}->{image_ext} . '" />';
		}
	} else {
		$form_image_start = '<!--';
		$form_image_end   = '-->';
	}

	my($form_file_start, $form_file_end);
	if (!$self->{config}->{use_file}) {
		$form_file_start = '<!--';
		$form_file_end   = '-->';
	}

	my($form_tburl_start, $form_tburl_end);
	if (!$self->{config}->{use_tburl}) {
		$form_tburl_start = '<!--';
		$form_tburl_end   = '-->';
	}

	my($form_ping_start, $form_ping_end);
	if (!$self->{config}->{ping_mode}) {
		$form_ping_start = '<!--';
		$form_ping_end   = '-->';
	}

	return(
		FORM_NO           => $no_ins->get_string,
		FORM_ID           => $id_ins->get_string,
		FORM_STAT         => $form_stat,
		FORM_BREAK        => $form_break,
		FORM_VIEW         => $form_view,
		FORM_COMT         => $form_comt,
		FORM_TB           => $form_tb,
		FORM_FIELD        => $form_field,
		FORM_FIELD_START  => $form_field_start,
		FORM_FIELD_END    => $form_field_end,
		FORM_DATE         => $form_date,
		FORM_NAME         => $form_name,
		FORM_NAME_START   => $form_name_start,
		FORM_NAME_END     => $form_name_end,
		FORM_SUBJ         => $subj_ins->get_string,
		FORM_TEXT         => $text_ins->get_string,
		FORM_PRICE        => $price_ins->get_string,
		FORM_STOCK        => $form_stock,
		FORM_STOCK_START  => $form_stock_start,
		FORM_STOCK_END    => $form_stock_end,
		FORM_RELATE       => $form_relate,
		FORM_RELATE_START => $form_relate_start,
		FORM_RELATE_END   => $form_relate_end,
		FORM_ICON         => $form_icon,
		FORM_ICON_START   => $form_icon_start,
		FORM_ICON_END     => $form_icon_end,
		FORM_IMAGE        => $form_image,
		FORM_IMAGE_START  => $form_image_start,
		FORM_IMAGE_END    => $form_image_end,
		FORM_FILE_START   => $form_file_start,
		FORM_FILE_END     => $form_file_end,
		FORM_HOST         => $host_ins->get_string,
		FORM_TBURL_START  => $form_tburl_start,
		FORM_TBURL_END    => $form_tburl_end,
		FORM_PING_START   => $form_ping_start,
		FORM_PING_END     => $form_ping_end
	);
}

### コメントフォーム作成
sub comment_form {
	my $self = shift;
	my($no, $pno, $stat, $date, $name, $mail, $url, $subj, $text, $rate, $pwd, $host) = @_;

	my $no_ins   = new webliberty::String($no);
	my $pno_ins  = new webliberty::String($pno);
	my $stat_ins = new webliberty::String($stat);
	my $date_ins = new webliberty::String($date);
	my $name_ins = new webliberty::String($name);
	my $mail_ins = new webliberty::String($mail);
	my $url_ins  = new webliberty::String($url);
	my $subj_ins = new webliberty::String($subj);
	my $text_ins = new webliberty::String($text);
	my $rate_ins = new webliberty::String($rate);
	my $pwd_ins  = new webliberty::String($pwd);
	my $host_ins = new webliberty::String($host);

	$no_ins->create_number;
	$pno_ins->create_number;
	$stat_ins->create_number;
	$date_ins->create_line;
	$name_ins->create_line;
	$mail_ins->create_line;
	$url_ins->create_line;
	$subj_ins->create_line;
	$text_ins->create_text;
	$rate_ins->create_line;
	$pwd_ins->create_line;
	$host_ins->create_line;

	$date_ins->create_plain;
	$name_ins->create_plain;
	$mail_ins->create_plain;
	$url_ins->create_plain;
	$subj_ins->create_plain;
	$text_ins->create_plain;
	$rate_ins->create_plain;
	$pwd_ins->create_plain;
	$host_ins->create_plain;

	if (!$url_ins->get_string) {
		$url_ins->set_string('http://');
	}

	my($form_rate, $form_rate_start, $form_rate_end);
	if ($self->{config}->{rate_mode}) {
		foreach my $rate_info (split(/<>/, $self->{config}->{rate_info})) {
			my($rate_rank, $rate_name) = split(/\,/, $rate_info, 2);

			if ($rate_ins->get_string eq $rate_rank) {
				$form_rate .= "<option value=\"$rate_rank\" selected=\"selected\">$rate_name</option>";
			} else {
				$form_rate .= "<option value=\"$rate_rank\">$rate_name</option>";
			}
		}

		$form_rate = "<select name=\"rate\" xml:lang=\"ja\" lang=\"ja\"><option value=\"\">選択してください</option>$form_rate</select>";
	} else {
		$form_rate_start = '<!--';
		$form_rate_end   = '-->';
	}

	return(
		FORM_NO         => $no_ins->get_string,
		FORM_PNO        => $pno_ins->get_string,
		FORM_DATE       => $date_ins->get_string,
		FORM_NAME       => $name_ins->get_string,
		FORM_MAIL       => $mail_ins->get_string,
		FORM_URL        => $url_ins->get_string,
		FORM_SUBJ       => $subj_ins->get_string,
		FORM_TEXT       => $text_ins->get_string,
		FORM_RATE       => $form_rate,
		FORM_RATE_START => $form_rate_start,
		FORM_RATE_END   => $form_rate_end,
		FORM_PWD        => $pwd_ins->get_string,
		FORM_HOST       => $host_ins->get_string
	);
}

### 商品データ作成
sub catalog_article {
	my $self = shift;
	my($no, $id, $stat, $break, $view, $comt, $tb, $field, $date, $name, $subj, $text, $price, $icon, $image, $file, $host) = @_;

	my $no_ins    = new webliberty::String($no);
	my $id_ins    = new webliberty::String($id);
	my $stat_ins  = new webliberty::String($stat);
	my $break_ins = new webliberty::String($break);
	my $view_ins  = new webliberty::String($view);
	my $comt_ins  = new webliberty::String($comt);
	my $tb_ins    = new webliberty::String($tb);
	my $field_ins = new webliberty::String($field);
	my $date_ins  = new webliberty::String($date);
	my $name_ins  = new webliberty::String($name);
	my $subj_ins  = new webliberty::String($subj);
	my $text_ins  = new webliberty::String($text);
	my $price_ins = new webliberty::String($price);
	my $icon_ins  = new webliberty::String($icon);
	my $image_ins = new webliberty::String($image);
	my $file_ins  = new webliberty::String($file);
	my $host_ins  = new webliberty::String($host);

	$no_ins->create_number;
	$id_ins->create_line;
	$stat_ins->create_number;
	$break_ins->create_number;
	$view_ins->create_number;
	$comt_ins->create_number;
	$tb_ins->create_number;
	$field_ins->create_line;
	$date_ins->create_line;
	$name_ins->create_line;
	$subj_ins->create_line;
	$text_ins->create_text;
	$price_ins->create_line;
	$icon_ins->create_line;
	$image_ins->create_line;
	$file_ins->create_line;
	$host_ins->create_line;

	if ($stat_ins->get_string == 1) {
		$stat_ins->set_string('公開');
	} else {
		$stat_ins->set_string('下書き');
	}

	my($article_new_start, $article_new_end);
	if ($date_ins->get_string =~ /^(\d\d\d\d)(\d\d)(\d\d)\d\d\d\d$/) {
		my($sec, $min, $hour, $day, $mon, $year, $week) = localtime(time);
		my $day_ins = new webliberty::Date;

		if ($day_ins->get_interval(sprintf("%04d-%02d-%02d", $year + 1900, $mon + 1, $day), "$1-$2-$3") >= $self->{config}->{new_days}) {
			$article_new_start = '<!--';
			$article_new_end   = '-->';
		}
	}

	my($article_year, $article_month, $article_day, $article_hour, $article_minute, $article_week, $article_date_start, $article_date_end);
	if ($date_ins->get_string =~ /^(\d\d\d\d)(\d\d)(\d\d)(\d\d)(\d\d)$/) {
		my $day_ins  = new webliberty::Date;
		my $week = $day_ins->get_week("$1-$2-$3");

		$article_year   = $1;
		$article_month  = $2;
		$article_day    = $3;
		$article_hour   = $4;
		$article_minute = $5;
		$article_week   = ${$self->{init}->{weeks}}[$week];

		$date_ins->set_string("$article_year-$article_month-$article_day $article_hour:$article_minute");
	} else {
		$article_date_start = '<!--';
		$article_date_end   = '-->';
	}

	my $fcode_ins = new webliberty::Encoder($field_ins->get_string);
	if ($field_ins->get_string =~ /^(.+)<>(.+)$/) {
		$field_ins->set_string("$1::$2");
	} elsif (!$field_ins->get_string) {
		$field_ins->set_string('未分類');
	}
	my($article_field_start, $article_field_end);
	if (!$self->{config}->{use_field}) {
		$article_field_start = '<!--';
		$article_field_end   = '-->';
	}

	my($article_name_start, $article_name_end);
	if (!$self->{config}->{user_mode}) {
		$article_name_start = '<!--';
		$article_name_end   = '-->';
	}
	my $article_user = $name_ins->get_string;

	if ($self->{config}->{user_mode}) {
		if ($self->{user}->{$name_ins->get_string}) {
			$name_ins->set_string($self->{user}->{$name_ins->get_string});
		} else {
			$name_ins->set_string($name_ins->get_string);
		}
	} else {
		$name_ins->set_string('管理者');
	}

	if (!$subj_ins->get_string) {
		$subj_ins->set_string('No Title');
	}

	$text_ins->replace_string('<br />', "\n");
	$text_ins->permit_html;

	my $info_path;
	if ($self->{init}->{script_file} =~ /([^\/\\]*)$/) {
		$info_path = "$self->{config}->{site_url}$1";
	}

	my($article_stock, $article_stock_start, $article_stock_end, $article_soldout_start, $article_soldout_end);
	if ($self->{config}->{use_stock}) {
		if ($self->{query}->{mode} eq 'admin' and $self->{query}->{stock}) {
			$article_stock = $self->{query}->{stock};
		} elsif (-s "$self->{init}->{data_stock_dir}" . $no_ins->get_string . "\.$self->{init}->{data_ext}") {
			open(STOCK, "$self->{init}->{data_stock_dir}" . $no_ins->get_string . "\.$self->{init}->{data_ext}") or $self->error("Read Error : $self->{init}->{data_stock_dir}" . $no_ins->get_string . "\.$self->{init}->{data_ext}");
			$article_stock = <STOCK>;
			close(STOCK);
		} else {
			$article_stock = 0;
		}
		if ($article_stock > 0) {
			$article_soldout_start = '<!--';
			$article_soldout_end   = '-->';
		} else {
			$article_stock_start = '<!--';
			$article_stock_end   = '-->';
		}
	} else {
		$article_stock_start = '<!--';
		$article_stock_end   = '-->';

		$article_soldout_start = '<!--';
		$article_soldout_end   = '-->';
	}

	my($article_relate, $article_relate_start, $article_relate_end);
	if ($self->{config}->{use_relate}) {
		if ($self->{query}->{mode} eq 'admin' and $self->{query}->{relate}) {
			my $relate_ins = new webliberty::String($self->{query}->{relate});
			$relate_ins->create_text;

			foreach (split(/<br \/>/, $relate_ins->get_string)) {
				my($id, $subj) = split(/,/, $_, 2);

				if (!$subj) {
					$subj = $id;
				}

				$article_relate .= "<option value=\"$id\">$subj</option>";
			}
		} elsif (-s "$self->{init}->{data_relate_dir}" . $no_ins->get_string . "\.$self->{init}->{data_ext}") {
			open(RELATE, "$self->{init}->{data_relate_dir}" . $no_ins->get_string . "\.$self->{init}->{data_ext}") or $self->error("Read Error : $self->{init}->{data_relate_dir}" . $no_ins->get_string . "\.$self->{init}->{data_ext}");
			while (<RELATE>) {
				chomp;
				my($id, $subj) = split(/\t/, $_, 2);

				if (!$subj) {
					$subj = $id;
				}

				$article_relate .= "<option value=\"$id\">$subj</option>";
			}
			close(RELATE);
		}
	}
	if (!$article_relate) {
		$article_relate_start = '<!--';
		$article_relate_end   = '-->';
	}

	if (!$price_ins->get_string) {
		$price_ins->set_string('-');
	}

	my($article_icon_start, $article_icon_end);
	if ($icon_ins->get_string) {
		my $file_path;
		if ($self->{init}->{data_icon_path}) {
			$file_path = $self->{init}->{data_icon_path};
		} else {
			$self->{init}->{data_icon_dir} =~ s/^\.\///;
			$file_path = "$self->{config}->{site_url}$self->{init}->{data_icon_dir}";
		}
		$icon_ins->set_string("<img src=\"$file_path" . $icon_ins->get_string . "\" alt=\"アイコン\" />");
	} else {
		$article_icon_start = '<!--';
		$article_icon_end   = '-->';
	}

	$self->{init}->{data_image_dir}     =~ s/^\.\///;
	$self->{init}->{data_upfile_dir}    =~ s/^\.\///;
	$self->{init}->{data_thumbnail_dir} =~ s/^\.\///;

	my $info_image_path     = "$self->{config}->{site_url}$self->{init}->{data_image_dir}";
	my $info_upfile_path    = "$self->{config}->{site_url}$self->{init}->{data_upfile_dir}";
	my $info_thumbnail_path = "$self->{config}->{site_url}$self->{init}->{data_thumbnail_dir}";

	my($article_image, $article_image_start, $article_image_end);
	if ($image_ins->get_string) {
		my $file_ins = new webliberty::File($self->{init}->{data_image_dir} . $image_ins->get_string);
		my($width, $height) = $file_ins->get_size;

		if ($self->{init}->{data_image_path}) {
			$info_image_path = $self->{init}->{data_image_path};
		}

		$article_image = "<img src=\"$info_image_path" . $image_ins->get_string . "\" alt=\"" . $subj_ins->get_string . "\" width=\"$width\" height=\"$height\" />";
	} else {
		$article_image_start = '<!--';
		$article_image_end   = '-->';
	}

	my($article_files, $article_file_start, $article_file_end);
	my @article_file = split(/<>/, $file_ins->get_string);

	my $file_all = 0;
	foreach (@article_file) {
		if ($_) {
			$file_all++;
		}
	}

	my $target = " $self->{config}->{file_attribute}";
	$target =~ s/&quot;/"/g;

	if ($article_file[0]) {
		for (my $i = $#article_file + 1; $i > 0; $i--) {
			my $article_file = $article_file[$i - 1];

			my $file_ins = new webliberty::File("$self->{init}->{data_upfile_dir}$article_file");
			my($width, $height) = $file_ins->get_size;

			my $flag;
			if ($width > $self->{config}->{img_maxwidth}) {
				$height = int($height / ($width / $self->{config}->{img_maxwidth}));
				$width  = $self->{config}->{img_maxwidth};

				$flag = 1;
			}

			if ($self->{init}->{data_upfile_path}) {
				$info_upfile_path = $self->{init}->{data_upfile_path};
			}
			if ($self->{init}->{data_thumbnail_path}) {
				$info_thumbnail_path = $self->{init}->{data_thumbnail_path};
			}

			my($file_path, $file);
			if ($self->{config}->{thumbnail_mode} and $flag) {
				$file_path = $info_thumbnail_path;
			} else {
				$file_path = $info_upfile_path;
			}

			if ($article_file) {
				if ($self->{agent} eq 'mobile' and $width > 0 and $height > 0) {
					$file = "<a href=\"$file_path$article_file\">ファイル $article_file</a><br />";
				} elsif ($flag and $width > 0 and $height > 0) {
					$file = "<a href=\"$file_path$article_file\"$target><img src=\"$file_path$article_file\" alt=\"ファイル $article_file\" width=\"$width\" height=\"$height\" /></a>";
				} elsif ($width > 0 and $height > 0) {
					$file = "<img src=\"$file_path$article_file\" alt=\"ファイル $article_file\" width=\"$width\" height=\"$height\" />";
				} else {
					$file = "<a href=\"$info_upfile_path$article_file\"$target>ファイル $article_file</a><br />";
				}
			}

			if ($text_ins->get_string =~ /\$FILE${i}_path/) {
				$file = "$info_upfile_path$article_file";

				$text_ins->replace_string('\$FILE' . $i . '_path', $file);
			} elsif ($text_ins->get_string =~ /\$FILE${i}_full(\([^\)]+\))?/) {
				if ($self->{agent} ne 'mobile' and $flag and $width > 0 and $height > 0) {
					my($width, $height) = $file_ins->get_size;

					$file = "<img src=\"$info_upfile_path$article_file\" alt=\"ファイル $article_file\" width=\"$width\" height=\"$height\" />";
				}

				if ($1 =~ /\((.+)\)/) {
					my $alt = $1;
					$file =~ s/ファイル $article_file/$alt/;
				}

				$text_ins->replace_string('\$FILE' . $i . '_full(\([^\)]+\))?', $file);
			} elsif ($text_ins->get_string =~ /\$FILE${i}_l(\([^\)]+\))?/) {
				if ($1 =~ /\((.+)\)/) {
					my $alt = $1;
					$file =~ s/ファイル $article_file/$alt/;
				}

				$file =~ s/ \/>/ style="float:left" \/>/;

				$text_ins->replace_string('\$FILE' . $i . '_l(\([^\)]+\))?', $file);
			} elsif ($text_ins->get_string =~ /\$FILE${i}_r(\([^\)]+\))?/) {
				if ($1 =~ /\((.+)\)/) {
					my $alt = $1;
					$file =~ s/ファイル $article_file/$alt/;
				}

				$file =~ s/ \/>/ style="float:right" \/>/;

				$text_ins->replace_string('\$FILE' . $i . '_r(\([^\)]+\))?', $file);
			} elsif ($text_ins->get_string =~ /\$FILE${i}_c(\([^\)]+\))?/) {
				if ($1 =~ /\((.+)\)/) {
					my $alt = $1;
					$file =~ s/ファイル $article_file/$alt/;
				}

				$file = "<span style=\"text-align:center;display:block;\">$file</span>";

				$text_ins->replace_string('\$FILE' . $i . '_c(\([^\)]+\))?', $file);
			} elsif ($text_ins->get_string =~ /\$FILE$i(\([^\)]+\))?/) {
				if ($1 =~ /\((.+)\)/) {
					my $alt = $1;
					$file =~ s/ファイル $article_file/$alt/;
				}

				$text_ins->replace_string('\$FILE' . $i . '(\([^\)]+\))?', $file);
			} else {
				if ($view_ins->get_string) {
					if ($self->{query}->{file}) {
						if ($i == $self->{query}->{file}) {
							$article_files .= $file;
						}
					} else {
						if ($i == 1) {
							$article_files .= $file;
						}
					}
				} else {
					$article_files .= $file;
				}
			}
		}
	}
	if (!$article_files) {
		$article_file_start = '<!--';
		$article_file_end   = '-->';
	}

	my($article_navi_start, $article_navi_end, $article_navi_prev_start, $article_navi_prev_end, $article_navi_next_start, $article_navi_next_end);
	if ($file_ins->get_string and $view_ins->get_string and $file_all > 1) {
		my $info;
		if ($id_ins->get_string) {
			$info = 'id=' . $id_ins->get_string;
		} else {
			$info = 'no=' . $no_ins->get_string;
		}

		if ($self->{query}->{file} > 1) {
			$article_navi_prev_start = "<a href=\"$info_path?$info&amp;file=" . ($self->{query}->{file} - 1) . "\">";
			$article_navi_prev_end   = "</a>";
		}
		if ($self->{query}->{file} < $file_all) {
			my $next;
			if ($self->{query}->{file} > 1) {
				$next = $self->{query}->{file} + 1;
			} else {
				$next = 2;
			}

			$article_navi_next_start = "<a href=\"$info_path?$info&amp;file=$next\">";
			$article_navi_next_end   = "</a>";
		}
	} else {
		$article_navi_start = '<!--';
		$article_navi_end   = '-->';
	}

	my $article_navi = $self->{query}->{file} || 1;

	if ($text_ins->get_string =~ /(^|[^-]+)(-----)[^-]+/) {
		my $spliter = quotemeta($2);

		my $continue;
		if ($text_ins->get_string =~ /(^|[^-]+)(-----)(.+)(-----)[^-]+/) {
			$spliter  = quotemeta("$2$3$4");
			$continue = $3;
		} else {
			$continue = $self->{config}->{continue_text};
		}

		my $info_path;
		if ($self->{init}->{script_file} =~ /([^\/\\]*)$/) {
			$info_path = "$self->{config}->{site_url}$1";
		}

		if ($self->{query}->{mode} or $self->{query}->{continue}) {
			if ($self->{query}->{mode} eq 'rss') {
				$text = (split(/$spliter/, $text_ins->get_string, 2))[0];
				$text_ins->set_string("$text");
			} else {
				$text_ins->replace_string($spliter, '<span id="continue" ><span style="display:none;">続き</span></span>');
			}
		} else {
			$text = (split(/$spliter/, $text_ins->get_string, 2))[0];
			if ($id_ins->get_string) {
				$text_ins->set_string("$text<a href=\"$info_path?id=$id&amp;continue=on#continue\">$continue</a>");
			} else {
				$text_ins->set_string("$text<a href=\"$info_path?no=$no&amp;continue=on#continue\">$continue</a>");
			}

			if (!$break_ins->get_string) {
				$text_ins->set_string($text_ins->get_string . '</p>');
			}
		}
	}

	if ($break_ins->get_string) {
		if ($self->{config}->{decoration_mode}) {
			my $decoration_ins = new webliberty::Decoration($text_ins->get_string);
			$decoration_ins->init_decoration(
				'article'   => 'no' . $no_ins->get_string .'_',
				'paragraph' => $self->{config}->{paragraph_mode},
				'heading'   => 'h4,h5,h6'
			);
			$text_ins->set_string($decoration_ins->create_decoration);
		} else {
			if ($self->{config}->{paragraph_mode}) {
				$text_ins->replace_string("\n\n", "</p><p>");
			}
			$text_ins->set_string("<p>" . $text_ins->get_string . '</p>');
			$text_ins->replace_string("\n", '<br />');
		}
	}
	if ($self->{config}->{autolink_mode}) {
		$text_ins->create_link($self->{config}->{autolink_attribute});
	}

	my $article_url;
	if ($no_ins->get_string) {
		if ($id_ins->get_string) {
			$article_url = $id_ins->get_string;
		} else {
			$article_url = $no_ins->get_string;
		}

		if ($self->{config}->{html_archive_mode} and $self->{agent} ne 'mobile') {
			if ($self->{init}->{archive_path}) {
				$article_url = $self->{init}->{archive_path} . $article_url . "\.$self->{init}->{archive_ext}";
			} elsif ($self->{init}->{archive_dir} =~ /([^\/\\]*\/)$/) {
				$article_url = "$self->{config}->{site_url}$1" . $article_url . "\.$self->{init}->{archive_ext}";
			}
		} else {
			if ($self->{init}->{script_file} =~ /([^\/\\]*)$/) {
				if ($id_ins->get_string) {
					$article_url = "$self->{config}->{site_url}$1?id=" . $article_url;
				} else {
					$article_url = "$self->{config}->{site_url}$1?no=" . $article_url;
				}
			}
		}
	} else {
		if ($self->{init}->{archive_path}) {
			$article_url = $self->{init}->{archive_path};
		} else {
			$article_url = $self->{config}->{site_url};
		}
	}

	my $article_info;
	if ($article_image) {
		$article_info = $article_image;
	} elsif ($icon_ins->get_string) {
		$article_info = $icon_ins->get_string;
	} else {
		$article_info = $subj_ins->get_string;
	}

	if (!$self->{query}->{mode} and !$self->{query}->{no} and !$self->{query}->{id}) {
		$subj_ins->trim_string($self->{config}->{subj_length}, '...');
	}

	my($article_comment, $article_comment_start, $article_comment_end);
	if (-s $self->{init}->{data_comt_dir} . $no_ins->get_string . "\.$self->{init}->{data_ext}") {
		open(COMT, $self->{init}->{data_comt_dir} . $no_ins->get_string . "\.$self->{init}->{data_ext}") or $self->error("Read Error : $self->{init}->{data_comt_dir}" . $no_ins->get_string . "\.$self->{init}->{data_ext}");
		while (<COMT>) {
			$article_comment++;
		}
		close(COMT);
	} else {
		$article_comment = 0;
	}
	if ($self->{query}->{mode} or !$comt_ins->get_string) {
		$article_comment_start = '<!--';
		$article_comment_end   = '-->';
	}

	my($article_trackback, $article_trackback_start, $article_trackback_end);
	if (-s $self->{init}->{data_tb_dir} . $no_ins->get_string . "\.$self->{init}->{data_ext}") {
		open(TB, $self->{init}->{data_tb_dir} . $no_ins->get_string . "\.$self->{init}->{data_ext}") or $self->error("Read Error : $self->{init}->{data_comt_dir}" . $no_ins->get_string . "\.$self->{init}->{data_ext}");
		while (<TB>) {
			$article_trackback++;
		}
		close(TB);
	} else {
		$article_trackback = 0;
	}
	if ($self->{query}->{mode} or !$tb_ins->get_string) {
		$article_trackback_start = '<!--';
		$article_trackback_end   = '-->';
	}

	my($article_admin_start, $article_admin_end);
	if ($self->{query}->{mode} or *STDOUT ne "*main::STDOUT" or !$self->{admin} or !$no_ins->get_string) {
		$article_admin_start = '<!--';
		$article_admin_end   = '-->';
	}

	my %option;
	if (-s $self->{init}->{data_option}) {
		my %type;
		open(OPTION, $self->{init}->{data_option}) or $self->error("Read Error : $self->{init}->{data_option}");
		while (<OPTION>) {
			chomp;
			my($id, $name, $type, $default) = split(/\t/);

			$type{"option_$id"} = $type;

			$option{'OPTION_' . uc($id)}            = '';
			$option{'OPTION_' . uc($id) . '_START'} = '<!--';
			$option{'OPTION_' . uc($id) . '_END'}   = '-->';
		}
		close(OPTION);

		if ($self->{query}->{exec_preview}) {
			my $option;
			foreach (keys %{$self->{query}}) {
				if (${$self->{query}}{$_}) {
					my $query = ${$self->{query}}{$_};

					if ($_ =~ /^option_.+/ and $query) {
						$query =~ s/\r?\n/\r/g;
						$query =~ s/\r/<>/g;
					}

					if ($type{$_} eq 'option') {
						my $data;
						foreach my $option (/<>/, $query) {
							$data .= "<option value=\"$option\">$option</option>";
						}
						$query = $data;
					} elsif ($type{$_} eq 'textarea') {
						$query =~ s/<>/<br \/>/g;
					} elsif ($type{$_} eq 'checkbox') {
						$query =~ s/((<>)+)/、/g;
					}

					$option{uc($_)}            = $query;
					$option{uc($_) . '_START'} = '';
					$option{uc($_) . '_END'}   = '';
				}
			}
		} elsif (open(OPTION, "$self->{init}->{data_option_dir}" . $no_ins->get_string . "\.$self->{init}->{data_ext}")) {
			while (<OPTION>) {
				chomp;
				my($key, $value) = split(/\t/);

				if ($type{$key} eq 'option') {
					my $data;
					foreach my $option (split(/<>/, $value)) {
						$data .= "<option value=\"$option\">$option</option>";
					}
					$value = $data;
				} elsif ($type{$key} eq 'textarea') {
					$value =~ s/<>/<br \/>/g;
				} elsif ($type{$key} eq 'checkbox') {
					$value =~ s/((<>)+)/、/g;
				}

				$option{uc($key)}            = $value;
				$option{uc($key) . '_START'} = '';
				$option{uc($key) . '_END'}   = '';
			}
			close(OPTION);
		} elsif (open(OPTION, $self->{init}->{data_option})) {
			while (<OPTION>) {
				chomp;
				my($id, $name, $type, $default) = split(/\t/);

				$option{uc("option_$id")}            = '';
				$option{uc("option_$id") . '_START'} = '<!--';
				$option{uc("option_$id") . '_END'}   = '-->';
			}
			close(OPTION);
		}
	}

	my $plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
	my %plugin = $plugin_ins->article(
		'type'  => 'catalog',
		'date'  => $date_ins->get_string,
		'no'    => $no_ins->get_string,
		'id'    => $id_ins->get_string,
		'stat'  => $stat_ins->get_string,
		'field' => $field_ins->get_string,
		'name'  => $name_ins->get_string
	);

	return(
		ARTICLE_NO              => $no_ins->get_string,
		ARTICLE_ID              => $id_ins->get_string,
		ARTICLE_STAT            => $stat_ins->get_string,
		ARTICLE_FIELD           => $field_ins->get_string,
		ARTICLE_FNO             => $self->{field}->{$field},
		ARTICLE_FCODE           => $fcode_ins->url_encode,
		ARTICLE_FIELD_START     => $article_field_start,
		ARTICLE_FIELD_END       => $article_field_end,
		ARTICLE_DATE            => $date_ins->get_string,
		ARTICLE_DATE_START      => $article_date_start,
		ARTICLE_DATE_END        => $article_date_end,
		ARTICLE_YEAR            => $article_year,
		ARTICLE_MONTH           => $article_month,
		ARTICLE_DAY             => $article_day,
		ARTICLE_HOUR            => $article_hour,
		ARTICLE_MINUTE          => $article_minute,
		ARTICLE_WEEK            => $article_week,
		ARTICLE_NAME            => $name_ins->get_string,
		ARTICLE_NAME_START      => $article_name_start,
		ARTICLE_NAME_END        => $article_name_end,
		ARTICLE_USER            => $article_user,
		ARTICLE_SUBJ            => $subj_ins->get_string,
		ARTICLE_TEXT            => $text_ins->get_string,
		ARTICLE_PRICE           => $price_ins->get_string,
		ARTICLE_STOCK           => $article_stock,
		ARTICLE_STOCK_START     => $article_stock_start,
		ARTICLE_STOCK_END       => $article_stock_end,
		ARTICLE_SOLDOUT_START   => $article_soldout_start,
		ARTICLE_SOLDOUT_END     => $article_soldout_end,
		ARTICLE_RELATE          => $article_relate,
		ARTICLE_RELATE_START    => $article_relate_start,
		ARTICLE_RELATE_END      => $article_relate_end,
		ARTICLE_ICON            => $icon_ins->get_string,
		ARTICLE_ICON_START      => $article_icon_start,
		ARTICLE_ICON_END        => $article_icon_end,
		ARTICLE_IMAGE           => $article_image,
		ARTICLE_IMAGE_START     => $article_image_start,
		ARTICLE_IMAGE_END       => $article_image_end,
		ARTICLE_FILE            => $file_all,
		ARTICLE_FILES           => $article_files,
		ARTICLE_FILE_START      => $article_file_start,
		ARTICLE_FILE_END        => $article_file_end,
		ARTICLE_NAVI            => $article_navi,
		ARTICLE_NAVI_START      => $article_navi_start,
		ARTICLE_NAVI_END        => $article_navi_end,
		ARTICLE_NAVI_PREV_START => $article_navi_prev_start,
		ARTICLE_NAVI_PREV_END   => $article_navi_prev_end,
		ARTICLE_NAVI_NEXT_START => $article_navi_next_start,
		ARTICLE_NAVI_NEXT_END   => $article_navi_next_end,
		ARTICLE_HOST            => $host_ins->get_string,
		ARTICLE_URL             => $article_url,
		ARTICLE_INFO            => $article_info,
		ARTICLE_NEW_START       => $article_new_start,
		ARTICLE_NEW_END         => $article_new_end,
		ARTICLE_COMMENT         => $article_comment,
		ARTICLE_COMMENT_START   => $article_comment_start,
		ARTICLE_COMMENT_END     => $article_comment_end,
		ARTICLE_TRACKBACK       => $article_trackback,
		ARTICLE_TRACKBACK_START => $article_trackback_start,
		ARTICLE_TRACKBACK_END   => $article_trackback_end,
		ARTICLE_ADMIN_START     => $article_admin_start,
		ARTICLE_ADMIN_END       => $article_admin_end,
		%option,
		%plugin
	);
}

### コメントデータ作成
sub comment_article {
	my $self = shift;
	my($no, $pno, $stat, $date, $name, $mail, $url, $subj, $text, $rate, $pwd, $host) = @_;

	my $no_ins   = new webliberty::String($no);
	my $pno_ins  = new webliberty::String($pno);
	my $stat_ins = new webliberty::String($stat);
	my $date_ins = new webliberty::String($date);
	my $name_ins = new webliberty::String($name);
	my $mail_ins = new webliberty::String($mail);
	my $url_ins  = new webliberty::String($url);
	my $subj_ins = new webliberty::String($subj);
	my $text_ins = new webliberty::String($text);
	my $rate_ins = new webliberty::String($rate);
	my $pwd_ins  = new webliberty::String($pwd);
	my $host_ins = new webliberty::String($host);

	$no_ins->create_number;
	$pno_ins->create_number;
	$stat_ins->create_number;
	$date_ins->create_line;
	$name_ins->create_line;
	$mail_ins->create_line;
	$url_ins->create_line;
	$subj_ins->create_line;
	$text_ins->create_text;
	$rate_ins->create_line;
	$pwd_ins->create_line;
	$host_ins->create_line;

	my($article_new_start, $article_new_end);
	if (time - $date_ins->get_string > 60 * 60 * 24 * $self->{config}->{new_days}) {
		$article_new_start = '<!--';
		$article_new_end   = '-->';
	}

	my($sec, $min, $hour, $day, $mon, $year, $week) = localtime($date_ins->get_string);

	my $article_year   = sprintf("%04d", $year + 1900);
	my $article_month  = sprintf("%02d", $mon + 1);
	my $article_day    = sprintf("%02d", $day);
	my $article_hour   = sprintf("%02d", $hour);
	my $article_minute = sprintf("%02d", $min);
	my $article_week   = ${$self->{init}->{weeks}}[$week];

	$date_ins->set_string("$article_year-$article_month-$article_day $article_hour:$article_minute");

	if (!$subj_ins->get_string) {
		$subj_ins->set_string('No Title');
	}
	if ($url_ins->get_string eq 'http://') {
		$url_ins->set_string('');
	}

	my($article_mail_start, $article_mail_end);
	if ($mail_ins->get_string) {
		$article_mail_start = '<a href="mailto:' . $mail_ins->get_string . '">';
		$article_mail_end   = '</a>';
	} else {
		$article_mail_start = '<!--';
		$article_mail_end   = '-->';
	}

	my($article_url_start, $article_url_end);
	if ($url_ins->get_string) {
		my $target = " $self->{config}->{autolink_attribute}";
		$target =~ s/&quot;/"/g;

		$article_url_start = '<a href="' . $url_ins->get_string . "\"$target>";
		$article_url_end   = '</a>';
	} else {
		$article_url_start = '<!--';
		$article_url_end   = '-->';
	}

	if ($self->{config}->{paragraph_mode}) {
		$text_ins->replace_string('<br /><br />', '</p><p>');
	}
	$text_ins->set_string('<p>' . $text_ins->get_string . '</p>');

	if ($self->{config}->{quotation_color}) {
		$text = $text_ins->get_string;

		my $quotation_color = $self->{config}->{quotation_color};

		$text =~ s/([\>]|^)(&gt;|＞)([^<]*)/$1<span style=\"color:$quotation_color;\">$2$3<\/span>/g;

		$text_ins->set_string($text);
	}
	if ($self->{config}->{autolink_mode}) {
		$text_ins->create_link($self->{config}->{autolink_attribute});
	}

	my($article_edit_start, $article_edit_end);
	if ($self->{query}->{exec_preview}) {
		$article_edit_start = '<!--';
		$article_edit_end   = '-->';
	}

	my($article_rate, $article_rate_start, $article_rate_end);
	if ($rate_ins->get_string) {
		foreach my $rate_info (split(/<>/, $self->{config}->{rate_info})) {
			my($rate_rank, $rate_name) = split(/\,/, $rate_info, 2);

			if ($rate_ins->get_string eq $rate_rank) {
				$rate_ins->set_string($rate_name);

				last;
			}
		}
	} else {
		$article_rate_start = '<!--';
		$article_rate_end   = '-->';
	}

	if ($stat_ins->get_string == 1) {
		$stat_ins->set_string('承認');
	} else {
		$stat_ins->set_string('未承認');

		if (*STDOUT eq "*main::STDOUT" and $self->{admin}) {
			$text_ins->set_string('<p><strong>このコメントは未承認のため、管理者にのみ公開されています。</strong></p>' . $text_ins->get_string);
		} else {
			if ($self->{query}->{mode} ne 'admin' and $self->{query}->{mode} ne 'edit') {
				$name_ins->set_string('未承認');
				$mail_ins->set_string('');
				$url_ins->set_string('');
				$subj_ins->set_string('未承認');
				$text_ins->set_string('<p>管理者に承認されるまで内容は表示されません。</p>');
				$rate_ins->get_string('');

				$article_mail_start = '<!--';
				$article_mail_end   = '-->';

				$article_url_start = '<!--';
				$article_url_end   = '-->';

				$article_rate_start = '<!--';
				$article_rate_end   = '-->';
			}
		}
	}

	my $plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
	my %plugin = $plugin_ins->article(
		'type' => 'comment',
		'no'   => $no_ins->get_string,
		'pno'  => $pno_ins->get_string,
		'stat' => $stat_ins->get_string,
		'date' => $date_ins->get_string,
		'name' => $name_ins->get_string,
		'subj' => $subj_ins->get_string,
		'host' => $host_ins->get_string
	);

	return(
		ARTICLE_NO         => $no_ins->get_string,
		ARTICLE_PNO        => $pno_ins->get_string,
		ARTICLE_STAT       => $stat_ins->get_string,
		ARTICLE_DATE       => $date_ins->get_string,
		ARTICLE_YEAR       => $article_year,
		ARTICLE_MONTH      => $article_month,
		ARTICLE_DAY        => $article_day,
		ARTICLE_HOUR       => $article_hour,
		ARTICLE_MINUTE     => $article_minute,
		ARTICLE_WEEK       => $article_week,
		ARTICLE_NAME       => $name_ins->get_string,
		ARTICLE_MAIL       => $mail_ins->get_string,
		ARTICLE_MAIL_START => $article_mail_start,
		ARTICLE_MAIL_END   => $article_mail_end,
		ARTICLE_URL        => $url_ins->get_string,
		ARTICLE_URL_START  => $article_url_start,
		ARTICLE_URL_END    => $article_url_end,
		ARTICLE_SUBJ       => $subj_ins->get_string,
		ARTICLE_TEXT       => $text_ins->get_string,
		ARTICLE_RATE       => $rate_ins->get_string,
		ARTICLE_RATE_START => $article_rate_start,
		ARTICLE_RATE_END   => $article_rate_end,
		ARTICLE_PWD        => $pwd_ins->get_string,
		ARTICLE_HOST       => $host_ins->get_string,
		ARTICLE_NEW_START  => $article_new_start,
		ARTICLE_NEW_END    => $article_new_end,
		ARTICLE_EDIT_START => $article_edit_start,
		ARTICLE_EDIT_END   => $article_edit_end,
		%plugin
	);
}

### トラックバックデータ作成
sub trackback_article {
	my $self = shift;
	my($no, $pno, $stat, $date, $blog, $title, $url, $excerpt) = @_;

	my $no_ins      = new webliberty::String($no);
	my $pno_ins     = new webliberty::String($pno);
	my $stat_ins    = new webliberty::String($stat);
	my $date_ins    = new webliberty::String($date);
	my $blog_ins    = new webliberty::String($blog);
	my $title_ins   = new webliberty::String($title);
	my $url_ins     = new webliberty::String($url);
	my $excerpt_ins = new webliberty::String($excerpt);

	$no_ins->create_number;
	$pno_ins->create_number;
	$stat_ins->create_number;
	$date_ins->create_line;
	$blog_ins->create_line;
	$title_ins->create_line;
	$url_ins->create_line;
	$excerpt_ins->create_line;

	my($article_new_start, $article_new_end);
	if (time - $date_ins->get_string > 60 * 60 * 24 * $self->{config}->{new_days}) {
		$article_new_start = '<!--';
		$article_new_end   = '-->';
	}

	my($sec, $min, $hour, $day, $mon, $year, $week) = localtime($date_ins->get_string);

	my $article_year   = sprintf("%02d", $year + 1900);
	my $article_month  = sprintf("%02d", $mon + 1);
	my $article_day    = sprintf("%02d", $day);
	my $article_hour   = sprintf("%02d", $hour);
	my $article_minute = sprintf("%02d", $min);
	my $article_week   = ${$self->{init}->{weeks}}[$week];

	$date_ins->set_string("$article_year-$article_month-$article_day $article_hour:$article_minute");

	$excerpt_ins->replace_string('&amp;', '&');
	$excerpt_ins->replace_string('&lt;', '<');
	$excerpt_ins->replace_string('&gt;', '>');
	$excerpt_ins->replace_string('<[^>]*>', '');
	$excerpt_ins->replace_string('<', '&lt;');
	$excerpt_ins->replace_string('>', '&gt;');
	$excerpt_ins->replace_string('&', '&amp');

	$excerpt_ins->trim_string(300, '...');

	if ($stat_ins->get_string == 1) {
		$stat_ins->set_string('承認');
	} else {
		$stat_ins->set_string('未承認');

		if (*STDOUT eq "*main::STDOUT" and $self->{admin}) {
			$excerpt_ins->set_string('<strong>このトラックバックは未承認のため、管理者にのみ公開されています。</strong><br />' . $excerpt_ins->get_string);
		} else {
			if ($self->{query}->{mode} ne 'admin') {
				$blog_ins->set_string('未承認');
				$title_ins->set_string('未承認');
				$url_ins->set_string('');
				$excerpt_ins->set_string('管理者に承認されるまで内容は表示されません。');
			}
		}
	}

	my $plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
	my %plugin = $plugin_ins->article(
		'type'  => 'trackback',
		'no'    => $no_ins->get_string,
		'pno'   => $pno_ins->get_string,
		'stat'  => $stat_ins->get_string,
		'date'  => $date_ins->get_string,
		'blog'  => $blog_ins->get_string,
		'title' => $title_ins->get_string,
		'url'   => $url_ins->get_string,
	);

	return(
		TRACKBACK_NO        => $no_ins->get_string,
		TRACKBACK_PNO       => $pno_ins->get_string,
		TRACKBACK_STAT      => $stat_ins->get_string,
		TRACKBACK_DATE      => $date_ins->get_string,
		TRACKBACK_YEAR      => $article_year,
		TRACKBACK_MONTH     => $article_month,
		TRACKBACK_DAY       => $article_day,
		TRACKBACK_HOUR      => $article_hour,
		TRACKBACK_MINUTE    => $article_minute,
		TRACKBACK_WEEK      => $article_week,
		TRACKBACK_BLOG      => $blog_ins->get_string,
		TRACKBACK_TITLE     => $title_ins->get_string,
		TRACKBACK_URL       => $url_ins->get_string,
		TRACKBACK_EXCERPT   => $excerpt_ins->get_string,
		TRACKBACK_NEW_START => $article_new_start,
		TRACKBACK_NEW_END   => $article_new_end,
		%plugin
	);
}

### データ更新
sub update {
	my $self = shift;

	if ($self->{config}->{html_index_mode} or $self->{config}->{html_archive_mode} or $self->{config}->{html_field_mode} or $self->{config}->{show_navigation}) {
		require webliberty::App::List;
	}

	if ($self->{config}->{html_index_mode} or $self->{config}->{html_archive_mode} or $self->{config}->{html_field_mode}) {
		my $stdout = *STDOUT;

		#各分類を構築
		if ($self->{config}->{html_field_mode}) {
			foreach (split(/<>/, $self->{config}->{html_field_list})) {
				my($file, $field) = split(/,/, $_, 2);

				$field =~ s/&/&amp;/g;
				$field =~ s/::/&lt;&gt;/g;

				my $dammy;
				if ($self->{init}->{rewrite_mode}) {
					my $init_ins = new webliberty::App::Init;
					$dammy->{init} = $init_ins->get_init;
				} else {
					$dammy->{init} = $self->{init};
				}
				$dammy->{query}->{field} = $field;

				open(HTML, ">$file") or $self->error("Write Error : $file");
				*STDOUT = *HTML;
				my $app_ins = new webliberty::App::List($dammy->{init}, $self->{config}, $dammy->{query});
				$app_ins->run;
				close(HTML);

				if ($self->{init}->{chmod_mode}) {
					if ($self->{init}->{suexec_mode}) {
						chmod(0604, "$file") or $self->error("Chmod Error : $file");
					} else {
						chmod(0666, "$file") or $self->error("Chmod Error : $file");
					}
				}
			}
		}

		#アーカイブを構築
		if ($self->{config}->{html_archive_mode}) {
			my %index;
			open(FH, $self->{init}->{data_catalog_index}) or $self->error("Read Error : $self->{init}->{data_catalog_index}");
			while (<FH>) {
				chomp;
				my($date, $no, $id, $stat, $field, $name) = split(/\t/);

				$index{$no} = $id;
			}
			close(FH);

			foreach (split(/\n/, $self->{query}->{no})) {
				my $file_name;
				if ($index{$_}) {
					$file_name = $index{$_};
				} else {
					$file_name = $_;
				}

				my $dammy;
				if ($self->{init}->{rewrite_mode}) {
					my $init_ins = new webliberty::App::Init;
					$dammy->{init} = $init_ins->get_init;
				} else {
					$dammy->{init} = $self->{init};
				}
				$dammy->{query}->{no} = $_;

				open(HTML, ">$self->{init}->{archive_dir}$file_name\.$self->{init}->{archive_ext}") or $self->error("Write Error : $self->{init}->{archive_dir}$file_name\.$self->{init}->{archive_ext}");
				*STDOUT = *HTML;
				my $app_ins = new webliberty::App::List($dammy->{init}, $self->{config}, $dammy->{query});
				$app_ins->run;
				close(HTML);

				if ($self->{init}->{chmod_mode}) {
					if ($self->{init}->{suexec_mode}) {
						chmod(0604, "$self->{init}->{archive_dir}$file_name\.$self->{init}->{archive_ext}") or $self->error("Chmod Error : $self->{init}->{archive_dir}$file_name\.$self->{init}->{archive_ext}");
					} else {
						chmod(0666, "$self->{init}->{archive_dir}$file_name\.$self->{init}->{archive_ext}") or $self->error("Chmod Error : $self->{init}->{archive_dir}$file_name\.$self->{init}->{archive_ext}");
					}
				}
			}
		}

		#インデックスを構築
		if ($self->{config}->{html_index_mode}) {
			my $dammy;
			if ($self->{init}->{rewrite_mode}) {
				my $init_ins = new webliberty::App::Init;
				$dammy->{init} = $init_ins->get_init;
			} else {
				$dammy->{init} = $self->{init};
			}

			open(HTML, ">$self->{init}->{html_file}") or $self->error("Write Error : $self->{init}->{html_file}");
			*STDOUT = *HTML;
			my $app_ins = new webliberty::App::List($dammy->{init}, $self->{config});
			$app_ins->run;
			close(HTML);
		}

		*STDOUT = $stdout;
	}

	#ナビゲーションを構築
	if ($self->{config}->{show_navigation}) {
		my $app_ins     = new webliberty::App::List($self->{init}, $self->{config});
		my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config});
		my $plugin_ins  = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});

		my $skin_ins = new webliberty::Skin;
		$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_view}");
		$skin_ins->replace_skin(
			$catalog_ins->info,
			$plugin_ins->run
		);

		my($navi_start, $navi_end);
		if ($self->{config}->{pos_navigation}) {
			$navi_start = $app_ins->get_navi . $skin_ins->get_data('logs_head');
			$navi_end   = $skin_ins->get_data('logs_foot');
		} else {
			$navi_start = $skin_ins->get_data('logs_head');
			$navi_end   = $skin_ins->get_data('logs_foot') . $app_ins->get_navi;
		}

		my $script_ins = new webliberty::Script;

		my($flag, $message) = $script_ins->create_jscript(
			file     => $self->{init}->{js_navi_start_file},
			contents => $navi_start,
			break    => 1
		);
		if (!$flag) {
			$self->error($message);
		}

		my($flag, $message) = $script_ins->create_jscript(
			file     => $self->{init}->{js_navi_end_file},
			contents => $navi_end,
			break    => 1
		);
		if (!$flag) {
			$self->error($message);
		}
	}

	#サムネイル画像を作成
	if ($self->{config}->{thumbnail_mode}) {
		my $resize_pl;
		if ($self->{config}->{thumbnail_mode} == 2) {
			$resize_pl = $self->{init}->{resize_pl};
		}

		require webliberty::Thumbnail;
		my $thumbnail_ins = new webliberty::Thumbnail;
		my($flag, $message) = $thumbnail_ins->create_thumbnail(
			resize_pl     => $resize_pl,
			file_dir      => $self->{init}->{data_upfile_dir},
			thumbnail_dir => $self->{init}->{data_thumbnail_dir},
			img_max_width => $self->{config}->{img_maxwidth},
			limit         => 10
		);
		if (!$flag) {
			$self->error($message);
		}
	}

	#JSファイルを作成
	if ($self->{config}->{js_title_mode} or $self->{config}->{js_title_field_mode} or $self->{config}->{js_text_mode} or $self->{config}->{js_text_field_mode}) {
		opendir(DIR, $self->{init}->{data_catalog_dir}) or $self->error("Read Error : $self->{init}->{data_catalog_dir}");
		my @dir = sort { $b <=> $a } readdir(DIR);
		closedir(DIR);

		my $info_archive_path;
		if ($self->{config}->{html_archive_mode}) {
			if ($self->{init}->{archive_dir} =~ /([^\/\\]*\/)$/) {
				$info_archive_path = "$self->{config}->{site_url}$1";
			}
		} else {
			if ($self->{init}->{script_file} =~ /([^\/\\]*)$/) {
				$info_archive_path = "$self->{config}->{site_url}$1";
			}
		}

		my %title_field;
		if ($self->{config}->{js_title_field_mode}) {
			foreach (split(/<>/, $self->{config}->{js_title_field_list})) {
				my($file, $field) = split(/,/, $_, 2);

				$title_field{$field} = 0;
			}
		}

		my %text_field;
		if ($self->{config}->{js_text_field_mode}) {
			foreach (split(/<>/, $self->{config}->{js_text_field_list})) {
				my($file, $field) = split(/,/, $_, 2);

				$text_field{$field} = 0;
			}
		}

		my $skin_ins = new webliberty::Skin;
		$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_js_title}");
		$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_js_text}");
		$skin_ins->replace_skin(
			$self->info
		);

		my($js_title_data, $js_text_data, %js_title_data, %js_text_data, $i);

		foreach my $entry (@dir) {
			if ($entry !~ /^\d\d\d\d\d\d\.$self->{init}->{data_ext}$/) {
				next;
			}
			open(FH, "$self->{init}->{data_catalog_dir}$entry") or $self->error("Read Error : $self->{init}->{data_catalog_dir}$entry");
			while (<FH>) {
				chomp;
				my($no, $id, $stat, $break, $view, $comt, $tb, $field, $date, $name, $subj, $text, $price, $icon, $image, $file, $host) = split(/\t/);

				if (!$stat) {
					next;
				}

				$i++;
				if (!$self->{config}->{js_title_field_mode} and !$self->{config}->{js_text_field_mode} and $i > $self->{config}->{js_title_size} and $i > $self->{config}->{js_text_size}) {
					last;
				}

				my %article = $self->catalog_article($no, $id, $stat, $break, $view, $comt, $tb, $field, $date, $name, $subj, $text, $price, $icon, $image, $file, $host);

				my $title_data = $skin_ins->get_replace_data(
					'jstitle',
					%article
				);
				my $text_data = $skin_ins->get_replace_data(
					'jstext',
					%article
				);

				if (exists($title_field{$article{'ARTICLE_FIELD'}})) {
					$title_field{$article{'ARTICLE_FIELD'}}++;

					if ($title_field{$article{'ARTICLE_FIELD'}} <= $self->{config}->{js_title_size}) {
						$js_title_data{$article{'ARTICLE_FIELD'}} .= $title_data;
					}
				}
				if (exists($text_field{$article{'ARTICLE_FIELD'}})) {
					$text_field{$article{'ARTICLE_FIELD'}}++;

					if ($text_field{$article{'ARTICLE_FIELD'}} <= $self->{config}->{js_text_size}) {
						$js_text_data{$article{'ARTICLE_FIELD'}} .= $text_data;
					}
				}

				if ($field =~ /^(.+)<>.+$/) {
					my $parent = $1;

					if (exists($title_field{$parent})) {
						$title_field{$parent}++;

						if ($title_field{$parent} <= $self->{config}->{js_title_size}) {
							$js_title_data{$parent} .= $title_data;
						}
					}
					if (exists($text_field{$parent})) {
						$text_field{$parent}++;

						if ($text_field{$parent} <= $self->{config}->{js_text_size}) {
							$js_text_data{$parent} .= $text_data;
						}
					}
				}

				if ($self->{config}->{js_title_size} and $i <= $self->{config}->{js_title_size}) {
					$js_title_data .= $title_data;
				}
				if ($self->{config}->{js_text_size} and $i <= $self->{config}->{js_text_size}) {
					$js_text_data .= $text_data;
				}
			}
			close(FH);
		}

		my $script_ins = new webliberty::Script;

		if ($self->{config}->{js_title_mode}) {
			my($flag, $message) = $script_ins->create_jscript(
				file     => $self->{init}->{js_title_file},
				contents => $skin_ins->get_data('jstitle_head') . $js_title_data . $skin_ins->get_data('jstitle_foot'),
				break    => 1
			);
			if (!$flag) {
				$self->error($message);
			}
		}
		if ($self->{config}->{js_text_mode}) {
			my($flag, $message) = $script_ins->create_jscript(
				file     => $self->{init}->{js_text_file},
				contents => $skin_ins->get_data('jstext_head') . $js_text_data . $skin_ins->get_data('jstext_foot'),
				break    => 1
			);
			if (!$flag) {
				$self->error($message);
			}
		}

		if ($self->{config}->{js_title_field_mode}) {
			foreach (split(/<>/, $self->{config}->{js_title_field_list})) {
				my($file, $field) = split(/,/, $_, 2);

				my($flag, $message) = $script_ins->create_jscript(
					file     => $file,
					contents => $skin_ins->get_data('jstitle_head') . $js_title_data{$field} . $skin_ins->get_data('jstitle_foot'),
					break    => 1
				);
				if (!$flag) {
					$self->error($message);
				}
			}
		}
		if ($self->{config}->{js_text_field_mode}) {
			foreach (split(/<>/, $self->{config}->{js_text_field_list})) {
				my($file, $field) = split(/,/, $_, 2);

				my($flag, $message) = $script_ins->create_jscript(
					file     => $file,
					contents => $skin_ins->get_data('jstext_head') . $js_text_data{$field} . $skin_ins->get_data('jstext_foot'),
					break    => 1
				);
				if (!$flag) {
					$self->error($message);
				}
			}
		}
	}

	return;
}

### 初期設定変更
sub rewrite {
	my $self = shift;
	my %args = @_;

	my %data;
	if ($args{'data'}) {
		($data{'date'}, $data{'no'}, $data{'id'}, $data{'stat'}, $data{'field'}, $data{'name'}) = split(/\t/, $args{'data'});
		$data{'field'} =~ s/<>/::/;
	}

	foreach my $rewrite (%args) {
		if ($rewrite and $args{$rewrite}) {
			my $flag = 1;

			foreach my $data (split(/&/, $rewrite)) {
				my($key, $value) = split(/=/, $data);

				if ($key =~ /^\{([^\}]+)\}\{([^\}]+)\}$/ and $1 eq 'query') {
					my $query = $2;
					if ($value =~ /^\{([^\}]*)\}$/ and $self->{query}->{$query} !~ /^$1$/) {
						$flag = 0;

						last;
					}
				} elsif ($key =~ /^\{([^\}]+)\}\{([^\}]+)\}$/ and $1 eq 'data') {
					my $data = $2;
					if ($value =~ /^\{([^\}]*)\}$/ and $data{$data} !~ /^$1$/) {
						$flag = 0;

						last;
					}
				}
			}
			if ($flag) {
				foreach my $data (split(/&/, $args{$rewrite})) {
					my($key, $value) = split(/=/, $data);

					if ($key =~ /^\{([^\}]+)\}$/) {
						$key = $1;
					}
					if ($value =~ /^\{([^\}]+)\}$/) {
						$value = $1;
					}
					$self->{init}->{$key} = $value;
				}
			}
		}
	}

	return $self->{init};
}

### エラー出力
sub error {
	my $self    = shift;
	my $message = shift;

	if (open(FH, "$self->{init}->{skin_dir}$self->{init}->{skin_error}")) {
		print $self->header;
		while (<FH>) {
			s/\$\{INFO_ERROR\}/$message/g;
			print;
		}
		close(FH);
	} else {
		$self->SUPER::error($message);
	}

	exit;
}

1;
