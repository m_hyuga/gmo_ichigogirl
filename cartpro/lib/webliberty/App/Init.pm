#webliberty::App::Init.pm (2009/04/06)
#Copyright(C) 2002-2009 Knight, All rights reserved.

package webliberty::App::Init;

use strict;

### コンストラクタ
sub new {
	my $class = shift;

	my $self = {
		init   => undef,
		config => undef,
		label  => undef
	};
	bless $self, $class;

	$self->{init}   = $self->_set_init;
	$self->{config} = $self->_set_config;
	$self->{label}  = $self->_set_label;

	return $self;
}

### 初期設定取得
sub get_init {
	my $self = shift;
	my $name = shift;

	my $init;

	if ($name) {
		$init = $self->{init}->{$name};
	} else {
		$init = $self->{init};
	}

	return $init;
}

### 環境設定取得
sub get_config {
	my $self = shift;
	my $name = shift;

	my $config;

	if ($name) {
		$config = $self->{config}->{$name};
	} else {
		$config = $self->{config};
	}

	return $config;
}

### ラベル取得
sub get_label {
	my $self = shift;
	my $name = shift;

	my $label;

	if ($name) {
		$label = $self->{label}->{$name};
	} else {
		$label = $self->{label};
	}

	return $label;
}

### 初期設定
sub _set_init {
	my $self = shift;

	my $init = {
		#基本設定
		script        => 'Web Cart Professional',
		version       => '1.24',
		copyright     => 'Copyright(C) 2002-2008 Knight',
		script_file   => './cart.cgi',
		tb_file       => './cart-tb.cgi',
		html_file     => './index.html',
		parse_size    => 15000,
		jcode_mode    => 0,
		chmod_mode    => 1,
		suexec_mode   => 0,
		location_mode => 1,
		rewrite_mode  => 0,
		des_key       => '',

		#ログファイル
		data_dir           => './data/',
		data_config        => './data/init.cgi',
		data_user          => './data/user.log',
		data_profile       => './data/profile.log',
		data_record        => './data/record.log',
		data_field         => './data/field.log',
		data_top           => './data/top.log',
		data_menu          => './data/menu.log',
		data_link          => './data/link.log',
		data_rank          => './data/rank.log',
		data_customer      => './data/customer.log',
		data_catalog_dir   => './data/catalog/',
		data_catalog_index => './data/catalog/index.log',
		data_comt_dir      => './data/comment/',
		data_comt_index    => './data/comment/index.log',
		data_tb_dir        => './data/trackback/',
		data_tb_index      => './data/trackback/index.log',
		data_lock          => './data/cart.lock',
		data_stock_dir     => './data/stock/',
		data_relate_dir    => './data/relate/',
		data_image_dir     => './data/image/',
		data_upfile_dir    => './data/upfile/',
		data_thumbnail_dir => './data/thumbnail/',
		data_icon_dir      => './data/icon/',
		data_icon          => './data/icon.log',
		data_option_dir    => './data/option/',
		data_option        => './data/option.log',
		data_tmp_file      => 'Temporary.file',
		data_ext           => 'log',

		#スキンファイル
		skin_dir               => './skin/',
		skin_header            => 'header.html',
		skin_footer            => 'footer.html',
		skin_catalog           => 'catalog.html',
		skin_navigation        => 'navigation.html',
		skin_view              => 'view.html',
		skin_comment           => 'comment.html',
		skin_complete          => 'complete.html',
		skin_trackback         => 'trackback.html',
		skin_image             => 'image.html',
		skin_edit              => 'edit.html',
		skin_list              => 'list.html',
		skin_search            => 'search.html',
		skin_profile           => 'profile.html',
		skin_rate              => 'rate.html',
		skin_rank              => 'rank.html',
		skin_cart              => 'cart.html',
		skin_order_cart        => 'order_cart.html',
		skin_order_login       => 'order_login.html',
		skin_order_input       => 'order_input.html',
		skin_order_preview     => 'order_preview.html',
		skin_order_complete    => 'order_complete.html',
		skin_regist_input      => 'regist_input.html',
		skin_regist_preview    => 'regist_preview.html',
		skin_regist_complete   => 'regist_complete.html',
		skin_login             => 'login.html',
		skin_login_input       => 'login_input.html',
		skin_login_preview     => 'login_preview.html',
		skin_login_complete    => 'login_complete.html',
		skin_login_confirm     => 'login_confirm.html',
		skin_login_delete      => 'login_delete.html',
		skin_login_point       => 'login_point.html',
		skin_password          => 'password.html',
		skin_password_complete => 'password_complete.html',
		skin_admin             => 'admin.html',
		skin_admin_work        => 'admin_work.html',
		skin_admin_navi        => 'admin_navi.html',
		skin_admin_form        => 'admin_form.html',
		skin_admin_edit        => 'admin_edit.html',
		skin_admin_comment     => 'admin_comment.html',
		skin_admin_trackback   => 'admin_trackback.html',
		skin_admin_customer    => 'admin_customer.html',
		skin_admin_confirm     => 'admin_confirm.html',
		skin_admin_modify      => 'admin_modify.html',
		skin_admin_field       => 'admin_field.html',
		skin_admin_option      => 'admin_option.html',
		skin_admin_icon        => 'admin_icon.html',
		skin_admin_top         => 'admin_top.html',
		skin_admin_menu        => 'admin_menu.html',
		skin_admin_link        => 'admin_link.html',
		skin_admin_profile     => 'admin_profile.html',
		skin_admin_pwd         => 'admin_pwd.html',
		skin_admin_env         => 'admin_env.html',
		skin_admin_user        => 'admin_user.html',
		skin_admin_build       => 'admin_build.html',
		skin_admin_record      => 'admin_record.html',
		skin_admin_status      => 'admin_status.html',
		skin_js_title          => 'js_title.html',
		skin_js_text           => 'js_text.html',
		skin_error             => 'error.html',

		#アーカイブファイル
		archive_dir => './archives/',
		archive_ext => 'html',

		#JSファイル
		js_navi_start_file => './data/navi_start.js',
		js_navi_end_file   => './data/navi_end.js',
		js_title_file      => './data/title.js',
		js_text_file       => './data/text.js',

		#repng2jpeg用ファイル
		resize_pl => './resize.pl',

		#プラグイン用設定
		plugin_dir => './lib/webliberty/Plugin/',

		#特殊サーバー用設定
		data_image_path     => '',
		data_upfile_path    => '',
		data_thumbnail_path => '',
		data_icon_path      => '',
		archive_path        => '',
	};

	#曜日の表記
	@{$init->{weeks}} = ('日', '月', '火', '水', '木', '金', '土');

	#カレンダーの月表記
	@{$init->{months}} = ('1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12');

	#拡張設定
	%{$init->{rewrite}} = (
		'' => '',
		'' => '',
		'' => '',
		'' => '',
		'' => ''
	);

	return $init;
}

### 環境設定
sub _set_config {
	my $self = shift;

	my $config = {
		#基本設定
		site_title       => 'Shopping Cart',
		back_url         => 'http://your.site.addr/index.html',
		site_description => 'オンラインショッピングページ。',
		site_url         => '',
		ssl_url          => '',
		sendmail_path    => '/usr/sbin/sendmail',
		sendmail_list    => 'your@mail.addr',

		#商品注文の設定
		order_payment   => '銀行振込,0<>郵便振替,0<>代金引換,350',
		order_delivery  => '',
		order_date_mode => '0',
		order_time_list => '午前中<>12～14時<>14～16時<>16～18時<>18～20時<>20～21時',
		order_pref      => '北海道,1260<>青森県,1050<>岩手県,1050<>宮城県,1050<>秋田県,1050<>山形県,1050<>福島県,1050<>茨城県,525<>栃木県,525<>群馬県,525<>埼玉県,525<>千葉県,525<>東京都,525<>神奈川県,525<>山梨県,525<>長野県,840<>新潟県,840<>富山県,840<>石川県,840<>福井県,840<>岐阜県,840<>静岡県,840<>愛知県,840<>三重県,840<>滋賀県,840<>京都府,840<>大阪府,840<>兵庫県,840<>奈良県,840<>和歌山県,840<>鳥取県,1050<>島根県,1050<>岡山県,1050<>広島県,1050<>山口県,1050<>徳島県,1050<>香川県,1050<>愛媛県,1050<>高知県,1050<>福岡県,1050<>佐賀県,1050<>長崎県,1050<>熊本県,1050<>大分県,1050<>宮崎県,1050<>鹿児島県,1050<>沖縄県,1260',
		order_free      => '5000',

		#送信メールの設定
		sendmail_order_subj  => '商品ご注文内容確認',
		sendmail_order_body  => 'ご注文ありがとうございます。<>以下の内容で承りましたのでご確認ください。',
		sendmail_regist_subj => 'お客様情報登録確認',
		sendmail_regist_body => 'ご利用ありがとうございます。<>以下の内容で登録しましたのでご確認ください。',
		sendmail_edit_subj   => 'お客様情報変更確認',
		sendmail_edit_body   => 'ご利用ありがとうございます。<>以下の内容で変更しましたのでご確認ください。',
		sendmail_delete_subj => 'お客様情報削除確認',
		sendmail_delete_body => 'ご利用ありがとうございました。<>以下のお客様情報を削除しましたのでご確認ください。',
		sendmail_pwd_subj    => 'パスワード再発行',
		sendmail_pwd_body    => 'ご利用ありがとうございます。<>以下のパスワードを再発行しましたのでご確認ください。',
		sendmail_name        => '○○商店',
		sendmail_addr        => 'your@mail.addr',
		sendmail_signature   => '- - - - - - - - - - - - - - - - - - - -<><>○○商店<>Mail : your@mail.addr<>URL  : http://your.page.addr/<><>- - - - - - - - - - - - - - - - - - - -',

		#顧客管理の設定
		customer_mode => '0',
		point_mode    => '0',
		point_price   => '100',
		point_value   => '5',

		#インデックスページの設定
		top_size           => '10',
		top_delimiter_size => '10',
		top_field          => '1',
		top_field_list     => '',
		top_text           => '0',
		top_break          => '1',

		#ログの表示設定
		data_sort    => '0',
		subj_length  => '30',
		page_size    => '30',
		navi_size    => '10',
		list_size    => '0',
		cmtlist_size => '0',
		tblist_size  => '0',
		admin_size   => '10',

		#RSSの設定
		rss_mode       => '0',
		rss_length     => '300',
		rss_size       => '10',
		rss_field_list => '',

		#登録画面の表示設定
		use_field  => '1',
		use_icon   => '0',
		use_file   => '1',
		use_image  => '1',
		max_file   => '3',
		use_stock  => '0',
		use_relate => '0',
		use_tburl  => '0',

		#登録商品の初期設定
		default_stat  => '1',
		default_break => '1',
		default_view  => '0',
		default_comt  => '0',
		default_tb    => '0',
		comt_stat     => '1',
		tb_stat       => '1',

		#登録商品の表示設定
		title_mode         => '0',
		paragraph_mode     => '1',
		autolink_mode      => '1',
		autolink_attribute => 'class=&quot;top&quot;',
		continue_text      => '続きを読む',
		new_days           => '3',
		decoration_mode    => '0',
		img_maxwidth       => '400',
		thumbnail_mode     => '0',
		file_attribute     => 'class=&quot;top&quot;',
		quotation_color    => '#AAAAAA',

		#商品評価の設定
		rate_mode => '0',
		rate_info => '-2,☆<>-1,☆☆<>0,☆☆☆<>1,☆☆☆☆<>2,☆☆☆☆☆',
		rate_size => '10',

		#売り上げランキングの設定
		rank_mode => '0',
		rank_size => '10',

		#ナビゲーションの表示設定
		show_calendar    => '0',
		show_field       => '1',
		show_search      => '1',
		show_past        => '0',
		show_menu        => '0',
		menu_list        => '',
		show_link        => '0',
		link_list        => '',
		date_navigation  => '1',
		field_navigation => '1',
		show_navigation  => '0',
		pos_navigation   => '0',

		#プロフィールの表示設定
		profile_mode  => '0',
		profile_break => '1',

		#ユーザー管理の設定
		user_mode      => '0',
		auth_comment   => '0',
		auth_trackback => '0',
		auth_customer  => '0',
		auth_field     => '0',
		auth_icon      => '0',
		auth_option    => '0',
		auth_top       => '0',
		auth_menu      => '0',
		auth_link      => '0',
		record_size    => '50',

		#メール通知の設定
		sendmail_cmt_mode   => '0',
		sendmail_tb_mode    => '0',
		sendmail_stock_mode => '0',
		sendmail_admin      => '',
		sendmail_length     => '500',
		sendmail_detail     => '0',
		sendmail_stock_size => '3',

		#更新PINGの設定
		ping_mode => '0',
		ping_list => 'http://www.blogpeople.net/servlet/weblogUpdates<>http://ping.myblog.jp<>http://blog.goo.ne.jp/XMLRPC<>http://ping.bloggers.jp/rpc/',

		#Cookieの設定
		cookie_id       => 'webcart',
		cookie_cart     => 'webcart_cart',
		cookie_customer => 'webcart_customer',
		cookie_holddays => '90',
		cookie_admin    => 'webcart_admin',

		#HTMLファイル書き出しの設定
		html_index_mode   => '0',
		html_archive_mode => '0',
		html_field_mode   => '0',
		html_field_list   => './archives/illust/index.html,イラスト<>./archives/person/index.html,イラスト::人物の絵',

		#JSファイル書き出しの設定
		js_title_mode       => '0',
		js_title_field_mode => '0',
		js_title_field_list => './data/illust_title.js,イラスト<>./data/person_title.js,イラスト::人物の絵',
		js_title_size       => '5',
		js_text_mode        => '0',
		js_text_field_mode  => '0',
		js_text_field_list  => './data/illust_text.js,イラスト<>./data/person_text.js,イラスト::人物の絵',
		js_text_size        => '3',

		#投稿制限の設定
		base_url         => '',
		black_list       => 'anonymizer.com<>delegate',
		proxy_mode       => '1',
		ng_word          => '',
		need_word        => '',
		need_japanese    => '0',
		max_link         => '0',
		wait_time        => '60',
		black_list_tb    => 'http://spam.site.addr/',
		ng_word_tb       => '',
		need_word_tb     => '',
		need_japanese_tb => '0',
		need_link_tb     => '0'
	};

	return $config;
}

### ラベル設定
sub _set_label {
	my $self = shift;

	my $label = {
		#パソコンモード用ラベル
		pc_no    => '商品番号',
		pc_id    => '商品ID',
		pc_stat  => '状態',
		pc_break => '改行の変換',
		pc_view  => 'ファイルの表示方法',
		pc_comt  => 'コメントの受付',
		pc_tb    => 'トラックバックの受付',
		pc_field => '分類',
		pc_date  => '投稿日時',
		pc_name  => '名前',
		pc_subj  => '題名',
		pc_text  => 'メッセージ',
		pc_price => '価格',
		pc_icon  => 'アイコン',
		pc_image => 'ミニ画像',
		pc_file  => 'ファイル',
		pc_host  => 'ホスト',

		pc_pno   => '商品番号',
		pc_mail  => 'Ｅメール',
		pc_url   => 'ＵＲＬ',
		pc_rate  => '評価',
		pc_pwd   => '削除キー'
	};

	return $label;
}

1;
