#webliberty::App::Rank.pm (2007/12/25)
#Copyright(C) 2002-2007 Knight, All rights reserved.

package webliberty::App::Rank;

use strict;
use base qw(webliberty::Basis);
use webliberty::File;
use webliberty::Skin;
use webliberty::Plugin;
use webliberty::App::Catalog;

### コンストラクタ
sub new {
	my $class = shift;

	my $self = {
		init   => shift,
		config => shift,
		query  => shift,
		plugin => undef,
		update => undef
	};
	bless $self, $class;

	return $self;
}

### メイン処理
sub run {
	my $self = shift;

	if (!$self->{config}->{rank_mode}) {
		$self->error('不正なアクセスです。');
	}

	if ($self->{init}->{rewrite_mode}) {
		my $catalog_ins = new webliberty::App::Catalog($self->{init}, '', $self->{query});
		$self->{init} = $catalog_ins->rewrite(%{$self->{init}->{rewrite}});
	}

	$self->output;

	return;
}

### ファイル表示
sub output {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	#ページ表示準備
	my $skin_ins = new webliberty::Skin;
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_header}", available => 'header');
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_rank}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_footer}", available => 'footer');

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});

	$skin_ins->replace_skin(
		$catalog_ins->info,
		%{$self->{plugin}}
	);

	#表示用商品取得
	my(%rank, $i);
	open(FH, "$self->{init}->{data_rank}") or $self->error("Read Error : $self->{init}->{data_rank}");
	while (<FH>) {
		chomp;
		my($count, $id) = split(/\t/);

		$i++;
		if ($i > $self->{config}->{rank_size}) {
			last;
		}

		$rank{$id} = $count;
	}
	close(FH);

	#商品データ取得
	opendir(DIR, $self->{init}->{data_catalog_dir}) or $self->error("Read Error : $self->{init}->{data_catalog_dir}");
	my @dir = sort { $b <=> $a } readdir(DIR);
	closedir(DIR);

	my %detail;
	foreach my $entry (@dir) {
		if ($entry !~ /^\d\d\d\d\d\d\.$self->{init}->{data_ext}$/) {
			next;
		}

		open(FH, "$self->{init}->{data_catalog_dir}$entry") or $self->error("Read Error : $self->{init}->{data_catalog_dir}$entry");
		while (<FH>) {
			chomp;
			my($no, $id, $stat, $break, $view, $comt, $tb, $field, $date, $name, $subj, $text, $price, $icon, $image, $file, $host) = split(/\t/);

			if ($rank{$id}) {
				$detail{$id} = "$_\n";
			}
		}
		close(FH);
	}

	#データ出力
	print $self->header;
	print $skin_ins->get_data('header');
	print $skin_ins->get_data('rank_head');

	$i = 0;

	open(FH, "$self->{init}->{data_rank}") or $self->error("Read Error : $self->{init}->{data_rank}");
	while (<FH>) {
		chomp;
		my($count, $id) = split(/\t/);
		my($no, $id, $stat, $break, $view, $comt, $tb, $field, $date, $name, $subj, $text, $price, $icon, $image, $file, $host) = split(/\t/, $detail{$id});

		$i++;
		if ($i > $self->{config}->{rank_size}) {
			last;
		}

		print $skin_ins->get_replace_data(
			'rank',
			$catalog_ins->catalog_article($no, $id, $stat, $break, $view, $comt, $tb, $field, $date, $name, $subj, $text, $price, $icon, $image, $file, $host),
			RANK_RANK  => $i,
			RANK_COUNT => $count
		);
	}

	print $skin_ins->get_data('rank_foot');
	print $skin_ins->get_data('footer');

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

1;
