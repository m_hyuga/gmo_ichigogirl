#webliberty::App::Login.pm (2007/12/25)
#Copyright(C) 2002-2007 Knight, All rights reserved.

package webliberty::App::Login;

use strict;
use base qw(webliberty::Basis);
use webliberty::String;
use webliberty::Host;
use webliberty::Lock;
use webliberty::Skin;
use webliberty::Sendmail;
use webliberty::Plugin;
use webliberty::App::Catalog;
use webliberty::App::Customer;

### コンストラクタ
sub new {
	my $class = shift;

	my $self = {
		init   => shift,
		config => shift,
		query  => shift,
		plugin => undef,
		update => undef
	};
	bless $self, $class;

	return $self;
}

### メイン処理
sub run {
	my $self = shift;

	if (!$self->{config}->{customer_mode}) {
		$self->error('不正なアクセスです。');
	}

	if ($self->{init}->{rewrite_mode}) {
		my $catalog_ins = new webliberty::App::Catalog($self->{init}, '', $self->{query});
		$self->{init} = $catalog_ins->rewrite(%{$self->{init}->{rewrite}});
	}

	my $customer_ins = new webliberty::App::Customer($self->{init}, $self->{config}, $self->{query});

	if ($self->{query}->{work} eq 'edit') {
		$self->input;
	} elsif ($self->{query}->{work} eq 'preview') {
		if ($self->{query}->{del}) {
			$self->confirm;
		} else {
			my @error_message = $customer_ins->check;

			if ($error_message[0]) {
				$self->input(@error_message);
			} else {
				$self->preview;
			}
		}
	} elsif ($self->{query}->{work} eq 'complete') {
		my @error_message = $customer_ins->check;

		if ($error_message[0] or $self->{query}->{exec_back}) {
			$self->input;
		} else {
			$self->complete;
		}
	} elsif ($self->{query}->{work} eq 'del') {
		$self->del;
	} elsif ($self->{query}->{work} eq 'point') {
		$self->point;
	} else {
		$self->output;
	}

	return;
}

### 入力フォーム表示
sub input {
	my $self  = shift;
	my @error = @_;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	my $login_mail_ins = new webliberty::String($self->{query}->{login_mail});
	my $login_pwd_ins  = new webliberty::String($self->{query}->{login_pwd});

	if (!$login_mail_ins->get_string or !$login_pwd_ins->get_string) {
		$self->error('Ｅメールとパスワードを入力してください。');
	}

	#ページ表示準備
	my $skin_ins = new webliberty::Skin;
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_header}", available => 'header');
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_login_input}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_footer}", available => 'footer');

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});

	$skin_ins->replace_skin(
		$catalog_ins->info,
		%{$self->{plugin}}
	);

	my($order_name, $order_kana, $order_mail, $order_post, $order_pref, $order_addr, $order_phone, $order_fax, $send_name, $send_kana, $send_post, $send_pref, $send_addr, $send_phone, $send_fax, $user_pwd, $cfm_pwd);

	if ($self->{query}->{work} ne 'edit') {
		foreach (split(/<>/, $self->{config}->{order_pref})) {
			my($name, $fee) = split(/,/);

			if ($self->{query}->{order_pref} eq $name) {
				$order_pref .= "<option value=\"$name\" selected=\"selected\">$name</option>";
			} else {
				$order_pref .= "<option value=\"$name\">$name</option>";
			}
			if ($self->{query}->{send_pref} eq $name) {
				$send_pref .= "<option value=\"$name\" selected=\"selected\">$name</option>";
			} else {
				$send_pref .= "<option value=\"$name\">$name</option>";
			}
		}

		$order_name  = $self->{query}->{order_name};
		$order_kana  = $self->{query}->{order_kana};
		$order_mail  = $self->{query}->{order_mail};
		$order_post  = $self->{query}->{order_post};
		$order_addr  = $self->{query}->{order_addr};
		$order_phone = $self->{query}->{order_phone};
		$order_fax   = $self->{query}->{order_fax};
		$send_name   = $self->{query}->{send_name};
		$send_kana   = $self->{query}->{send_kana};
		$send_post   = $self->{query}->{send_post};
		$send_addr   = $self->{query}->{send_addr};
		$send_phone  = $self->{query}->{send_phone};
		$send_fax    = $self->{query}->{send_fax};
		$user_pwd    = $self->{query}->{user_pwd};
		$cfm_pwd     = $self->{query}->{cfm_pwd} || $self->{query}->{user_pwd};
	} else {
		my $flag;

		open(FH, "$self->{init}->{data_customer}") or $self->error("Read Error : $self->{init}->{data_customer}");
		while (<FH>) {
			chomp;
			my($data_date, $data_stat, $data_point, $data_payment, $data_delivery, $data_user_pwd, $data_order_name, $data_order_kana, $data_order_mail, $data_order_post, $data_order_pref, $data_order_addr, $data_order_phone, $data_order_fax, $data_send_name, $data_send_kana, $data_send_post, $data_send_pref, $data_send_addr, $data_send_phone, $data_send_fax) = split(/\t/);

			if ($login_mail_ins->get_string eq $data_order_mail) {
				if (!$login_pwd_ins->check_password($data_user_pwd)) {
					$self->error('Ｅメールまたはパスワードが違います。');
				}

				foreach (split(/<>/, $self->{config}->{order_pref})) {
					my($name, $fee) = split(/,/);

					if ($data_order_pref eq $name) {
						$order_pref .= "<option value=\"$name\" selected=\"selected\">$name</option>";
					} else {
						$order_pref .= "<option value=\"$name\">$name</option>";
					}
					if ($data_send_pref eq $name) {
						$send_pref .= "<option value=\"$name\" selected=\"selected\">$name</option>";
					} else {
						$send_pref .= "<option value=\"$name\">$name</option>";
					}
				}

				$order_name  = $data_order_name;
				$order_kana  = $data_order_kana;
				$order_mail  = $data_order_mail;
				$order_post  = $data_order_post;
				$order_addr  = $data_order_addr;
				$order_phone = $data_order_phone;
				$order_fax   = $data_order_fax;
				$send_name   = $data_send_name;
				$send_kana   = $data_send_kana;
				$send_post   = $data_send_post;
				$send_addr   = $data_send_addr;
				$send_phone  = $data_send_phone;
				$send_fax    = $data_send_fax;
				$user_pwd    = $login_pwd_ins->get_string;
				$cfm_pwd     = $login_pwd_ins->get_string;

				$flag = 1;

				last;
			}
		}
		close(FH);

		if (!$flag) {
			$self->error('Ｅメールまたはパスワードが違います。');
		}
	}

	#データ出力
	print $self->header;
	print $skin_ins->get_data('header');
	print $skin_ins->get_data('contents');

	if ($error[0]) {
		print $skin_ins->get_data('error_head');

		foreach (@error) {
			print $skin_ins->get_replace_data(
				'error',
				ERROR_MESSAGE => $_
			);
		}

		print $skin_ins->get_data('error_foot');
	}

	print $skin_ins->get_replace_data(
		'form',
		FORM_LOGIN_MAIL => $login_mail_ins->get_string,
		FORM_LOGIN_PWD  => $login_pwd_ins->get_string,

		FORM_ORDER_NAME  => $order_name,
		FORM_ORDER_KANA  => $order_kana,
		FORM_ORDER_MAIL  => $order_mail,
		FORM_ORDER_POST  => $order_post,
		FORM_ORDER_PREF  => $order_pref,
		FORM_ORDER_ADDR  => $order_addr,
		FORM_ORDER_PHONE => $order_phone,
		FORM_ORDER_FAX   => $order_fax,

		FORM_SEND_NAME  => $send_name,
		FORM_SEND_KANA  => $send_kana,
		FORM_SEND_POST  => $send_post,
		FORM_SEND_PREF  => $send_pref,
		FORM_SEND_ADDR  => $send_addr,
		FORM_SEND_PHONE => $send_phone,
		FORM_SEND_FAX   => $send_fax,

		FORM_USER_PWD => $user_pwd,
		FORM_CFM_PWD  => $cfm_pwd
	);
	print $skin_ins->get_data('footer');

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

### 確認画面表示
sub preview {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	my $login_mail_ins = new webliberty::String($self->{query}->{login_mail});
	my $login_pwd_ins  = new webliberty::String($self->{query}->{login_pwd});

	#ページ表示準備
	my $skin_ins = new webliberty::Skin;
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_header}", available => 'header');
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_login_preview}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_footer}", available => 'footer');

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});

	$skin_ins->replace_skin(
		$catalog_ins->info,
		%{$self->{plugin}}
	);

	my($send_start, $send_end);
	if (!$self->{query}->{send_name}) {
		$send_start = '<!--';
		$send_end   = '-->';
	}

	#データ出力
	print $self->header;
	print $skin_ins->get_data('header');
	print $skin_ins->get_data('contents');
	print $skin_ins->get_replace_data(
		'form',
		VIEW_ORDER_NAME  => $self->{query}->{order_name},
		VIEW_ORDER_KANA  => $self->{query}->{order_kana},
		VIEW_ORDER_MAIL  => $self->{query}->{order_mail},
		VIEW_ORDER_POST  => $self->{query}->{order_post},
		VIEW_ORDER_PREF  => $self->{query}->{order_pref},
		VIEW_ORDER_ADDR  => $self->{query}->{order_addr},
		VIEW_ORDER_PHONE => $self->{query}->{order_phone},
		VIEW_ORDER_FAX   => $self->{query}->{order_fax} || '-',
		VIEW_SEND_NAME   => $self->{query}->{send_name},
		VIEW_SEND_KANA   => $self->{query}->{send_kana},
		VIEW_SEND_POST   => $self->{query}->{send_post},
		VIEW_SEND_PREF   => $self->{query}->{send_pref},
		VIEW_SEND_ADDR   => $self->{query}->{send_addr},
		VIEW_SEND_PHONE  => $self->{query}->{send_phone},
		VIEW_SEND_FAX    => $self->{query}->{send_fax} || '-',
		VIEW_SEND_START  => $send_start,
		VIEW_SEND_END    => $send_end,
		VIEW_USER_PWD    => $self->{query}->{user_pwd},

		FORM_LOGIN_MAIL => $login_mail_ins->get_string,
		FORM_LOGIN_PWD  => $login_pwd_ins->get_string,

		FORM_ORDER_NAME  => $self->{query}->{order_name},
		FORM_ORDER_KANA  => $self->{query}->{order_kana},
		FORM_ORDER_MAIL  => $self->{query}->{order_mail},
		FORM_ORDER_POST  => $self->{query}->{order_post},
		FORM_ORDER_PREF  => $self->{query}->{order_pref},
		FORM_ORDER_ADDR  => $self->{query}->{order_addr},
		FORM_ORDER_PHONE => $self->{query}->{order_phone},
		FORM_ORDER_FAX   => $self->{query}->{order_fax},
		FORM_SEND_NAME   => $self->{query}->{send_name},
		FORM_SEND_KANA   => $self->{query}->{send_kana},
		FORM_SEND_POST   => $self->{query}->{send_post},
		FORM_SEND_PREF   => $self->{query}->{send_pref},
		FORM_SEND_ADDR   => $self->{query}->{send_addr},
		FORM_SEND_PHONE  => $self->{query}->{send_phone},
		FORM_SEND_FAX    => $self->{query}->{send_fax},
		FORM_USER_PWD    => $self->{query}->{user_pwd}
	);
	print $skin_ins->get_data('footer');

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

### 登録完了
sub complete {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	my $lock_ins = new webliberty::Lock($self->{init}->{data_lock});
	if (!$lock_ins->file_lock) {
		$self->error('ファイルがロックされています。時間をおいてもう一度投稿してください。');
	}

	#顧客情報編集
	my $customer_ins = new webliberty::App::Customer($self->{init}, $self->{config}, $self->{query});
	$customer_ins->edit;

	$lock_ins->file_unlock;

	#登録完了メール作成
	my $sendmail_edit_body = $self->{config}->{sendmail_edit_body};
	$sendmail_edit_body =~ s/<>/\n/g;

	my $host_ins = new webliberty::Host;

	my $mail_body = "$sendmail_edit_body\n";
	$mail_body .= "\n";
	$mail_body .= "■お客様情報\n";
	$mail_body .= "\n";
	$mail_body .= "名前　　：$self->{query}->{order_name}\n";
	$mail_body .= "ふりがな：$self->{query}->{order_kana}\n";
	$mail_body .= "Ｅメール：$self->{query}->{order_mail}\n";
	$mail_body .= "郵便番号：$self->{query}->{order_post}\n";
	$mail_body .= "都道府県：$self->{query}->{order_pref}\n";
	$mail_body .= "住所　　：$self->{query}->{order_addr}\n";
	$mail_body .= "電話番号：$self->{query}->{order_phone}\n";
	$mail_body .= "FAX番号 ：$self->{query}->{order_fax}\n";
	$mail_body .= "\n";
	if ($self->{query}->{send_name}) {
		$mail_body .= "■配送先情報\n";
		$mail_body .= "\n";
		$mail_body .= "名前　　：$self->{query}->{send_name}\n";
		$mail_body .= "ふりがな：$self->{query}->{send_kana}\n";
		$mail_body .= "郵便番号：$self->{query}->{send_post}\n";
		$mail_body .= "都道府県：$self->{query}->{send_pref}\n";
		$mail_body .= "住所　　：$self->{query}->{send_addr}\n";
		$mail_body .= "電話番号：$self->{query}->{send_phone}\n";
		$mail_body .= "FAX番号 ：$self->{query}->{send_fax}\n";
		$mail_body .= "\n";
	}
	$mail_body .= "■その他\n";
	$mail_body .= "\n";
	$mail_body .= "パスワード：$self->{query}->{user_pwd}\n";
	$mail_body .= "送信ホスト：" . $host_ins->get_host . "\n";
	$mail_body .= "\n";

	my $sendmail_signature = $self->{config}->{sendmail_signature};
	$sendmail_signature =~ s/<>/\n/g;

	$mail_body .= "$sendmail_signature";

	#メール送信
	my $sendmail_ins = new webliberty::Sendmail($self->{config}->{sendmail_path});
	foreach (split(/<>/, "$self->{config}->{sendmail_list}<>$self->{query}->{order_mail}")) {
		if (!$_) {
			next;
		}

		my($flag, $message) = $sendmail_ins->sendmail(
			send_to   => $_,
			send_from => $self->{config}->{sendmail_addr},
			subject   => $self->{config}->{sendmail_edit_subj},
			name      => $self->{config}->{sendmail_name},
			message   => $mail_body
		);
		if (!$flag) {
			$self->error($message);
		}
	}

	#ページ表示準備
	my $skin_ins = new webliberty::Skin;
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_header}", available => 'header');
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_login_complete}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_footer}", available => 'footer');

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});

	$skin_ins->replace_skin(
		$catalog_ins->info,
		%{$self->{plugin}}
	);

	#データ出力
	print $self->header;
	print $skin_ins->get_data('header');
	print $skin_ins->get_data('contents');
	print $skin_ins->get_data('footer');

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

### 削除確認
sub confirm {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	my $login_mail_ins = new webliberty::String($self->{query}->{login_mail});
	my $login_pwd_ins  = new webliberty::String($self->{query}->{login_pwd});

	#ページ表示準備
	my $skin_ins = new webliberty::Skin;
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_header}", available => 'header');
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_login_confirm}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_footer}", available => 'footer');

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});

	$skin_ins->replace_skin(
		$catalog_ins->info,
		%{$self->{plugin}}
	);

	#データ出力
	print $self->header;
	print $skin_ins->get_data('header');
	print $skin_ins->get_replace_data(
		'contents',
		FORM_LOGIN_MAIL => $login_mail_ins->get_string,
		FORM_LOGIN_PWD  => $login_pwd_ins->get_string
	);
	print $skin_ins->get_data('footer');

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

### 削除完了
sub del {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	my $lock_ins = new webliberty::Lock($self->{init}->{data_lock});
	if (!$lock_ins->file_lock) {
		$self->error('ファイルがロックされています。時間をおいてもう一度投稿してください。');
	}

	my($customer_name, $customer_mail, $customer_info, $flag);

	open(FH, "$self->{init}->{data_customer}") or $self->error("Read Error : $self->{init}->{data_customer}");
	while (<FH>) {
		chomp;
		my($date, $stat, $point, $payment, $delivery, $user_pwd, $order_name, $order_kana, $order_mail, $order_post, $order_pref, $order_addr, $order_phone, $order_fax, $send_name, $send_kana, $send_post, $send_pref, $send_addr, $send_phone, $send_fax, $host) = split(/\t/);

		if ($self->{query}->{login_mail} eq $order_mail) {
			$customer_name = $order_name;
			$customer_mail = $order_mail;

			$customer_info .= "■お客様情報\n";
			$customer_info .= "\n";
			$customer_info .= "名前　　：$order_name\n";
			$customer_info .= "ふりがな：$order_kana\n";
			$customer_info .= "Ｅメール：$order_mail\n";
			$customer_info .= "郵便番号：$order_post\n";
			$customer_info .= "都道府県：$order_pref\n";
			$customer_info .= "住所　　：$order_addr\n";
			$customer_info .= "電話番号：$order_phone\n";
			$customer_info .= "FAX番号 ：$order_fax\n";
			$customer_info .= "\n";
			if ($send_name) {
				$customer_info .= "■配送先情報\n";
				$customer_info .= "\n";
				$customer_info .= "名前　　：$send_name\n";
				$customer_info .= "ふりがな：$send_kana\n";
				$customer_info .= "郵便番号：$send_post\n";
				$customer_info .= "都道府県：$send_pref\n";
				$customer_info .= "住所　　：$send_addr\n";
				$customer_info .= "電話番号：$send_phone\n";
				$customer_info .= "FAX番号 ：$send_fax\n";
				$customer_info .= "\n";
			}

			$flag = 1;

			last;
		}
	}
	close(FH);

	if (!$flag) {
		$self->error('指定されたお客様情報は存在しません。');
	}

	#顧客情報削除
	my $customer_ins = new webliberty::App::Customer($self->{init}, $self->{config}, $self->{query});
	$customer_ins->del;

	$lock_ins->file_unlock;

	#削除完了メール作成
	my $sendmail_delete_body = $self->{config}->{sendmail_delete_body};
	$sendmail_delete_body =~ s/<>/\n/g;

	my $host_ins = new webliberty::Host;

	my $mail_body = "$sendmail_delete_body\n";
	$mail_body .= "\n";
	$mail_body .= "$customer_info";
	$mail_body .= "■その他\n";
	$mail_body .= "\n";
	$mail_body .= "送信ホスト：" . $host_ins->get_host . "\n";
	$mail_body .= "\n";

	my $sendmail_signature = $self->{config}->{sendmail_signature};
	$sendmail_signature =~ s/<>/\n/g;

	$mail_body .= "$sendmail_signature";

	#メール送信
	my $sendmail_ins = new webliberty::Sendmail($self->{config}->{sendmail_path});
	foreach (split(/<>/, "$self->{config}->{sendmail_list}<>$customer_mail")) {
		if (!$_) {
			next;
		}

		my($flag, $message) = $sendmail_ins->sendmail(
			send_to   => $_,
			send_from => $self->{config}->{sendmail_addr},
			subject   => $self->{config}->{sendmail_delete_subj},
			name      => $self->{config}->{sendmail_name},
			message   => $mail_body
		);
		if (!$flag) {
			$self->error($message);
		}
	}

	#ページ表示準備
	my $skin_ins = new webliberty::Skin;
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_header}", available => 'header');
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_login_delete}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_footer}", available => 'footer');

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});

	$skin_ins->replace_skin(
		$catalog_ins->info,
		%{$self->{plugin}}
	);

	#データ出力
	print $self->header;
	print $skin_ins->get_data('header');
	print $skin_ins->get_data('contents');
	print $skin_ins->get_data('footer');

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

### ポイント確認画面表示
sub point {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	my($customer_name, $customer_mail, $customer_point, $flag);

	open(FH, "$self->{init}->{data_customer}") or $self->error("Read Error : $self->{init}->{data_customer}");
	while (<FH>) {
		chomp;
		my($date, $stat, $point, $payment, $delivery, $user_pwd, $order_name, $order_kana, $order_mail, $order_post, $order_pref, $order_addr, $order_phone, $order_fax, $send_name, $send_kana, $send_post, $send_pref, $send_addr, $send_phone, $send_fax, $host) = split(/\t/);

		if ($self->{query}->{login_mail} eq $order_mail) {
			$customer_name  = $order_name;
			$customer_mail  = $order_mail;
			$customer_point = $point;

			$flag = 1;

			last;
		}
	}
	close(FH);

	if (!$flag) {
		$self->error('指定されたお客様情報は存在しません。');
	}

	my $skin_ins = new webliberty::Skin;
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_header}", available => 'header');
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_login_point}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_footer}", available => 'footer');

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});

	$skin_ins->replace_skin(
		$catalog_ins->info,
		%{$self->{plugin}}
	);

	print $self->header;
	print $skin_ins->get_data('header');
	print $skin_ins->get_replace_data(
		'contents',
		VIEW_NAME  => $customer_name,
		VIEW_MAIL  => $customer_mail,
		VIEW_POINT => $customer_point,
		VIEW_PRICE => $self->{config}->{point_price},
		VIEW_VALUE => $self->{config}->{point_value}
	);
	print $skin_ins->get_data('footer');

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

### ログイン画面表示
sub output {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	my $skin_ins = new webliberty::Skin;
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_header}", available => 'header');
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_login}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_footer}", available => 'footer');

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});

	$skin_ins->replace_skin(
		$catalog_ins->info,
		%{$self->{plugin}}
	);

	print $self->header;
	print $skin_ins->get_data('header');
	print $skin_ins->get_data('contents_head');

	my $work_data;
	$work_data = "edit\t登録情報編集・削除\n";
	if ($self->{config}->{point_mode}) {
		$work_data .= "point\tポイント確認\n";
	}

	foreach (split(/\n/, $work_data)) {
		my($work_id, $work_name) = split(/\t/);

		print $skin_ins->get_replace_data(
			'work',
			WORK_ID   => $work_id,
			WORK_NAME => $work_name
		);
	}

	print $skin_ins->get_data('contents_foot');
	print $skin_ins->get_data('footer');

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

1;
