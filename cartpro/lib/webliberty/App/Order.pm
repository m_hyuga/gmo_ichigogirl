#webliberty::App::Order.pm (2007/12/25)
#Copyright(C) 2002-2007 Knight, All rights reserved.

package webliberty::App::Order;

use strict;
use base qw(webliberty::Basis);
use webliberty::String;
use webliberty::Host;
use webliberty::Cookie;
use webliberty::Lock;
use webliberty::Skin;
use webliberty::Sendmail;
use webliberty::Plugin;
use webliberty::App::Catalog;
use webliberty::App::Customer;

### コンストラクタ
sub new {
	my $class = shift;

	my $self = {
		init   => shift,
		config => shift,
		query  => shift,
		plugin => undef,
		html   => undef,
		update => undef
	};
	bless $self, $class;

	return $self;
}

### メイン処理
sub run {
	my $self = shift;

	if ($self->{init}->{rewrite_mode}) {
		my $catalog_ins = new webliberty::App::Catalog($self->{init}, '', $self->{query});
		$self->{init} = $catalog_ins->rewrite(%{$self->{init}->{rewrite}});
	}

	my $customer_ins = new webliberty::App::Customer($self->{init}, $self->{config}, $self->{query});

	if ($self->{query}->{work} eq 'input') {
		$self->cart;
		$self->input;
	} elsif ($self->{query}->{work} eq 'preview') {
		my @error_message = $customer_ins->check;

		my $point = $self->cart(@error_message);
		if ($error_message[0]) {
			$self->input(@error_message);
		} else {
			$self->preview($point);
		}
	} elsif ($self->{query}->{work} eq 'order') {
		my @error_message = $customer_ins->check;

		if ($error_message[0] or $self->{query}->{exec_back}) {
			$self->cart;
			$self->input;
		} else {
			$self->order;
		}
	} elsif ($self->{query}->{work} eq 'complete') {
		$self->complete;
	} else {
		if ($self->{config}->{customer_mode}) {
			$self->cart;
			$self->login;
		} else {
			$self->cart;
			$self->input;
		}
	}

	return;
}

### カート内容表示
sub cart {
	my $self  = shift;
	my @error = @_;

	my $goods_ins = new webliberty::String($self->{query}->{goods});
	$goods_ins->create_plain;

	my(%stock, %detail);
	foreach (split(/\n/, $goods_ins->get_string)) {
		my($no, $code, $option, $amount) = split(/\t/);

		if ($self->{config}->{use_stock}) {
			if (-s "$self->{init}->{data_stock_dir}$no\.$self->{init}->{data_ext}") {
				open(FH, "$self->{init}->{data_stock_dir}$no\.$self->{init}->{data_ext}") or $self->error("Read Error : $self->{init}->{data_stock_dir}$no\.$self->{init}->{data_ext}");
				my $stock = <FH>;
				close(FH);

				if ($amount + $stock{$no} > $stock) {
					$self->error("<em>$code</em>の在庫数が<em>$stock</em>になりました。この商品を注文する事ができません。");
				}
			} else {
				$self->error("この商品の在庫数は<em>0</em>です。これ以上カートに入れることはできません。");
			}

			$stock{$no} += $amount;
		}

		$detail{$no} = $code;
	}

	opendir(DIR, $self->{init}->{data_catalog_dir}) or $self->error("Read Error : $self->{init}->{data_catalog_dir}");
	my @dir = sort { $b <=> $a } readdir(DIR);
	closedir(DIR);

	foreach my $entry (@dir) {
		if ($entry !~ /^\d\d\d\d\d\d\.$self->{init}->{data_ext}$/) {
			next;
		}

		open(FH, "$self->{init}->{data_catalog_dir}$entry") or $self->error("Read Error : $self->{init}->{data_catalog_dir}$entry");
		while (<FH>) {
			chomp;
			my($no, $id, $stat, $break, $view, $comt, $tb, $field, $date, $name, $subj, $text, $price, $icon, $image, $file, $host) = split(/\t/);

			if ($detail{$no}) {
				$detail{$no} = "$no\t$id\t$stat\t$break\t$view\t$comt\t$tb\t$field\t$date\t$name\t$subj\t$text\t$price\t$icon\t$image\t$file\t$host";
			}
		}
		close(FH);
	}

	#ページ表示準備
	my $skin_ins = new webliberty::Skin;
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_header}", available => 'header');
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_order_cart}");

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});

	$skin_ins->replace_skin(
		$catalog_ins->info,
		%{$self->{plugin}}
	);

	#出力用データ作成
	$self->{html} .= $skin_ins->get_data('header');
	$self->{html} .= $skin_ins->get_data('cart_head');

	my $subtotal = 0;

	foreach (split(/\n/, $goods_ins->get_string)) {
		my($no, $code, $option, $amount) = split(/\t/);
		my($no, $id, $stat, $break, $view, $comt, $tb, $field, $date, $name, $subj, $text, $price, $icon, $image, $file, $host) = split(/\t/, $detail{$no});

		my($option_start, $option_end);
		if (!$option) {
			$option_start = '<!--';
			$option_end   = '-->';
		}

		$subtotal += $price * $amount;

		$self->{html} .= $skin_ins->get_replace_data(
			'cart',
			$catalog_ins->catalog_article($no, $id, $stat, $break, $view, $comt, $tb, $field, $date, $name, $subj, $text, $price, $icon, $image, $file, $host),
			CART_NO           => $_,
			CART_OPTION       => $option,
			CART_OPTION_START => $option_start,
			CART_OPTION_END   => $option_end,
			CART_AMOUNT       => $amount,
			CART_FEE          => $price * $amount
		);
	}

	my $point;
	if ($self->{query}->{point}) {
		my $use_point;
		my $flag;

		open(FH, "$self->{init}->{data_customer}") or $self->error("Read Error : $self->{init}->{data_customer}");
		while (<FH>) {
			chomp;
			my($date, $stat, $point, $payment, $delivery, $user_pwd, $order_name, $order_kana, $order_mail, $order_post, $order_pref, $order_addr, $order_phone, $order_fax, $send_name, $send_kana, $send_post, $send_pref, $send_addr, $send_phone, $send_fax, $host) = split(/\t/);

			if ($self->{query}->{order_mail} eq $order_mail) {
				if ($point <= $subtotal) {
					$use_point = $point;
				} else {
					$use_point = $subtotal;
				}
				$flag = 1;

				last;
			}
		}
		close(FH);

		if (!$flag) {
			$self->error('ポイント情報を取得できません。');
		}

		$point = $use_point;
	}

	my $carriage;
	if ($self->{query}->{send_pref}) {
		$carriage = $self->{query}->{send_pref};
	} else {
		$carriage = $self->{query}->{order_pref};
	}

	my $payment;
	foreach (split(/<>/, $self->{config}->{order_payment})) {
		my($name, $fee) = split(/,/);

		if ($self->{query}->{payment} eq $name) {
			$payment = $fee;

			last;
		}
	}
	if ($self->{query}->{payment} and $payment eq '') {
		$self->error("お支払方法を取得できません。");
	}

	my $i = 0;
	if ($self->{config}->{order_delivery}) {
		foreach (split(/<>/, $self->{config}->{order_delivery})) {
			if ($self->{query}->{delivery} eq $_) {
				last;
			}
			$i++;
		}
	}

	foreach (split(/<>/, $self->{config}->{order_pref})) {
		my($name, $fee) = split(/,/);

		my @fee = split(/\//, $fee);

		if ($carriage eq $name) {
			$carriage = $fee[$i];

			last;
		}
	}
	if ($self->{query}->{delivery} and $carriage eq '') {
		$self->error("送料を取得できません。");
	}

	my($point_start, $point_end, $payment_start, $payment_end, $carriage_start, $carriage_end, $total_start, $total_end);
	if ($error[0] or $self->{query}->{work} ne 'preview') {
		$point_start    = '<!--';
		$point_end      = '-->';
		$payment_start  = '<!--';
		$payment_end    = '-->';
		$carriage_start = '<!--';
		$carriage_end   = '-->';
		$total_start    = '<!--';
		$total_end      = '-->';
	}
	if (!$self->{query}->{point}) {
		$point_start = '<!--';
		$point_end   = '-->';
	}
	if ($payment <= 0) {
		$payment_start = '<!--';
		$payment_end   = '-->';
	}

	my($cart_free_start, $cart_free_end);
	if ($self->{config}->{order_free} and $self->{config}->{order_free} <= $subtotal) {
		$carriage = 0;
	} else {
		$cart_free_start = '<!--';
		$cart_free_end   = '-->';
	}

	my $total = $subtotal + $payment + $carriage - $point;

	$self->{html} .= $skin_ins->get_replace_data(
		'cart_foot',
		CART_SUBTOTAL       => $subtotal,
		CART_POINT          => $point,
		CART_POINT_START    => $point_start,
		CART_POINT_END      => $point_end,
		CART_PAYMENT        => $payment,
		CART_PAYMENT_START  => $payment_start,
		CART_PAYMENT_END    => $payment_end,
		CART_CARRIAGE       => $carriage,
		CART_CARRIAGE_START => $carriage_start,
		CART_CARRIAGE_END   => $carriage_end,
		CART_TOTAL          => $total,
		CART_TOTAL_START    => $total_start,
		CART_TOTAL_END      => $total_end,
		CART_FREE           => $self->{config}->{order_free},
		CART_FREE_START     => $cart_free_start,
		CART_FREE_END       => $cart_free_end
	);

	return $point;
}

### ログインフォーム表示
sub login {
	my $self  = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	my $goods_ins = new webliberty::String($self->{query}->{goods});
	$goods_ins->create_plain;

	#ページ表示準備
	my $skin_ins = new webliberty::Skin;
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_order_login}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_footer}", available => 'footer');

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});

	$skin_ins->replace_skin(
		$catalog_ins->info,
		%{$self->{plugin}}
	);

	#出力用データ作成
	$self->{html} .= $skin_ins->get_replace_data(
		'contents',
		FORM_GOODS => $goods_ins->get_string
	);
	$self->{html} .= $skin_ins->get_data('footer');

	print $self->header;
	print $self->{html};

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

### 入力フォーム表示
sub input {
	my $self  = shift;
	my @error = @_;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	my $login_mail_ins = new webliberty::String($self->{query}->{login_mail});
	my $login_pwd_ins  = new webliberty::String($self->{query}->{login_pwd});
	my $goods_ins      = new webliberty::String($self->{query}->{goods});

	$goods_ins->create_plain;

	#ページ表示準備
	my $skin_ins = new webliberty::Skin;
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_order_input}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_footer}", available => 'footer');

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});

	$skin_ins->replace_skin(
		$catalog_ins->info,
		%{$self->{plugin}}
	);

	my($payment, $delivery, $delivery_start, $delivery_end, $date, $date_start, $date_end, $order_name, $order_kana, $order_mail, $order_post, $order_pref, $order_addr, $order_phone, $order_fax, $send_name, $send_kana, $send_post, $send_pref, $send_addr, $send_phone, $send_fax, $user_pwd, $cfm_pwd, $pwd_start, $pwd_end, $comment, $point, $point_start, $point_end, $save, $save_start, $save_end);

	if ($self->{query}->{work} and $self->{query}->{work} ne 'input') {
		foreach (split(/<>/, $self->{config}->{order_payment})) {
			my($name, $fee) = split(/,/);

			if ($self->{query}->{payment} eq $name) {
				$payment .= "<option value=\"$name\" selected=\"selected\">$name</option>";
			} else {
				$payment .= "<option value=\"$name\">$name</option>";
			}
		}

		if ($self->{config}->{order_delivery}) {
			foreach (split(/<>/, $self->{config}->{order_delivery})) {
				if ($self->{query}->{delivery} eq $_) {
					$delivery .= "<option value=\"$_\" selected=\"selected\">$_</option>";
				} else {
					$delivery .= "<option value=\"$_\">$_</option>";
				}
			}
		} else {
			$delivery_start = '<!--';
			$delivery_end   = '-->';
		}

		if ($self->{config}->{order_date_mode}) {
			$date  = "<input type=\"text\" name=\"date_month\" size=\"5\" value=\"$self->{query}->{date_month}\" />月 ";
			$date .= "<input type=\"text\" name=\"date_day\" size=\"5\" value=\"$self->{query}->{date_day}\" />日 ";
			$date .= "<select name=\"date_time\" xml:lang=\"ja\" lang=\"ja\">";
			$date .= "<option value=\"\">選択してください</option>";
			foreach (split(/<>/, $self->{config}->{order_time_list})) {
				if ($self->{query}->{date_time} eq $_) {
					$date .= "<option value=\"$_\" selected=\"selected\">$_</option>";
				} else {
					$date .= "<option value=\"$_\">$_</option>";
				}
			}
			$date .= "</select>";
		} else {
			$date_start = '<!--';
			$date_end   = '-->';
		}

		foreach (split(/<>/, $self->{config}->{order_pref})) {
			my($name, $fee) = split(/,/);

			if ($self->{query}->{order_pref} eq $name) {
				$order_pref .= "<option value=\"$name\" selected=\"selected\">$name</option>";
			} else {
				$order_pref .= "<option value=\"$name\">$name</option>";
			}
			if ($self->{query}->{send_pref} eq $name) {
				$send_pref .= "<option value=\"$name\" selected=\"selected\">$name</option>";
			} else {
				$send_pref .= "<option value=\"$name\">$name</option>";
			}
		}

		my $comment_ins = new webliberty::String($self->{query}->{comment});
		$comment_ins->create_plain;

		$order_name  = $self->{query}->{order_name};
		$order_kana  = $self->{query}->{order_kana};
		$order_mail  = $self->{query}->{order_mail};
		$order_post  = $self->{query}->{order_post};
		$order_addr  = $self->{query}->{order_addr};
		$order_phone = $self->{query}->{order_phone};
		$order_fax   = $self->{query}->{order_fax};
		$send_name   = $self->{query}->{send_name};
		$send_kana   = $self->{query}->{send_kana};
		$send_post   = $self->{query}->{send_post};
		$send_addr   = $self->{query}->{send_addr};
		$send_phone  = $self->{query}->{send_phone};
		$send_fax    = $self->{query}->{send_fax};
		$user_pwd    = $self->{query}->{user_pwd};
		$cfm_pwd     = $self->{query}->{cfm_pwd} || $self->{query}->{user_pwd};
		$pwd_start   = $self->{query}->{pwd_start};
		$pwd_end     = $self->{query}->{pwd_end};
		$comment     = $comment_ins->get_string;
		$save        = $self->{query}->{save} ? ' checked="checked"' : '';

		if ($self->{config}->{customer_mode}) {
			if ($login_mail_ins->get_string) {
				$pwd_start = '<!--';
				$pwd_end   = '-->';

				if ($self->{config}->{point_mode}) {
					$point = $self->{query}->{point} ? ' checked="checked"' : '';
				} else {
					$point_start = '<!--';
					$point_end   = '-->';
				}
			} else {
				$point_start = '<!--';
				$point_end   = '-->';
			}

			$save_start = '<!--';
			$save_end   = '-->';
		} else {
			$pwd_start   = '<!--';
			$pwd_end     = '-->';
			$point_start = '<!--';
			$point_end   = '-->';
		}
	} else {
		if (!$self->{config}->{customer_mode}) {
			my $cookie_ins = new webliberty::Cookie($self->{config}->{cookie_customer}, $self->{init}->{des_key});

			foreach (split(/<>/, $self->{config}->{order_payment})) {
				my($name, $fee) = split(/,/);

				if ($cookie_ins->get_cookie('payment') eq $name) {
					$payment .= "<option value=\"$name\" selected=\"selected\">$name</option>";
				} else {
					$payment .= "<option value=\"$name\">$name</option>";
				}
			}

			if ($self->{config}->{order_delivery}) {
				foreach (split(/<>/, $self->{config}->{order_delivery})) {
					if ($cookie_ins->get_cookie('delivery') eq $_) {
						$delivery .= "<option value=\"$_\" selected=\"selected\">$_</option>";
					} else {
						$delivery .= "<option value=\"$_\">$_</option>";
					}
				}
			} else {
				$delivery_start = '<!--';
				$delivery_end   = '-->';
			}

			if ($self->{config}->{order_date_mode}) {
				$date  = "<input type=\"text\" name=\"date_month\" size=\"5\" value=\"\" />月 ";
				$date .= "<input type=\"text\" name=\"date_day\" size=\"5\" value=\"\" />日 ";
				$date .= "<select name=\"date_time\" xml:lang=\"ja\" lang=\"ja\">";
				$date .= "<option value=\"\">選択してください</option>";
				foreach (split(/<>/, $self->{config}->{order_time_list})) {
					$date .= "<option value=\"$_\">$_</option>";
				}
				$date .= "</select>";
			} else {
				$date_start = '<!--';
				$date_end   = '-->';
			}

			foreach (split(/<>/, $self->{config}->{order_pref})) {
				my($name, $fee) = split(/,/);

				if ($cookie_ins->get_cookie('order_pref') eq $name) {
					$order_pref .= "<option value=\"$name\" selected=\"selected\">$name</option>";
				} else {
					$order_pref .= "<option value=\"$name\">$name</option>";
				}
				if ($cookie_ins->get_cookie('send_pref') eq $name) {
					$send_pref .= "<option value=\"$name\" selected=\"selected\">$name</option>";
				} else {
					$send_pref .= "<option value=\"$name\">$name</option>";
				}
			}

			$order_name  = $cookie_ins->get_cookie('order_name');
			$order_kana  = $cookie_ins->get_cookie('order_kana');
			$order_mail  = $cookie_ins->get_cookie('order_mail');
			$order_post  = $cookie_ins->get_cookie('order_post');
			$order_addr  = $cookie_ins->get_cookie('order_addr');
			$order_phone = $cookie_ins->get_cookie('order_phone');
			$order_fax   = $cookie_ins->get_cookie('order_fax');
			$send_name   = $cookie_ins->get_cookie('send_name');
			$send_kana   = $cookie_ins->get_cookie('send_kana');
			$send_post   = $cookie_ins->get_cookie('send_post');
			$send_addr   = $cookie_ins->get_cookie('send_addr');
			$send_phone  = $cookie_ins->get_cookie('send_phone');
			$send_fax    = $cookie_ins->get_cookie('send_fax');
			$pwd_start   = '<!--';
			$pwd_end     = '-->';
			$point_start = '<!--';
			$point_end   = '-->';
			$save        = $cookie_ins->get_cookie('order_mail') ? ' checked="checked"' : '';
		} elsif ($login_mail_ins->get_string) {
			if (!$login_pwd_ins->get_string) {
				$self->error('パスワードが入力されていません。');
			}

			my $flag;

			open(FH, "$self->{init}->{data_customer}") or $self->error("Read Error : $self->{init}->{data_customer}");
			while (<FH>) {
				chomp;
				my($data_date, $data_stat, $data_point, $data_payment, $data_delivery, $data_user_pwd, $data_order_name, $data_order_kana, $data_order_mail, $data_order_post, $data_order_pref, $data_order_addr, $data_order_phone, $data_order_fax, $data_send_name, $data_send_kana, $data_send_post, $data_send_pref, $data_send_addr, $data_send_phone, $data_send_fax) = split(/\t/);

				if ($login_mail_ins->get_string eq $data_order_mail) {
					if (!$data_stat) {
						$self->error('このユーザー情報は現在利用できません。');
					}
					if (!$login_pwd_ins->check_password($data_user_pwd)) {
						$self->error('Ｅメールまたはパスワードが違います。');
					}

					foreach (split(/<>/, $self->{config}->{order_payment})) {
						my($name, $fee) = split(/,/);

						if ($data_payment eq $name) {
							$payment .= "<option value=\"$name\" selected=\"selected\">$name</option>";
						} else {
							$payment .= "<option value=\"$name\">$name</option>";
						}
					}

					if ($self->{config}->{order_delivery}) {
						foreach (split(/<>/, $self->{config}->{order_delivery})) {
							if ($data_delivery eq $_) {
								$delivery .= "<option value=\"$_\" selected=\"selected\">$_</option>";
							} else {
								$delivery .= "<option value=\"$_\">$_</option>";
							}
						}
					} else {
						$delivery_start = '<!--';
						$delivery_end   = '-->';
					}

					if ($self->{config}->{order_date_mode}) {
						$date  = "<input type=\"text\" name=\"date_month\" size=\"5\" value=\"\" />月 ";
						$date .= "<input type=\"text\" name=\"date_day\" size=\"5\" value=\"\" />日 ";
						$date .= "<select name=\"date_time\" xml:lang=\"ja\" lang=\"ja\">";
						$date .= "<option value=\"\">選択してください</option>";
						foreach (split(/<>/, $self->{config}->{order_time_list})) {
							$date .= "<option value=\"$_\">$_</option>";
						}
						$date .= "</select>";
					} else {
						$date_start = '<!--';
						$date_end   = '-->';
					}

					foreach (split(/<>/, $self->{config}->{order_pref})) {
						my($name, $fee) = split(/,/);

						if ($data_order_pref eq $name) {
							$order_pref .= "<option value=\"$name\" selected=\"selected\">$name</option>";
						} else {
							$order_pref .= "<option value=\"$name\">$name</option>";
						}
						if ($data_send_pref eq $name) {
							$send_pref .= "<option value=\"$name\" selected=\"selected\">$name</option>";
						} else {
							$send_pref .= "<option value=\"$name\">$name</option>";
						}
					}

					$order_name  = $data_order_name;
					$order_kana  = $data_order_kana;
					$order_mail  = $data_order_mail;
					$order_post  = $data_order_post;
					$order_addr  = $data_order_addr;
					$order_phone = $data_order_phone;
					$order_fax   = $data_order_fax;
					$send_name   = $data_send_name;
					$send_kana   = $data_send_kana;
					$send_post   = $data_send_post;
					$send_addr   = $data_send_addr;
					$send_phone  = $data_send_phone;
					$send_fax    = $data_send_fax;
					$pwd_start   = '<!--';
					$pwd_end     = '-->';
					$save_start  = '<!--';
					$save_end    = '-->';

					if ($self->{config}->{point_mode}) {
						$point = $self->{query}->{point} ? ' checked="checked"' : '';
					} else {
						$point_start = '<!--';
						$point_end   = '-->';
					}

					$flag = 1;

					last;
				}
			}
			close(FH);

			if (!$flag) {
				$self->error('指定されたお客様情報は存在しません。');
			}
		} else {
			foreach (split(/<>/, $self->{config}->{order_payment})) {
				my($name, $fee) = split(/,/);

				$payment .= "<option value=\"$name\">$name</option>";
			}

			if ($self->{config}->{order_delivery}) {
				foreach (split(/<>/, $self->{config}->{order_delivery})) {
					$delivery .= "<option value=\"$_\">$_</option>";
				}
			} else {
				$delivery_start = '<!--';
				$delivery_end   = '-->';
			}

			if ($self->{config}->{order_date_mode}) {
				$date  = "<input type=\"text\" name=\"date_month\" size=\"5\" value=\"\" />月 ";
				$date .= "<input type=\"text\" name=\"date_day\" size=\"5\" value=\"\" />日 ";
				$date .= "<select name=\"date_time\" xml:lang=\"ja\" lang=\"ja\">";
				$date .= "<option value=\"\">選択してください</option>";
				foreach (split(/<>/, $self->{config}->{order_time_list})) {
					$date .= "<option value=\"$_\">$_</option>";
				}
				$date .= "</select>";
			} else {
				$date_start = '<!--';
				$date_end   = '-->';
			}

			foreach (split(/<>/, $self->{config}->{order_pref})) {
				my($name, $fee) = split(/,/);

				$order_pref .= "<option value=\"$name\">$name</option>";
				$send_pref  .= "<option value=\"$name\">$name</option>";
			}

			$point_start = '<!--';
			$point_end   = '-->';
			$save_start  = '<!--';
			$save_end    = '-->';
		}
	}

	#出力用データ作成
	$self->{html} .= $skin_ins->get_data('contents');

	if ($error[0]) {
		$self->{html} .= $skin_ins->get_data('error_head');

		foreach (@error) {
			$self->{html} .= $skin_ins->get_replace_data(
				'error',
				ERROR_MESSAGE => $_
			);
		}

		$self->{html} .= $skin_ins->get_data('error_foot');
	}

	$self->{html} .= $skin_ins->get_replace_data(
		'form',
		FORM_LOGIN_MAIL => $login_mail_ins->get_string,
		FORM_LOGIN_PWD  => $login_pwd_ins->get_string,

		FORM_GOODS          => $goods_ins->get_string,
		FORM_PAYMENT        => $payment,
		FORM_DELIVERY       => $delivery,
		FORM_DELIVERY_START => $delivery_start,
		FORM_DELIVERY_END   => $delivery_end,
		FORM_DATE           => $date,
		FORM_DATE_START     => $date_start,
		FORM_DATE_END       => $date_end,

		FORM_ORDER_NAME  => $order_name,
		FORM_ORDER_KANA  => $order_kana,
		FORM_ORDER_MAIL  => $order_mail,
		FORM_ORDER_POST  => $order_post,
		FORM_ORDER_PREF  => $order_pref,
		FORM_ORDER_ADDR  => $order_addr,
		FORM_ORDER_PHONE => $order_phone,
		FORM_ORDER_FAX   => $order_fax,

		FORM_SEND_NAME  => $send_name,
		FORM_SEND_KANA  => $send_kana,
		FORM_SEND_POST  => $send_post,
		FORM_SEND_PREF  => $send_pref,
		FORM_SEND_ADDR  => $send_addr,
		FORM_SEND_PHONE => $send_phone,
		FORM_SEND_FAX   => $send_fax,

		FORM_USER_PWD    => $user_pwd,
		FORM_CFM_PWD     => $cfm_pwd,
		FORM_PWD_START   => $pwd_start,
		FORM_PWD_END     => $pwd_end,
		FORM_COMMENT     => $comment,
		FORM_POINT       => $point,
		FORM_POINT_START => $point_start,
		FORM_POINT_END   => $point_end,
		FORM_SAVE        => $save,
		FORM_SAVE_START  => $save_start,
		FORM_SAVE_END    => $save_end
	);
	$self->{html} .= $skin_ins->get_data('footer');

	print $self->header;
	print $self->{html};

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

### 確認画面表示
sub preview {
	my $self  = shift;
	my $point = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	my $login_mail_ins = new webliberty::String($self->{query}->{login_mail});
	my $login_pwd_ins  = new webliberty::String($self->{query}->{login_pwd});
	my $goods_ins      = new webliberty::String($self->{query}->{goods});
	my $comment_ins    = new webliberty::String($self->{query}->{comment});

	$goods_ins->create_plain;
	$comment_ins->create_plain;

	#ページ表示準備
	my $skin_ins = new webliberty::Skin;
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_order_preview}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_footer}", available => 'footer');

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});

	$skin_ins->replace_skin(
		$catalog_ins->info,
		%{$self->{plugin}}
	);

	my($date, $date_start, $date_end);
	if ($self->{query}->{date_month} or $self->{query}->{date_day} or $self->{query}->{date_time}) {
		if ($self->{query}->{date_month}) {
			$date .= "$self->{query}->{date_month}月";
		}
		if ($self->{query}->{date_day}) {
			$date .= "$self->{query}->{date_day}日";
		}
		if ($self->{query}->{date_time}) {
			$date .= "$self->{query}->{date_time}";
		}
	} else {
		$date_start = '<!--';
		$date_end   = '-->';
	}

	my($delivery_start, $delivery_end);
	if (!$self->{query}->{delivery}) {
		$delivery_start = '<!--';
		$delivery_end   = '-->';
	}

	my($send_start, $send_end);
	if (!$self->{query}->{send_name}) {
		$send_start = '<!--';
		$send_end   = '-->';
	}

	my($point_start, $point_end);
	if (!$self->{query}->{point}) {
		$point_start = '<!--';
		$point_end   = '-->';
	}

	my($save_start, $save_end);
	if (!$self->{query}->{save}) {
		$save_start = '<!--';
		$save_end   = '-->';
	}

	#出力用データ作成
	$self->{html} .= $skin_ins->get_replace_data(
		'form',
		VIEW_PAYMENT        => $self->{query}->{payment},
		VIEW_DELIVERY       => $self->{query}->{delivery},
		VIEW_DELIVERY_START => $delivery_start,
		VIEW_DELIVERY_END   => $delivery_end,
		VIEW_DATE           => $date,
		VIEW_DATE_START     => $date_start,
		VIEW_DATE_END       => $date_end,

		VIEW_ORDER_NAME  => $self->{query}->{order_name},
		VIEW_ORDER_KANA  => $self->{query}->{order_kana},
		VIEW_ORDER_MAIL  => $self->{query}->{order_mail},
		VIEW_ORDER_POST  => $self->{query}->{order_post},
		VIEW_ORDER_PREF  => $self->{query}->{order_pref},
		VIEW_ORDER_ADDR  => $self->{query}->{order_addr},
		VIEW_ORDER_PHONE => $self->{query}->{order_phone},
		VIEW_ORDER_FAX   => $self->{query}->{order_fax} || '-',

		VIEW_SEND_NAME  => $self->{query}->{send_name},
		VIEW_SEND_KANA  => $self->{query}->{send_kana},
		VIEW_SEND_POST  => $self->{query}->{send_post},
		VIEW_SEND_PREF  => $self->{query}->{send_pref},
		VIEW_SEND_ADDR  => $self->{query}->{send_addr},
		VIEW_SEND_PHONE => $self->{query}->{send_phone},
		VIEW_SEND_FAX   => $self->{query}->{send_fax} || '-',
		VIEW_SEND_START => $send_start,
		VIEW_SEND_END   => $send_end,

		VIEW_USER_PWD    => $self->{query}->{user_pwd},
		VIEW_PWD_START   => '',
		VIEW_PWD_END     => '',
		VIEW_COMMENT     => $comment_ins->create_text || 'なし',
		VIEW_POINT_START => $point_start,
		VIEW_POINT_END   => $point_end,
		VIEW_SAVE        => '',
		VIEW_SAVE_START  => $save_start,
		VIEW_SAVE_END    => $save_end,

		FORM_LOGIN_MAIL => $login_mail_ins->get_string,
		FORM_LOGIN_PWD  => $login_pwd_ins->get_string,

		FORM_GOODS       => $goods_ins->get_string,
		FORM_PAYMENT     => $self->{query}->{payment},
		FORM_DELIVERY    => $self->{query}->{delivery},
		FORM_DATE_MONTH  => $self->{query}->{date_month},
		FORM_DATE_DAY    => $self->{query}->{date_day},
		FORM_DATE_TIME   => $self->{query}->{date_time},
		FORM_ORDER_NAME  => $self->{query}->{order_name},
		FORM_ORDER_KANA  => $self->{query}->{order_kana},
		FORM_ORDER_MAIL  => $self->{query}->{order_mail},
		FORM_ORDER_POST  => $self->{query}->{order_post},
		FORM_ORDER_PREF  => $self->{query}->{order_pref},
		FORM_ORDER_ADDR  => $self->{query}->{order_addr},
		FORM_ORDER_PHONE => $self->{query}->{order_phone},
		FORM_ORDER_FAX   => $self->{query}->{order_fax},
		FORM_SEND_NAME   => $self->{query}->{send_name},
		FORM_SEND_KANA   => $self->{query}->{send_kana},
		FORM_SEND_POST   => $self->{query}->{send_post},
		FORM_SEND_PREF   => $self->{query}->{send_pref},
		FORM_SEND_ADDR   => $self->{query}->{send_addr},
		FORM_SEND_PHONE  => $self->{query}->{send_phone},
		FORM_SEND_FAX    => $self->{query}->{send_fax},
		FORM_USER_PWD    => $self->{query}->{user_pwd},
		FORM_COMMENT     => $comment_ins->create_plain,
		FORM_POINT       => $point,
		FORM_SAVE        => $self->{query}->{save}
	);
	$self->{html} .= $skin_ins->get_data('footer');

	print $self->header;
	print $self->{html};

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

### 注文実行
sub order {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	my $goods_ins = new webliberty::String($self->{query}->{goods});
	$goods_ins->create_plain;

	my $lock_ins = new webliberty::Lock($self->{init}->{data_lock});
	if (!$lock_ins->file_lock) {
		$self->error('ファイルがロックされています。時間をおいてもう一度投稿してください。');
	}

	my(%stock, %detail);
	foreach (split(/\n/, $goods_ins->get_string)) {
		my($no, $code, $option, $amount) = split(/\t/);

		if ($self->{config}->{use_stock}) {
			if (-s "$self->{init}->{data_stock_dir}$no\.$self->{init}->{data_ext}") {
				open(FH, "$self->{init}->{data_stock_dir}$no\.$self->{init}->{data_ext}") or $self->error("Read Error : $self->{init}->{data_stock_dir}$no\.$self->{init}->{data_ext}");
				my $stock = <FH>;
				close(FH);

				if ($amount + $stock{$no} > $stock) {
					$self->error("<em>$code</em>の在庫数は<em>$stock</em>になりました。この商品を注文する事ができません。");
				}
			} else {
				$self->error("この商品の在庫数は<em>0</em>です。これ以上カートに入れることはできません。");
			}

			$stock{$no} += $amount;
		}

		$detail{$no} .= $code;
	}

	opendir(DIR, $self->{init}->{data_catalog_dir}) or $self->error("Read Error : $self->{init}->{data_catalog_dir}");
	my @dir = sort { $b <=> $a } readdir(DIR);
	closedir(DIR);

	foreach my $entry (@dir) {
		if ($entry !~ /^\d\d\d\d\d\d\.$self->{init}->{data_ext}$/) {
			next;
		}

		open(FH, "$self->{init}->{data_catalog_dir}$entry") or $self->error("Read Error : $self->{init}->{data_catalog_dir}$entry");
		while (<FH>) {
			chomp;
			my($no, $id, $stat, $break, $view, $comt, $tb, $field, $date, $name, $subj, $text, $price, $icon, $image, $file, $host) = split(/\t/);

			if ($detail{$no}) {
				$detail{$no} = "$no\t$id\t$stat\t$break\t$view\t$comt\t$tb\t$field\t$date\t$name\t$subj\t$text\t$price\t$icon\t$image\t$file\t$host";
			}
		}
		close(FH);
	}

	#注文完了メール作成
	my $sendmail_order_body = $self->{config}->{sendmail_order_body};
	$sendmail_order_body =~ s/<>/\n/g;

	my $mail_body  = "$sendmail_order_body\n\n■ご注文内容\n\n";
	my $stock_info = '';
	my $subtotal = 0;

	foreach (split(/\n/, $goods_ins->get_string)) {
		my($no, $code, $option, $amount) = split(/\t/);
		my($no, $id, $stat, $break, $view, $comt, $tb, $field, $date, $name, $subj, $text, $price, $icon, $image, $file, $host) = split(/\t/, $detail{$no});

		if ($self->{config}->{use_stock}) {
			if (-s "$self->{init}->{data_stock_dir}$no\.$self->{init}->{data_ext}") {
				open(FH, "$self->{init}->{data_stock_dir}$no\.$self->{init}->{data_ext}") or $self->error("Read Error : $self->{init}->{data_stock_dir}$no\.$self->{init}->{data_ext}");
				my $stock = <FH>;
				close(FH);

				$stock -= $amount;

				open(FH, ">$self->{init}->{data_stock_dir}$no\.$self->{init}->{data_ext}") or $self->error("Write Error : $self->{init}->{data_stock_dir}$no\.$self->{init}->{data_ext}");
				print FH $stock;
				close(FH);

				if ($stock <= $self->{config}->{sendmail_stock_size}) {
					$stock_info .= "[$id] $subj ... $stock\n";
				}
			}
		}

		if ($option) {
			$option = "($option)";
		}

		my $fee = $price * $amount;
		$subtotal += $fee;

		$mail_body .= "[$id] $subj$option ... $price円 × $amount ＝ $fee円\n";
	}

	my $point;
	if ($self->{config}->{point_mode}) {
		$point = (int(($subtotal - $self->{query}->{point}) / $self->{config}->{point_price}) * $self->{config}->{point_value}) - $self->{query}->{point};
	} else {
		$point = 0;
	}

	my $carriage;
	if ($self->{query}->{send_pref}) {
		$carriage = $self->{query}->{send_pref};
	} else {
		$carriage = $self->{query}->{order_pref};
	}

	my $payment;
	foreach (split(/<>/, $self->{config}->{order_payment})) {
		my($name, $fee) = split(/,/);

		if ($self->{query}->{payment} eq $name) {
			$payment = $fee;

			last;
		}
	}
	if ($payment eq '') {
		$self->error("お支払方法を取得できません。");
	}

	my $i = 0;
	if ($self->{config}->{order_delivery}) {
		foreach (split(/<>/, $self->{config}->{order_delivery})) {
			if ($self->{query}->{delivery} eq $_) {
				last;
			}
			$i++;
		}
	}

	foreach (split(/<>/, $self->{config}->{order_pref})) {
		my($name, $fee) = split(/,/);

		my @fee = split(/\//, $fee);

		if ($carriage eq $name) {
			$carriage = $fee[$i];

			last;
		}
	}

	if ($self->{config}->{order_free} and $self->{config}->{order_free} <= $subtotal) {
		$carriage = 0;
	}

	my $total = $subtotal + $payment + $carriage - $self->{query}->{point};

	my $comment_ins = new webliberty::String($self->{query}->{comment});
	$comment_ins->create_plain;

	my $host_ins = new webliberty::Host;

	$mail_body .= "\n";
	$mail_body .= "小計　　：$subtotal円\n";
	if ($self->{query}->{point} > 0) {
		$mail_body .= "ポイント：-$self->{query}->{point}円\n";
	}
	if ($payment > 0) {
		$mail_body .= "手数料　：$payment円\n";
	}
	$mail_body .= "送料　　：$carriage円\n";
	$mail_body .= "合計　　：$total円\n";
	$mail_body .= "\n";
	$mail_body .= "■ご注文情報\n";
	$mail_body .= "\n";
	$mail_body .= "支払方法：$self->{query}->{payment}\n";
	if ($self->{query}->{delivery}) {
		$mail_body .= "配達方法：$self->{query}->{delivery}\n";
	}
	if ($self->{query}->{date_month} or $self->{query}->{date_day} or $self->{query}->{date_time}) {
		my $date;
		if ($self->{query}->{date_month}) {
			$date .= "$self->{query}->{date_month}月";
		}
		if ($self->{query}->{date_day}) {
			$date .= "$self->{query}->{date_day}日";
		}
		if ($self->{query}->{date_time}) {
			$date .= "$self->{query}->{date_time}";
		}

		$mail_body .= "配達日時：$dateを希望\n";
	}
	$mail_body .= "\n";
	$mail_body .= "■お客様情報\n";
	$mail_body .= "\n";
	$mail_body .= "名前　　：$self->{query}->{order_name}\n";
	$mail_body .= "ふりがな：$self->{query}->{order_kana}\n";
	$mail_body .= "Ｅメール：$self->{query}->{order_mail}\n";
	$mail_body .= "郵便番号：$self->{query}->{order_post}\n";
	$mail_body .= "都道府県：$self->{query}->{order_pref}\n";
	$mail_body .= "住所　　：$self->{query}->{order_addr}\n";
	$mail_body .= "電話番号：$self->{query}->{order_phone}\n";
	$mail_body .= "FAX番号 ：$self->{query}->{order_fax}\n";
	$mail_body .= "\n";
	if ($self->{query}->{send_name}) {
		$mail_body .= "■配送先情報\n";
		$mail_body .= "\n";
		$mail_body .= "名前　　：$self->{query}->{send_name}\n";
		$mail_body .= "ふりがな：$self->{query}->{send_kana}\n";
		$mail_body .= "郵便番号：$self->{query}->{send_post}\n";
		$mail_body .= "都道府県：$self->{query}->{send_pref}\n";
		$mail_body .= "住所　　：$self->{query}->{send_addr}\n";
		$mail_body .= "電話番号：$self->{query}->{send_phone}\n";
		$mail_body .= "FAX番号 ：$self->{query}->{send_fax}\n";
		$mail_body .= "\n";
	}
	$mail_body .= "■その他\n";
	$mail_body .= "\n";
	if ($point > 0) {
		$mail_body .= "ポイント：$pointポイント取得しました\n";
	} elsif ($point < 0) {
		my $point_info = $point * -1;
		$mail_body .= "ポイント：$point_infoポイント減少しました\n";
	}
	if ($comment_ins->get_string) {
		$mail_body .= "連絡事項：\n" . $comment_ins->get_string . "\n";
		$mail_body .= "\n";
	}
	$mail_body .= "送信ホスト：" . $host_ins->get_host . "\n";
	$mail_body .= "\n";

	my $sendmail_signature = $self->{config}->{sendmail_signature};
	$sendmail_signature =~ s/<>/\n/g;

	$mail_body .= "$sendmail_signature";

	#顧客情報登録
	if ($self->{config}->{customer_mode}) {
		my $customer_ins = new webliberty::App::Customer($self->{init}, $self->{config}, $self->{query});
		$customer_ins->regist(point => $point);
	} else {
		my $cookie_ins = new webliberty::Cookie($self->{config}->{cookie_customer}, $self->{init}->{des_key});
		$cookie_ins->set_holddays($self->{config}->{cookie_holddays});
		if ($self->{config}->{ssl_url}) {
			$cookie_ins->set_secure(1);
		}
		if ($self->{query}->{save}) {
			$cookie_ins->set_cookie(
				payment     => $self->{query}->{payment},
				delivery    => $self->{query}->{delivery},
				order_pref  => $self->{query}->{order_pref},
				send_pref   => $self->{query}->{send_pref},
				order_name  => $self->{query}->{order_name},
				order_kana  => $self->{query}->{order_kana},
				order_mail  => $self->{query}->{order_mail},
				order_post  => $self->{query}->{order_post},
				order_addr  => $self->{query}->{order_addr},
				order_phone => $self->{query}->{order_phone},
				order_fax   => $self->{query}->{order_fax},
				send_name   => $self->{query}->{send_name},
				send_kana   => $self->{query}->{send_kana},
				send_post   => $self->{query}->{send_post},
				send_addr   => $self->{query}->{send_addr},
				send_phone  => $self->{query}->{send_phone},
				send_fax    => $self->{query}->{send_fax}
			);
		} else {
			$cookie_ins->set_cookie;
		}
	}

	#売り上げランキング登録
	if ($self->{config}->{rank_mode}) {
		my %goods;
		foreach (split(/\n/, $goods_ins->get_string)) {
			my($no, $code, $option, $amount) = split(/\t/);

			$goods{$code} += $amount;
		}

		my(@ranks, %flag);

		open(FH, "$self->{init}->{data_rank}") or $self->error("Read Error : $self->{init}->{data_rank}");
		while (<FH>) {
			chomp;
			my($count, $id) = split(/\t/);

			if ($goods{$id}) {
				$count += $goods{$id};

				$flag{$id} = 1;
			}
			push(@ranks, "$count\t$id\n");
		}
		close(FH);

		foreach (split(/\n/, $goods_ins->get_string)) {
			my($no, $code, $option, $amount) = split(/\t/);

			if (!$flag{$code}) {
				push(@ranks, "$amount\t$code\n");
			}
		}

		open(FH, ">$self->{init}->{data_rank}") or $self->error("Write Error : $self->{init}->{data_rank}");
		print FH sort { $b <=> $a } @ranks;
		close(FH);
	}

	$lock_ins->file_unlock;

	#データ更新
	foreach (split(/\n/, $goods_ins->get_string)) {
		my($no, $code, $option, $amount) = split(/\t/);

		if ($self->{update}->{query}->{no}) {
			$self->{update}->{query}->{no} .= "\n";
		}
		$self->{update}->{query}->{no} .= $no;
	}

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{update}->{query});
	$catalog_ins->update;

	#メール送信
	my $sendmail_ins = new webliberty::Sendmail($self->{config}->{sendmail_path});
	foreach (split(/<>/, "$self->{config}->{sendmail_list}<>$self->{query}->{order_mail}")) {
		if (!$_) {
			next;
		}

		my($flag, $message) = $sendmail_ins->sendmail(
			send_to   => $_,
			send_from => $self->{config}->{sendmail_addr},
			subject   => $self->{config}->{sendmail_order_subj},
			name      => $self->{config}->{sendmail_name},
			message   => $mail_body
		);
		if (!$flag) {
			$self->error($message);
		}
	}

	#在庫数通知
	if ($self->{config}->{sendmail_stock_mode} and $stock_info) {
		my($sec, $min, $hour, $day, $mon, $year, $week) = localtime(time);
		my $date = sprintf("%02d/%02d %02d:%02d", $mon + 1, $day, $hour, $min);

		my $mail_body;
		$mail_body  = "$self->{config}->{site_title}の商品在庫数が少なくなっています。\n";
		$mail_body .= "$date に注文された商品のうち、在庫数が$self->{config}->{sendmail_stock_size}より少ない商品は以下のとおりです。\n";
		$mail_body .= "\n";
		$mail_body .= "$stock_info";

		foreach (split(/<>/, $self->{config}->{sendmail_list})) {
			if (!$_) {
				next;
			}

			my($flag, $message) = $sendmail_ins->sendmail(
				send_to   => $_,
				send_from => $self->{config}->{sendmail_addr},
				subject   => "$self->{config}->{site_title}の商品在庫数",
				name      => $self->{config}->{sendmail_name},
				message   => $mail_body
			);
			if (!$flag) {
				$self->error($message);
			}
		}
	}

	my $info_path;
	if ($self->{init}->{script_file} =~ /([^\/\\]*)$/) {
		$info_path = "$self->{config}->{site_url}$1";
	}

	if ($self->{init}->{location_mode} and !$self->{config}->{ssl_url}) {
		if ($ENV{'PERLXS'} eq 'PerlIS') {
			print "HTTP/1.0 302 Temporary Redirection\r\n";
			print "Content-type: text/html\n";
		}
		print "Location: $info_path?mode=order&work=complete\n\n";
	} else {
		print $self->header;
		print "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n";
		print "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n";
		print "<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"ja\" lang=\"ja\">\n";
		print "<head>\n";
		print "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n";
		print "<meta http-equiv=\"refresh\" content=\"0; url=$info_path?mode=order&amp;work=complete\">\n";
		print "<title>注文完了</title>\n";
		print "</head>\n";
		print "<body>\n";
		print "<h1>注文完了</h1>\n";
		print "<p>注文が完了しました。自動的にページが更新されない場合、以下のURLをクリックしてください。</p>\n";
		print "<ul>\n";
		print "\t<li><a href=\"$info_path?mode=order&amp;work=complete\">$info_path?mode=order&amp;work=complete</a></li>\n";
		print "</ul>\n";
		print "</body>\n";
		print "</html>\n";
	}

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

### 注文完了
sub complete {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	#カートクリア
	my $cookie_ins = new webliberty::Cookie($self->{config}->{cookie_cart}, $self->{init}->{des_key});
	$cookie_ins->set_holddays($self->{config}->{cookie_holddays});
	$cookie_ins->set_cookie;

	#ページ表示準備
	my $skin_ins = new webliberty::Skin;
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_header}", available => 'header');
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_order_complete}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_footer}", available => 'footer');

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});

	$skin_ins->replace_skin(
		$catalog_ins->info,
		%{$self->{plugin}}
	);

	#データ出力
	print $self->header;
	print $skin_ins->get_data('header');
	print $skin_ins->get_data('contents');
	print $skin_ins->get_data('footer');

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

1;
