#webliberty::App::Cart.pm (2007/12/25)
#Copyright(C) 2002-2007 Knight, All rights reserved.

package webliberty::App::Cart;

use strict;
use base qw(webliberty::Basis);
use webliberty::String;
use webliberty::Cookie;
use webliberty::Skin;
use webliberty::Plugin;
use webliberty::App::Catalog;

### コンストラクタ
sub new {
	my $class = shift;

	my $self = {
		init   => shift,
		config => shift,
		query  => shift,
		plugin => undef,
		update => undef
	};
	bless $self, $class;

	return $self;
}

### メイン処理
sub run {
	my $self = shift;

	if ($self->{init}->{rewrite_mode}) {
		my $catalog_ins = new webliberty::App::Catalog($self->{init}, '', $self->{query});
		$self->{init} = $catalog_ins->rewrite(%{$self->{init}->{rewrite}});
	}

	if ($self->{query}->{work}) {
		if ($self->{query}->{work} eq 'putin') {
			$self->putin;
		} elsif ($self->{query}->{work} eq 'update') {
			$self->update;
		} elsif ($self->{query}->{work} eq 'del') {
			$self->del;
		} elsif ($self->{query}->{work} eq 'clear') {
			$self->clear;
		}

		if ($self->{init}->{location_mode}) {
			if ($ENV{'PERLXS'} eq 'PerlIS') {
				print "HTTP/1.0 302 Temporary Redirection\r\n";
				print "Content-type: text/html\n";
			}
			print "Location: $self->{init}->{script_file}?mode=cart\n\n";
		} else {
			print $self->header;
			print "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n";
			print "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n";
			print "<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"ja\" lang=\"ja\">\n";
			print "<head>\n";
			print "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n";
			print "<meta http-equiv=\"refresh\" content=\"0; url=$self->{init}->{script_file}?mode=cart\">\n";
			print "<title>処理完了</title>\n";
			print "</head>\n";
			print "<body>\n";
			print "<h1>処理完了</h1>\n";
			print "<p>処理が完了しました。自動的にページが更新されない場合、以下のURLをクリックしてください。</p>\n";
			print "<ul>\n";
			print "\t<li><a href=\"$self->{init}->{script_file}?mode=cart\">$self->{init}->{script_file}?mode=cart</a></li>\n";
			print "</ul>\n";
			print "</body>\n";
			print "</html>\n";
		}
	} else {
		$self->output;
	}

	return;
}

### 商品追加
sub putin {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	my $no_ins     = new webliberty::String($self->{query}->{no});
	my $code_ins   = new webliberty::String($self->{query}->{code});
	my $amount_ins = new webliberty::String($self->{query}->{amount});
	my $option_ins = new webliberty::String($self->{query}->{option});

	if ($no_ins->get_string =~ /[^\d]/) {
		$self->error("登録番号の指定が不正です。");
	}
	if ($code_ins->get_string =~ /[^\w\d\-\_]/) {
		$self->error("商品コードの指定が不正です。");
	}
	if ($amount_ins->get_string =~ /[^\d]/) {
		$self->error("商品数の指定が不正です。");
	}

	$option_ins->create_text;
	$option_ins->replace_string('<br />', '、');

	my $cookie_ins = new webliberty::Cookie($self->{config}->{cookie_cart}, $self->{init}->{des_key});

	my %cookie;
	if ($cookie_ins->get_cookie) {
		%cookie = %{$cookie_ins->get_cookie};
	}

	my @key = sort { $a <=> $b } keys %cookie;
	my(%stock, $flag, $this_amount);

	foreach (@key) {
		my($no, $code, $option, $amount) = split(/\t/, $cookie{"$_"});

		if ($code_ins->get_string eq $code and $option_ins->get_string eq $option) {
			$this_amount = $amount + $amount_ins->get_string;
			$cookie{$_} = $no_ins->get_string . "\t" . $code_ins->get_string . "\t" . $option_ins->get_string . "\t$this_amount";

			$flag = 1;
		} else {
			$stock{$no} += $amount;
		}
	}
	if (!$flag) {
		$this_amount = $amount_ins->get_string;
		$cookie{$key[$#key] + 1} = $no_ins->get_string . "\t" . $code_ins->get_string . "\t" . $option_ins->get_string . "\t$this_amount";
	}

	if ($self->{config}->{use_stock}) {
		if (-s "$self->{init}->{data_stock_dir}" . $no_ins->get_string . "\.$self->{init}->{data_ext}") {
			open(FH, "$self->{init}->{data_stock_dir}" . $no_ins->get_string . "\.$self->{init}->{data_ext}") or $self->error("Read Error : $self->{init}->{data_stock_dir}" . $no_ins->get_string . "\.$self->{init}->{data_ext}");
			my $stock = <FH>;
			close(FH);

			if ($this_amount + $stock{$no_ins->get_string} > $stock) {
				$self->error("この商品の在庫数は<em>$stock</em>です。これ以上カートに入れることはできません。");
			}
		} else {
			$self->error("この商品の在庫数は<em>0</em>です。これ以上カートに入れることはできません。");
		}
	}

	$cookie_ins->set_holddays($self->{config}->{cookie_holddays});
	$cookie_ins->set_cookie(
		%cookie
	);

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

### カート更新
sub update {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	my $cookie_ins = new webliberty::Cookie($self->{config}->{cookie_cart}, $self->{init}->{des_key});

	my(%cookie, %new_cookie);
	if ($cookie_ins->get_cookie) {
		%cookie = %{$cookie_ins->get_cookie};
	}

	my @key = sort { $a <=> $b } keys %cookie;

	my %stock;

	foreach (@key) {
		my $amount_ins = new webliberty::String($self->{query}->{$_});
		if ($amount_ins->get_string =~ /[^\d]/) {
			$self->error("商品数の指定が不正です。");
		}

		if ($amount_ins->get_string) {
			my($no, $code, $option, $amount) = split(/\t/, $cookie{"$_"});

			if ($self->{config}->{use_stock}) {
				if (-s "$self->{init}->{data_stock_dir}$no\.$self->{init}->{data_ext}") {
					open(FH, "$self->{init}->{data_stock_dir}$no\.$self->{init}->{data_ext}") or $self->error("Read Error : $self->{init}->{data_stock_dir}$no\.$self->{init}->{data_ext}");
					my $stock = <FH>;
					close(FH);

					if ($amount_ins->get_string + $stock{$no} > $stock) {
						$self->error("<em>$code</em>の在庫数は<em>$stock</em>です。");
					}
				} else {
					$self->error("この商品の在庫数は<em>0</em>です。これ以上カートに入れることはできません。");
				}

				$stock{$no} += $amount;
			}

			$new_cookie{$_} = "$no\t$code\t$option\t" . $amount_ins->get_string;
		}
	}

	$cookie_ins->set_holddays($self->{config}->{cookie_holddays});
	$cookie_ins->set_cookie(
		%new_cookie
	);

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

### 商品削除
sub del {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	my $no_ins = new webliberty::String($self->{query}->{no});
	if ($no_ins->get_string =~ /[^\d]/) {
		$self->error("識別番号の指定が不正です。");
	}

	my $cookie_ins = new webliberty::Cookie($self->{config}->{cookie_cart}, $self->{init}->{des_key});

	my(%cookie, %new_cookie);
	if ($cookie_ins->get_cookie) {
		%cookie = %{$cookie_ins->get_cookie};
	}

	my @key = sort { $a <=> $b } keys %cookie;

	foreach (@key) {
		my($no, $code, $option, $amount) = split(/\t/, $cookie{"$_"});

		if ($no_ins->get_string != $_) {
			$new_cookie{$_} = $cookie{$_};
		}
	}

	$cookie_ins->set_holddays($self->{config}->{cookie_holddays});
	$cookie_ins->set_cookie(
		%new_cookie
	);

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

### カートクリア
sub clear {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	my $cookie_ins = new webliberty::Cookie($self->{config}->{cookie_cart}, $self->{init}->{des_key});
	$cookie_ins->set_holddays($self->{config}->{cookie_holddays});
	$cookie_ins->set_cookie;

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

### カート内容表示
sub output {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	#商品データ取得
	my $cookie_ins = new webliberty::Cookie($self->{config}->{cookie_cart}, $self->{init}->{des_key});

	my %cookie;
	if ($cookie_ins->get_cookie) {
		%cookie = %{$cookie_ins->get_cookie};
	}

	my @key = sort { $a <=> $b } keys %cookie;

	my %detail;
	foreach (@key) {
		my($no, $code, $option, $amount) = split(/\t/, $cookie{"$_"});

		$detail{$no} = $code;
	}

	opendir(DIR, $self->{init}->{data_catalog_dir}) or $self->error("Read Error : $self->{init}->{data_catalog_dir}");
	my @dir = sort { $b <=> $a } readdir(DIR);
	closedir(DIR);

	foreach my $entry (@dir) {
		if ($entry !~ /^\d\d\d\d\d\d\.$self->{init}->{data_ext}$/) {
			next;
		}

		open(FH, "$self->{init}->{data_catalog_dir}$entry") or $self->error("Read Error : $self->{init}->{data_catalog_dir}$entry");
		while (<FH>) {
			chomp;
			my($no, $id, $stat, $break, $view, $comt, $tb, $field, $date, $name, $subj, $text, $price, $icon, $image, $file, $host) = split(/\t/);

			if ($detail{$no}) {
				$detail{$no} = "$no\t$id\t$stat\t$break\t$view\t$comt\t$tb\t$field\t$date\t$name\t$subj\t$text\t$price\t$icon\t$image\t$file\t$host";
			}
		}
		close(FH);
	}

	#ページ表示準備
	my $skin_ins = new webliberty::Skin;
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_header}", available => 'header');
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_cart}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_footer}", available => 'footer');

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});

	$skin_ins->replace_skin(
		$catalog_ins->info,
		%{$self->{plugin}}
	);

	#データ出力
	print $self->header;
	print $skin_ins->get_data('header');
	print $skin_ins->get_data('cart_head');

	my $subtotal = 0;
	my $cart_goods;
	my $flag;

	foreach my $key (@key) {
		my($no, $code, $option, $amount) = split(/\t/, $cookie{"$key"});
		my($no, $id, $stat, $break, $view, $comt, $tb, $field, $date, $name, $subj, $text, $price, $icon, $image, $file, $host) = split(/\t/, $detail{$no});

		my($option_start, $option_end);
		if (!$option) {
			$option_start = '<!--';
			$option_end   = '-->';
		}

		$subtotal += $price * $amount;

		$cart_goods .= "$no\t$code\t$option\t$amount\n";

		print $skin_ins->get_replace_data(
			'cart',
			$catalog_ins->catalog_article($no, $id, $stat, $break, $view, $comt, $tb, $field, $date, $name, $subj, $text, $price, $icon, $image, $file, $host),
			CART_NO           => $key,
			CART_OPTION       => $option,
			CART_OPTION_START => $option_start,
			CART_OPTION_END   => $option_end,
			CART_AMOUNT       => "<input type=\"text\" name=\"$key\" size=\"3\" value=\"$amount\" style=\"ime-mode:disabled;\" />",
			CART_FEE          => $price * $amount
		);

		$flag = 1;
	}

	my($cart_free, $cart_free_start, $cart_free_end, $cart_free_off_start, $cart_free_off_end);
	if ($self->{config}->{order_free} and $subtotal) {
		if ($self->{config}->{order_free} > $subtotal) {
			$cart_free_start = '<!--';
			$cart_free_end   = '-->';

			$cart_free = $self->{config}->{order_free} - $subtotal;
		} else {
			$cart_free_off_start = '<!--';
			$cart_free_off_end   = '-->';

			$cart_free = $self->{config}->{order_free};
		}
	} else {
		$cart_free_start = '<!--';
		$cart_free_end   = '-->';

		$cart_free_off_start = '<!--';
		$cart_free_off_end   = '-->';
	}

	my($cart_goods_start, $cart_goods_end);
	if (!$flag) {
		$cart_goods_start = '<!--';
		$cart_goods_end   = '-->';

		print $skin_ins->get_data('void');
	}

	print $skin_ins->get_replace_data(
		'cart_foot',
		CART_SUBTOTAL       => $subtotal,
		CART_FREE           => $cart_free,
		CART_FREE_START     => $cart_free_start,
		CART_FREE_END       => $cart_free_end,
		CART_FREE_OFF_START => $cart_free_off_start,
		CART_FREE_OFF_END   => $cart_free_off_end,
		CART_GOODS          => $cart_goods,
		CART_GOODS_START    => $cart_goods_start,
		CART_GOODS_END      => $cart_goods_end
	);
	print $skin_ins->get_data('footer');

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

1;
