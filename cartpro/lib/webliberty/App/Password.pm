#webliberty::App::Password.pm (2007/12/25)
#Copyright(C) 2002-2007 Knight, All rights reserved.

package webliberty::App::Password;

use strict;
use base qw(webliberty::Basis);
use webliberty::File;
use webliberty::Lock;
use webliberty::Skin;
use webliberty::Sendmail;
use webliberty::Plugin;
use webliberty::App::Catalog;
use webliberty::App::Customer;

### コンストラクタ
sub new {
	my $class = shift;

	my $self = {
		init   => shift,
		config => shift,
		query  => shift,
		plugin => undef,
		update => undef
	};
	bless $self, $class;

	return $self;
}

### メイン処理
sub run {
	my $self = shift;

	if (!$self->{config}->{customer_mode}) {
		$self->error('不正なアクセスです。');
	}

	if ($self->{init}->{rewrite_mode}) {
		my $catalog_ins = new webliberty::App::Catalog($self->{init}, '', $self->{query});
		$self->{init} = $catalog_ins->rewrite(%{$self->{init}->{rewrite}});
	}

	if ($self->{query}->{work} eq 'complete') {
		$self->complete;
	} else {
		$self->output;
	}

	return;
}

### 送信完了
sub complete {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	if (!$self->{query}->{login_mail}) {
		$self->error('Ｅメールを入力してください。');
	}

	my($customer_name, $customer_mail, $flag);

	open(FH, "$self->{init}->{data_customer}") or $self->error("Read Error : $self->{init}->{data_customer}");
	while (<FH>) {
		chomp;
		my($date, $stat, $point, $payment, $delivery, $user_pwd, $order_name, $order_kana, $order_mail, $order_post, $order_pref, $order_addr, $order_phone, $order_fax, $send_name, $send_kana, $send_post, $send_pref, $send_addr, $send_phone, $send_fax, $host) = split(/\t/);

		if ($self->{query}->{login_mail} eq $order_mail) {
			$customer_name = $order_name;
			$customer_mail = $order_mail;

			$flag = 1;

			last;
		}
	}
	close(FH);

	if (!$flag) {
		$self->error('指定されたお客様情報は存在しません。');
	}

	my $lock_ins = new webliberty::Lock($self->{init}->{data_lock});
	if (!$lock_ins->file_lock) {
		$self->error('ファイルがロックされています。時間をおいてもう一度投稿してください。');
	}

	#パスワード再発行
	my $customer_ins = new webliberty::App::Customer($self->{init}, $self->{config}, $self->{query});
	my $new_pwd = $customer_ins->password;

	$lock_ins->file_unlock;

	#送信メール作成
	my $sendmail_pwd_body = $self->{config}->{sendmail_pwd_body};
	$sendmail_pwd_body =~ s/<>/\n/g;

	my $mail_body = "$sendmail_pwd_body\n";
	$mail_body .= "\n";
	$mail_body .= "新パスワード：$new_pwd\n";
	$mail_body .= "\n";

	my $sendmail_signature = $self->{config}->{sendmail_signature};
	$sendmail_signature =~ s/<>/\n/g;

	$mail_body .= "$sendmail_signature";

	#メール送信
	my $sendmail_ins = new webliberty::Sendmail($self->{config}->{sendmail_path});
	foreach (split(/<>/, "$self->{config}->{sendmail_list}<>$customer_mail")) {
		if (!$_) {
			next;
		}

		my($flag, $message) = $sendmail_ins->sendmail(
			send_to   => $_,
			send_from => $self->{config}->{sendmail_addr},
			subject   => $self->{config}->{sendmail_pwd_subj},
			name      => $self->{config}->{sendmail_name},
			message   => $mail_body
		);
		if (!$flag) {
			$self->error($message);
		}
	}

	my $skin_ins = new webliberty::Skin;
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_header}", available => 'header');
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_password_complete}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_footer}", available => 'footer');

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});

	$skin_ins->replace_skin(
		$catalog_ins->info,
		%{$self->{plugin}}
	);

	print $self->header;
	print $skin_ins->get_data('header');
	print $skin_ins->get_data('contents');
	print $skin_ins->get_data('footer');

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

### 入力画面表示
sub output {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	my $skin_ins = new webliberty::Skin;
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_header}", available => 'header');
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_password}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_footer}", available => 'footer');

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});

	$skin_ins->replace_skin(
		$catalog_ins->info,
		%{$self->{plugin}}
	);

	print $self->header;
	print $skin_ins->get_data('header');
	print $skin_ins->get_data('contents');
	print $skin_ins->get_data('footer');

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

1;
