#webliberty::App::Rate.pm (2007/12/25)
#Copyright(C) 2002-2007 Knight, All rights reserved.

package webliberty::App::Rate;

use strict;
use base qw(webliberty::Basis);
use webliberty::File;
use webliberty::Skin;
use webliberty::Plugin;
use webliberty::App::Catalog;

### コンストラクタ
sub new {
	my $class = shift;

	my $self = {
		init   => shift,
		config => shift,
		query  => shift,
		plugin => undef,
		update => undef
	};
	bless $self, $class;

	return $self;
}

### メイン処理
sub run {
	my $self = shift;

	if (!$self->{config}->{rate_mode}) {
		$self->error('不正なアクセスです。');
	}

	if ($self->{init}->{rewrite_mode}) {
		my $catalog_ins = new webliberty::App::Catalog($self->{init}, '', $self->{query});
		$self->{init} = $catalog_ins->rewrite(%{$self->{init}->{rewrite}});
	}

	$self->output;

	return;
}

### ファイル表示
sub output {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	#ページ表示準備
	my $skin_ins = new webliberty::Skin;
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_header}", available => 'header');
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_rate}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_footer}", available => 'footer');

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});

	$skin_ins->replace_skin(
		$catalog_ins->info,
		%{$self->{plugin}}
	);

	#評価取得
	opendir(DIR, $self->{init}->{data_comt_dir}) or $self->error("Read Error : $self->{init}->{data_comt_dir}");
	my @dir = readdir(DIR);
	closedir(DIR);

	my %rate;
	foreach my $entry (@dir) {
		if ($entry !~ /^\d+\.$self->{init}->{data_ext}$/) {
			next;
		}

		open(FH, "$self->{init}->{data_comt_dir}$entry") or $self->error("Read Error : $self->{init}->{data_comt_dir}$entry");
		while (<FH>) {
			chomp;
			my($no, $pno, $stat, $date, $name, $mail, $url, $subj, $text, $rate, $pwd, $host) = split(/\t/);

			$rate{$pno} += $rate;
		}
		close(FH);
	}

	#商品データ取得
	opendir(DIR, $self->{init}->{data_catalog_dir}) or $self->error("Read Error : $self->{init}->{data_catalog_dir}");
	my @dir = sort { $b <=> $a } readdir(DIR);
	closedir(DIR);

	my %detail;
	foreach my $entry (@dir) {
		if ($entry !~ /^\d\d\d\d\d\d\.$self->{init}->{data_ext}$/) {
			next;
		}

		open(FH, "$self->{init}->{data_catalog_dir}$entry") or $self->error("Read Error : $self->{init}->{data_catalog_dir}$entry");
		while (<FH>) {
			chomp;
			my($no, $id, $stat, $break, $view, $comt, $tb, $field, $date, $name, $subj, $text, $price, $icon, $image, $file, $host) = split(/\t/);

			$detail{$no} = "$subj\t$icon\t$image";
		}
		close(FH);
	}

	my(@data, $i);
	open(FH, $self->{init}->{data_catalog_index}) or $self->error("Read Error : $self->{init}->{data_catalog_index}");
	while (<FH>) {
		chomp;
		my($date, $no, $id, $stat, $field, $name) = split(/\t/);
		my($subj, $icon, $image) = split(/\t/, $detail{$no});

		if (!$stat) {
			next;
		}
		if ($rate{$no} < 1) {
			next;
		}

		push(@data, "$rate{$no}\t$no\t$id\t$stat\t$field\t$date\t$name\t$subj\t$icon\t$image")
	}
	close(FH);

	#データソート
	@data = sort { $b <=> $a } @data;

	#データ出力
	print $self->header;
	print $skin_ins->get_data('header');
	print $skin_ins->get_data('rate_head');

	foreach (@data) {
		my($rate, $no, $id, $stat, $field, $date, $name, $subj, $icon, $image) = split(/\t/);

		$i++;
		if ($i > $self->{config}->{rate_size}) {
			last;
		}

		print $skin_ins->get_replace_data(
			'rate',
			$catalog_ins->catalog_article($no, $id, $stat, '', '', '', '', $field, $date, '', $subj, '', '', $icon, $image, '', ''),
			RATE_RANK  => $i,
			RATE_POINT => $rate
		);
	}

	print $skin_ins->get_data('rate_foot');
	print $skin_ins->get_data('footer');

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

1;
