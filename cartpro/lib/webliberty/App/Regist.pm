#webliberty::App::Regist.pm (2007/12/25)
#Copyright(C) 2002-2007 Knight, All rights reserved.

package webliberty::App::Regist;

use strict;
use base qw(webliberty::Basis);
use webliberty::String;
use webliberty::Host;
use webliberty::Lock;
use webliberty::Skin;
use webliberty::Sendmail;
use webliberty::Plugin;
use webliberty::App::Catalog;
use webliberty::App::Customer;

### コンストラクタ
sub new {
	my $class = shift;

	my $self = {
		init   => shift,
		config => shift,
		query  => shift,
		plugin => undef,
		update => undef
	};
	bless $self, $class;

	return $self;
}

### メイン処理
sub run {
	my $self = shift;

	if (!$self->{config}->{customer_mode}) {
		$self->error('不正なアクセスです。');
	}

	if ($self->{init}->{rewrite_mode}) {
		my $catalog_ins = new webliberty::App::Catalog($self->{init}, '', $self->{query});
		$self->{init} = $catalog_ins->rewrite(%{$self->{init}->{rewrite}});
	}

	my $customer_ins = new webliberty::App::Customer($self->{init}, $self->{config}, $self->{query});

	if ($self->{query}->{work} eq 'preview') {
		my @error_message = $customer_ins->check;

		if ($error_message[0]) {
			$self->input(@error_message);
		} else {
			$self->preview;
		}
	} elsif ($self->{query}->{work} eq 'complete') {
		my @error_message = $customer_ins->check;

		if ($error_message[0] or $self->{query}->{exec_back}) {
			$self->input;
		} else {
			$self->complete;
		}
	} else {
		$self->input;
	}

	return;
}

### 入力フォーム表示
sub input {
	my $self  = shift;
	my @error = @_;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	#ページ表示準備
	my $skin_ins = new webliberty::Skin;
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_header}", available => 'header');
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_regist_input}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_footer}", available => 'footer');

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});

	$skin_ins->replace_skin(
		$catalog_ins->info,
		%{$self->{plugin}}
	);

	my($order_name, $order_kana, $order_mail, $order_post, $order_pref, $order_addr, $order_phone, $order_fax, $send_name, $send_kana, $send_post, $send_pref, $send_addr, $send_phone, $send_fax, $user_pwd, $cfm_pwd);

	if ($self->{query}->{work}) {
		foreach (split(/<>/, $self->{config}->{order_pref})) {
			my($name, $fee) = split(/,/);

			if ($self->{query}->{order_pref} eq $name) {
				$order_pref .= "<option value=\"$name\" selected=\"selected\">$name</option>";
			} else {
				$order_pref .= "<option value=\"$name\">$name</option>";
			}
			if ($self->{query}->{send_pref} eq $name) {
				$send_pref .= "<option value=\"$name\" selected=\"selected\">$name</option>";
			} else {
				$send_pref .= "<option value=\"$name\">$name</option>";
			}
		}

		$order_name  = $self->{query}->{order_name};
		$order_kana  = $self->{query}->{order_kana};
		$order_mail  = $self->{query}->{order_mail};
		$order_post  = $self->{query}->{order_post};
		$order_addr  = $self->{query}->{order_addr};
		$order_phone = $self->{query}->{order_phone};
		$order_fax   = $self->{query}->{order_fax};
		$send_name   = $self->{query}->{send_name};
		$send_kana   = $self->{query}->{send_kana};
		$send_post   = $self->{query}->{send_post};
		$send_addr   = $self->{query}->{send_addr};
		$send_phone  = $self->{query}->{send_phone};
		$send_fax    = $self->{query}->{send_fax};
		$user_pwd    = $self->{query}->{user_pwd};
		$cfm_pwd     = $self->{query}->{cfm_pwd} || $self->{query}->{user_pwd};
	} else {
		foreach (split(/<>/, $self->{config}->{order_pref})) {
			my($name, $fee) = split(/,/);

			$order_pref .= "<option value=\"$name\">$name</option>";
			$send_pref  .= "<option value=\"$name\">$name</option>";
		}
	}

	#データ出力
	print $self->header;
	print $skin_ins->get_data('header');
	print $skin_ins->get_data('contents');

	if ($error[0]) {
		print $skin_ins->get_data('error_head');

		foreach (@error) {
			print $skin_ins->get_replace_data(
				'error',
				ERROR_MESSAGE => $_
			);
		}

		print $skin_ins->get_data('error_foot');
	}

	print $skin_ins->get_replace_data(
		'form',
		FORM_ORDER_NAME  => $order_name,
		FORM_ORDER_KANA  => $order_kana,
		FORM_ORDER_MAIL  => $order_mail,
		FORM_ORDER_POST  => $order_post,
		FORM_ORDER_PREF  => $order_pref,
		FORM_ORDER_ADDR  => $order_addr,
		FORM_ORDER_PHONE => $order_phone,
		FORM_ORDER_FAX   => $order_fax,

		FORM_SEND_NAME  => $send_name,
		FORM_SEND_KANA  => $send_kana,
		FORM_SEND_POST  => $send_post,
		FORM_SEND_PREF  => $send_pref,
		FORM_SEND_ADDR  => $send_addr,
		FORM_SEND_PHONE => $send_phone,
		FORM_SEND_FAX   => $send_fax,

		FORM_USER_PWD => $user_pwd,
		FORM_CFM_PWD  => $cfm_pwd
	);
	print $skin_ins->get_data('footer');

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

### 確認画面表示
sub preview {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	#ページ表示準備
	my $skin_ins = new webliberty::Skin;
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_header}", available => 'header');
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_regist_preview}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_footer}", available => 'footer');

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});

	$skin_ins->replace_skin(
		$catalog_ins->info,
		%{$self->{plugin}}
	);

	my($send_start, $send_end);
	if (!$self->{query}->{send_name}) {
		$send_start = '<!--';
		$send_end   = '-->';
	}

	#データ出力
	print $self->header;
	print $skin_ins->get_data('header');
	print $skin_ins->get_data('contents');
	print $skin_ins->get_replace_data(
		'form',
		VIEW_ORDER_NAME  => $self->{query}->{order_name},
		VIEW_ORDER_KANA  => $self->{query}->{order_kana},
		VIEW_ORDER_MAIL  => $self->{query}->{order_mail},
		VIEW_ORDER_POST  => $self->{query}->{order_post},
		VIEW_ORDER_PREF  => $self->{query}->{order_pref},
		VIEW_ORDER_ADDR  => $self->{query}->{order_addr},
		VIEW_ORDER_PHONE => $self->{query}->{order_phone},
		VIEW_ORDER_FAX   => $self->{query}->{order_fax} || '-',
		VIEW_SEND_NAME   => $self->{query}->{send_name},
		VIEW_SEND_KANA   => $self->{query}->{send_kana},
		VIEW_SEND_POST   => $self->{query}->{send_post},
		VIEW_SEND_PREF   => $self->{query}->{send_pref},
		VIEW_SEND_ADDR   => $self->{query}->{send_addr},
		VIEW_SEND_PHONE  => $self->{query}->{send_phone},
		VIEW_SEND_FAX    => $self->{query}->{send_fax} || '-',
		VIEW_SEND_START  => $send_start,
		VIEW_SEND_END    => $send_end,
		VIEW_USER_PWD    => $self->{query}->{user_pwd},

		FORM_ORDER_NAME  => $self->{query}->{order_name},
		FORM_ORDER_KANA  => $self->{query}->{order_kana},
		FORM_ORDER_MAIL  => $self->{query}->{order_mail},
		FORM_ORDER_POST  => $self->{query}->{order_post},
		FORM_ORDER_PREF  => $self->{query}->{order_pref},
		FORM_ORDER_ADDR  => $self->{query}->{order_addr},
		FORM_ORDER_PHONE => $self->{query}->{order_phone},
		FORM_ORDER_FAX   => $self->{query}->{order_fax},
		FORM_SEND_NAME   => $self->{query}->{send_name},
		FORM_SEND_KANA   => $self->{query}->{send_kana},
		FORM_SEND_POST   => $self->{query}->{send_post},
		FORM_SEND_PREF   => $self->{query}->{send_pref},
		FORM_SEND_ADDR   => $self->{query}->{send_addr},
		FORM_SEND_PHONE  => $self->{query}->{send_phone},
		FORM_SEND_FAX    => $self->{query}->{send_fax},
		FORM_USER_PWD    => $self->{query}->{user_pwd}
	);
	print $skin_ins->get_data('footer');

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

### 登録完了
sub complete {
	my $self = shift;

	my $plugin_ins;
	if (!$self->{update}->{plugin}) {
		$plugin_ins = new webliberty::Plugin($self->{init}, $self->{config}, $self->{query});
		%{$self->{plugin}} = $plugin_ins->run;
	}

	my $lock_ins = new webliberty::Lock($self->{init}->{data_lock});
	if (!$lock_ins->file_lock) {
		$self->error('ファイルがロックされています。時間をおいてもう一度投稿してください。');
	}

	#顧客情報編集
	my $customer_ins = new webliberty::App::Customer($self->{init}, $self->{config}, $self->{query});
	$customer_ins->regist;

	$lock_ins->file_unlock;

	#登録完了メール作成
	my $sendmail_regist_body = $self->{config}->{sendmail_regist_body};
	$sendmail_regist_body =~ s/<>/\n/g;

	my $host_ins = new webliberty::Host;

	my $mail_body = "$sendmail_regist_body\n";
	$mail_body .= "\n";
	$mail_body .= "■お客様情報\n";
	$mail_body .= "\n";
	$mail_body .= "名前　　：$self->{query}->{order_name}\n";
	$mail_body .= "ふりがな：$self->{query}->{order_kana}\n";
	$mail_body .= "Ｅメール：$self->{query}->{order_mail}\n";
	$mail_body .= "郵便番号：$self->{query}->{order_post}\n";
	$mail_body .= "都道府県：$self->{query}->{order_pref}\n";
	$mail_body .= "住所　　：$self->{query}->{order_addr}\n";
	$mail_body .= "電話番号：$self->{query}->{order_phone}\n";
	$mail_body .= "FAX番号 ：$self->{query}->{order_fax}\n";
	$mail_body .= "\n";
	if ($self->{query}->{send_name}) {
		$mail_body .= "■配送先情報\n";
		$mail_body .= "\n";
		$mail_body .= "名前　　：$self->{query}->{send_name}\n";
		$mail_body .= "ふりがな：$self->{query}->{send_kana}\n";
		$mail_body .= "郵便番号：$self->{query}->{send_post}\n";
		$mail_body .= "都道府県：$self->{query}->{send_pref}\n";
		$mail_body .= "住所　　：$self->{query}->{send_addr}\n";
		$mail_body .= "電話番号：$self->{query}->{send_phone}\n";
		$mail_body .= "FAX番号 ：$self->{query}->{send_fax}\n";
		$mail_body .= "\n";
	}
	$mail_body .= "■その他\n";
	$mail_body .= "\n";
	$mail_body .= "パスワード：$self->{query}->{user_pwd}\n";
	$mail_body .= "送信ホスト：" . $host_ins->get_host . "\n";
	$mail_body .= "\n";

	my $sendmail_signature = $self->{config}->{sendmail_signature};
	$sendmail_signature =~ s/<>/\n/g;

	$mail_body .= "$sendmail_signature";

	#メール送信
	my $sendmail_ins = new webliberty::Sendmail($self->{config}->{sendmail_path});
	foreach (split(/<>/, "$self->{config}->{sendmail_list}<>$self->{query}->{order_mail}")) {
		if (!$_) {
			next;
		}

		my($flag, $message) = $sendmail_ins->sendmail(
			send_to   => $_,
			send_from => $self->{config}->{sendmail_addr},
			subject   => $self->{config}->{sendmail_regist_subj},
			name      => $self->{config}->{sendmail_name},
			message   => $mail_body
		);
		if (!$flag) {
			$self->error($message);
		}
	}

	#ページ表示準備
	my $skin_ins = new webliberty::Skin;
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_header}", available => 'header');
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_regist_complete}");
	$skin_ins->parse_skin("$self->{init}->{skin_dir}$self->{init}->{skin_footer}", available => 'footer');

	my $catalog_ins = new webliberty::App::Catalog($self->{init}, $self->{config}, $self->{query});

	$skin_ins->replace_skin(
		$catalog_ins->info,
		%{$self->{plugin}}
	);

	#データ出力
	print $self->header;
	print $skin_ins->get_data('header');
	print $skin_ins->get_data('contents');
	print $skin_ins->get_data('footer');

	if (!$self->{update}->{plugin}) {
		$plugin_ins->complete;
		$self->{update}->{plugin} = 1;
	}

	return;
}

1;
