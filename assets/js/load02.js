// Author Paroday(c) http://paro2day.blog122.fc2.com/blog-entry-560.html
// 20100526

// FC2 COMMENT PREVIEW
// ID : checkPreBody, checkPreBox, textPreBody
var PdyCommentPreview = {
 addListener : function(elm,type,func){
	if(elm.addEventListener){elm.addEventListener(type,func,false);
	}else if(elm.attachEvent){elm.attachEvent('on'+type,func);}
 },
 removeListener : function(elm,type,func){
	if(elm.removeEventListener){elm.removeEventListener(type,func,false);
	}else if(elm.detachEvent){elm.detachEvent('on'+type,func);}
 },
 main : function(){
	var _d=document,pst=_d.getElementById('comment');
	if(!pst){pst=(location.href.indexOf('=edit')<0)?_d.getElementsByName('comment[body]')[0]:_d.getElementsByName('edit[body]')[0];}
	if(!pst){return;}
	PdyCommentPreview.addListener(pst,'keyup',PdyCommentPreview.kup);
	var R=_d.createElement('span'),P=_d.createElement('input'),L=_d.createElement('label');
	R.style.display='block';
	R.style.wordBreak='break-all';
	R.id='checkPreBody';
	pst.parentNode.appendChild(R);
	P.style.display='none';
	P.id='checkPreBox';
	P.type='checkbox';P.value='Preview';
	P.setAttribute('checked', 'checked');
	R.appendChild(P);
	var ctrl=(navigator.userAgent.indexOf('Opera')>=0)?'(Ctrl) ':' ';
	L.appendChild(_d.createTextNode(' Preview'+ctrl));
	L.htmlFor='checkPreBox';
	L.style.display='none';
	R.appendChild(L);
	pst.onblur=PdyCommentPreview.blur;
	var Sa=R.cloneNode(false);
	Sa.id='textPreBody';
	Sa.style.wordBreak='break-all';
	Sa.style.padding='5px 0';
	Sa.style.width='90%';
	R.appendChild(Sa);
	PdyCommentPreview.pre();
 },
 sub : function(){
	var _d=document,pst=_d.getElementById('comment');
	if(!pst){pst=(location.href.indexOf('=edit')<0)?_d.getElementsByName('comment[body]')[0]:_d.getElementsByName('edit[body]')[0];}
	if(this.id=='checkPreBox'){
		if(this.checked===true){
			PdyCommentPreview.pre();
			PdyCommentPreview.addListener(pst,'keyup',PdyCommentPreview.kup);
		}else{
			PdyCommentPreview.removeListener(pst,'keyup',PdyCommentPreview.kup);
			_d.getElementById('textPreBody').innerHTML='';
		}
	}else{
		PdyCommentPreview.removeListener(pst,'keyup',PdyCommentPreview.kup);
		_d.getElementById('checkPreBox').checked=false;
		pst.focus();
		PdyCommentPreview.pre(1);
	}
 },
 pre : function(y){
	var _d=document,pst=_d.getElementById('comment'),t;
	if(!pst){pst=(location.href.indexOf('=edit')<0)?_d.getElementsByName('comment[body]')[0]:_d.getElementsByName('edit[body]')[0];}
	if(y!=1){
		t=pst.value;
	}else{
		if(_d.selection){
			t=_d.selection.createRange().text;
		}else if(pst.selectionStart||pst.selectionStart===0){
			t=pst.value.substring(pst.selectionStart,pst.selectionEnd);
		}else{return;}
	}
	t=t.replace(/"/g,'&quot;').replace(/</g,'&lt;').replace(/>/g,'&gt;').replace(/\n/g,'<br \/>');
	var e=['font-weight:bold"','font-size:','color:#','background-color:#','text-align:left"','text-align:right"','text-align:center"','.gif" \/>',];
	if(/\[.+\].*\[\/.*?\]/.test(t)){
		var a='<span style="',z='<\/span>',c=/\[文字色:(\w{6})\](.*?)\[\/文字色\]/g,bc=/\[背景色:(\w{6})\](.*?)\[\/背景色\]/g,da='<div style="',dz='<\/div>';
//		t=t.replace(/\[太字\](.*?)\[\/太字\]/g,a+e[0]+'$1'+z)
//			.replace(/\[サイズ:([1-7])\](.*?)\[\/サイズ\]/g,a+e[1]+'$1em\">$2'+z)
//			.replace(c,a+e[2]+'$1\">$2'+z)
//			.replace(bc,a+e[3]+'$1\">$2'+z)
//			.replace(/\[左揃え\](.*?)\[\/左揃え\]/g,da+e[4]+'$1'+dz)
//			.replace(/\[右揃え\](.*?)\[\/右揃え\]/g,da+e[5]+'$1'+dz)
//			.replace(/\[中央揃え\](.*?)\[\/中央揃え\]/g,da+e[6]+'$1'+dz)
//			.replace(/\[URL\((https?)(:\/\/[A-Za-z0-9\+\$\;\?\.%,!#~\*\/:@&=_\-]+)\)\](.*?)\[\/URL\]/g,'<a href="$1$2" target="_blank">$3</a>')
//			.replace(/\[URL\((.*?)\)\](.*?)\[\/URL\]/g,'$2');


		while(/\[背景色:(\w{6})\]((?!背景色).)*?\[サイズ:([1-7])\]/.test(t)){
			t=t.replace(/\[背景色:(\w{6})\](((?!背景色).)*?)\[サイズ:([1-7])\]/,'\[背景色:$1\]$2\[サイズ:$4:$1\]');
		}

		while(/\[太字\](.*?)\[\/太字\]/g.test(t)){t=t.replace(/\[太字\](.*?)\[\/太字\]/g,a+e[0]+'>$1'+z);}
		while(/\[サイズ:([1-7])\](.*?)\[\/サイズ\]/g.test(t)){t=t.replace(/\[サイズ:([1-7])\](.*?)\[\/サイズ\]/g,a+e[1]+'$1rem\">$2'+z);}
		while(/\[サイズ:([1-7]):(\w{6})\](.*?)\[\/サイズ\]/g.test(t)){t=t.replace(/\[サイズ:([1-7]):(\w{6})\](.*?)\[\/サイズ\]/g,a+e[1]+'$1rem;background-color:#$2\">$3'+z);}
		while(c.test(t)){t=t.replace(c,a+e[2]+'$1">$2'+z);}
		while(bc.test(t)){t=t.replace(bc,a+e[3]+'$1">$2'+z);}
		while(/\[左揃え\](.*?)\[\/左揃え\]/g.test(t)){t=t.replace(/\[左揃え\](.*?)\[\/左揃え\]/g,da+e[4]+'>$1'+dz);}
		while(/\[右揃え\](.*?)\[\/右揃え\]/g.test(t)){t=t.replace(/\[右揃え\](.*?)\[\/右揃え\]/g,da+e[5]+'>$1'+dz);}
		while(/\[中央揃え\](.*?)\[\/中央揃え\]/g.test(t)){t=t.replace(/\[中央揃え\](.*?)\[\/中央揃え\]/g,da+e[6]+'>$1'+dz);}
		while(/\[URL\((https?)(:\/\/[A-Za-z0-9\+\$\;\?\.%,!#~\*\/:@&=_\-]+)\)\](.*?)\[\/URL\]/g.test(t)){t=t.replace(/\[URL\((https?)(:\/\/[A-Za-z0-9\+\$\;\?\.%,!#~\*\/:@&=_\-]+)\)\](.*?)\[\/URL\]/g,'<a href="$1$2" target="_blank">$3</a>');}
		while(/\[URL\((.*?)\)\](.*?)\[\/URL\]/g.test(t)){t=t.replace(/\[URL\((.*?)\)\](.*?)\[\/URL\]/g,'$2');}

	}
	if(/\[...:.-\d+\]/.test(t)){
		var m='<img class="emoji" src="/assets/js/img/';
		t=t.replace(/\[絵文字:(v|k)-(5[0-3]\d|54[0-5]|[1-4]\d\d|[1-9]\d|[1-9])\]/g,m+'$1/$2'+e[7]).replace(/\[絵文字:(i)-(2[0-7]\d|28[0-2]|1\d\d|[1-9]\d|[1-9])\]/g,m+'$1/$2'+e[7]).replace(/\[絵文字:(e)-(5[0-2]\d|51[0-8]|[1-4]\d\d|[1-9]\d|[1-9])\]/g,m+'$1/$2'+e[7]);
	}
	_d.getElementById('textPreBody').innerHTML=t;
 },
 kup : function(k){
	var ey=(k)?k.which:event.keyCode;
	if(ey>15&&ey<21&&ey!=17||ey>31&&ey<41){return;}
	PdyCommentPreview.pre();
 },
 blur : function(){
	if(document.getElementById('checkPreBox').checked===true){PdyCommentPreview.pre();}
 }
};
// FC2 COMMENT DECORATION2
// ID : FinsertImg, FcolorTable, FiconTable
var FcommentDecoration = {
 main : function(){
	var _d=document,wCheck=_d.getElementById('FinsertImg');
	var pst=_d.getElementById('comment');
	if(!pst){pst=(location.href.indexOf('=edit')<0)?_d.getElementsByName('comment[body]')[0]:_d.getElementsByName('edit[body]')[0];}
	if(!wCheck&&!pst){return;}
	var dA=_d.createElement('div');
	dA.id='toolbar';
	dA.style.position='relative';
	dA.style.padding='0 0 4px';
	dA.style.width='90%';
	if(!wCheck){
		pst.parentNode.insertBefore(dA,pst);
	}else{
		wCheck.appendChild(dA);
	}
	var m=_d.createElement('img');
	m.width=40;m.height=40;
	m.style.border='outset 1px';
	m.style.margin='0 4px 0 0';
	m.style.cursor='pointer';
	var g=['bold','size','color','bcolor','jleft','jright','jcenter','emoji','img','url','pdf'];
	var p=['太字','サイズ','文字色','背景色','左揃え','右揃え','中央揃え','絵文字','画像','URL','PDF'];
	var FS=FcolorIcon.size,FC=FcolorIcon.color,FB=FcolorIcon.bcolor,FG=FcolorIcon.img,FI=FcolorIcon.icon;
	for(var i=0,c;i<11;i++){
		c=m.cloneNode(true);
		c.src='/assets/js/img/'+g[i]+'.png';
		c.alt=g[i];
		c.id='tool_'+g[i];
		c.title=p[i];
		dA.appendChild(c);
		if(g[i].search(/bold/) >= 0 || g[i].search(/jleft/) >= 0 || g[i].search(/jright/) >= 0 || g[i].search(/jcenter/) >= 0 || g[i].search(/url/) >= 0|| g[i].search(/pdf/) >= 0) {
			c.onclick=FcommentDecoration.insertTag;
		}else{
			c.onclick=FcommentDecoration.colorIcon;
		}
	}
	var tA=_d.createElement('table'),tB=_d.createElement('tbody'),tR=_d.createElement('tr'),tD=_d.createElement('td');
	tA.id='FsizeTable';
	tA.style.margin='3px 0 0';
	tA.style.border='1px #cccccc none';
	tA.style.display='none';
	tA.style.width='auto';
	tA.style.background='none';
	tA.style.borderCollapse='separate';
	tA.cellPadding='0';
	tA.cellSpacing='1';
	tD.style.padding='0px 5px 0px 0px';
	tD.style.border='1px #cccccc none';
	tD.style.width='45px';
	tD.style.height='40px';
	tD.style.background='none';
	tD.style.textAlign='center';
	tD.style.cursor='default';
	var img=_d.createElement('img'),imgs='/assets/js/img/',sl='/',pg='.png';
	for(var l=0,Sl=FS.length,imgtag;l<Sl;l++){
		if(l%30===0){tRa=tR.cloneNode(true);}
		tDa=tD.cloneNode(true);
		sizeimg=img.cloneNode(true);
		sizeimg.src=imgs+'s'+sl+FS[l]+pg;
		sizeimg.width=40;
		sizeimg.height=40;
		sizeimg.alt=FS[l];
		sizeimg.onclick=FcommentDecoration.insertTag;
		tDa.appendChild(sizeimg);
		tRa.style.border='1px #cccccc none';
		tRa.appendChild(tDa);
		if(l>0&&l%29===0||l===Sl-1){tA.appendChild(tRa);}
	}
	dA.appendChild(tA);
	PdyCommentPreview.main();
	var tAc=tA.cloneNode(false);
	tAc.id='FcolorTable';
	tD.style.padding='0';
	tD.style.border='outset 2px';
	tD.style.width='40px';
	tD.style.height='40px';
	tD.style.cursor='pointer';
	for(var j=-1,Cl=FC.length,tRa,tDa,sh='#';j<Cl;j++){
		tDa=tD.cloneNode(true);
		if(j===-1){
			tRa=tR.cloneNode(true);
			var cimg=_d.createElement('img');
			cimg.width=40;
			cimg.height=40;
			cimg.src='/assets/js/img/color.png';
			tDa.style.cursor='default';
			tDa.style.padding='0px 5px 0px 0px ';
			tDa.style.border='0px';
//			tDa.style.width='45px';
			tDa.appendChild(cimg);
		}else{
			tDa.title='c'+FC[j];
			tDa.style.background=sh+FC[j];
			tDa.onclick=FcommentDecoration.insertTag;
		}
		tRa.style.border='1px #cccccc none';
		tRa.appendChild(tDa);
		if(j===Cl-1){tAc.appendChild(tRa);}
	}
	dA.appendChild(tAc);
	var tAb=tA.cloneNode(false);
	tAb.id='FbcolorTable';
	for(var j=-1,Bl=FB.length,tRa,tDa,sh='#';j<Bl;j++){
		tDa=tD.cloneNode(true);
		if(j===-1){
			tRa=tR.cloneNode(true);
			var bcimg=_d.createElement('img');
			bcimg.width=40;
			bcimg.height=40;
			bcimg.src='/assets/js/img/bcolor.png';
			tDa.style.cursor='default';
			tDa.style.padding='0px 5px 0px 0px ';
			tDa.style.border='0px';
//			tDa.style.width='45px';
			tDa.appendChild(bcimg);
		}else{
			tDa.title='b'+FB[j];
			tDa.style.background=sh+FB[j];
			tDa.onclick=FcommentDecoration.insertTag;
		}
		tRa.style.border='1px #cccccc none';
		tRa.appendChild(tDa);
		if(j===Bl-1){tAb.appendChild(tRa);}
	}
	dA.appendChild(tAb);
	var tAe=tA.cloneNode(false);
	tAe.id='FiconTable';
	tAe.style.border='1px #cccccc none';
	tAe.style.background='#ffffcc';
	tD.style.border='0px #cccccc none';
	tD.style.width='40px';
	tD.style.height='40px';
	tD.style.background='none';
	tD.style.textAlign='center';
	tD.style.cursor='default';
	var gi='.gif';
	img.style.cursor='pointer';
	img.style.verticalAlign='middle';
	for(var k=0,Il=FI.length,ico;k<Il;k++){
		if(k%30===0){tRa=tR.cloneNode(true);}
		tDa=tD.cloneNode(true);
		ico=img.cloneNode(true);
		ico.src=imgs+FI[k].charAt(0)+sl+FI[k].slice(2)+gi;
		ico.alt=FI[k];
		ico.onclick=FcommentDecoration.insertTag;
		tDa.appendChild(ico);
		tRa.style.border='0px #cccccc none';
		tRa.appendChild(tDa);
		if(k>0&&k%29===0||k===Il-1){tAe.appendChild(tRa);}
	}
	dA.appendChild(tAe);
	var tAi=tA.cloneNode(false);
	tAi.id='FimgTable';
	tAi.style.border='1px #cccccc none';
	tAi.style.background='none';
	tD.style.border='1px #cccccc none';
	tD.style.padding='0px 5px 0px 0px';
	tD.style.width='45px';
	tD.style.height='40px';
	tD.style.background='none';
	tD.style.textAlign='center';
	tD.style.cursor='default';
	var pg='.png';
	for(var l=0,Gl=FG.length,imgtag;l<Gl;l++){
		if(l%30===0){tRa=tR.cloneNode(true);}
		tDa=tD.cloneNode(true);
		imgtag=img.cloneNode(true);
		imgtag.src=imgs+'i'+sl+FG[l]+pg;
		imgtag.width=40;imgtag.height=40;
		imgtag.alt=FG[l];
		imgtag.onclick=FcommentDecoration.insertTag;
		tDa.appendChild(imgtag);
		tRa.style.border='1px #cccccc none';
		tRa.appendChild(tDa);
		if(l>0&&l%29===0||l===Gl-1){tAi.appendChild(tRa);}
	}
	dA.appendChild(tAi);
 },
 insertTag : function(){
	var _d=document,pst=_d.getElementById('comment'),v,a,z,r;
	var ab=0;

	if(!pst){pst=(location.href.indexOf('=edit')<0)?_d.getElementsByName('comment[body]')[0]:_d.getElementsByName('edit[body]')[0];}
	pst.focus();
	if(this.title){
		v=this.title;
		if(v.length==7){
			index = v.charAt(0);
			v=v.slice(1);
			if(index=='b'){
				a='[背景色:'+v+']';z='[/背景色]';r='';
			}else{
				a='[文字色:'+v+']';z='[/文字色]';r='';
			}
		}else{
			if(v.search(/URL/) >= 0){
				a='['+v+'()]';z='[/'+v+']';r='';
				ab=2;
			}else{
				a='['+v+']';z='[/'+v+']';r='';
			}
		}
	}else{
		v=this.alt;
		if(v.search(/size/) >= 0){
			v=v.replace('size','');
			a='[サイズ:'+v+']';z='[/サイズ]';r='';
		}else if(v.search(/img/) >= 0){
			a='{'+v+'}';z='';r='';
		}else{
			a='[絵文字:'+v+']';z='';r='';
		}
	}

	if(_d.selection){
		var ds=_d.selection.createRange();
		ds.text=(ds.text)?a+ds.text+z:a+r+z;
	}else if(pst.selectionStart||pst.selectionStart===0){
		var s=pst.selectionStart,e=pst.selectionEnd;
		var az=pst.value.substring(s,e),sa=pst.value.substring(0,s),sz=pst.value.substring(e);
		pst.value=(az)?sa+a+az+z+sz:sa+a+r+z +sz;
		pos = sa.length + a.length-ab;
		pst.setSelectionRange(pos,pos);
	}else{
		pst.value+=a+r+z;
	}
	pst.blur();
	pst.focus();
 },
 colorIcon : function(){
	var _d=document;
	var a = this.alt;
	var CW=_d.getElementById('FcolorTable');
	if(a.search(/size/) >= 0) {
		CW=_d.getElementById('FsizeTable');
	}else if(a.search(/bcolor/) >= 0) {
		CW=_d.getElementById('FbcolorTable');
	}else if(a.search(/emoji/) >= 0) {
		CW=_d.getElementById('FiconTable');
	}else if(a.search(/img/) >= 0) {
		CW=_d.getElementById('FimgTable');
	}
	CW.style.display=(CW.style.display=='none')?'':'none';
 }
};
PdyCommentPreview.addListener(window,'load',FcommentDecoration.main);
