<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=7" />
	<meta http-equiv="X-UA-Compatible" content="IE=9">
	<title></title>

	<meta name="robots" content="noindex,nofollow" />

	{Asset::js('fp/ro.js')} {Asset::js('fp/gotop.js')}
	{Asset::js('../../common/js/jquery-1.11.1.min.js')}
	{Asset::css('fp/sub.css')}

	{Asset::css('../../common/css/news.css')}
	
	<script type="text/javascript">
	$(function(){
		var _UA = navigator.userAgent;
		if (_UA.indexOf('iPhone') > 0 || (_UA.indexOf('Android') > 0  && navigator.userAgent.indexOf('Mobile') == -1) || _UA.indexOf('iPod') > 0 || navigator.userAgent.indexOf('iPad') > 0) {
				$("article a").each(function() { 
					var tow = $(this).attr('href').replace('detail.','sp\/detail.');
					$(this).attr('href',tow);
				});
				$("link[rel=stylesheet]").each(function() { 
					var one = $(this).attr('href').replace('\/common\/css\/news','\/sp\/common\/css\/news');
					$(this).attr('href',one);
				});
		}
	});
	</script>
</head>
<body class="top_news">
	<div>
	<!--{assign var=new_date value=$smarty.now-24*60*60*Digconst::NEW_DAY}
	{foreach from=$category_list key=sort_id item=category_info}
	{if $category_info.count gt 0}-->
	<!--{if !empty($category_info.category_name)}
	<h3>{$category_info.category_name}</h3>
	{/if}-->
	{foreach from=$topics_list item=topics}
	{if $topics.sort_id eq $sort_id}
		<article>
		{assign var=variables value=["id"=>$topics.topics_id, "prev"=>1]}
		{if $topics.type_flg eq 2}
			{if $topics.prev_flg eq 1}
			<a href="{Uri::create('detail/:id/:prev', $variables)}" target="_top">
			{else}
			<a href="{Uri::create('detail/:id', $variables)}" target="_top">
			{/if}
		{else}
			{if $topics.prev_flg eq 1}
			<a href="{Uri::create('detail.html?no=:id&prev=1', $variables)}" target="_top">
			{else}
			<a href="{Uri::create('detail.html?no=:id', $variables)}" target="_top">
			{/if}
		{/if}
		<!--{if $topics.topics_date|date_format:"%Y-%m-%d" > $new_date|date_format:"%Y-%m-%d"}
			{Asset::img ('new.png', ["width" =>25])}
		{/if}-->
			<span class="main_title">{$topics.topics_date|date_format:"%Y.%m.%d"}</span>　<span>{$topics.title}</span>
			<!--{if $topics.thumbnail_flg eq '1'}
				{foreach from=$thumbnail_img_list key=num item=thumbnail_img}
					{if $num eq $topics.topics_id}
						{$thumbnail_img}
					{/if}
				{/foreach}
			{/if}-->
			</a>
		</article>
	{/if}
	{/foreach}
	</div>
	{/if}
	{/foreach}

</body>
</html>
