			<div id="main">
				<div id="mainInner">
					{assign var=img_option value=['alt'=>"トピックス`$act_type`", 'width'=>50]}
					<h2 class="h2">{Asset::img('admin/ttl_new.png', $img_option)}トピックス{$act_type}</h2>

					{if $error_messages|@count > 0}
					<!-- エラーメッセージ -->
					<p class="red mbottom10">
						{foreach from=$error_messages item=error_message}
							{$error_message}</br>
						{/foreach}
					</p>
					<!--  /エラーメッセージ-->
					{/if}

					<form name="regist" method="post" action="{Uri::create('admin/regist')}" enctype="multipart/form-data">
					<div id="mainInnerBox">
						<!-- 新規トピックス登録 -->
						<table id="regTbl">
							<tr>
								<th>公開</th>
								<td><label><input type="checkbox" name="publish_flg" id="publish_flg" {$publish_flg_checked}>&nbsp;公開する</label></td>
							</tr>
							<tr>
								<th>公開日</th>
								<td><input type="text" name="topics_date_d" id="datepicker" value="{$topics_date_d}" required></td>
							</tr>
							<tr>
								<th>公開時間</th>
								<td>
									<div class="spDownBox">
										<select name="topics_date_h">
										{section name=cnt start=0 loop=24}
											{if $smarty.section.cnt.index eq $topics_date_h}
											<option value="{$smarty.section.cnt.index}" selected>{$smarty.section.cnt.index}</option>
											{else}
											<option value="{$smarty.section.cnt.index}">{$smarty.section.cnt.index}</option>
											{/if}
										{/section}
										</select>
										時

										<select name="topics_date_m">
										{section name=cnt start=0 loop=60}
											{if $smarty.section.cnt.index eq $topics_date_m}
											<option value="{$smarty.section.cnt.index}" selected>{$smarty.section.cnt.index}</option>
											{else}
											<option value="{$smarty.section.cnt.index}">{$smarty.section.cnt.index}</option>
											{/if}
										{/section}
										</select>
										分
									</div>

								</td>
							</tr>

							<tr>
								<th>カテゴリ</th>
								<td>
									<p>
										{html_options name=category_id options=$categorys selected=$category_id}
									</p>
								</td>
							</tr>
							<tr>
								<th>タイトル</th>
								<td>
									<div id="thumbnail">
										<p>
											<span class="contents_ttl">タイトル名</span>
											<input type="text" name="title" id="title" value="{$topics_title}" required>
											<label><input type="checkbox" name="thumbnail_flg" id="thumbnail_flg" {$thumbnail_flg_checked}>&nbsp;サムネイル画像を表示</label>
										</p>
										<p>
											<span class="contents_ttl">サムネイル画像</span>
											<input type="file" name="thumbnail_file" id="thumbnail_file" disabled="disabled">{$thumbnail_file_name}
											<input type="hidden" name="thumbnail_img_id" id="thumbnail_img_id" value="{$thumbnail_img_id}">
											<input type="hidden" name="old_thumbnail_file" id="old_thumbnail_save_file" value="{$thumbnail_file_name}">
											<input type="hidden" name="old_thumbnail_save_file" id="old_thumbnail_save_file" value="{$thumbnail_save_file_name}"><br />
											<span class="red f08">※JPG画像のみ指定可能</span>
										</p>
									</div>
								</td>
							</tr>
							<tr>
								<th rowspan="2">本文</th>
								<td>
									<div class="contentsInnerBox">
									<p><span class="contents_ttl">内容</span>
									<p><textarea name="comment[body]" id="comment" class="textarea animated">{$body}</textarea></p>
										<span class="red f08">{literal}※本文中に画像を表示する場合、{img1}〜{img12}を記入してください。<br />
										※画像は6MByte以下のサイズを指定してください。<br />
										※大きいサイズの画像を一度に多く指定すると、登録に失敗する可能性があります。<br />
										画像を多く登録する場合は、トピックス変更を利用して複数回に分けてください。<br />
										目安として、スマートフォンで撮影した画像(2MByte程度)は一度に3枚まで登録できます。{/literal}</span>
									</p>

									{foreach from=$img_array key=num item=img}
									<p class="mbottom10"><span class="contents_ttl">画像&nbsp;{$num}</span>
										<input type="file" name="img_file{$num}" id="img_file{$num}">
										幅<input type="text" name="img_size_w{$num}" id="img_size_w{$num}" value="{$img.img_size_w}" style="width: 50px;">px
										<input type="hidden" name="topics_img_id{$num}" id="topics_img_id{$num}" value="{$img.topics_img_id}">
										<input type="hidden" name="old_img_file{$num}" id="old_img_file{$num}" value="{$img.img_file_name}">
										<input type="hidden" name="old_img_save_file{$num}" id="old_img_save_file{$num}" value="{$img.save_file_name}">
										{if $img.img_file_name neq ''}
										<span class="tright mleft10">
										{$img.img_file_name} <label><input type="checkbox" name="img_del{$num}" id="img_del{$num}" {if $img.del_flg eq '1'}checked{/if}>ファイル削除</label>
										</span>
										{/if}
									</p>
									{/foreach}
									<br />
									<p class="mbottom10" id="pdf"><span class="contents_ttl">PDFファイル</span>
										<input type="file" name="pdf_file" id="pdf_file">
										<input type="hidden" name="old_pdf_file" id="old_pdf_save_file" value="{$pdf_file_name}">
										<input type="hidden" name="old_pdf_save_file" id="old_pdf_save_file" value="{$pdf_save_file_name}">
										{if $pdf_file_name neq ''}
										<span class="tright mleft10">
										{$pdf_file_name} <label><input type="checkbox" name="pdf_del" id="pdf_del" {$pdf_del_checked}>ファイル削除</label>
										</span>
										{/if}
									</p>
									</div>
								</td>
							</tr>
						</table>

					</div>

					<div id="confirmBox">
						<input type="submit" name="preview_btn" id="preview_btn" value="プレビュー反映" class="button_p">
						<input type="submit" name="regist_btn" id="regist_btn" value="{$act_type}" class="button">
					</div>
					<input type="hidden" name="topics_id" id="topics_id" value="{$topics_id}">
					</form>
					<!-- /新規トピックス登録 -->

				</div>
			</div>
			<script type="text/javascript">var FcolorIcon = {
						size : [
							'size1', 'size2', 'size3', 'size4', 'size5', 'size6', 'size7'
						],color : [
							'000000', '666666', '999999', 'cccccc', 'ffffff',
							'ff0000', 'ff00ff', 'ff8800', 'ffff00', '00ff00', '00aa33',
							'0000ff', '0088ee', '00ffff'
						],bcolor : [
							'000000', '666666', '999999', 'cccccc', 'ffffff',
							'ff0000', 'ff00ff', 'ff8800', 'ffff00', '00ff00', '00aa33',
							'0000ff', '0088ee', '00ffff'
						],img : [
							'img1','img2','img3','img4','img5','img6',
							'img7','img8','img9','img10','img11','img12'
						 ],icon : [
// 							'k-1','k-2','k-3',
							'v-218', 'v-221', 'v-217', 'v-116', 'v-236', 'v-237', 'v-291', 'v-315',
							'v-344', 'v-359', 'v-371', 'v-274', 'v-231', 'v-278', 'v-277', 'v-279',
							'v-280'
						 ]
			 }</script>