<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>ユーザー登録</title>
</head>
<body>
 <form name="form1" method="post" action="{Uri::create('admin/createuser')}">
		<table width="300" border="1">
			<tr>
				<th colspan="2" scope="row">ユーザー登録</th>
			</tr>
		{if $messages neq ''}
 			<tr>
				<td colspan="2" scope="row">
				 	{foreach from=$messages item=message}
				 		{$message}</br>
					{/foreach}
				 </td>
			</tr>
		{/if}
			<tr>
				<th scope="row">認証ID</th>
				<td><label for="admin_id"></label><input name="admin_id" type="password" id="admin_id" value=""></td>
			</tr>
			<tr>
				<th scope="row">ユーザーID</th>
				<td><label for="user_id"></label><input name="user_id" type="text" id="user_id" value="{$user_id}"></td>
			</tr>
			<tr>
				<th scope="row">パスワード</th>
				<td><label for="password"></label> <input type="password" name="password" id="password"></td>
			</tr>
			<tr>
				<th scope="row">ユーザー名</th>
				<td><label for="user_name"></label> <input name="user_name" type="text" id="user_name" value="{$user_name}"></td>
			</tr>
			<tr>
				<th colspan="2" scope="row"><input type="submit" name="button"
					id="button" value="登録"></th>
			</tr>
		</table>
	</form>
</body>
</html>