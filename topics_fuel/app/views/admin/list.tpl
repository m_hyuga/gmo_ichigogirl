			<div id="main">
				<div id="mainInner">
					{assign var=img_option value=['alt'=>'トピックス一覧', 'width'=>50]}
					<h2 class="h2">{Asset::img('admin/ttl_list.png', $img_option)}トピックス一覧</h2>

					{if $error_messages neq ''}
					<!-- エラーメッセージ -->
					<p class="red mbottom10">
						{$error_messages}
					</p>
					<!--  /エラーメッセージ-->
					{/if}

					<div id="mainInnerBox">
						<!-- ログインした日時 -->
						<div id="loginDate">
							<p>{$disp_datetime}</p>
						</div>
						<!-- /ログインした日時 -->

						<!-- トピックス記事内訳 -->
						<ul id="listIndex">
							<li>全て（{$cnt.all}）</li>
							<li>公開（{$cnt.publish}）</li>
							<li>公開待ち（{$cnt.publish_wait}）</li>
							<li>非公開（{$cnt.non_publish}）</li>
							<li>プレビュー登録（{$cnt.preview}）</li>
						</ul>
						<!-- /トピックス記事内訳 -->

						<!-- 記事一覧 -->
						<form name="del_form" method="post" action="{Uri::create('admin/list')}">
						<table id="listTbl">
							<thead>
							<tr>
								<th>記事No.</th>
								<th>タイトル</th>
								<th>公開日</th>
								<th>公開時間</th>
								<th>状態</th>
								<th>カテゴリ</th>
								<th>登録者</th>
								<th>操作</th>
								<th>削除</th>
							</tr>
							</thead>
							<tbody>
							{foreach from=$topics_list key=num item=topics}
							<tr>
								<td>{$topics.topics_id}</td>
								<td style="word-break: break-all;">{$topics.title}</td>
								<td>{$topics.topics_disp_date}</td>
								<td>{$topics.topics_disp_time}</td>
								<td>
									{if $topics.prev_flg eq '1'}
										プレビュー<br />登録
									{elseif $topics.publish_flg eq '1'}
										非公開
									{elseif $topics.topics_date <= $now_datetime}
										公開
									{else}
										公開待ち
									{/if}
								</td>
								<td style="word-break: break-all;">
								{if $topics.category.category_name neq ''}
									{$topics.category.category_name}
								{else}
									-
								{/if}
								</td>
								<td>{$topics.user.user_name}</td>
								<td>
									{assign var=variables value=["id"=>$topics.topics_id]}
									<a href="{Uri::create('admin/regist/:id', $variables)}">編集</a>
								</td>
								<td><input type="checkbox" name="del[]" value="{$num}"></td>
							</tr>
							{/foreach}
							</tbody>
					   	</table>

						<div id="pageChange">
							<p>
								<span class="mright20">{$pager_previous}</span>
								<span class="mright20">{$current_page}/{$total_pages} ページ</span>
								{$pager_next}
							</p>
						</div>

						<div id="confirmBox">
							<label>選択したトピックスを削除&nbsp;<input type="checkbox" name="del_confirm" id="del_confirm"></label>
							<input type="submit" name="delete_btn" id="delete_btn" value="実行" class="button" disabled="disabled">
						</div>
						<input type="hidden" name="act" id="act" value="delete">
						</form>
						<!-- /記事一覧 -->

					</div>

				</div>
			</div>