			<div id="main">
				<div id="mainInner">
					{assign var=img_option value=['alt'=>'マニュアル', 'width'=>50]}
					<h2 class="h2">{Asset::img('admin/ttl_manual.png', $img_option)}マニュアル</h2>

					<div id="mainInnerBox">
						<EMBED SRC="{$manual_url}{Digconst::PDF_ZOOM}" type="application/pdf" width="100%" height="600">
					</div>

				</div>
			</div>