			<div id="main">
				<div id="mainInner">
					{assign var=img_option value=['alt'=>'カテゴリ管理', 'width'=>50]}
					<h2 class="h2">{Asset::img('admin/ttl_cat.png', $img_option)}カテゴリ管理</h2>

					{if $error_messages|@count > 0}
					<!-- エラーメッセージ -->
					<p class="red mbottom10">
						{foreach from=$error_messages item=error_message}
						{$error_message}</br>
						{/foreach}
					</p>
					<!--  /エラーメッセージ-->
					{/if}

					<div id="mainInnerBox">

						<!-- 新規カテゴリ登録 -->
						<div class="catBox">
							<h3 class="h3">新規カテゴリ登録</h3>

							<form name="regist" method="post" action="{Uri::create('admin/category')}">
							<p class="tleft mbottom10 mtop10">新規カテゴリ名&nbsp;
								<input type="text" name="category_name" id="category_name" value="{$category_name}" class="mbottom10">
							</p>

							<div id="confirmBox">
								<input type="submit" name="regist" id="regist" value="登録" class="button">
								<input type="hidden" name="{Digconst::PARAM_N_CATEGORY_ACT}" id="{Digconst::PARAM_N_CATEGORY_ACT}" value="{Digconst::PARAM_V_CATEGORY_ACT_R}">
							</div>
							</form>

						</div>
						<!-- /新規カテゴリ登録 -->

						<!-- カテゴリ一覧 -->
						<div class="catBox">
							<h3 class="h3">カテゴリ一覧</h3>
							<form name="edit" method="post" action="category">
							<table id="catTbl">
								<thead>
								<tr>
									<th>No.</th>
									<th>ソートID</th>
									<th>カテゴリ</th>
									<th>登録者</th>
									<th>削除</th>
								</tr>
								</thead>
								<tbody>
								{foreach from=$category_list item=category}
								<tr>
									<td>{$category.category_id}</td>
									<td>
										<select name="sort_id{$category.category_id}">
										{section name=sort_id start=1 loop=$sort_max_num}
											{assign "selected" ""}
											{if $smarty.section.sort_id.index eq $category.sort_id}
												{assign "selected" "selected"}
											{/if}
											<option value="{$smarty.section.sort_id.index}"{$selected}>{$smarty.section.sort_id.index}</option>
										{/section}
										</select>
									</td>
									<td>
										<input type="text" name="category_name{$category.category_id}" id="category_name{$category.category_id}" value="{$category.category_name}" list="category_list">
									</td>
									<td>{$category.user.user_name}</td>
									<td><input type="checkbox" name="del[]" value="{$category.category_id}"></td>
									<input type="hidden" name="category_id[]" value="{$category.category_id}">
								</tr>
								{/foreach}
							</table>

							<div id="confirmBox">
									<input type="submit" name="delete_btn" id="delete_btn" value="実行" class="button">
									<input type="hidden" name="{Digconst::PARAM_N_CATEGORY_ACT}" id="{Digconst::PARAM_N_CATEGORY_ACT}" value="{Digconst::PARAM_V_CATEGORY_ACT_E}">
							</div>
							<datalist id="category_list">
								{foreach from=$default_category_list item=default_category}
								<option value="{$default_category}">
								{/foreach}
							</datalist>
							</form>
						</div>
						<!-- /カテゴリ一覧 -->
					</div>
				</div>
			</div>
