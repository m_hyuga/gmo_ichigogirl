<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>{$title}</title>

<meta name="description" content="">
<meta name="keywords" content="">

<link rel="stylesheet" href="" media="all">
<link rel="shortcut icon" href="img/favicon.ico">

{Asset::css('reset.css')}
{Asset::css('style.css')}


{Asset::js('jquery-1.10.1.min.js')}
{Asset::js('jquery.rollover.js')}


<!-- ロールオーバー -->
<script type="text/javascript">
jQuery(document).ready(function($) {
  $('#loginId a img, #logout a img').rollover();
});
</script>

<!-- スマホ時メニュー開閉 -->
<script>
$(function(){
  var cwin = $(window).width();
  $("#toggle").click(function(){
	$("#menu").slideToggle();
	return false;
  });
  $(window).resize(function(){
	var rwin = $(window).width();
	if(cwin===rwin){
		return false;
	}
	cwin = rwin;
	var p = 640;
	if(cwin > p){
	  $("#menu").show();
	} else {
	  $("#menu").hide();
	}
  });
});
</script>


{Asset::js('respond.src.js')}
<!--[if lt IE 9]>
{Asset::js('html5shiv-printshiv.js')}
<![endif]-->


{if $add_head_array|@count > 0} {foreach from=$add_head_array
item=add_head} {$add_head} {/foreach} {/if}
</head>
<body>

	{assign var=img_option value=['width'=>30]}
	<!-- container -->
	<div id="container">

		<!-- header -->
		<header id="header">
			<div id="headerInner">
				<div id="loginId">
					<a href="{Uri::create('admin/list')}">{Asset::img('admin/login_img.png', $img_option)}<span>{$user_name}さん ログイン中</span></a>
				</div>
				<div id="logout">
					{assign var=variables value=["logout"=>1]}
					<a href="{Uri::create('admin/login?logout=:logout', $variables)}">{Asset::img('admin/logout_img.png', $img_option)}<span>ログアウト</span></a>
				</div>
			</div>
		</header>
		<!-- /header -->

		<!-- contents -->
		<div id="contents">

			<!-- gnav -->
			<div id="gnav">
				<div id="toggle"><a href="#">menu</a></div>
				<ul id="menu">
					<a href="{Uri::create('admin/list')}"><li>{Asset::img('admin/gnav_list.png', $img_option)}トピックス一覧</li ></a>
					<a href="{Uri::create('admin/regist')}"><li>{Asset::img('admin/gnav_new.png', $img_option)}新規トピックス登録</li></a>
					<a href="{Uri::create('admin/category')}"><li class="borderBottom">{Asset::img('admin/gnav_cat.png', $img_option)}カテゴリ管理</li></a>
					<a href="{Uri::create(Digconst::PATH_MANUAL_PDF)}" target="_blank"><li>{Asset::img('admin/gnav_manual.png', $img_option)}マニュアル</li></a>
					<a href="{Uri::create(Digconst::PATH_AGREEMENT_PDF)}" target="_blank"><li class="borderBottom">{Asset::img('admin/gnav_agree.png', $img_option)}利用規約</li></a>
					<a href="{Uri::create('index.html')}" target="_blank"><li class="last">{Asset::img('admin/gnav_home.png', $img_option)}ホームページに戻る</li></a>
				</ul>
			</div>
			<!-- /gnav -->

			<!-- main -->
			{include file=$content_tpl}
			<!-- /main -->


		</div>
		<!-- /contents -->

		<!-- footer -->
		<div id="footer">
			<address>Copyright &copy; BizPalette Design. All Rights Reserved. </address>
		</div>
		<!-- /footer -->

	</div>
	<!-- /container -->

{Asset::js('retina-1.1.0.min.js')}
</body>
</html>