<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
<title></title>

<meta name="description" content="">
<meta name="keywords" content="">

<link rel="stylesheet" href="" media="all">
<link rel="shortcut icon" href="img/favicon.ico">
{Asset::css('reset.css')}
{Asset::css('style.css')}

{Asset::js('jquery-1.10.1.min.js')}
{Asset::js('jquery.rollover.js')}

<!-- ロールオーバー -->
<script type="text/javascript">
jQuery(document).ready(function($) {
  $('#loginId a img, #logout a img').rollover();
});
</script>

<!-- スマホ時メニュー開閉 -->
<script>
$(function(){
  $("#toggle").click(function(){
	$("#menu").slideToggle();
	return false;
  });
  $(window).resize(function(){
	var win = $(window).width();
	var p = 640;
	if(win > p){
	  $("#menu").show();
	} else {
	  $("#menu").hide();
	}
  });
});
</script>

{Asset::js('respond.src.js')}
<!--[if lt IE 9]>
{Asset::js('html5shiv-printshiv.js')}
<![endif]-->
<head>
<body>

	<!-- container -->
	<div id="container" style="background:none;">

		{assign var=img_option value=['width'=>30]}
		<!-- header -->
		<header id="header">
			<div id="headerInner">
				<div id="loginTtl">
				<a href="#">{Asset::img('admin/login_img.png', $img_option)}<span>BizPalette Design トピックス管理画面</span></a>
				</div>
			</div>
		</header>

		<!-- /header -->

		<!-- contents -->
		<div id="contents">

			<!-- main -->
			<div id="logMain">
				<div id="mainInner">
					<h2 class="h2">ログイン</h2>

					<form name="login" method="post" action="{Uri::create('admin/login')}">

					<div id="mainInnerBox">
						<p class="red mbottom10">&nbsp;
						{if $logout_messag neq ''}
							{$logout_messag}
						{/if}
						</p>
						<p>パスワード</p>
						<input type="password" name="password" id="password" class="mbottom10">
						<p class="red mbottom10">&nbsp;
						{if $error_messages neq ''}
							{foreach from=$error_messages item=error_message}
								{$error_message}
							{/foreach}
						{/if}
						</p>
						<div id="confirmBox">
							<input type="submit" value="ログイン" class="button" name="button" id="button">
						</div>
					</div>
					</form>
				</div>
			</div>
			<!-- /main -->
		</div>
		<!-- /contents -->

		<!-- footer -->
		<div id="footer" class="sp-log">
			<address>Copyright &copy; BizPalette Design. All Rights Reserved. </address>
		</div>
		<!-- /footer -->

	</div>
	<!-- /container -->

{Asset::js('retina-1.1.0.min.js')}
</body>
</html>