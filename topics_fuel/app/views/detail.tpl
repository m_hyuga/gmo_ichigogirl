<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<script src="../common/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="../common/js/common.js"></script>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=7" />
	<meta http-equiv="X-UA-Compatible" content="IE=9" />

	<title></title>

	<meta name="robots" content="noindex,nofollow" />

	{Asset::js('jquery-1.10.1.min.js')}
	{Asset::js('fp/ro.js')}
	{Asset::js('fp/gotop.js')}
	{Asset::css('fp/sub.css')}
	{Asset::css('../../common/css/news.css')}
	<script type="text/javascript">
	$(function(){
		var _UA = navigator.userAgent;
		if (_UA.indexOf('iPhone') > 0 || (_UA.indexOf('Android') > 0  && navigator.userAgent.indexOf('Mobile') == -1) || _UA.indexOf('iPod') > 0 || navigator.userAgent.indexOf('iPad') > 0) {
				$("link[rel=stylesheet]").each(function() { 
					var one = $(this).attr('href').replace('\/common\/css\/news','\/sp\/common\/css\/news');
					$(this).attr('href',one);
				});
		}
	});
	</script>
</head>
<body class="detail_page">
{if $topics_info ne '' && $topics_info|@count > 0}
	<dl class="title_area cf">
		<dt>
			{assign var=new_date value=$smarty.now-24*60*60*Digconst::NEW_DAY}
			<p>{$topics_info.topics_date|date_format:"%Y.%m.%d"}</p>
			<h3>{$topics_info.title}</h3>
		</dt>
		<dd><a href="../list.html" target="_parent"><img src="../common/images/news/btn_list_off.png" alt="一覧を見る" width="161" height="33"></a></dd>
	</dl>
	<div class="detail_main">
	{$body}
	</div>
{/if}
</body>
</html>
