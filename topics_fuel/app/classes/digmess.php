<?php
/**
 * メッセージ定数クラス
 *
 * @author s.sekiguchi
 *
 */
class Digmess {


	const I_LOGIN_LOGOUT    = 'ログアウトしました。';
	const I_LIST_DELETE     = 'トピックスを削除しました。';
	const I_REGIST_PREVIEW  = 'トピックスをプレビュー登録しました。<br>%s こちら %sよりアクセスしてご確認ください。';
	const I_CATEGORY_REGIST = 'カテゴリ情報の登録が完了しました。';
	const I_CATEGORY_EDIT   = 'カテゴリ情報の変更が完了しました。';
	const I_CATEGORY_DELETE = 'No%sのカテゴリ情報を削除しました。';


	const E_LOGIN_NOLOGIN  = 'ログインしていません。';
	const E_LOGIN_FAILED   = '認証に失敗しました';
	const E_LIST_NO_SELECT = 'トピックスが選択されていません。';
	const E_REGIST_FAILED  = '%sに失敗しました';
	const E_REGIST_PDF_DELETE = 'PDFファイル を削除できません。';

	const E_REGIST_POST_SIZE_OVER = 'ファイルが大きすぎる為、処理が出来ませんでした。';

	const E_REGIST_IMG_SIZE_OVER  = '%s ( %s ) のファイルサイズ(%sByte)が上限(%sByte)を超えてます。';
	const E_REGIST_IMG_NO_COVERED = '%s ( %s ) のファイル形式を確認してください。';
	const E_REGIST_IMG_FAILED     = '%s ( %s ) は不正なファイルです。[%s]';


	const ELOG_LOGIN_NOLOGIN         = 'No login.';
	const ELOG_LOGIN_NOSESSION       = 'Login check is failed. (%s : %s)';

	const ELOG_REGIST_IMG_SIZE_OVER  = 'File size is over the maximum. (%s : %s [%sbyte]) (max_size : %sbyte)';
	const ELOG_REGIST_IMG_NO_COVERED = 'File format is failed.  (%s : %s)';
	const ELOG_REGIST_IMG_FAILED     = 'File is failed. (%s : %s) (code : %s)';

	const ELOG_REGIST_POST_SIZE_OVER = 'POST size is over the maximum. (%s)';

	const ELOG_SYSTEM_ERROR         = 'System error.';

}