<?php
/**
 * ユーザー情報モデル
 *
 * @author s.sekiguchi
 *
 */
class Model_Admin_User extends Orm\Model {

	// テーブル名の定義
	protected static $_table_name = 'tbl_user';

	// フィールド名の定義
	protected static $_properties = array (
			'user_id',
			'user_name',
			'password',
			'role',
			'del_flg',
			'create_date',
			'update_date'
	);

	// 主キーの定義
	protected static $_primary_key = array (
			'user_id'
	);
	protected static $_observers = array (
			'Orm\\Observer_CreatedAt' => array (
					'events' => array (
							'before_insert'
					),
					'mysql_timestamp' => true,
					'property' => 'create_date'
			),
			'Orm\\Observer_UpdatedAt' => array (
					'events' => array (
							'before_insert',
							'before_save'
					),
					'mysql_timestamp' => true,
					'property' => 'update_date'
			)
	);
	protected static $_belongs_to = array (
			'topics' => array (
					'model_to' => 'Model_Admin_Topics',
					'key_from' => 'user_id',
					'key_to' => 'user_id',
					'cascade_save' => false,
					'cascade_delete' => false
			),
			'category' => array (
					'model_to' => 'Model_Admin_Category',
					'key_from' => 'user_id',
					'key_to' => 'user_id',
					'cascade_save' => false,
					'cascade_delete' => false
			)
	);
}
