<?php
/**
 * マニュアル情報モデル
 *
 * @author s.sekiguchi
 *
 */
class Model_Admin_Manual extends Orm\Model {

	// テーブル名の定義
	protected static $_table_name = 'tbl_manual';

	// フィールド名の定義
	protected static $_properties = array (
			'manual_id',
			'file_name',
			'version',
			'manual_date',
			'del_flg',
			'user_id',
			'create_date',
			'update_date'
	);

	// 主キーの定義
	protected static $_primary_key = array (
			'manual_id'
	);
	protected static $_observers = array (
			'Orm\\Observer_CreatedAt' => array (
					'events' => array (
							'before_insert'
					),
					'mysql_timestamp' => true,
					'property' => 'create_date'
			),
			'Orm\\Observer_UpdatedAt' => array (
					'events' => array (
							'before_insert',
							'before_save'
					),
					'mysql_timestamp' => true,
					'property' => 'update_date'
			)
	);
}
