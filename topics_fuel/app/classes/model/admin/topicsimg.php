<?php
/**
 * トピックス画像情報モデル
 *
 * @author s.sekiguchi
 *
 */
class Model_Admin_Topicsimg extends Orm\Model {

	// テーブル名の定義
	protected static $_table_name = 'tbl_topics_img';

	// フィールド名の定義
	protected static $_properties = array (
			'topics_img_id',
			'topics_id',
			'img_type',
			'img_no',
			'img_file',
			'img_size_h',
			'img_size_w',
			'del_flg',
			'user_id',
			'create_date',
			'update_date'
	);

	// 主キーの定義
	protected static $_primary_key = array (
			'topics_img_id'
	);
	protected static $_observers = array (
			'Orm\\Observer_CreatedAt' => array (
					'events' => array (
							'before_insert'
					),
					'mysql_timestamp' => true,
					'property' => 'create_date'
			),
			'Orm\\Observer_UpdatedAt' => array (
					'events' => array (
							'before_insert',
							'before_save'
					),
					'mysql_timestamp' => true,
					'property' => 'update_date'
			)
	);
	protected static $_belongs_to = array (
			'topics_img' => array (
					'model_to' => 'Model_Admin_Topics',
					'key_from' => 'topics_id',
					'key_to' => 'topics_id',
					'cascade_save' => true,
					'cascade_delete' => false
			)
	);
}
