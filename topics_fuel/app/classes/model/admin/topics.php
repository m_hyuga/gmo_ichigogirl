<?php
/**
 * トピックス情報モデル
 *
 * @author s.sekiguchi
 *
 */
class Model_Admin_Topics extends Orm\Model {

	// テーブル名の定義
	protected static $_table_name = 'tbl_topics';

	// フィールド名の定義
	protected static $_properties = array (
			'topics_id',
			'publish_flg',
			'title',
			'thumbnail_flg',
			'topics_date',
			'category_id',
			'type_flg',
			'pdf_file',
			'body',
			'prev_flg',
			'del_flg',
			'user_id',
			'create_date',
			'update_date'
	);
	protected static $_has_many = array (
			'topics_img' => array (
					'model_to' => 'Model_Admin_Topicsimg',
					'key_from' => 'topics_id',
					'key_to' => 'topics_id',
					'cascade_save' => true,
					'cascade_delete' => false
			)
	);
	protected static $_has_one = array (
			'user' => array (
					'model_to' => 'Model_Admin_User',
					'key_from' => 'user_id',
					'key_to' => 'user_id',
					'cascade_save' => false,
					'cascade_delete' => false
			),
			'category' => array (
					'model_to' => 'Model_Admin_Category',
					'key_from' => 'category_id',
					'key_to' => 'category_id',
					'cascade_save' => false,
					'cascade_delete' => false
			)
	);

	// 主キーの定義
	protected static $_primary_key = array (
			'topics_id'
	);
	protected static $_observers = array (
			'Orm\\Observer_CreatedAt' => array (
					'events' => array (
							'before_insert'
					),
					'mysql_timestamp' => true,
					'property' => 'create_date'
			),
			'Orm\\Observer_UpdatedAt' => array (
					'events' => array (
							'before_insert',
							'before_save'
					),
					'mysql_timestamp' => true,
					'property' => 'update_date'
			)
	);

	/**
	 * バリデーション
	 */
	public static function validate($val) {
		// カスタムバリデーション
		$val->add_callable ( 'digvalidation' );

		// バリデーションルールセット
		$val->add ( 'topics_date_d', '公開日時(日付)' )->add_rule ( 'required' );
		$val->add ( 'topics_date_h', '公開日時(時)' )->add_rule ( 'required' );
		$val->add ( 'topics_date_m', '公開日時(分)' )->add_rule ( 'required' );
		// $val->add('category_id', 'カテゴリ') ->add_rule('required');
		$val->add ( 'title', 'タイトル名' )->add_rule ( 'required' )->add_rule ( 'max_length', Digconst::TOPICS_TITLE_MAX_LENGHT );
// 		$val->add ( 'type_flg', '表示形式' )->add_rule ( 'required' );

		return $val;
	}

	/**
	 * バリデーション(アップロード必須)
	 */
	public static function validate_upload_required($val, $feild, $label) {

		// バリデーションルールセット
		$val->add ( $feild, $label )->add_rule ( 'upload_required' );

		return $val;
	}

	/**
	 * バリデーション(テキスト形式関連)
	 */
	public static function validate_text($val) {

		// バリデーションルールセット
		$val->add ( 'comment.body', '本文' )->add_rule ( 'required' )->add_rule ( 'max_length', Digconst::TOPICS_BODY_MAX_LENGHT );;
		for($i = 1; $i <= Digconst::TOPICS_IMG_MAX_NO; $i ++) {
			$val->add ( "img_size_w" . $i, "画像" . $i . "の幅" )->add_rule ( 'required' )->add_rule ( 'valid_string', array ('numeric') )->add_rule ( 'numeric_between',Digconst::TOPICS_IMG_MIN_SIZE_W, Digconst::TOPICS_IMG_MAX_SIZE_W);
		}
		return $val;
	}

	/**
	 * ファイルのアップロード
	 * 正常なファイルはテンポラリディレクトリに保存
	 *
	 * @var file_type ファイル形式(画像：1、PDF：2)
	 *
	 * @return ファイル情報(ファイル名、保存ファイルパス、エラーメッセージ)
	 */
	public static function file_upload($file_type = 1) {
		$file_info_array = array ();
		$config = '';
		$type = '';
		if ($file_type === 1) {
			$file_info_array = array ('thumbnail_file' => 'サムネイル画像');

			for($i=1;$i<=Digconst::TOPICS_IMG_MAX_NO;$i++) {
				$file_info_array['img_file'.$i] = '画像' . $i;

			}

			$config = array (
					'mime_whitelist' => array (
							'image/jpeg'
					),
					'path' => APPPATH . Digconst::PATH_TMP_IMG,
					'randomize' => true
			);
			$type = Digconst::FILE_TYPE_IMG;
		} else {
			$file_info_array = array (
					'pdf_file' => 'PDFファイル'
			);
			$config = array (
					'mime_whitelist' => array (
							'application/pdf'
					),
					'path' => APPPATH . Digconst::PATH_TMP_PDF,
					'randomize' => true
			);
			$type = Digconst::FILE_TYPE_PDF;
		}

		$uploadfile_array = array ();

		// ファイル検証
		Upload::process ( $config );
		// ファイル保存
		Upload::save ();

// 		$mat = new Model_Admin_Topics();

		// 正常なアップロードファイル
		$files = Upload::get_files ();
		foreach ( $files as $file ) {
			$field = $file ['field'];
			if (! array_key_exists ( $field, $file_info_array )) {
				// 対象外ファイルはスキップ
				continue;
			}
			$file_name = $file ['name'];
			$save_file_name = $file ['saved_as'];

// 			if ($file_type === 1) {
// 				/** 画像容量圧縮 **/
// 				// 作業用メモリサイズ変更
// 				$upload_file_path = APPPATH . Digconst::PATH_TMP_IMG.$file['saved_as'];
// 				$mat->_setMemoryForImage( $upload_file_path );
// 				$image=Image::forge(array('quality'=>70));
// 				$image->load($upload_file_path)->save($upload_file_path);
// 			}

			$uploadfile_array [$field] = array (
					'file_name' => $file_name,
					'save_file_name' => $save_file_name,
					'error_message' => '',
					'type' => $type
			);
		}

		// アップロードエラー
		foreach ( Upload::get_errors () as $error_file ) {
			$field = $error_file ['field'];
			$file_name = $error_file ['name'];
			if (! array_key_exists ( $field, $file_info_array )) {
				// 対象外ファイルはスキップ
				continue;
			}

			$error_code = '';
			foreach($error_file ['errors'] as $error) {
				$error_code = $error['error'];

			}
			if ($error_code === Upload::UPLOAD_ERR_NO_FILE) {
				// ファイルの未指定エラーはスキップ
				continue;
			}
			// エラーメッセージ
			$error_message = '';
			switch ($error_code) {
				case Upload::UPLOAD_ERR_MAX_SIZE :
					$file_size = $error_file ['size'];
					$file_max_size = Config::get('upload.max_size');
					$error_message = sprintf(Digmess::E_REGIST_IMG_SIZE_OVER, $file_info_array [$field], $file_name, $file_size, $file_max_size);
					$log_message = sprintf(Digmess::ELOG_REGIST_IMG_SIZE_OVER, $field, $file_name, $file_size, $file_max_size);
					Log::error('['.__METHOD__.'] '.$log_message);
					break;
				case Upload::UPLOAD_ERR_MIME_NOT_WHITELISTED :
					$error_message = sprintf(Digmess::E_REGIST_IMG_NO_COVERED, $file_info_array [$field], $file_name);
					$log_message = sprintf(Digmess::ELOG_REGIST_IMG_NO_COVERED, $field, $file_name);
					Log::error('['.__METHOD__.'] '.$log_message);
					break;
				default :
					$error_message = sprintf(Digmess::E_REGIST_IMG_FAILED, $file_info_array [$field], $file_name, $error_code);
					$log_message = sprintf(Digmess::ELOG_REGIST_IMG_FAILED, $field, $file_name, $error_code);
					Log::error('['.__METHOD__.'] '.$log_message);
					break;
			}

			$uploadfile_array [$field] = array (
					'file_name' => '',
					'save_file_name' => '',
					'error_message' => $error_message,
					'type' => ''
			);
		}

		return $uploadfile_array;
	}

	/**
	 * アップロードファイルの移動
	 *
	 * アップロードファイルを一時フォルダから公開ディレクトリへ移動する
	 *
	 * @param array $file_info_list
	 *        	ファイル情報リスト
	 * @param string $topics_id
	 *        	トピックスID
	 */
	public static function remove_file($file_info_list, $topics_id) {
		if (empty ( $file_info_list )) {
			return;
		}

		foreach ( $file_info_list as $key => $file_info ) {
			$ng_flg = false;
			$img_no = '';
			switch ($file_info ['type']) {
				case Digconst::FILE_TYPE_IMG :
					$from_path = APPPATH . Digconst::PATH_TMP_IMG;
					$to_path_base = DOCROOT . Digconst::PATH_TOPICS_IMG;

					if($key == 'thumbnail_file') {
						$img_no = '0';
					} else if(strpos($key, 'img_file') === 0) {
						// 本文画像は、フィールド名からimg_fileを除いた値
						$img_no = str_replace ('img_file', '', $key);
					}
					break;
				case Digconst::FILE_TYPE_PDF :
					$from_path = APPPATH . Digconst::PATH_TMP_PDF;
					$to_path_base = DOCROOT . Digconst::PATH_TOPICS_PDF;
					break;
				default :
					$ng_flg = true;
					break;
			}

			if ($ng_flg) {
				continue;
			}

			$to_path = $to_path_base . $topics_id . '/';
			if (! is_dir ( $to_path )) {
				// 移動先ディレクトリ作成
				File::create_dir ( $to_path_base, $topics_id, 0755 );
			}

			// 画像番号が空でない場合、移動先パスにサブディレクトリを追加する
			if(!empty($img_no) || $img_no === '0') {
				// 現在の移動先ディレクトをベースパスにする
				$to_path_base = $to_path;
				$to_path .=  $img_no . '/';
				if (! is_dir ( $to_path )) {
					// 移動先ディレクトリ作成
					File::create_dir ( $to_path_base, $img_no, 0755 );
				}
			}

			if (! file_exists ( $from_path . $file_info ['save_file_name'] )) {
				// 移動元ディレクトリにファイルが存在しない場合はスキップ
				continue;
			}

			$to_file_path = $to_path . $file_info ['file_name'];
			$to_file_path = mb_convert_encoding($to_file_path, Digconst::ENCODE_TYPE_LOCAL, "AUTO");

			if (file_exists ( $to_file_path )) {
				// 移動先ディレクトリにファイルが存在する場合は、ファイルを削除

				File::delete ( $to_file_path );
			}

			// ファイルコピー

			File::copy ( $from_path . $file_info ['save_file_name'], $to_file_path );
			File::delete ( $from_path . $file_info ['save_file_name'] );
		}
	}

	/**
	 * 画像ファイルにあわせて利用可能なメモリサイズを変える
	 */
	private function _setMemoryForImage( $filename ) {
		$imageInfo = getimagesize($filename);
		$MB = 1048576;  // number of bytes in 1M
		$K64 = 65536;    // number of bytes in 64K
		$TWEAKFACTOR = 1.5;  // Or whatever works for you
		$memoryNeeded = round( ( $imageInfo[0] * $imageInfo[1]
				* $imageInfo['bits']
				* $imageInfo['channels'] / 8
				+ $K64
		) * $TWEAKFACTOR
		);
		//ini_get('memory_limit') only works if compiled with "--enable-memory-limit" also
		//Default memory limit is 8MB so well stick with that.
		//To find out what yours is, view your php.ini file.
		$memoryLimitMB = (integer) ini_get('memory_limit');
		$memoryLimit = 8 * $MB;
		if (function_exists('memory_get_usage') &&
		memory_get_usage() + $memoryNeeded > $memoryLimit)
		{
			$newLimit = $memoryLimitMB + ceil( ( memory_get_usage()
					+ $memoryNeeded
					- $memoryLimit
			) / $MB
			);
			ini_set( 'memory_limit', $newLimit . 'M' );
			return true;
		}else
			return false;
	}
}
