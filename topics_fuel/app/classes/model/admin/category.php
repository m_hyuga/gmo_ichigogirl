<?php
/**
 * カテゴリ情報モデル
 *
 * @author s.sekiguchi
 *
 */
class Model_Admin_Category extends Orm\Model {

	// テーブル名の定義
	protected static $_table_name = 'tbl_category';

	// フィールド名の定義
	protected static $_properties = array (
			'category_id',
			'sort_id',
			'category_name',
			'class_id',
			'category_img',
			'del_flg',
			'user_id',
			'create_date',
			'update_date'
	);
	protected static $_has_one = array (
			'user' => array (
					'model_to' => 'Model_Admin_User',
					'key_from' => 'user_id',
					'key_to' => 'user_id',
					'cascade_save' => false,
					'cascade_delete' => false
			)
	);

	// 主キーの定義
	protected static $_primary_key = array (
			'category_id'
	);
	protected static $_observers = array (
			'Orm\\Observer_CreatedAt' => array (
					'events' => array (
							'before_insert'
					),
					'mysql_timestamp' => true,
					'property' => 'create_date'
			),
			'Orm\\Observer_UpdatedAt' => array (
					'events' => array (
							'before_insert',
							'before_save'
					),
					'mysql_timestamp' => true,
					'property' => 'update_date'
			)
	);

	/**
	 * バリデーション
	 */
	// 登録バリデーション
	public static function validate($val) {
		// バリデーションルールセット
		$val->add ( 'category_name', 'カテゴリ名' )->add_rule ( 'required' )->add_rule ( 'max_length', Digconst::CATEGORY_MAX_LENGHT );

		return $val;
	}

	// 変更バリデーション
	public static function validate_edit($val, $category_id_list, $del_category_list) {
		// カスタムバリデーション
		$val->add_callable ( 'digvalidation' );
		// ソードID重複チェック用リスト
		$sort_id_list = array ();

		foreach ( $category_id_list as $category_id ) {
			if (in_array ( $category_id, $del_category_list )) {
				// 削除対象のカテゴリはスキップ
				continue;
			}
			// バリデーションルールセット
			$val->add ( 'category_name' . $category_id, 'No' . $category_id . 'のカテゴリ名' )->add_rule ( 'required' )->add_rule ( 'max_length', Digconst::CATEGORY_MAX_LENGHT );

			$sort_id = Input::post ( 'sort_id' . $category_id );
			if (in_array ( $sort_id, $sort_id_list )) {
				$val->add ( 'sort_id' . $category_id, 'No' . $category_id . 'のソートID' )->add_rule ( 'duplication_error' );
			} else {
				array_push ( $sort_id_list, $sort_id );
			}
		}
		return $val;
	}
}
