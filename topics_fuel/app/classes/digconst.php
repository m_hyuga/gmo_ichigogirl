<?php
/**
 * 定数クラス
 *
 * @author s.sekiguchi
 *
 */
class Digconst {


	// 管理画面パス
	const ADMIN_PATH = 'admin/';


	/** URL **/
	const URL_NOLOGIN         = '/admin/login?nologin=1'; // 認証失敗
	const URL_TOPICS_LIST     = '/admin/list';            // トピックス一覧
	const URL_REGIST_REGIST   = '/admin/regist';          // トピックス登録(変更)完了
	const URL_REGIST_COMPRETE = '/admin/complete';        // トピックス登録(変更)完了
	const URL_ERROR = '/admin/error';           // システムエラー

	/** ファイルパス **/
	const PATH_PDF           = 'pdf/'; // 固定PDFディレクトリ
	const PATH_AGREEMENT_PDF = '/pdf/agreement/agreement_sample.pdf'; // 利用規約PDFディレクトリ
	const PATH_MANUAL_PDF    = '/pdf/manual/manual.pdf';       // マニュアルPDFディレクトリ

	const PATH_TMP_IMG = '/tmp/topics/img/';    // 画像ファイル一時保存
	const PATH_TMP_PDF = '/tmp/topics/pdf/';    // PDFファイル一時保存

	const PATH_TOPICS_IMG = 'assets/img/'; // トピックス画像ディレクトリ
	const PATH_TOPICS_PDF = 'pdf/';        // トピックスPDFディレクトリ


	/** 画面関連 **/

	// ビュー名
	const VIEW_MAIN  = 'admin/main';    // メイン
	const VIEW_LOGIN = 'admin/login';   // ログイン

	// テンプレート名
	const TPL_LIST      = 'list.tpl';      // トピックス一覧
	const TPL_REGIST    = 'regist.tpl';    // トピックス登録(変更)
	const TPL_COMPLETE  = 'complete.tpl';  // トピックス登録(変更)完了
	const TPL_CATEGORY  = 'category.tpl';  // カテゴリ管理
	const TPL_MANUAL    = 'manual.tpl';    // マニュアル
	const TPL_AGREEMENT = 'agreement.tpl'; // 利用規約
	const TPL_ERROR     = 'error.tpl';     // システムエラー

	// タイトル
	const TITLE_LOGIN     = 'ログイン';         // ログイン
	const TITLE_LIST      = 'トピックス一覧';   // トピックス一覧
	const TITLE_REGIST    = 'トピックス%s';     // トピックス登録(変更)
	const TITLE_COMPLETE  = 'トピックス%s完了'; // トピックス登録(変更)完了
	const TITLE_CATEGORY  = 'カテゴリ管理';     // カテゴリ管理
	const TITLE_AGREEMENT = '利用規約';         // 利用規約
	const TITLE_MANUAL    = 'マニュアル';       // マニュアル
	const TITLE_ERROR     = 'システムエラー';   // システムエラー


	/** パラメータ関連 **/

	// パラメータ名
	const PARAM_N_CATEGORY_ACT = 'act';    // カテゴリ操作種別

	// パラメータ値
	const PARAM_V_CATEGORY_ACT_R = 'regist';  // カテゴリ登録操作
	const PARAM_V_CATEGORY_ACT_E = 'edit';    // カテゴリ変更操作
	const PARAM_V_TOPICS_ACT_R  = '登録';     // トピックス登録操作
	const PARAM_V_TOPICS_ACT_E  = '変更';     // トピックス変更操作

	// フラッシュ変数名
	const FL_NAME_ACT_TYPE  = 'act_type';    // トピックス操作種別
	const FL_NAME_TOPICS_ID = 'topics_id';   // トピックスID
	const FL_NAME_TOPIC_DEL = 'del_message'; // トピックス削除メッセージ


	/** 設定値 **/

	// 画像数
	const TOPICS_IMG_MAX_NO = 12;  // 本文画像数

	// ページャー(管理サイト)
	const PAGINATION_URI_SEG  = 3; // URI中のページ番号を示すセグメント数
	const PAGINATION_NUM_LINK = 5; // ページ番号の表示数
	const PAGINATION_PER_PAGE = 10; // 1ページに表示するデータ件数

	// ページャー(ホームページ)
	const HP_PAGINATION_NUM_LINK = 3; // ページ番号の表示数
	const HP_PAGINATION_PER_PAGE = 5; // 1ページに表示するデータ件数

	// 日時フォーマット
	const DATETIME_FORMAT           = 'Y-m-d H:i:s';
	const DATETIME_DISP_FORMAT_DATE = 'Y年m月d日';
	const DATETIME_DISP_FORMAT_TIME = 'H時i分';
	const DATETIME_DISP_FORMAT_WEEK = '(%s) ';
	const DATETIME_DISP_STRING_DATE = '%04d年%02d月%02d日(%s)';
	const DATETIME_DISP_STRING_TIME = '%02d時%02d分';


	// 画像デフォルトサイズ
	const IMG_SIZE_TOPICS_W    = 500; // トピックス本文画像(幅)
	const IMG_SIZE_TOPICS_H    = 500; // トピックス本文画像(高さ)
	const IMG_SIZE_THUMBNAIL_W = 60;  // サムネイル画像(幅)
	const IMG_SIZE_THUMBNAIL_H = 60;  // サムネイル画像(高さ)

	const IMG_HSPACE_TOPICS = 10;    // トピックス本文画像の左右余白
	const IMG_VSPACE_TOPICS = 10;    // トピックス本文画像の上下余白

	const IMG_ALIGN_LEFT = "left";


	const IMG_CONVERT_TAG = '{img%d}';  // トピックス本文中の画像変換タグ

	// ファイルタイプ
	const FILE_TYPE_IMG = 'img'; // 画像
	const FILE_TYPE_PDF = 'pdf'; // PDF


	// プレビューリンクHTML
	const PREVIEW_LINK_HTML_S = '<a href="%s" target="_blank">'; // 開始タグ
	const PREVIEW_LINK_HTML_E = '</a>';                          // 終了タグ

	// 埋め込みPDFのパラメータ
	const PDF_ZOOM = '#zoom=75';  // デフォルトサイズ


	/** バリデーション値**/
	// トピックス
	const TOPICS_TITLE_MAX_LENGHT = 100;
	const TOPICS_BODY_MAX_LENGHT  = 5000;
	const TOPICS_IMG_MAX_SIZE_W   = 500; // トピックス本文画像(幅)の最大値
	const TOPICS_IMG_MIN_SIZE_W   = 10;  // トピックス本文画像(幅)の最小値

	// カテゴリ
	const CATEGORY_MAX_LENGHT  = 20;


	/** カテゴリ **/
	const CATEGORY_NAME_DEF    = '';    // デフォルトカテゴリ名
	const CATEGORY_ID_DEF      = '';            // デフォルトカテゴリID
	const CATEGORY_SORT_ID_DEF = 0;             // デフォルトカテゴリソートID

	public static $CATEGORY_DEFAULT_LIST = array('お知らせ', 'カテゴリ１');

	// エンコーディング
	const ENCODE_TYPE_LOCAL = 'UTF8';
	const ENCODE_TYPE_WEB   = 'UTF8';

	// リクエストサイズ上限(8MByte)
	const LIMIT_CONTENT_LENGTH = 8388608;

	// 新着期間(日)
	const NEW_DAY = 7;

	//  画像クラス
	const IMG_CLASS = 'topics_photo';
}