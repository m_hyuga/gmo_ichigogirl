<?php
/**
 * オリジナルバリデーションクラス
 *
 * @author s.sekiguchi
 */
class DigValidation {

	/**
	 * アップロードファイルチェック
	 */
	public static function _validation_upload_required() {

		// バリデーション対象のフィールドIDを取得
		$active_field = \Validation::active_field ();
		foreach ( Upload::get_errors () as $file ) {
			if ($file ['field'] == $active_field->name) {
				if ($file ['errors'] [0] ['error'] === Upload::UPLOAD_ERR_NO_FILE) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 重複チェック
	 *
	 * チェック処理は各モデル内で行い、エラーだった場合に呼び出す
	 */
	public static function _validation_duplication_error() {
		return false;
	}
}