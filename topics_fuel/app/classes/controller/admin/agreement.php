<?php
/**
 * 利用規約画面
 *
 * 利用規約を表示する画面
 *
 * @author s.sekiguchi
 *
 */
class Controller_Admin_Agreement extends Controller {
	public function action_index() {
		// 認証チェック
		$auth = Auth::instance ();
		$result = $auth->check ();
		if (! $result) {
			// 未ログイン時、ログイン画面へリダイレクト
			Response::redirect (Digconst::URL_NOLOGIN);
		}
		$user_name = $auth->get ( 'user_name' );

		// 利用規約情報取得
		$agreement_info = Model_Admin_Agreement::find ( 'last', array (
				'order_by' => 'version'
		) );

		// 利用規約ファイルパス作成
		\Asset::instance ()->add_type ( 'pdf', Digconst::PATH_PDF );
		$agreement_url = \Asset::instance ()->get_file ( Digconst::PATH_AGREEMENT_PDF . $agreement_info ['file_name'], 'pdf' );

		$view = View_Smarty::forge (Digconst::VIEW_MAIN);
		$view->set ( 'title', Digconst::TITLE_AGREEMENT );
		$view->set ( 'user_name', $user_name );
		$view->set ( 'content_tpl', Digconst::ADMIN_PATH . Digconst::TPL_AGREEMENT);
		$view->set_safe ( 'add_head_array', '' );

		$view->set ( 'agreement_info', $agreement_info );
		$view->set ( 'agreement_url', $agreement_url );

		return $view;
	}
}