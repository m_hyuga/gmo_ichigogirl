<?php
/**
 * カテゴリ管理画面
 *
 * カテゴリを管理する画面
 *
 * @author s.sekiguchi
 *
 */
class Controller_Admin_Category extends Controller {
	public function action_index() {
		// 認証チェック
		$auth = Auth::instance ();
		$result = $auth->check ();
		if (! $result) {
			// 未ログイン時、ログイン画面へリダイレクト
			Response::redirect (Digconst::URL_NOLOGIN);
		}
		$user_name = $auth->get ( 'user_name' );

		// エラーメッセージ初期化
		$error_messages = array ();

		// カテゴリ名
		$category_name = '';

		// POSTリクエスト
		if (Input::post ()) {
			$act = Input::post (Digconst::PARAM_N_CATEGORY_ACT);
			if ($act === Digconst::PARAM_V_CATEGORY_ACT_R) {

				/**
				 * カテゴリ登録
				 */
				// バリデーション
				$val = Validation::forge ();
				$val = Model_Admin_Category::validate ( $val );

				if ($val->run ()) {
					// カテゴリ名取得
					$category_name = Input::post ( 'category_name' );
					// 最新のソートIDを取得
					$max_sort_id = Model_Admin_Category::query ()->where ( 'del_flg', 0 )->max ( 'sort_id' );
					$sort_id = 1;
					if (! empty ( $max_sort_id )) {
						$sort_id = intval ( $max_sort_id ) + 1;
					}

					$data = array (
							'sort_id' => $sort_id,
							'category_name' => $category_name,
							'class_id' => '',
							'category_img' => '',
							'del_flg' => 0,
							'user_id' => \Session::get ( 'user_id' )
					);
					$category = Model_Admin_Category::forge ( $data );

					if ($category->save ()) {
						$category_name = "";
						$error_messages [] = Digmess::I_CATEGORY_REGIST;
					}
				} else {
					$error_messages = array_merge ( $error_messages, $val->error () );
				}
			} else if ($act === Digconst::PARAM_V_CATEGORY_ACT_E) {

				/**
				 * カテゴリ変更
				 */
				// 先に削除処理
				$del_category_list = Input::post ( 'del' );
				if ($del_category_list) {
					$data = array (
							'del_flg' => 1
					);

					foreach ( $del_category_list as $del_category_id ) {
						$del_category = Model_Admin_Category::find ( $del_category_id, array (
								'where' => array (
										array (
												'del_flg',
												0
										)
								)
						) );
						if ($del_category) {
							$del_category->set ( $data );
							$del_category->save ();
							$error_messages [] = sprintf(Digmess::I_CATEGORY_DELETE, $del_category_id);
						}
					}
				} else {
					$del_category_list = array ();
				}

				// カテゴリIDリスト取得
				$category_id_list = Input::post ( 'category_id' );
				if (empty ( $category_id_list )) {
					$category_id_list = array ();
				}
				// バリデーション
				$val = Validation::forge ();
				$val = Model_Admin_Category::validate_edit ( $val, $category_id_list, $del_category_list );

				if ($val->run ()) {
					$edit_flg = false;
					// 変更用カテゴリリスト
					$edit_category_list = array ();
					foreach ( $category_id_list as $category_id ) {
						if (in_array ( $category_id, $del_category_list )) {
							// 削除対象カテゴリはスキップ
							continue;
						}
						// カテゴリ名取得
						$edit_category_name = Input::post ( 'category_name' . $category_id );
						$edit_sort_id = Input::post ( 'sort_id' . $category_id );

						// ソートIDをキーにカテゴリ情報をセット
						$edit_category_list [$edit_sort_id] = array (
								'category_id' => $category_id,
								'category_name' => $edit_category_name
						);
					}

					// キー(ソートID)の昇順でソートしなおす
					ksort ( $edit_category_list );

					$new_sort_id = 1;
					foreach ( $edit_category_list as $edit_category ) {
						$data = array (
								'sort_id' => $new_sort_id,
								'category_name' => $edit_category ['category_name']
						);
						$category = Model_Admin_Category::find ( $edit_category ['category_id'], array (
								'where' => array (
										array (
												'del_flg',
												0
										)
								)
						) );
						if ($category) {
							$category->set ( $data );
							$category->save ();
						}
						$edit_flg = true;
						$new_sort_id ++;
					}
					if ($edit_flg) {
						$error_messages [] = Digmess::I_CATEGORY_EDIT;
					}
				} else {
					$error_messages = array_merge ( $error_messages, $val->error () );
				}
			}
		}

		// カテゴリ一覧取得
		$category_list = array ();
		// 表示カテゴリ取得
		$category_list = Model_Admin_Category::find ( 'all', array (
				'related' => array (
						'user'
				),
				'where' => array (
						array (
								'del_flg',
								0
						)
				),
				'order_by' => array (
						'sort_id'
				)
		) );

		// デフォルトカテゴリ取得
		$default_category_list = Digconst::$CATEGORY_DEFAULT_LIST;

		$sort_max_num = count ( $category_list ) + 1;

		$view = View_Smarty::forge (Digconst::VIEW_MAIN);
		$view->set ( 'title', Digconst::TITLE_CATEGORY);
		$view->set ( 'user_name', $user_name );
		$view->set ( 'content_tpl', Digconst::ADMIN_PATH . Digconst::TPL_CATEGORY);
		$view->set_safe ( 'add_head_array', '' );

		$view->set ( 'error_messages', $error_messages );
		$view->set ( 'category_name', $category_name );
		$view->set ( 'category_list', $category_list );
		$view->set ( 'default_category_list', $default_category_list );
		$view->set ( 'sort_max_num', $sort_max_num );

		return $view;
	}
}