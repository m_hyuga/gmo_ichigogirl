<?php
/**
 * トップページ
 *
 *
 * @author s.sekiguchi
 *
 */
class Controller_Admin_Index extends Controller {
	public function action_index() {
		Response::redirect ( 'admin/login' );
	}
}