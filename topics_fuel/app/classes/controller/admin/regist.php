<?php
/**
 * トピックス登録/更新画面
 *
 * トピックスを新規登録、又は更新する画面
 *
 * @author s.sekiguchi
 *
 */
class Controller_Admin_Regist extends Controller {
	public function action_index($topics_id = null) {

		// 認証チェック
		$auth = Auth::instance ();
		$result = $auth->check ();
		if (! $result) {
			// 未ログイン時、ログイン画面へリダイレクト
			Response::redirect (Digconst::URL_NOLOGIN);
		}
		$user_name = $auth->get ( 'user_name' );

		// 登録/変更
		$act_type = Digconst::PARAM_V_TOPICS_ACT_R;

		// エラーメッセージ初期化
		$error_messages = array ();

		$post_topics_id = Input::post ( 'topics_id' );
		if(!empty($post_topics_id)) {
			$topics_id = $post_topics_id;
		}

		// フラッシュ変数のトピックスIDを取得(プレビュー時のリダイレクト用)
		$sess_topics_id = Session::get_flash ( Digconst::FL_NAME_TOPICS_ID );
		if(!empty($sess_topics_id)){
			$topics_id = $sess_topics_id;
			$preview_link_s = sprintf(Digconst::PREVIEW_LINK_HTML_S, Uri::create('preview.html'));
			$preview_link_e = Digconst::PREVIEW_LINK_HTML_E;
			array_push ( $error_messages, sprintf(Digmess::I_REGIST_PREVIEW, $preview_link_s, $preview_link_e));
		}

		/**
		 * 項目の初期化
		 */
		if (! empty ( $topics_id )) {
			// 変更
			// トピックス情報取得
			$topics_list = Model_Admin_Topics::find ( $topics_id, array (
					'related' => array (
							'topics_img' => array (
									'join_on' => array (
											array (
													'del_flg',
													'=',
													DB::expr ( 0 )
											)
									)
							)
					),
					'where' => array (
							array (
									'del_flg',
									0
							)
					)
			) );
			// 登録情報をセット
			$publish_flg_checked = ($topics_list ['publish_flg'] === '0') ? 'checked' : '';
			$publish_flg = $topics_list ['publish_flg'];

			$topics_date_d = str_replace ( '-', '/', mb_substr ( $topics_list ['topics_date'], 0, 10 ) );
			$topics_date_h = mb_substr ( $topics_list ['topics_date'], 11, 2 );
			$topics_date_m = mb_substr ( $topics_list ['topics_date'], 14, 2 );

			$category_id = $topics_list ['category_id'];
			$title = $topics_list ['title'];
			$thumbnail_flg = $topics_list ['thumbnail_flg'];
			$thumbnail_flg_checked = ($topics_list ['thumbnail_flg'] === '1') ? 'checked' : '';

			$body = $topics_list ['body'];

			$pdf_file_name = $topics_list ['pdf_file'];
			// ファイルが登録されている場合、一時保存名に*****をセット
			$pdf_save_file_name = (! empty ( $topics_list ['pdf_file'] )) ? '*****' : '';
			$pdf_del_checked = '';

			$thumbnail_img_id = '';
			$thumbnail_file_name = '';
			$thumbnail_save_file_name = '';
			$img_array = array ();
			for($i = 1; $i <= Digconst::TOPICS_IMG_MAX_NO; $i ++) {
				$img_array [$i] = array (
						'topics_img_id' => '',
						'img_file_name' => '',
						'save_file_name' => '',
						'img_size_w' => Digconst::IMG_SIZE_TOPICS_W,
						'del_flg' => ''
				);
			}
			if (! empty ( $topics_list ['topics_img'] )) {
				foreach ( $topics_list ['topics_img'] as $topics_img ) {
					if ($topics_img ['img_type'] === '0') {
						$thumbnail_img_id = $topics_img ['topics_img_id'];
						$thumbnail_file_name = $topics_img ['img_file'];
						// ファイルが登録されている場合、一時保存名に*****をセット
						$thumbnail_save_file_name = '*****';
					} else {
						$img_array [$topics_img ['img_no']] = array (
								'topics_img_id' => $topics_img ['topics_img_id'],
								'img_file_name' => $topics_img ['img_file'],
								'save_file_name' => '*****',
								'img_size_w' => $topics_img ['img_size_w'],
								'del_flg' => ''
						);
					}
				}
			}

			// 操作タイプ
			$act_type = Digconst::PARAM_V_TOPICS_ACT_E;
		} else {
			// 新規登録

			$publish_flg_checked = 'checked';
			$publish_flg = '0';
			// 公開日時に現在日時をセット
			$topics_date_d = date ( "Y/m/d" );
			$topics_date_h = date ( "H" );
			$topics_date_m = date ( "i" );

			$category_id = '';
			$title = '';
			$thumbnail_img_id = '';
			$thumbnail_flg = '0';
			$thumbnail_flg_checked = '';
			$thumbnail_file_name = '';
			$thumbnail_save_file_name = '';
			$body = '';
			$img_array = array ();
			for($i = 1; $i <= Digconst::TOPICS_IMG_MAX_NO; $i ++) {
				$img_array [$i] = array (
						'topics_img_id' => '',
						'img_file_name' => '',
						'save_file_name' => '',
						'img_size_w' => Digconst::IMG_SIZE_TOPICS_W,
						'del_flg' => ''
				);
			}
			$pdf_file_name = '';
			$pdf_save_file_name = '';
			$pdf_del_checked = '';
		}

		/**
		 * カテゴリプルダウン作成
		 */
		// カテゴリ情報取得
		$category_info_list = Model_Admin_Category::find ( 'all', array (
				'where' => array (
						array (
								'del_flg',
								0
						)
				),
				'order_by' => array (
						'sort_id'
				)
		) );
		// カテゴリリスト
		$categorys = array (
				'' => '--カテゴリ選択--'
		);
		foreach ( $category_info_list as $category_info ) {
			$categorys [$category_info ['category_id']] = $category_info ['category_name'];
		}

		// リクエスト大容量対策
		$post_ok = true;
		if(isset($_SERVER['CONTENT_LENGTH']) && $_SERVER['CONTENT_LENGTH']  > Digconst::LIMIT_CONTENT_LENGTH) {
			$post_ok = false;
// 			$error_messages [] = Digmess::E_REGIST_POST_SIZE_OVER;
			$log_message = sprintf(Digmess::ELOG_REGIST_POST_SIZE_OVER, $_SERVER['CONTENT_LENGTH']);
			Log::error('['.__METHOD__.'] '.$log_message);

			// システムエラー画面へリダイレクト
			Response::redirect (Digconst::URL_ERROR);
		}

		/**
		 * 登録(変更)リクエスト
		 */
		if (Input::post () && $post_ok) {

			$prev_flg = 0;

			$preview_btn = Input::post ('preview_btn');
			if(!empty($preview_btn)) {
				$prev_flg = 1;
			}

			$upload_file_array = array ();

			// バリデーション
			$val = Validation::forge ();
			$val = Model_Admin_Topics::validate ( $val );

			// 画像ファイルアップロード
			$img_upload_file_array = Model_Admin_Topics::file_upload ( 1 );
			// PDFファイルアップロード
			$pdf_upload_file_array = Model_Admin_Topics::file_upload ( 2 );

			// 画像ファイルとPDFファイルをマージ
			$upload_file_array = array_merge ( $upload_file_array, $img_upload_file_array );
			$upload_file_array = array_merge ( $upload_file_array, $pdf_upload_file_array );
			foreach ( $upload_file_array as $upload_file ) {
				if (! empty ( $upload_file ['error_message'] )) {
					array_push ( $error_messages, $upload_file ['error_message'] );
				}
			}

			/**
			 * 入力データセット start>>
			 */

			$topics_id = Input::post ( 'topics_id' );

			// 公開
			$publish_flg_checked = (Input::post ( 'publish_flg' ) === "on") ? 'checked' : '';
			$publish_flg = (Input::post ( 'publish_flg' ) === "on") ? '0' : '1';

			// 公開日付
			$topics_date_d = Input::post ( 'topics_date_d' );
			$topics_date_h = Input::post ( 'topics_date_h' );
			$topics_date_m = Input::post ( 'topics_date_m' );

			// カテゴリ
			$category_id = Input::post ( 'category_id' );

			// タイトル
			$title = Input::post ( 'title' );

			// サムネイル画像
			$thumbnail_img_id = Input::post ( 'thumbnail_img_id' );
			$thumbnail_flg_checked = (Input::post ( 'thumbnail_flg' ) === "on") ? 'checked' : '';
			if (Input::post ( 'thumbnail_flg' ) === "on") {
				$thumbnail_flg = '1';
				if (isset ( $upload_file_array ['thumbnail_file'] )) {
					$thumbnail_file_name = $upload_file_array ['thumbnail_file'] ['file_name'];
					$thumbnail_save_file_name = $upload_file_array ['thumbnail_file'] ['save_file_name'];
				} else {
					// ファイル未指定の場合、前回指定ファイルがあればそれをセット
					$old_thumbnail_file = Input::post ( 'old_thumbnail_file' );
					if (! empty ( $old_thumbnail_file )) {
						$thumbnail_file_name = Input::post ( 'old_thumbnail_file' );
						$upload_file_array ['thumbnail_file'] ['file_name'] = $thumbnail_file_name;
						$upload_file_array ['thumbnail_file'] ['type'] = 'img';
					}
					$old_thumbnail_save_file = Input::post ( 'old_thumbnail_save_file' );
					if (! empty ( $old_thumbnail_save_file )) {
						$thumbnail_save_file_name = Input::post ( 'old_thumbnail_save_file' );
						$upload_file_array ['thumbnail_file'] ['save_file_name'] = $thumbnail_save_file_name;
					}

					if (empty ( $thumbnail_file_name )) {
						// アップロード必須バリデーション
						$val = Model_Admin_Topics::validate_upload_required ( $val, 'thumbnail_file', 'サムネイル画像' );
					}
				}
			} else {
				$thumbnail_flg = '0';
			}

			$pdf_del = Input::post ( 'pdf_del' );
			$pdf_del_checked = (empty ( $pdf_del )) ? '' : 'checked';
			$pdf_del_flg = (! empty ( $pdf_del )) ? '1' : '';

			/** 本文 **/
			// 画像
			for($i = 1; $i <= Digconst::TOPICS_IMG_MAX_NO; $i ++) {
				$topics_img_id = Input::post ( 'topics_img_id' . $i );
				$img_file_name = '';
				$save_file_name = '';
				$img_fild = 'img_file' . $i;
				$img_size_w = Input::post ( 'img_size_w' . $i );
				$img_del = Input::post ( 'img_del' . $i );
				$del_flg = (! empty ( $img_del )) ? '1' : '';
				if (isset ( $upload_file_array [$img_fild] )) {
					$img_file_name = $upload_file_array [$img_fild] ['file_name'];
					$save_file_name = $upload_file_array [$img_fild] ['save_file_name'];
				} else {
					// ファイル未指定の場合、前回指定ファイルがあればそれをセット
					$old_img_file = Input::post ( 'old_img_file' . $i );
					if (! empty ( $old_img_file )) {
						$img_file_name = Input::post ( 'old_img_file' . $i );
						$upload_file_array [$img_fild] ['file_name'] = $img_file_name;
						$upload_file_array [$img_fild] ['type'] = 'img';
					}
					$old_img_save_file = Input::post ( 'old_img_save_file' . $i );
					if (! empty ( $old_img_save_file )) {
						$save_file_name = Input::post ( 'old_img_save_file' . $i );
						$upload_file_array [$img_fild] ['save_file_name'] = $save_file_name;
					}

// 					if (! empty ( $img_size_w ) && empty ( $img_file_name )) {
// 						// アップロード必須バリデーション
// 						$val = Model_Admin_Topics::validate_upload_required ( $val, 'img_file' . $i, '画像' . $i );
// 					}
				}

				$img_array [$i] = array (
						'topics_img_id' => $topics_img_id,
						'img_file_name' => $img_file_name,
						'save_file_name' => $save_file_name,
						'img_size_w' => $img_size_w,
						'del_flg' => $del_flg
				);
			}

			// 内容
			$comment = Input::post ( 'comment' );
			$body = $comment['body'];

			$val = Model_Admin_Topics::validate_text ( $val );

			// PDFファイル
			if (isset ( $upload_file_array ['pdf_file'] )) {
				$pdf_file_name = $upload_file_array ['pdf_file'] ['file_name'];
				$pdf_save_file_name = $upload_file_array ['pdf_file'] ['save_file_name'];
			} else {
				// ファイル未指定の場合、前回指定ファイルがあればそれをセット
				$old_pdf_file = Input::post ( 'old_pdf_file' );
				if (! empty ( $old_pdf_file )) {
					$pdf_file_name = Input::post ( 'old_pdf_file' );
					$upload_file_array ['pdf_file'] ['file_name'] = $pdf_file_name;
					$upload_file_array ['pdf_file'] ['type'] = 'pdf';
				}
				$old_pdf_save_file = Input::post ( 'old_pdf_save_file' );
				if (! empty ( $old_pdf_save_file )) {
					$pdf_save_file_name = Input::post ( 'old_pdf_save_file' );
					$upload_file_array ['pdf_file'] ['save_file_name'] = $pdf_save_file_name;
				}

// 				if (empty ( $pdf_file_name )) {
// 					// アップロード必須バリデーション
// 					$val = Model_Admin_Topics::validate_upload_required ( $val, 'pdf_file', 'PDFファイル' );
// 				}
// 				if (! empty ( $pdf_del )) {
// 					$error_messages [] = Digmess::E_REGIST_PDF_DELETE;
// 				}
			}
			/**
			 * 入力データセット <<end
			 */

			if ($val->run ()) {
				if (empty ( $error_messages )) {
					$data = array (
							'publish_flg' => $publish_flg,
							'title' => $title,
							'thumbnail_flg' => $thumbnail_flg,
							'topics_date' => sprintf ( '%s %s:%s', $topics_date_d, $topics_date_h, $topics_date_m ),
							'category_id' => $category_id,
							'type_flg' => 1,
							'pdf_file' => (empty ( $pdf_del_flg )) ? $pdf_file_name : '',
							'body' => $body,
							'prev_flg' => $prev_flg,
							'del_flg' => 0,
							'user_id' => \Session::get ( 'user_id' )
					);
					if (empty ( $topics_id )) {
						$topics = Model_Admin_Topics::forge ( $data );
					} else {
						$topics = Model_Admin_Topics::find ( $topics_id, array (
								'where' => array (
										array (
												'del_flg',
												0
										)
								)
						) );
					}
					$topics->set ( $data );

					// サムネイル画像ファイル
					if (! empty ( $thumbnail_file_name ) || ! empty ( $thumbnail_img_id )) {
						$del_flg = 0;
						if ($thumbnail_flg === '0') {
							$del_flg = 1;
						}

						$data = array (
								'img_type' => 0,
								'img_no' => 0,
								'img_file' => $thumbnail_file_name,
								'img_size_h' => '',
								'img_size_w' => '',
								'del_flg' => $del_flg,
								'user_id' => \Session::get ( 'user_id' )
						);
						if (empty ( $thumbnail_img_id )) {
							$thumbnail_img = Model_Admin_Topicsimg::forge ( $data );
						} else {
							$thumbnail_img = Model_Admin_Topicsimg::find ( $thumbnail_img_id, array (
									'where' => array (
											array (
													'del_flg',
													0
											)
									)
							) );
						}
						$thumbnail_img->set ( $data );
						$topics->topics_img [] = $thumbnail_img;
					}

					// 画像ファイル
					foreach ( $img_array as $no => $img ) {
						if (empty ( $img ['img_file_name'] ) && empty ( $img ['topics_img_id'] )) {
							continue;
						}
						$del_flg = (! empty ( $img ['del_flg'] )) ? 1 : 0;
						$data = array (
								'img_type' => 1,
								'img_no' => $no,
								'img_file' => $img ['img_file_name'],
								'img_size_h' => '',
								'img_size_w' => $img ['img_size_w'],
								'del_flg' => $del_flg,
								'user_id' => \Session::get ( 'user_id' )
						);
						if (empty ( $img ['topics_img_id'] )) {
							$topics_img = Model_Admin_Topicsimg::forge ( $data );
						} else {
							$topics_img = Model_Admin_Topicsimg::find ( $img ['topics_img_id'], array (
									'where' => array (
											array (
													'del_flg',
													0
											)
									)
							) );
						}
						$topics_img->set ( $data );
						$topics->topics_img [] = $topics_img;
					}

					/**
					 * 更新完了
					 */
					if ($topics->save ()) {
						// アップロードファイルの移動
						Model_Admin_Topics::remove_file ( $upload_file_array, $topics->topics_id );

						if($prev_flg === 0) {
							// 登録時、画面遷移
							Session::set_flash (Digconst::FL_NAME_ACT_TYPE, $act_type );
							Response::redirect ( Digconst::URL_REGIST_COMPRETE );
						}
						// プレビュー時、画面遷移
						Session::set_flash (Digconst::FL_NAME_TOPICS_ID, $topics->topics_id );
						Response::redirect ( Digconst::URL_REGIST_REGIST );
					} else {
						array_push ( $error_messages, sprintf(Digmess::E_REGIST_FAILED, $act_type));
					}
				}
			} else {
				$error_messages = array_merge ( $error_messages, $val->error () );
			}
		}

		$add_head_array = array ();
		$add_head_array [] = Asset::css ( 'jquery-ui.css' );
		$add_head_array [] = Asset::js ( 'jquery-1.11.0.js' );
		$add_head_array [] = Asset::js ( 'jquery-ui.min.js' );
		$add_head_array [] = Asset::js ( 'jquery.ui.datepicker-ja.min.js' );
		$add_head_array [] = Asset::js ( 'jquery.autosize.js' );
		$add_head_array [] = Asset::js ( 'load02.js' );
		$add_head_array [] = Asset::js ( 'regist.js' );

		// ビューセット
		$view = View_Smarty::forge ( Digconst::VIEW_MAIN  );
		$view->set ( 'title', sprintf(Digconst::TITLE_REGIST, $act_type) );
		$view->set ( 'user_name', $user_name );
		$view->set ( 'content_tpl', Digconst::ADMIN_PATH . Digconst::TPL_REGIST);
		$view->set_safe ( 'add_head_array', $add_head_array );

		$view->set ( 'topics_id', $topics_id);
		$view->set ( 'publish_flg_checked', $publish_flg_checked );
		$view->set ( 'topics_date_d', $topics_date_d );
		$view->set ( 'topics_date_h', $topics_date_h );
		$view->set ( 'topics_date_m', $topics_date_m );
		$view->set ( 'categorys', $categorys );
		$view->set ( 'category_id', $category_id );
		$view->set ( 'topics_title', $title );
		$view->set ( 'thumbnail_img_id', $thumbnail_img_id );
		$view->set ( 'thumbnail_flg_checked', $thumbnail_flg_checked );
		$view->set ( 'thumbnail_file_name', $thumbnail_file_name );
		$view->set ( 'thumbnail_save_file_name', $thumbnail_save_file_name );
		$view->set ( 'body', $body );
		$view->set ( 'img_array', $img_array );
		$view->set ( 'pdf_file_name', $pdf_file_name );
		$view->set ( 'pdf_save_file_name', $pdf_save_file_name );
		$view->set ( 'pdf_del_checked', $pdf_del_checked );

		$view->set ( 'act_type', $act_type );
		$view->set_safe ( 'error_messages', $error_messages );

		return $view;
	}
}