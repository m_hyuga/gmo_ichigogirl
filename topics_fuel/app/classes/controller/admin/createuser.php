<?php
/**
 * ユーザー登録
 *
 * ※システム外画面
 *
 * @author s.sekiguchi
 *
 */
class Controller_Admin_Createuser extends Controller {
	public function action_index() {
		$messages = '';
		$user_id = '';
		$user_name = '';

		$view = View_Smarty::forge ( 'admin/createuser' );

		if (Input::post ()) {
			// バリデーション
			$val = Validation::forge ();


			$admin_id  = Input::post ( 'admin_id' );
			$user_id   = Input::post ( 'user_id' );
			$user_name = Input::post ( 'user_name' );

			if ($admin_id) {
				// メッセージセット
				$val->set_message ( 'required', ':label が未入力です。' );
				$val->set_message ( 'match_value', '認証IDが違います。' );

				$val->add ( 'admin_id', '認証ID' )->add_rule ( 'required' )->add_rule ( 'match_value', 'kwo30W53', true );
				$val->add ( 'user_id', 'ユーザーID' )->add_rule ( 'required' )->add_rule ( 'valid_string', array ('alpha','numeric','dashes') );
				$val->add ( 'password', 'パスワード' )->add_rule ( 'required' )->add_rule ( 'valid_string', array ('alpha','numeric') );
				$val->add ( 'user_name', 'ユーザー名' )->add_rule ( 'required' );

				if ($val->run ()) {
					$password = Input::post ( 'password' );

					// Authのインスタンス化
					$auth = Auth::instance ();

					// ユーザー登録
					$result = $auth->create_user ( $user_id, $password, $user_name );

					if ($result === 3) {
						$messages [] = '同じユーザーID又はパスワードが登録済みです。';
					} else {
						$messages [] = '登録完了しました。';
						$user_id = '';
						$user_name = '';
					}
				} else {
					$messages = $val->error ();
				}
			} else {
				$messages [] = '認証IDが未入力です。';
			}
		}

		// エラー表示
		$view->set ( 'messages', $messages );
		$view->set ( 'user_id', $user_id );
		$view->set ( 'user_name', $user_name );

		return $view;
	}
}
