<?php
/**
 * ログイン画面
 *
 * ユーザー認証を行いシステムにログインする画面
 *
 * @author s.sekiguchi
 *
 */
class Controller_Admin_Login extends Controller {
	public function action_index() {
		// ログアウトメッセージ初期化
		$logout_messag = '';
		// エラーメッセージ初期化
		$error_messages = '';

		// ログイン情報破棄
		Auth::logout ();

		// ログイン画面ビュー設定
		$view = View_Smarty::forge (Digconst::VIEW_LOGIN);

		$auth = Auth::instance ();

		// ログアウト時
		if (Input::get ()) {
			if (Input::get ( 'logout' )) {
				$logout_messag = Digmess::I_LOGIN_LOGOUT;
			} else if (Input::get ( 'nologin' )) {
				$logout_messag = Digmess::E_LOGIN_NOLOGIN;
			}
		}

		// 認証時
		if (Input::post ()) {
			// バリデーション
			$val = Validation::forge ();
			// バリデーションルールセット
			$val->add ( 'password', 'パスワード' )->add_rule ( 'required' );

			if ($val->run ()) {
				if ($auth->login ( Input::post ( 'password' ) )) {
					// ログイン成功時
					Response::redirect (Digconst::URL_TOPICS_LIST);
				}
				$error_messages = Digmess::E_LOGIN_FAILED;
			} else {
				$error_messages = $val->error ();
			}
		}

		// エラー表示
		$view->set ( 'logout_messag', $logout_messag );
		$view->set ( 'error_messages', $error_messages );

		return $view;
	}
}