<?php
/**
 * トピックス登録/変更完了画面
 *
 * トピックスの登録/変更が完了した旨を表示する画面
 *
 * @author s.sekiguchi
 *
 */
class Controller_Admin_Complete extends Controller {
	public function action_index() {

		// 認証チェック
		$auth = Auth::instance ();
		$result = $auth->check ();
		if (! $result) {
			// 未ログイン時、ログイン画面へリダイレクト
			Response::redirect (Digconst::URL_NOLOGIN);
		}
		$user_name = $auth->get ( 'user_name' );

		// アクションタイプ(登録or変更)を取得
		$act_type = Session::get_flash ( Digconst::FL_NAME_ACT_TYPE );

		if (empty ( $act_type )) {
			// アクションタイプがない場合はトピックス一覧画面へリダイレクト
			Response::redirect ( Digconst::URL_TOPICS_LIST );
		}

		$view = View_Smarty::forge (Digconst::VIEW_MAIN);
		$view->set ( 'title', sprintf(Digconst::TITLE_COMPLETE, $act_type));
		$view->set ( 'user_name', $user_name );
		$view->set ( 'content_tpl', Digconst::ADMIN_PATH . Digconst::TPL_COMPLETE);
		$view->set_safe ( 'add_head_array', '' );

		$view->set ( 'act_type', $act_type );

		return $view;
	}
}