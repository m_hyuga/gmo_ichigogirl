<?php
/**
 * マニュアル画面
 *
 * マニュアルを表示する画面
 *
 * @author s.sekiguchi
 *
 */
class Controller_Admin_Manual extends Controller {
	public function action_index() {
		// 認証チェック
		$auth = Auth::instance ();
		$result = $auth->check ();
		if (! $result) {
			// 未ログイン時、ログイン画面へリダイレクト
			Response::redirect (Digconst::URL_NOLOGIN);
		}
		$user_name = $auth->get ( 'user_name' );

		// マニュアル情報取得
		$manual_info = Model_Admin_Manual::find ( 'last', array (
				'order_by' => 'version'
		) );

		// マニュアルファイルパス作成
		\Asset::instance ()->add_type ( 'pdf', Digconst::PATH_PDF );
		$manual_url = \Asset::instance ()->get_file ( Digconst::PATH_MANUAL_PDF . $manual_info ['file_name'], 'pdf' );

		$view = View_Smarty::forge ( Digconst::VIEW_MAIN  );
		$view->set ( 'title', Digconst::TITLE_MANUAL );
		$view->set ( 'user_name', $user_name );
		$view->set ( 'content_tpl', Digconst::ADMIN_PATH . Digconst::TPL_MANUAL);
		$view->set_safe ( 'add_head_array', '' );

		$view->set ( 'manual_info', $manual_info );
		$view->set ( 'manual_url', $manual_url );

		return $view;
	}
}