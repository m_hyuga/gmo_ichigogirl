<?php
/**
 * トピックス一覧画面
 *
 * 登録済みトピックスを一覧表示する画面
 *
 * @author s.sekiguchi
 *
 */
class Controller_Admin_List extends Controller {
	public function action_index() {
		// 認証チェック
		$auth = Auth::instance ();
		$result = $auth->check ();
		if (! $result) {
			// 未ログイン時、ログイン画面へリダイレクト
			Response::redirect (Digconst::URL_NOLOGIN);
		}
		$user_name = $auth->get ( 'user_name' );

		// エラーメッセージ初期化
		$error_messages = '';

		if (Session::get_flash (  )) {
			$error_messages = Session::get_flash (Digconst::FL_NAME_TOPIC_DEL);
		}

		if (Input::post ( 'act' )) {
			if (! Input::post ( 'del' )) {
				$error_messages = Digmess::E_LIST_NO_SELECT;
			} else {
				$del_topics_array = Input::post ( 'del' );
				$data = array (
						'del_flg' => 1
				);

				foreach ( $del_topics_array as $topics_id ) {
					$topics = Model_Admin_Topics::find ( $topics_id, array (
							'where' => array (
									array (
											'del_flg',
											0
									)
							)
					) );
					if ($topics) {
						$topics->set ( $data );
						$topics->save ();
					}
				}
				// 画面遷移
				Session::set_flash ( Digconst::FL_NAME_TOPIC_DEL, Digmess::I_LIST_DELETE );
				// 二重登録対策
				Response::redirect ( Digconst::URL_TOPICS_LIST );
			}
		}

		// 現在日時
		$now = time ();
		// 比較用
		$now_datetime = date ( Digconst::DATETIME_FORMAT, $now);
		// 表示用
		$disp_datetime_date = date ( Digconst::DATETIME_DISP_FORMAT_DATE, $now);
		$disp_datetime_time = date ( Digconst::DATETIME_DISP_FORMAT_TIME, $now);
		$week = array("日", "月", "火", "水", "木", "金", "土");
		$dispdatetime_week = sprintf(Digconst::DATETIME_DISP_FORMAT_WEEK, $week[date("w", $now)]);
		$disp_datetime = $disp_datetime_date . $dispdatetime_week . $disp_datetime_time;

		// 全件数取得
		$cnt ['all'] = Model_Admin_Topics::count ( array (
				'where' => array (
						array (
								'del_flg',
								0
						)
				)
		) );
		// 公開件数取得
		$cnt ['publish'] = Model_Admin_Topics::count ( array (
				'where' => array (
						array (
								'publish_flg',
								0
						),
						array (
								'topics_date',
								'<=',
								$now_datetime
						),
						array (
								'del_flg',
								0
						),
						array (
								'prev_flg',
								0
						),
				)
		) );
		// 公開待ち件数取得
		$cnt ['publish_wait'] = Model_Admin_Topics::count ( array (
				'where' => array (
						array (
								'publish_flg',
								0
						),
						array (
								'topics_date',
								'>',
								$now_datetime
						),
						array (
								'del_flg',
								0
						),
						array (
								'prev_flg',
								0
						),
				)
		) );
		// 未公開件数取得
		$cnt ['non_publish'] = Model_Admin_Topics::count ( array (
				'where' => array (
						array (
								'publish_flg',
								1
						),
						array (
								'del_flg',
								0
						),
						array (
								'prev_flg',
								0
						),
				)
		) );
		// プレビュー登録件数取得
		$cnt ['preview'] = Model_Admin_Topics::count ( array (
				'where' => array (
						array (
								'del_flg',
								0
						),
						array (
								'prev_flg',
								1
						),
				)
		) );

		// ページング設定
		$config = array (
				'pagination_url' => Uri::create('admin/list'),
				'uri_segment' => Digconst::PAGINATION_URI_SEG,
				'num_links' => Digconst::PAGINATION_NUM_LINK,
				'per_page' => Digconst::PAGINATION_PER_PAGE,
				'total_items' => $cnt ['all'],
		);
		$pagination = Pagination::forge ( 'pagination', $config );

		// 表示トピックス取得
		$topics_list = Model_Admin_Topics::find ( 'all', array (
				'related' => array (
						'user' => array (
									'join_on' => array (
											array (
													'del_flg',
													'=',
													DB::expr ( 0 )
											)
									)
							) ,
						'category' => array (
									'join_on' => array (
											array (
													'del_flg',
													'=',
													DB::expr ( 0 )
											)
									)
							)
				),
				'where' => array (
						array (
								'del_flg',
								0
						)
				),
				'limit' => $pagination->per_page,
				'offset' => $pagination->offset,
				'order_by' => array (
						'topics_date' => 'DESC',
						'topics_id' => 'DESC'
				)
		) );

		$topics_list = $this -> convert_topics_datetime($topics_list);

		// ヘッダーに追加する項目
		$add_head_array [] = Asset::js ( 'jquery-1.11.0.js' );
		$add_head_array [] = <<<EOF
		<script type='text/javascript'>
		//<![CDATA[
		$(window).load(function(){
			if($(this).attr('checked')) {
				$('#delete_btn').attr('disabled',false);
				$('#delete_btn').css('cursor','hand');
				;
			} else {
				$('#delete_btn').attr('disabled',true);
				$('#delete_btn').css('cursor','default');
			}
		});
		$(window).load(function(){
			$('#del_confirm').change(function(){
				if($(this).is(':checked')) {
					$('#delete_btn').attr('disabled',false);
					$('#delete_btn').css('cursor','hand');
				} else {
					$('#delete_btn').attr('disabled',true);
					$('#delete_btn').css('cursor','default');
				}
			});
		});
		//]]>
		</script>
EOF;
		$view = View_Smarty::forge ( Digconst::VIEW_MAIN );
		$view->set ( 'title', Digconst::TITLE_LIST );
		$view->set ( 'user_name', $user_name );
		$view->set ( 'content_tpl', Digconst::ADMIN_PATH . Digconst::TPL_LIST);
		$view->set_safe ( 'add_head_array', $add_head_array );

		$view->set ( 'error_messages', $error_messages );
		$view->set ( 'cnt', $cnt );
		$view->set ( 'topics_list', $topics_list );
		$view->set ( 'now_datetime', $now_datetime );
		$view->set ( 'disp_datetime', $disp_datetime );
		$view->set ( 'pager_previous', $pagination->previous (), false );
		$view->set ( 'total_pages', $pagination->total_pages );
		$view->set ( 'current_page', $pagination->current_page );
		$view->set ( 'pager_next', $pagination->next (), false );

		return $view;
	}

	/**
	 * 日時を表示用に変換
	 *
	 * @param unknown $topics_list
	 * @return unknown
	 */
	private function convert_topics_datetime($topics_list) {

		$weekday = array( "日", "月", "火", "水", "木", "金", "土" );
		foreach($topics_list as $key => $topics) {
			$topics_date = $topics['topics_date'];

			if(preg_match("/^([0-9]+)-([0-9]+)-([0-9]+)\s+([0-9]+):([0-9]+):([0-9]+)$/",$topics_date,$match)){
				$year  = $match[1];
				$month = $match[2];
				$day   = $match[3];
				$hour  = $match[4];
				$min   = $match[5];
				$week  = $weekday[date('w', strtotime($year.$month.$day))];
			}
			$topics_list[$key]['topics_disp_date'] = sprintf(Digconst::DATETIME_DISP_STRING_DATE, $year, $month, $day, $week);
			$topics_list[$key]['topics_disp_time'] = sprintf(Digconst::DATETIME_DISP_STRING_TIME, $hour, $min);
		}

		return $topics_list;
	}
}