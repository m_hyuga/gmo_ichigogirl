<?php
/**
 * システムエラー画面
 *
 * 想定外のエラーが発生した場合に表示する画面
 * ログイン情報を破棄してから画面を表示する
 *
 * @author s.sekiguchi
 *
 */
class Controller_Admin_Error extends Controller {
	public function action_index() {
// 		Log::error('['.__METHOD__.'] '.Digmess::ELOG_SYSTEM_ERROR);
		// 認証チェック
		$auth = Auth::instance ();
		$result = $auth->check ();
		if (! $result) {
			// ログイン情報破棄
			// 未ログイン時、ログイン画面へリダイレクト
			Response::redirect (Digconst::URL_NOLOGIN);
		}

		$user_name = $auth->get ( 'user_name' );

		// ログイン画面ビュー設定
		$view = View_Smarty::forge (Digconst::VIEW_MAIN);
		$view->set ( 'title', Digconst::TITLE_ERROR);
		$view->set ( 'user_name', $user_name );
		$view->set ( 'content_tpl',  Digconst::ADMIN_PATH . Digconst::TPL_ERROR);
		$view->set_safe ( 'add_head_array', '' );

		return $view;
	}
}