<?php
/**
 * 【フロントサイト】トピックス詳細
 *
 *
 * @author s.sekiguchi
 *
 */
class Controller_Detail extends Controller {
	public function action_index($topics_id = null, $prev = null) {
		if (empty ( $topics_id ) && $topics_id !== '0') {
			// トピックスIDが無ければトップページへリダイレクト
			Response::redirect ( '/index.html' );
		}

		// 現在日時取得
		$now_datetime = date ( "Y-m-d H:i:s", time () );

		// トピックス情報取得
		// トピックス情報は、公開のトピックスのみ
		// トピックス画像情報は本文画像のみ
		$topics_info_query = Model_Admin_Topics::query ()->related ( array (
				'topics_img' => array (
						'join_on' => array (
								array (
										'del_flg',
										'=',
										DB::expr ( 0 )
								),
// 								array (
// 										'img_type',
// 										'=',
// 										DB::expr ( 1 )
// 								)
						)
				)
		) )->where ( 'del_flg', 0 )->where ( 'topics_id', $topics_id );

		$topics_info_query->where_open()->where ( 'prev_flg', 0 )->where ( 'publish_flg', 0 )->where ( 'topics_date', '<=', $now_datetime )->where_close();
		if(!empty($prev)) {
			$topics_info_query->or_where('prev_flg', 1 );

		}

		$topics_info_list = $topics_info_query->get ();

		$topics_info = '';
		foreach($topics_info_list as $get_topics_info) {
			$topics_info = $get_topics_info;
			break;
		}

		$body = '';
		if (! empty ( $topics_info )) {

			// PDF形式はリダイレクト
			if ($topics_info ['type_flg'] == '2') {
				$view = View_Smarty::forge ( 'detail_pdf' );

				$pdf_file = $topics_info ['pdf_file'];
				$pdf_file = urlencode($pdf_file);

				$pdf_url = sprintf ( '/pdf/%s/%s', $topics_info ['topics_id'], $pdf_file );
				Response::redirect(Uri::create($pdf_url), 'location', 200);
			}

			// 本文を変換する
			$body = $this->_convert_body ( $topics_info );
		}

		$thumbnail_img = "";
		// サムネイル画像作成
		if (isset($topics_info ['thumbnail_flg']) && $topics_info ['thumbnail_flg'] === '1') {
			foreach ( $topics_info ['topics_img'] as $topics_img ) {
				if ($topics_img ['img_type'] === '0') {
					$file_name = mb_convert_encoding($topics_img ['img_file'], Digconst::ENCODE_TYPE_LOCAL, "AUTO");

					$img_tag = Asset::img ( $topics_info ['topics_id'] . '/0/' . $file_name, array (
							'width' => Digconst::IMG_SIZE_THUMBNAIL_W
					) );
					mb_language("Japanese");
					$thumbnail_img = mb_convert_encoding($img_tag, Digconst::ENCODE_TYPE_WEB, "AUTO");
				}
			}
		}

		$view = View_Smarty::forge ( 'detail' );
		$view->set ( 'topics_info', $topics_info );
		$view->set_safe ( 'body', $body );
		$view->set_safe ( 'thumbnail_img', $thumbnail_img );

		return $view;
	}

	/**
	 * 本文の変換
	 *
	 * 本文中に埋め込んだ特殊タグを実際のタグに変換する
	 *
	 * @param array $topics_info
	 *        	トピックス情報
	 * @return 変換後の本文
	 */
	private function _convert_body($topics_info) {
		// トピックス内容を取得
		$body = $topics_info ['body'];
		$topics_id = $topics_info ['topics_id'];
		$topics_img_list = $topics_info ['topics_img'];
		$topics_pdf_file = $topics_info ['pdf_file'];
		$topics_pdf_url = '';

		foreach ( $topics_img_list as $topics_img ) {
			$img_no = $topics_img ['img_no'];
			$img_size_w = ($topics_img ['img_size_w'] <= 0) ? 300 : $topics_img ['img_size_w'];
			$file_name = mb_convert_encoding($topics_img ['img_file'], Digconst::ENCODE_TYPE_LOCAL, "AUTO");
			$img_tag = Asset::img ( sprintf('%s/%s/%s', $topics_id, $img_no, $file_name), array (
					'width'  => $img_size_w,
					'hspace' => Digconst::IMG_HSPACE_TOPICS,
					'vspace' => Digconst::IMG_VSPACE_TOPICS,
					'class'  => Digconst::IMG_CLASS,
					//'align'  => Digconst::IMG_ALIGN_LEFT,
			) );
			mb_language("Japanese");
			$img_tag = mb_convert_encoding($img_tag, Digconst::ENCODE_TYPE_WEB, "AUTO");
			$convert_tag = sprintf(Digconst::IMG_CONVERT_TAG, $img_no);

			$body = str_replace ( $convert_tag, $img_tag, $body );
		}
		if(!empty($topics_pdf_file)) {
			$file_name = mb_convert_encoding($topics_pdf_file, Digconst::ENCODE_TYPE_LOCAL, "AUTO");
			$topics_pdf_path =sprintf('pdf/%s/', $topics_id);
			Asset::add_path($topics_pdf_path, 'pdf');
			$topics_pdf_url = Asset::get_file($file_name, 'pdf');
			mb_language("Japanese");
			$topics_pdf_url = mb_convert_encoding($topics_pdf_url, Digconst::ENCODE_TYPE_WEB, "AUTO");
		}

		$body = nl2br($body, true); // 改行を<br />に統一

		// 背景色に囲まれた文字サイズはタグを合わせる
		$pattern = '/\[背景色:([a-z0-9]{6})\](((?!背景色).)*?)\[サイズ:([1-7]{1})\]/s';
		$replace = "[背景色:\${1}]\${2}[サイズ:\${4}:\${1}]";
		while (preg_match($pattern, $body)) {
			$body = preg_replace ($pattern , $replace , $body );
		}
		// 太字
		$pattern = '/\[太字\](.*?)\[\/太字\]/s';
		$replace = "<span style=\"font-weight:bold\">\${1}</span>";
		while (preg_match($pattern, $body)) {
			$body = preg_replace ($pattern , $replace , $body );
		}
		// 文字サイズ
		$pattern = '/\[サイズ:([1-7]{1})\](.*?)\[\/サイズ\]/s';
		$replace = "<span style=\"font-size:\${1}rem;line-height:1.5\">\${2}</span>";
		while (preg_match($pattern, $body)) {
			$body = preg_replace ($pattern, $replace , $body);
		}
		// 背景色付き文字サイズ
		$pattern = '/\[サイズ:([1-7]{1}):([a-z0-9]{6})\](.*?)\[\/サイズ\]/s';
		$replace = "<span style=\"font-size:\${1}rem;line-height:1.5;background-color:#\${2}\">\${3}</span>";
		while (preg_match($pattern, $body)) {
			$body = preg_replace ($pattern, $replace , $body);
		}
		// 文字色
		$pattern = '/\[文字色:([a-z0-9]{6})\](.*?)\[\/文字色\]/s';
		$replace = "<span style=\"color:#\${1}\">\${2}</span>";
		while (preg_match($pattern, $body)) {
			$body = preg_replace ($pattern, $replace , $body);
		}
		// 背景色
		$pattern = '/\[背景色:([a-z0-9]{6})\](.*?)\[\/背景色\]/s';
		$replace = "<span style=\"background-color:#\${1}\">\${2}</span>";
		while (preg_match($pattern, $body)) {
			$body = preg_replace ($pattern, $replace , $body);
		}
		// 左揃え
		$pattern = '/\[左揃え\](.*?)\[\/左揃え\]/s';
		$replace = "<div style=\"text-align:left\">\${1}</div>";
		while (preg_match($pattern, $body)) {
			$body = preg_replace ($pattern, $replace , $body);
		}
		// 右揃え
		$pattern = '/\[右揃え\](.*?)\[\/右揃え\]/s';
		$replace = "<div style=\"text-align:right\">\${1}</div>";
		while (preg_match($pattern, $body)) {
			$body = preg_replace ($pattern, $replace , $body);
		}
		// 中央揃え
		$pattern = '/\[中央揃え\](.*?)\[\/中央揃え\]/s';
		$replace = "<div style=\"text-align:center\">\${1}</div>";
		while (preg_match($pattern, $body)) {
			$body = preg_replace ($pattern, $replace , $body);
		}
		$body = preg_replace ( '/\[絵文字:([a-z]{1})-([0-9]{3})\]/', "<img src=\"/assets/js/img/\${1}/\${2}.gif\">", $body );
		$body = preg_replace ( '/\[URL\((https?)(:\/\/[A-Za-z0-9\+\$\;\?\.%,!#~\*\/:@&=_\-]+)\)\](.*?)\[\/URL\]/s', "<a href=\"\${1}\${2}\" target=\"_blank\">\${3}</a>" , $body);
		$body = preg_replace ( '/\[URL\((.*?)\)\](.*?)\[\/URL\]/s', "\${2}" , $body);
		if(!empty($topics_pdf_url)){
			$body = preg_replace ( '/\[PDF\](.*?)\[\/PDF\]/s', "<a href=\"${topics_pdf_url}\" target=\"_blank\">\${1}</a>" , $body);
		} else {
			$body = preg_replace ( '/\[PDF\](.*?)\[\/PDF\]/s', "\${1}" , $body);
		}

// 		$body = str_replace ( '<div', '<br /><div', $body ); // Chromeで改行(Enter)すると<div>タグになるので<br />を挿入
// 		$body = str_replace ( '</p>', '</p><br />', $body ); // IEで改行(Enter)すると<div>タグになるので<br />を挿入
		$body = str_replace ( '<br />', '<br clear="left" />', $body ); // 回り込みを解除する
		$body = str_replace ( '<br>', '<br clear="left" />', $body ); // 回り込みを解除する
		$body .= '<br clear="left" />'; // 最後に回りこみ解除の改行を加える。

		$body = htmlspecialchars_decode ( $body );

		return $body;
	}
}