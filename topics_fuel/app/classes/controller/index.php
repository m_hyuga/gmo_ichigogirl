<?php
/**
 * 【フロントサイト】トップページ
 *
 *  index.htmlへリダイレクトする
 *
 * @author s.sekiguchi
 *
 */
class Controller_Index extends Controller {
	public function action_index() {
		Response::redirect ( '/index.html' );
	}
}