<?php
/**
 * 【フロントサイト】トピックス一覧
 *
 * @author s.sekiguchi
 *
 */
class Controller_List extends Controller {
	/**
	 * 一覧ページの一覧
	 *
	 * @param $prev プレビューフラグ
	 * @return view
	 */
	public function action_index($prev) {

		// 一覧件数
		$total_count = $this->_count_topics_list ($prev);

		// ページング設定
		$config = array (
				'pagination_url' => Uri::create('list'),
				'uri_segment'    => 2,
				'num_links'      => Digconst::HP_PAGINATION_NUM_LINK,
				'per_page'       => Digconst::HP_PAGINATION_PER_PAGE,
				'total_items'    => $total_count,
				'show_first'     => true,
				'show_last'      => true,
				'name'           => 'front',
		);
		$pagination = Pagination::forge ( 'pagination', $config );

		// トピックス一覧取得
		$topics_list = $this->_get_topics_list_page ( $prev, $pagination );
		// カテゴリ一覧取得
		$category_list = $this->_get_category_list ( $topics_list );
		// サムネイル画像一覧取得
		$thumbnail_img_list = $this->_get_thumbnail_img_list ( $topics_list );

		foreach ( $topics_list as $topics ) {
			// トピックス分け
			$category_flg = false;
			foreach ( $category_list as $sort_id => $category_info ) {
				if ($category_info ['category_id'] === $topics ['category_id']) {
					$topics ['sort_id'] = $sort_id;
					$category_list [$sort_id] ['count'] ++;
					$category_flg = true;
					break;
				}
			}

			if (! $category_flg) {
				$topics ['sort_id'] = 0;
				$category_list [Digconst::CATEGORY_SORT_ID_DEF] ['count'] ++;
			}

			$topics['body'] = $this->_delete_tag_body($topics);
		}

		$view = View_Smarty::forge ( 'list' );
		$view->set_safe ( 'category_list', $category_list );
		$view->set_safe ( 'topics_list', $topics_list );
		$view->set_safe ( 'thumbnail_img_list', $thumbnail_img_list );

		$view->set ( 'pagination', $pagination, false );

		return $view;
	}

	/**
	 * 一覧ページの一覧(カテゴリ指定)
	 *
	 * @param $prev プレビューフラグ
	 * @param $category_sort カテゴリ(ソート番号)
	 * @return view
	 */
	public function action_category($prev, $category_sort) {

		// 一覧件数
		$total_count = $this->_count_topics_list ( $prev, $category_sort );

		// ページング設定
		$config = array (
				'pagination_url' => Uri::create('list_c/'.$category_sort),
				'uri_segment'    => 3,
				'num_links'      => Digconst::HP_PAGINATION_NUM_LINK,
				'per_page'       => Digconst::HP_PAGINATION_PER_PAGE,
				'total_items'    => $total_count,
				'show_first'     => true,
				'show_last'      => true,
				'name'           => 'front',
		);
		$pagination = Pagination::forge ( 'pagination', $config );
		// トピックス一覧取得
		$topics_list = $this->_get_topics_list_page ( $prev, $pagination, $category_sort );
		// カテゴリ一覧取得
		$category_list = $this->_get_category_list ( $topics_list, $category_sort );
		// サムネイル画像一覧取得
		$thumbnail_img_list = $this->_get_thumbnail_img_list ( $topics_list );

		foreach ( $topics_list as $topics ) {
			// トピックス分け
			$category_flg = false;
			foreach ( $category_list as $sort_id => $category_info ) {
				if ($category_info ['category_id'] === $topics ['category_id']) {
					$topics ['sort_id'] = $sort_id;
					$category_list [$sort_id] ['count'] ++;
					$category_flg = true;
					break;
				}
			}

			if (! $category_flg) {
				$topics ['sort_id'] = 0;
				$category_list [Digconst::CATEGORY_SORT_ID_DEF] ['count'] ++;
			}

			$topics['body'] = $this->_delete_tag_body($topics);
		}

		$view = View_Smarty::forge ( 'list' );
		$view->set_safe ( 'category_list', $category_list );
		$view->set_safe ( 'topics_list', $topics_list );
		$view->set_safe ( 'thumbnail_img_list', $thumbnail_img_list );

		$view->set ( 'pagination', $pagination, false );

		return $view;
	}


	/**
	 * トップページの一覧
	 *
	 * @param $prev プレビューフラグ
	 * @param $limit 件数
	 * @param $category_sort カテゴリ(ソート番号)
	 * @return view
	 */
	public function action_top($prev, $limit = 0, $category_sort = null) {
		// トピックス一覧取得
		$topics_list = $this->_get_topics_list ( $prev, $limit, $category_sort );
		// カテゴリ一覧取得
		$category_list = $this->_get_category_list ( $topics_list, $category_sort );
		// サムネイル画像一覧取得
		$thumbnail_img_list = $this->_get_thumbnail_img_list ( $topics_list );

		foreach ( $topics_list as $topics ) {
			// トピックス分け
			$category_flg = false;
			foreach ( $category_list as $sort_id => $category_info ) {
				if ($category_info ['category_id'] === $topics ['category_id']) {
					$topics ['sort_id'] = $sort_id;
					$category_list [$sort_id] ['count'] ++;
					$category_flg = true;
					break;
				}
			}

			if (! $category_flg) {
				$topics ['sort_id'] = 0;
				$category_list [Digconst::CATEGORY_SORT_ID_DEF] ['count'] ++;
			}

			$topics['body'] = $this->_delete_tag_body($topics);
		}

		$view = View_Smarty::forge ( 'top' );
		$view->set_safe ( 'category_list', $category_list );
		$view->set_safe ( 'topics_list', $topics_list );
		$view->set_safe ( 'thumbnail_img_list', $thumbnail_img_list );

		return $view;
	}

	/**
	 * アーカイブ指定の一覧
	 *
	 * @param $prev プレビューフラグ
	 * @param $date 日付
	 * @param $limit 件数
	 * @return view
	 */
	public function action_archive($prev, $date=null) {
		$topics_date_list = array();
		if(!empty($date) && is_numeric($date)) {
			if(strlen($date) == 4) {
				$topics_date_list['y'] = $date;
			} elseif(strlen($date) == 5) {
				$topics_date_list['y'] = substr($date, 0, 4);
				$topics_date_list['m'] = sprintf("%02d",substr($date, 4));
			} elseif(strlen($date) == 6) {
				$topics_date_list['y'] = substr($date, 0, 4);
				$topics_date_list['m'] = substr($date, 4);
			}
		}
		// 一覧件数
		$total_count = $this->_count_topics_list ( $prev, null, $topics_date_list );

		// ページング設定
		$config = array (
				'pagination_url' => Uri::create('archive'),
				'uri_segment'    => 3,
				'num_links'      => Digconst::HP_PAGINATION_NUM_LINK,
				'per_page'       => Digconst::HP_PAGINATION_PER_PAGE,
				'total_items'    => $total_count,
		);
		$pagination = Pagination::forge ( 'pagination', $config );

		// トピックス一覧取得
		$topics_list = $this->_get_topics_list_page ( $prev, $pagination, null, $topics_date_list );
		// カテゴリ一覧取得
		$category_list = $this->_get_category_list ( $topics_list, null );
		// サムネイル画像一覧取得
		$thumbnail_img_list = $this->_get_thumbnail_img_list ( $topics_list );

		foreach ( $topics_list as $topics ) {
			// トピックス分け
			$category_flg = false;
			foreach ( $category_list as $sort_id => $category_info ) {
				if ($category_info ['category_id'] === $topics ['category_id']) {
					$topics ['sort_id'] = $sort_id;
					$category_list [$sort_id] ['count'] ++;
					$category_flg = true;
					break;
				}
			}

			if (! $category_flg) {
				$topics ['sort_id'] = 0;
				$category_list [Digconst::CATEGORY_SORT_ID_DEF] ['count'] ++;
			}

			$topics['body'] = $this->_delete_tag_body($topics);
		}

		$view = View_Smarty::forge ( 'list' );
		$view->set_safe ( 'category_list', $category_list );
		$view->set_safe ( 'topics_list', $topics_list );
		$view->set_safe ( 'thumbnail_img_list', $thumbnail_img_list );

		return $view;
	}

	/**
	 * トピックス一覧情報取得
	 *
	 * @param $prev プレビューフラグ
	 * @param $limit 表示件数
	 * @param $category_sort カテゴリ(ソート番号)
	 * @param $topics_date_list 日付(年月リスト)
	 * @return トピックス一覧
	 */
	private function _get_topics_list($prev, $limit = 0, $category_sort = null, $topics_date_list = array()) {
		// トピックス一覧クエリ作成
		$topics_list_query = $this->_make_topics_list_query($prev, null, $category_sort, $topics_date_list);
		// トピックス一覧取得
		$topics_list = $topics_list_query->get ();
		// 記事数指定
		if (! empty ( $limit ) && is_numeric ( $limit ) && $limit > 0) {
			$topics_list = array_slice ( $topics_list, 0, $limit );
		}
		return $topics_list;
	}

	/**
	 * トピックス一覧情報取得(ページング用)
	 *
	 * @param $prev プレビューフラグ
	 * @param $pagination ページネーション
	 * @param $category_sort カテゴリ(ソート番号)
	 * @param $topics_date_list 日付(年月リスト)
	 * @return トピックス一覧
	 */
	private function _get_topics_list_page($prev, $pagination, $category_sort = null, $topics_date_list = array()) {
		// トピックス一覧クエリ作成
		$topics_list_query = $this->_make_topics_list_query($prev, $pagination, $category_sort, $topics_date_list);
		// トピックス一覧取得
		$topics_list = $topics_list_query->get ();
		return $topics_list;
	}

	/**
	 * トピックス一覧件数取得
	 *
	 * @param $prev プレビューフラグ
	 * @param $category_sort カテゴリ(ソート番号)
	 * @param $topics_date_list 日付(年月リスト)
	 * @return トピックス一覧件数
	 */
	private function _count_topics_list($prev, $category_sort = null, $topics_date_list = array()) {
		// トピックス一覧クエリ作成
		$topics_list_query = $this->_make_topics_list_query($prev, null, $category_sort, $topics_date_list);
		// トピックス一覧件数取得
		$topics_list_count = $topics_list_query->count ();
		return $topics_list_count;
	}

	/**
	 * トピックス一覧情報取得クエリ作成
	 *
	 * @param $prev プレビューフラグ
	 * @param $pagination
	 * @param $category_sort カテゴリ(ソート番号)
	 * @param $topics_date_list 日付(年月リスト)
	 * @return トピックス一覧情報取得のクエリ
	 */
	private function _make_topics_list_query($prev, $pagination, $category_sort = null, $topics_date_list = array()) {

		// 現在日時取得
		$now_datetime = date ( "Y-m-d H:i:s", time () );
		// トピックス一覧取得条件作成
		// トピックス情報は、公開のトピックスのみ
		// トピックス画像情報はサムネイル画像のみ
		$topics_list_query = Model_Admin_Topics::query ()->related ( array (
				'topics_img' => array (
						'join_on' => array (
								array (
										'del_flg',
										'=',
										DB::expr ( 0 )
								),
								array (
										'img_type',
										'=',
										DB::expr ( 0 )
								)
						)
				)
		) )->where ( 'del_flg', 0 );

		if (! empty ( $category_sort ) && is_numeric ( $category_sort ) && $category_sort > 0) {
			$topics_list_query->related ( array (
					'category' => array (
							'join_type' => 'inner',
							'join_on' => array (
									array (
											'del_flg',
											'=',
											DB::expr ( 0 )
									),
									array (
											'sort_id',
											'=',
											$category_sort
									)
							)
					)
			) );
		} else if (is_numeric ( $category_sort ) && $category_sort == 0) {
			$topics_list_query->where ( 'category_id', 0 );
		}

		if(!empty($topics_date_list) && isset($topics_date_list['y'])) {
			if(isset($topics_date_list['m'])) {
				$topics_date = $topics_date_list['y'] . $topics_date_list['m'];
				$topics_list_query->where (  DB::expr ( 'DATE_FORMAT(topics_date, \'%Y%m\')' ), $topics_date );
			} else {
				$topics_date = $topics_date_list['y'];
				$topics_list_query->where (  DB::expr ( 'DATE_FORMAT(topics_date, \'%Y\')' ), $topics_date );
			}
		}

		$topics_list_query->where_open ()->where_open ()->where ( 'prev_flg', 0 )->where ( 'publish_flg', 0 )->where ( 'topics_date', '<=', $now_datetime )->where_close ();
		if (! empty ( $prev ) && $prev > 0) {
			$topics_list_query->or_where ( 'prev_flg', 1 );
		}
		$topics_list_query->where_close ();

		// ソート
		$topics_list_query->order_by ( 'topics_date', 'DESC' )->order_by ( 'topics_id', 'DESC' );

		if(!empty($pagination)) {
			// limitとoffsetを追加
			$topics_list_query->rows_limit ( $pagination->per_page )->rows_offset ( $pagination->offset );
		}

		return $topics_list_query;
	}

	/**
	 * カテゴリ一覧取得
	 *
	 * @param $topics_list トピックス一覧
	 * @param $category_sort カテゴリ(ソート番号)
	 * @return カテゴリ一覧
	 */
	private function _get_category_list($topics_list, $category_sort = null) {
		if (empty ( $topics_list )) {
			return array ();
		}

		// カテゴリ
		$category_list [Digconst::CATEGORY_SORT_ID_DEF] = array (
				'category_id' => Digconst::CATEGORY_ID_DEF,
				'category_name' => Digconst::CATEGORY_NAME_DEF,
				'count' => 0
		);

		if (! empty ( $category_sort )) {
			$db_category_list = Model_Admin_Category::query ()->where ( 'del_flg', 0 )->get ();
			foreach ( $db_category_list as $db_category_info ) {
				$category_id = $db_category_info ['category_id'];
				$category_name = $db_category_info ['category_name'];
				$sort_id = $db_category_info ['sort_id'];
				$category_list [$sort_id] = array (
						'category_id' => $category_id,
						'category_name' => $category_name,
						'count' => 0
				);
			}
		}

		ksort ( $category_list );

		return $category_list;
	}

	/**
	 * サムネイル画像一覧取得
	 *
	 * @param $topics_list トピックス一覧
	 * @return サムネイル画像一覧
	 */
	private function _get_thumbnail_img_list($topics_list) {
		$thumbnail_img_list = array ();
		foreach ( $topics_list as $topics ) {
			// サムネイル画像リスト作成
			if ($topics ['thumbnail_flg'] === '1') {
				foreach ( $topics ['topics_img'] as $topics_img ) {
					if ($topics_img ['img_type'] === '0') {
						$file_name = mb_convert_encoding ( $topics_img ['img_file'], Digconst::ENCODE_TYPE_LOCAL, "AUTO" );

						$img_tag = Asset::img ( $topics ['topics_id'] . '/0/' . $file_name, array (
								'width' => Digconst::IMG_SIZE_THUMBNAIL_W
						) );
						mb_language ( "Japanese" );
						$img_tag = mb_convert_encoding ( $img_tag, Digconst::ENCODE_TYPE_WEB, "AUTO" );
						$thumbnail_img_list [$topics ['topics_id']] = $img_tag;
					}
				}
			}
		}

		return $thumbnail_img_list;
	}

	/**
	 * 本文のタグ削除
	 *
	 * 本文中に埋め込んだタグを削除する
	 *
	 * @param array $topics_info
	 *        	トピックス情報
	 * @return 変換後の本文
	 */
	private function _delete_tag_body($topics_info) {
		// トピックス内容を取得
		$body = $topics_info ['body'];

		$body = nl2br ( $body, true ); // 改行を<br />に統一

		$pattern = '/<iframe[^>]*>/s';
		while ( preg_match ( $pattern, $body ) ) {
			$body = preg_replace ( $pattern, '', $body );
		}

		$pattern = '/\{img[0-9]+\}/s';
		while ( preg_match ( $pattern, $body ) ) {
			$body = preg_replace ( $pattern, '', $body );
		}

		// 背景色に囲まれた文字サイズはタグを合わせる
		$pattern = '/\[背景色:([a-z0-9]{6})\](((?!背景色).)*?)\[サイズ:([1-7]{1})\]/s';
		$replace = "[背景色:\${1}]\${2}[サイズ:\${4}:\${1}]";
		while ( preg_match ( $pattern, $body ) ) {
			$body = preg_replace ( $pattern, $replace, $body );
		}
		// 太字
		$pattern = '/\[太字\](.*?)\[\/太字\]/s';
		$replace = "\${1}";
		while ( preg_match ( $pattern, $body ) ) {
			$body = preg_replace ( $pattern, $replace, $body );
		}
		// 文字サイズ
		$pattern = '/\[サイズ:([1-7]{1})\](.*?)\[\/サイズ\]/s';
		$replace = "\${2}";
		while ( preg_match ( $pattern, $body ) ) {
			$body = preg_replace ( $pattern, $replace, $body );
		}
		// 背景色付き文字サイズ
		$pattern = '/\[サイズ:([1-7]{1}):([a-z0-9]{6})\](.*?)\[\/サイズ\]/s';
		$replace = "\${3}";
		while ( preg_match ( $pattern, $body ) ) {
			$body = preg_replace ( $pattern, $replace, $body );
		}
		// 文字色
		$pattern = '/\[文字色:([a-z0-9]{6})\](.*?)\[\/文字色\]/s';
		$replace = "\${2}";
		while ( preg_match ( $pattern, $body ) ) {
			$body = preg_replace ( $pattern, $replace, $body );
		}
		// 背景色
		$pattern = '/\[背景色:([a-z0-9]{6})\](.*?)\[\/背景色\]/s';
		$replace = "\${2}";
		while ( preg_match ( $pattern, $body ) ) {
			$body = preg_replace ( $pattern, $replace, $body );
		}
		// 左揃え
		$pattern = '/\[左揃え\](.*?)\[\/左揃え\]/s';
		$replace = "\${1}";
		while ( preg_match ( $pattern, $body ) ) {
			$body = preg_replace ( $pattern, $replace, $body );
		}
		// 右揃え
		$pattern = '/\[右揃え\](.*?)\[\/右揃え\]/s';
		$replace = "\${1}";
		while ( preg_match ( $pattern, $body ) ) {
			$body = preg_replace ( $pattern, $replace, $body );
		}
		// 中央揃え
		$pattern = '/\[中央揃え\](.*?)\[\/中央揃え\]/s';
		$replace = "\${1}";
		while ( preg_match ( $pattern, $body ) ) {
			$body = preg_replace ( $pattern, $replace, $body );
		}
		$body = preg_replace ( '/\[絵文字:([a-z]{1})-([0-9]{3})\]/', "", $body );
		$body = preg_replace ( '/\[URL\((https?)(:\/\/[A-Za-z0-9\+\$\;\?\.%,!#~\*\/:@&=_\-]+)\)\](.*?)\[\/URL\]/s', "\${3}", $body );
		$body = preg_replace ( '/\[URL\((.*?)\)\](.*?)\[\/URL\]/s', "\${2}", $body );
		$body = preg_replace ( '/\[PDF\](.*?)\[\/PDF\]/s', "\${1}", $body );

		$body = htmlspecialchars_decode ( $body );

		return $body;
	}
}