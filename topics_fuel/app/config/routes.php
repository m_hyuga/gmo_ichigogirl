<?php
return array(
	'_root_'  => 'index',  // The default route
	'_404_'   => 'index',    // The main 404 route

	'admin'               => 'admin/index',
	'admin/list/(:num)'   => 'admin/list/index/$1',
	'admin/regist/(:num)' => 'admin/regist/index/$1',

	'top'                    => 'list/top/0',
	'top/(:num)'             => 'list/top/0/$1',
	'top/(:num)/(:num)'      => 'list/top/0/$1/$2',

	'top/prev'               => 'list/top/1',
	'top/prev/(:num)'        => 'list/top/1/$1',
	'top/prev/(:num)/(:num)' => 'list/top/1/$1/$2',

	'list'        => 'list/index/0',
	'list/(:num)' => 'list/index/0/$1',

	'list_c'               => 'list/index/0',
	'list_c/(:num)'        => 'list/category/0/$1',
	'list_c/(:num)/(:num)' => 'list/category/0/$1/$2',

	'list/prev'        => 'list/index/1',
	'list/prev/(:num)' => 'list/index/1/$1',

	'list_c/prev'               => 'list/index/1',
	'list_c/prev/(:num)'        => 'list/category/1/$1',
	'list_c/prev/(:num)/(:num)' => 'list/category/1/$1/$2',

	'archive'                    => 'list/archive/0',
	'archive/(:num)'             => 'list/archive/0/$1',
	'archive/(:num)/(:num)'      => 'list/archive/0/$1/$2',
	'archive/prev'               => 'list/archive/1',
	'archive/prev/(:num)'        => 'list/archive/1/$1',
	'archive/prev/(:num)/(:num)' => 'list/archive/1/$1/$2',

	'detail/(:num)'        => 'detail/index/$1',
	'detail/(:num)/(:num)' => 'detail/index/$1/$2',
	'detail'               => 'detail/index/0',

);