<?php

return array(
		'required' => ':label が未入力です。',
		'max_length' => ':label は :param:1 文字以内で入力してください。',
		'valid_string' => ':label に不正な文字が含まれています。',
		'numeric_between' => ':label は :param:1 から :param:2 の範囲で指定してください。',
		'upload_required' => ':label を指定してください。',
		'duplication_error' => ':label と同じ値が他でも指定されています。',
);