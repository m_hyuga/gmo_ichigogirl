<!doctype html>
<html lang="ja-JP">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
	<meta charset="UTF-8">
	<meta content="" name="copyright">
	<meta name="description" content="福井市でベビー服や子供服ならいちごガールにお任せ！可愛い女の子の子供服を取り揃えています。アースマジックなど人気ブランドも取り扱っておりますので、ぜひお気軽にご来店下さい。福井市まで遠いという方の為の通販もございます。">
	<meta name="keywords" content="福井市,ベビー服,子供服,女の子,アースマジック,いちごガール">
	<meta name="viewport" content="user-scalable=yes" />
	<meta name="format-detection" content="telephone=no" />
	<title>Instagram | 福井市で女の子の子供服やベビー服ならいちごガール</title>
	<link rel="stylesheet" href="../common/css/meyer.css" />
	<link rel="stylesheet" href="../common/css/common.css" />
	<link rel="stylesheet" href="../common/css/instagram.css" />
	<script src="../common/js/jquery-1.11.1.min.js"></script>
	<script type="text/javascript" src="../common/js/common.js"></script>
	<script type="text/javascript" src="../common/js/jquery.smoothScroll.js"></script>
	<script type="text/javascript" src="../common/js/colorbox/jquery.colorbox.js"></script>
	<link rel="stylesheet" href="../common/js/colorbox/colorbox.css" />
	<!--[if lt IE 9]>
		<script type="text/javascript" src="../common/js/html5shiv.js"></script>
		<script type="text/javascript" src="../common/js/selectivizr-min.js"></script>
	<![endif]-->
	<script src="../common/js/jquery.xdomainajax.js" type="text/javascript"></script>
	<script type="text/javascript">
		$(function(){			
			var agent = navigator.userAgent;
			var redirectPass = 'sp';
			if(agent.search(/iPhone/) != -1 || agent.search(/iPad/) != -1 || agent.search(/iPod/) != -1 || agent.search(/Android/) != -1){
				location.href = redirectPass;
			}
		});
		$(window).load(function (){
			$(".cbox").colorbox();
			$('#content_instagram li:first-child,#content_instagram li:nth-child(5n+1)').css("margin-left","0");
		});
	</script>

<!-- GAコード -->
</head>
<body>
<div id="wrap" class="cf">
		<h2 class="img_instagram"><a href="../"><img src="../common/images/instagram/logo.png" alt="ichigogirl" width="164" height="164" class="over" ></a></h2>
		
		<div id="content_instagram">
			<dl>
				<dt>
					<h3>ichigogirl_official_ig</h3>
					<p><span>いちごガール</span>いちごガール専用アカウント♡RONI/EARTHMAGIC/panpantutuなどを取り扱う子供服専門店/福井♡ HP♡http://ichigogirl.shop-pro.jpBlog♡http://ameblo.jp/ichigo15girl/ ご予約も承ります♡ 0776-57-1539</p>
					<a href="http://ichigogirl.shop-pro.jp" target="_blank">http://ichigogirl.shop-pro.jp</a>
				</dt>
				<dd><a href="https://instagram.com/ichigogirl_official_ig/" target="_blank"><img src="../common/images/instagram/btn_toinsta_off.png" alt="いちごガールのInstagramページはこちら" width="248" height="55" ></a></dd>
			</dl>
			<ul class="cf">
			<?php require_once('./photo.php'); ?>
			</ul>
			<div class="clear"></div>
		</div>
		<p class="btn_area"><a href="https://instagram.com/ichigogirl_official_ig/" target="_blank"><img src="../common/images/instagram/btn_more_off.png" alt="もっとみる" width="464" height="55" ></a></p>

</div><!-- /#wrap -->
<p class="btn_totop"><a href="#wrap"><img src="../common/images/common/btn_totop.png" alt="page top" width="100" height="38" class="over"></a></p>
<footer>
<div id="footer" class="cf">
	<h2><a href="../common/images/common/logo_footer.png" rel="lightbox"><img src="../common/images/common/logo_footer.png" alt="" width="123" height="108" class="over"></a></h2>
	<div class="fll cf">
		<h3>子供服専門店 いちごガール</h3>
		<p>〒910-0837<br>福井市高柳1丁目203　カーサリブレ101</p>
		<img src="../common/images/common/img_tell.png" alt="0776-57-1539" width="171" height="32" class="fll">
		<a href="https://secure.shop-pro.jp/?mode=inq&shop_id=PA01108887" target="_blank"><img src="../common/images/common/btn_contact_off.jpg" alt="Contact" width="107" height="32" class="flr"></a>
	</div>
	<div class="flr">
		<nav>
			<ul>
				<li><a href="./brand/">Brand</a></li><!--
			--><li><a href="./ranking/">Ranking</a></li><!--
			--><li><a href="./newitem/">Newitem</a></li><!--
			--><li><a href="./instagram/">Instagram</a></li><!--
			--><li><a href="./access/">Access</a></li>
			</ul>
		</nav>
		<ul>
			<li><a href="http://ameblo.jp/ichigo15girl" target="_blank"><img src="../common/images/common/btn_blog_off.png" alt="official BLOG" width="115" height="40"></a></li><!--
			--><li><a href="http://ichigogirl.shop-pro.jp/" target="_blank"><img src="../common/images/common/btn_shopping_off.png" alt="Shoppingsite" width="115" height="40"></a></li><!--
			--><li><a href="https://instagram.com/ichigogirl_official_ig/" target="_blank"><img src="../common/images/common/btn_instagram_off.png" alt="Instagram" width="115" height="40"></a></li>
		</ul>
		<p>&copy;2015 Ichigo Girl All rights reserve</p>
	</div>
</div><!-- /#footer -->
</footer>
</body>
</html>
