<!doctype html>
<html lang="ja-JP">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
	<meta charset="UTF-8">
	<meta content="" name="copyright">
	<meta name="description" content="福井市でベビー服や子供服ならいちごガールにお任せ！可愛い女の子の子供服を取り揃えています。アースマジックなど人気ブランドも取り扱っておりますので、ぜひお気軽にご来店下さい。福井市まで遠いという方の為の通販もございます。">
	<meta name="keywords" content="福井市,ベビー服,子供服,女の子,アースマジック,いちごガール">
	<meta name="viewport" content="user-scalable=yes" />
	<meta name="format-detection" content="telephone=no" />
	<title>Instagram | 福井市で女の子の子供服やベビー服ならいちごガール</title>
	<link rel="stylesheet" href="../common/css/meyer.css" />
	<link rel="stylesheet" href="../common/css/common.css" />
	<link rel="stylesheet" href="../common/css/instagram.css" />
	<script src="../common/js/jquery-1.11.1.min.js"></script>
	<script type="text/javascript" src="../common/js/common.js"></script>
	<script type="text/javascript" src="../common/js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="../../common/js/colorbox/jquery.colorbox.js"></script>
	<link rel="stylesheet" href="../../common/js/colorbox/colorbox.css" />
	<script>
	$(window).load(function (){
		$(".cbox").colorbox();
		$('#content section li:first-child,#content section li:nth-child(4n+1)').css("margin-left","0");
	});
	</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-59273760-9', 'auto');
  ga('require', 'displayfeatures');
  ga('send', 'pageview');

</script>
</head>
<body>
<div id="wrap">

	<header>
		<h1><a href="../">女の子のかわいい子供服やベビー服なら福井市のいちごガール</a></h1>
		<a id="menu" href="javascript:boid(0);"><img src="../common/images/common/btn_menu.jpg" width="100%"></a>
		<nav style="display: none;">
			<ul class="nav_area">
				<li id="close"><a href="javascript:boid(0);"><img src="../common/images/common/btn_close.png" width="100%"></a></li>
				<li><a href="../brand/"><img src="../common/images/common/nav_brand.png" width="100%"></a></li>
				<li><a href="../../cartpro/cart.cgi"><img src="../common/images/common/nav_ranking.png" width="100%"></a></li>
				<li><a href="../newitem/"><img src="../common/images/common/nav_newitem.png" width="100%"></a></li>
				<li><a href="../instagram/"><img src="../common/images/common/nav_instagram.png" width="100%"></a></li>
				<li><a href="../access/"><img src="../common/images/common/nav_access.png" width="100%"></a></li>
				<li><a href="../list.html"><img src="../common/images/common/nav_news.png" width="100%"></a></li>
			</ul>
			<ul class="btn_area">
				<li><a href="http://ameblo.jp/ichigo15girl" target="_blank"><img src="../common/images/common/btn_blog.png" width="100%"></a></li>
				<li><a href="http://ichigogirl.shop-pro.jp/" target="_blank"><img src="../common/images/common/btn_shopping.png" width="100%"></a></li>
				<li><a href="https://instagram.com/ichigogirl_official_ig/" target="_blank"><img src="../common/images/common/btn_instagram.png" width="100%"></a></li>
			</ul>
		</nav>
	</header>
</div><!-- /#wrap -->
	<div id="content">
		<h2 class="main_title"><img src="../common/images/instagram/main_img.jpg" alt="instagram" width="100%"><br><img src="../common/images/instagram/ttl_insta.jpg" alt="instagram" width="100%"></h2>
		<section>
			<dl>
				<dt><img src="../common/images/instagram/ico_instagram.jpg" alt="instagram" width="100%"></dt>
				<dd><span>いちごガール</span>いちごガール専用アカウント♡RONI/EARTHMAGIC/panpantutuなどを取り扱う子供服専門店/福井♡ HP♡http://ichigogirl.shop-pro.jp　Blog♡http://ameblo.jp/ichigo15girl/ ご予約も承ります♡ 0776-57-1539<br><a href="http://ichigogirl.shop-pro.jp" target="_blank">http://ichigogirl.shop-pro.jp</a></dd>
				<p><a href="https://instagram.com/ichigogirl_official_ig/" target="_blank"><img src="../common/images/instagram/img_instagram.jpg" alt="いちごガールのInstagramページはこちら" width="100%"></a></p>
			</dl>
			<ul class="cf">
			<?php require_once('../../instagram/photo.php'); ?>
			</ul>
			<p class="btn_area"><a href="https://instagram.com/ichigogirl_official_ig/" target="_blank"><img src="../common/images/instagram/btn_more.png" alt="もっとみる" width="100%"></a></p>
		</section>
		
		<div id="appli_area">
			<h2><img src="../common/images/index/bg_download.png" alt="Ichigo Girl appli" width="100%"></h2>
			<ul>
				<li>
				<a href="https://itunes.apple.com/jp/app/burando-zi-gong-fu-zhuan-men/id927297040?mt=8" target="_blank"><img src="../common/images/index/btn_appstore.png" width="100%" alt=""></a>
				<p>iPhone用ダウンロ―ド</p>
				</li><!--
			--><li>
				<a href="https://play.google.com/store/apps/details?id=jp.digitallab.ichigogirl&hl=ja" target="_blank"><img src="../common/images/index/btn_android.png" width="100%" alt=""></a>
				<p>Android用ダウンロ―ド</p>
				</li>
			</ul>
		</div><!-- /#appli_area -->
		
	</div><!-- /#content -->
	
	<footer>
		<ul class="cf">
			<li><a href="../"><img src="../common/images/common/fnav_01.jpg" width="100%" alt="HOME"></a></li>
			<li><a href="../brand/"><img src="../common/images/common/fnav_02.jpg" width="100%" alt="Brand"></a></li>
			<li><a href="../../cartpro/cart.cgi"><img src="../common/images/common/fnav_03.jpg" width="100%" alt="Ranking"></a></li>
			<li><a href="../newitem/"><img src="../common/images/common/fnav_04.jpg" width="100%" alt="Newitem"></a></li>
			<li><a href="../instagram/"><img src="../common/images/common/fnav_05.jpg" width="100%" alt="Instagram"></a></li>
			<li><a href="../access/"><img src="../common/images/common/fnav_06.jpg" width="100%" alt="access"></a></li>
		</ul>
		<div class="f_inq">
			<div class="f_logo"><a href="../"><img src="../common/images/common/logo.png" width="100%" alt="いちごガール"></a></div>
			<h2>子供服専門店 いちごガール</h2>
			<p class="address">&#12306;910-0837　福井市高柳1丁目203　カーサリブレ101<br>TEL / 0776-57-1539</p>
			<p class="btn_area"><a href="https://secure.shop-pro.jp/?mode=inq&shop_id=PA01108887" target="_blank"><img src="../common/images/common/btn_contact.jpg" width="100%" alt="いちごガール"></a></p>
		</div>
		<p class="copy">&copy;2015 Ichigo Girl All rights reserve</p>
		</div>
		<p class="copy">&copy;2015 Ichigo Girl All rights reserve</p>
	</footer>
</body>
</html>
