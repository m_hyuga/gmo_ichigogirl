$(function(){
	//------------------------.over を透過
	$('a img:not(.nav_on)').hover(function(){
		$(this).attr('src', $(this).attr('src').replace('_off.', '_on.'));
			}, function(){
				if (!$(this).hasClass('currentPage')) {
				$(this).attr('src', $(this).attr('src').replace('_on.', '_off.'));
		}
	});	
		
		
	//------------------------上に戻るボタン関連
		$(".btn_totop").hide();
				 // ↑ページトップボタンを非表示にする
});
    $(window).on("scroll", function() {

        if ($(this).scrollTop() > 200) {
            // ↑ スクロール位置が100よりも小さい場合に以下の処理をする
            $('.btn_totop').slideDown("fast");
            // ↑ (100より小さい時は)ページトップボタンをスライドダウン
        } else {
            $('.btn_totop').slideUp("fast");
            // ↑ それ以外の場合の場合はスライドアップする。
        }

    // フッター固定する

        scrollHeight = $(document).height();
        // ドキュメントの高さ
        scrollPosition = $(window).height() + $(window).scrollTop();
        //　ウィンドウの高さ+スクロールした高さ→　現在のトップからの位置
        footHeight = $("footer").innerHeight();
        // フッターの高さ

        if ( scrollHeight - scrollPosition  <= footHeight ) {
        // 現在の下から位置が、フッターの高さの位置にはいったら
        //  ".gotop"のpositionをabsoluteに変更し、フッターの高さの位置にする
            $(".btn_totop").css({
                "position":"absolute",
                "bottom": footHeight
            });
        } else {
        // それ以外の場合は元のcssスタイルを指定
            $(".btn_totop").css({
                "position":"fixed",
                "bottom": "0px"
            });
        }
    });

    // トップへスムーススクロール
    $('.btn_totop a').click(function () {
			$('body,html').animate({
			scrollTop: 0
			}, 1000);
			// ページのトップへ 500 のスピードでスクロールする
			return false;
		});


 
 
 
 